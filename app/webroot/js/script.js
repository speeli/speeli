// Navigation scroll spy
$('body').scrollspy({
    target: '.navbar',
    offset: 71
});

// Navigation for single page mobile fix
$('.nav a').on('click', function(){
    $(".navbar-toggle").click() //bootstrap 3.x by Richard
});

// Menu toggle on scroll
$(document).on('scroll', function () {
    if ($(document).scrollTop() > 120) {
        $('.navbar').addClass('nav-open').removeClass('nav-close');
    } else {
        $('.navbar').removeClass('nav-open').addClass('nav-close');
        $('.navbar-collapse').removeClass('in');
    }
});

// Smooth scrolling for anchored links
$(function () {
    $('a[href*=#]:not([href=#])').click(function () {
        
        var offset_value = 70;
        if ($(document).width() < 768) {
          offset_value = 50;   
        }
        
        if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top - offset_value
                }, 400);
                return false;
            }
        }
    });
});

// tumblr feed api
window.onload=function(){

    for (var i=0;i<6;i++) {

        var thisPost = tumblr_api_read.posts[i];
        if(thisPost.type=="photo")
        {
            $('#tumblrfeed').append("<div class=\"col-sm-4 col-xs-6\"><a href=\"" + thisPost.url + "\" target=\"_blank\" class=\"tumblr-feed-link\"><img src=\"" + thisPost["photo-url-250"] + "\"/></a></div>");
        }
    }
}

// swipebox
;( function( $ ) {
    $( '.swipebox' ).swipebox();
} )( jQuery );