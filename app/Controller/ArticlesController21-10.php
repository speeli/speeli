<?php

class ArticlesController extends AppController {

    public $name = 'Articles';
    public $components = array('Paginator', 'RequestHandler', 'Email', 'Session');
    public $helpers = array('Js' => array('Prototype'), 'Paginator', 'Html', 'Form', 'Session');

    /**
     * This controller does not use a model
     *
     * @var array
     */
    public $uses = array();

    public function beforeFilter() {
        parent::beforeFilter();
        // Allow users to register and logout.
    }

    public function index() {
        $this->redirect('/');
    }

    public function emails() {
        $this->loadModel('Question');
        $later =date("Y-m-d H:i:s", strtotime("-5 week"));
        $this->paginate = array(
            'fields' => array('Question.id,Question.category_id,Question.links,Question.last,Question.edited,Question.quality,Question.expand', 'Question.views,Question.created,Question.image,Question.title_update,Question.title', 'Question.slug,Question.id,Question.status'),
            'limit' => 70,
            'conditions'=>array('Question.created >='=>$later),
            'order' => 'Question.views DESC');
        $questions = $this->Paginator->paginate('Question');
        $this->set('questions', $questions);
    }

    function escape_quotes($string) {
        $string = preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', $string);
        $string = preg_replace('/&nbsp;/', ' ', $string);
        $string = strip_tags($string);
        $string = htmlspecialchars(trim($string));
        $string = str_replace('"', "'", $string);

        return $string;
    }

    function validate_host($url) {
        $pattern = "/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?/";
        if (!preg_match($pattern, $url)) {
            return false;
        } else {
            if (preg_match("/http:/", $url)) {
                
            } else {
                $url = 'http://' . $url;
            }
            return $url;
        }
    }

    public function reverse() {
        $this->layout = $this->autoRender = false;
        $this->layout = false;
        $this->request->onlyAllow('ajax');

        if (isset($_POST['action'])) {

            $this->loadModel('Answer');
            $history = $this->Answer->find('first', array('fields' => array('Answer.id,Answer.title_update,Answer.body_update')
                , 'conditions' => array('Answer.id' => $_POST['speel_ans'])));
            if ($_POST['speel_type'] == "0") {

                $explode = explode(',,,', $history['Answer']['title_update']);
            } else {
                $explode = explode(',,,', $history['Answer']['body_update']);
            }
            echo $_POST['speel_ans'];
            echo $_POST['action'];

            echo count($explode) . "dfd";
            for ($i = 0; $i < count($explode); $i++) {

                if ($i == (int) $_POST['action']) {

                    if ($_POST['speel_type'] == "0") {
                        $array = unserialize(base64_decode($explode[$i]));

                        $array[2] = preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', $array[2]);
                        $array[2] = preg_replace('/&nbsp;/', ' ', $array[2]);
                        $array[2] = strip_tags($array[2]);
                        $array[2] = htmlspecialchars(trim($array[2]));
                        /* end */
                        $_POST['speel_ans'] = preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', $_POST['speel_ans']);
                        $_POST['speel_ans'] = preg_replace('/&nbsp;/', ' ', $_POST['speel_ans']);
                        $_POST['speel_ans'] = strip_tags($_POST['speel_ans']);
                        $_POST['speel_ans'] = htmlspecialchars(trim($_POST['speel_ans']));
                        /* end */
                        $update = $this->Answer->updateAll(
                                array('Answer.last_title' => '"' . $array[2] . '"'), array('Answer.id' => $_POST['speel_ans']));
                    } else {
                        $array[2] = preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', $array[2]);
                        $array[2] = preg_replace('/&nbsp;/', ' ', $array[2]);
                        $array[2] = strip_tags($array[2]);
                        $array[2] = htmlspecialchars(trim($array[2]));
                        /* end */
                        $_POST['speel_ans'] = preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', $_POST['speel_ans']);
                        $_POST['speel_ans'] = preg_replace('/&nbsp;/', ' ', $_POST['speel_ans']);
                        $_POST['speel_ans'] = strip_tags($_POST['speel_ans']);
                        $_POST['speel_ans'] = htmlspecialchars(trim($_POST['speel_ans']));
                        /* end */
                        $array = unserialize(base64_decode($explode[$i]));
                        echo $array[2];
                        $update = $this->Answer->updateAll(
                                array('Answer.last_body' => '"' . $array[2] . '"'), array('Answer.id' => $_POST['speel_ans']));
                    }
                    break;
                }
            }
        }
    }
	public function showmore_category() {
        $this->loadModel('Question');
        $this->layou=null;
           $category_id = array(7, 11, 9,3, 6 ,16);   
        for ($cid = 0; $cid < count($category_id); $cid++) {
            if ($cid == 0) {
                $seperate_coma = "MainCategory.id = " . $category_id[$cid];
            } else {
                $seperate_coma.=" OR MainCategory.id = " . $category_id[$cid];
            }
        }
        $this->loadModel('MainCategory'); //1  6 8 9 11
        $main_categories = $this->MainCategory->find('list', array('conditions' => array('OR' => array($seperate_coma)), 'fields' => 'MainCategory.name,MainCategory.slug'));

        $this->set('main_categories', $main_categories);
    }

    public function view($question_slug = false) {
        if ($question_slug != FALSE) {
            $this->loadModel('QuestionCategory');
            $this->loadModel('Answer');
            $this->loadModel('Question');
            $answers = $this->Answer->find('all', array(
                'fields' => array('Question.image_user,Answer.image_user,Question.last,Question.locked,Answer.last_title,Answer.last_body,Answer.sort,Question.selected,Question.views,Question.image,Question.status,Question.reference,Question.user_id,Question.title_update,Answer.title_update,Answer.body_update,Question.title_update', 'Answer.image', 'Answer.title', 'Question.id,Question.title,Question.slug,Answer.id,Answer.body,User.username,User.id'),
                'order' => array('Answer.sort ASC'),
                'conditions' => array('Question.slug' => $question_slug),
            ));
			$this->loadModel('MainCategory');
               $category_id = array(1, 6, 8, 9, 11, 16, 15);
                for ($cid = 0; $cid < count($category_id); $cid++) {
                    if ($cid == 0) {
                        $seperate_coma = "MainCategory.id = " . $category_id[$cid];
                    } else {
                        $seperate_coma.=" OR MainCategory.id = " . $category_id[$cid];
                    }
                }
                $main_categories = $this->MainCategory->find('list', array('conditions' => array('OR' => array($seperate_coma)), 'fields' => 'MainCategory.name,MainCategory.slug'));

                
                $this->set('main_categories',$main_categories);
            if ($answers) {
				 if (empty($answers[0]['Question']['image_user']) || ($answers[0]['Question']['image_user'] == 0)) {
                    $update = $this->Question->updateAll(
                            array('Question.image_user' => '"' . $answers[0]['Question']['image'] . '"'), array('Question.id' => $answers[0]['Question']['id']));
                }
                if (!($this->request->is('post'))) {

                    if (empty($answers[0]['Question']['last'])) {
                        if (!empty($answers[0]['Question']['title_update'])) {
                            $explode_title = explode(',,,', $answers[0]['Question']['title_update']);
                            $last = end($explode_title);
                            $titles = end(unserialize(base64_decode($last)));
                        } else {
                            $titles = $answers[0]['Question']['title'];
                        }

                        $this->request->data['last_art_title'] = $titles;
                        $update = $this->Question->updateAll(
                                array('Question.last' => '"' . $this->request->data['last_title'] . '"'), array('Question.id' => $answers[0]['Question']['id']));
                        foreach ($answers as $answer) {
                            if (empty($answer['Answer']['last_body'])) {
                                if (!empty($answer['Answer']['title_update'])) {
                                    $explode_title = explode(',,,', $answer['Answer']['title_update']);
                                    $last = end($explode_title);

                                    $title = end(unserialize(base64_decode($last)));
                                } else {
                                    $title = $answer['Answer']['title'];
                                }
                                if (!empty($answer['Answer']['body_update'])) {
                                    $explode_body = explode(',,,', $answer['Answer']['body_update']);
                                    $last = end($explode_body);
                                    $body = end(unserialize(base64_decode($last)));
                                } else {
                                    $body = $answer['Answer']['body'];
                                }
                                $this->request->data['last_title'] = $title;
                                $this->request->data['last_title'] = str_replace('"', "'", $title);
                                $this->request->data['last_body'] = $body;
                                $this->request->data['last_body'] = str_replace('"', "'", $body);

                                $update = $this->Answer->updateAll(
                                        array('Answer.last_title' => '"' . $this->request->data['last_title'] . '"', 'Answer.last_body' => '"' . $this->request->data['last_body'] . '"'), array('Answer.id' => $answer['Answer']['id']));
                            }
                        }
                    }
                }
                $countur = (int) $answers[0]['Question']['views'] + 1;
                $update = $this->Question->updateAll(
                        array('Question.views' => $countur), array('Question.id' => $answers[0]['Question']['id']));

                $category = $this->QuestionCategory->find('list', array(
                    'fields' => 'QuestionCategory.category_id,QuestionCategory.category_id',
                    'conditions' => array('QuestionCategory.question_id' => $answers[0]['Question']['id'])));

                $tags = $this->QuestionCategory->find('all', array(
                    'fields' => 'Category.id,Category.slug,Category.name',
                    'conditions' => array('QuestionCategory.question_id' => $answers[0]['Question']['id'])));
                $categories_all = $this->QuestionCategory->find('all', array(
                    'fields' => array('DISTINCT(Question.id),Question.image, Question.title_update , Question.slug', 'Question.title'),
                    'limit' => 5,
                    'conditions' => array('Question.status'=>1,'Question.image <>'=>Null,'Category.id' => $category, "NOT" => array('QuestionCategory.question_id' => $answers[0]['Question']['id']))));

                if (count($categories_all) < 5) {
                    $remain = 5 - count($categories_all);
                    $a2 = $this->QuestionCategory->find('all', array(
                        'order' => 'Question.views DESC',
                        'fields' => array('DISTINCT(Question.id),Question.image, Question.title_update , Question.slug', 'Question.title'),
						'conditions'=>array('Question.status'=>1,'Question.image <>'=>Null),
                        'limit' => $remain));
                    $categories_all = array_merge($categories_all, $a2);
                }
                $this->loadModel('User');
                $user = $this->User->find('first', array('fields' => array('User.id,User.username'), 'conditions' => array('User.id' => $answers[0]['Question']['user_id'])));
                if (!empty($user)) {
                    $this->set('question_write', $user);
                } else {
                    $this->set('question_write', NULL);
                }
                $this->set('categories', $categories_all);
                $this->set('answers', $answers);
                $this->set('tags', $tags);
            } else {
                
            }
        }
    }

        public function edit($question_slug) {
        $this->loadModel('Block');
        $test_ip = $this->get_client_ip_server();
        $block = $this->Block->find('first', array('fields' => array('Block.user_ip'), 'conditions' => array('Block.user_ip LIKE ' => "%$test_ip%")));

        if (!empty($block)) {
            $this->redirect('/');
        }
        if ($block['Block']['user_ip'] == $test_ip) {
            $this->redirect('/');
        }
        $user_session = $this->Session->read('user_quesli');

        $this->loadModel('Log');
        $this->loadModel('QuestionCategory');
        $this->loadModel('Question');
        $this->loadModel('Answer');
        $answers = $this->Answer->find('all', array(
            'fields' => array('Question.image_user,Answer.image_user,Question.reference,Answer.last_title,Answer.last_body,Answer.sort,Question.selected,Question.last,Question.views,Question.image,Question.status,Question.reference,Question.user_id,Question.title_update,Answer.title_update,Answer.body_update,Question.title_update', 'Answer.image', 'Answer.title', 'Question.id,Question.title,Question.slug,Answer.id,Answer.body,User.username,User.id'),
            'order' => array('Answer.sort ASC'), 'conditions' => array('Question.slug' => $question_slug),
        ));
        if (empty($answers[0]['Question']['image_user']) || ($answers[0]['Question']['image_user'] == 0)) {
            $update = $this->Question->updateAll(
                    array('Question.image_user' => '"' . $answers[0]['Question']['image'] . '"'), array('Question.id' => $answers[0]['Question']['id']));
        }
        $categories_all = $this->QuestionCategory->find('list', array(
            'fields' => array('QuestionCategory.category_id'),
            'limit' => 10,
            'conditions' => array('QuestionCategory.question_id' => $answers[0]['Question']['id'])));
        if (in_array(112, $categories_all)) {
            if ($user_session['User']['admin'] != 1) {
                $this->redirect('/');
            }
        }
        if ($answers) {
            if (!($this->request->is('post'))) {
                if ($_POST['as_clear'] == 1) {
                    if (empty($answers[0]['Question']['last'])) {
                        if (!empty($answers[0]['Question']['title_update'])) {
                            $explode_title = explode(',,,', $answers[0]['Question']['title_update']);
                            $last = end($explode_title);
                            $titles = end(unserialize(base64_decode($last)));
                        } else {
                            $titles = $answers[0]['Question']['title'];
                        }
                        $this->request->data['last_art_title'] = $titles;
                        $update = $this->Question->updateAll(
                                array('Question.last' => '"' . $this->escape_quotes($this->request->data['last_art_title'])), array('Question.id' => $answers[0]['Question']['id']));
                        foreach ($answers as $answer) {
                            if (empty($answer['Answer']['last_body'])) {
                                if (!empty($answer['Answer']['title_update'])) {
                                    $explode_title = explode(',,,', $answer['Answer']['title_update']);
                                    $last = end($explode_title);

                                    $title = end(unserialize(base64_decode($last)));
                                } else {
                                    $title = $answer['Answer']['title'];
                                }
                                if (!empty($answer['Answer']['body_update'])) {
                                    $explode_body = explode(',,,', $answer['Answer']['body_update']);
                                    $last = end($explode_body);
                                    $body = end(unserialize(base64_decode($last)));
                                } else {
                                    $body = $answer['Answer']['body'];
                                }
                                $this->request->data['last_title'] = $title;
                                $this->request->data['last_body'] = $body;
                                $update = $this->Answer->updateAll(
                                        array('Answer.last_title' => '"' . $this->request->data['last_title'] . '"', 'Answer.last_body' => '"' . $this->request->data['last_body'] . '"'), array('Answer.id' => $answer['Answer']['id']));
                            }
                        }
                    }
                } else {
// I am a spambot who cannot process Javascript or a user who has Javascript turned off
                }
            }
            $countur = (int) $answers[0]['Question']['views'] + 1;
            $update = $this->Question->updateAll(
                    array('Question.views' => $countur), array('Question.id' => $answers[0]['Question']['id']));
            $category = $this->QuestionCategory->find('list', array(
                'fields' => 'QuestionCategory.question_id,QuestionCategory.category_id',
                'conditions' => array('QuestionCategory.question_id' => $answers[0]['Question']['id'])));
            $categories_all = $this->QuestionCategory->find('all', array(
                'fields' => array('Question.status,Question.type,Question.id,Question.title,Question.slug,Question.user_id'),
                'conditions' => array('QuestionCategory.category_id' => $category, 'NOT' => array('QuestionCategory.question_id' => $answers[0]['Question']['id']))));
            $tags = $this->QuestionCategory->find('all', array(
                'fields' => 'Category.id,Category.slug,Category.name',
                'conditions' => array('QuestionCategory.question_id' => $answers[0]['Question']['id'])));

            $this->loadModel('User');
            $user = $this->User->find('first', array('fields' => array('User.id,User.username'), 'conditions' => array('User.id' => $answers[0]['Question']['user_id'])));
            if (!empty($user)) {
                $this->set('question_write', $user);
            } else {
                $this->set('question_write', NULL);
            }
            $this->set('categories', $categories_all);
            $this->set('answers', $answers);
            $this->set('tags', $tags);
        } else {
            $this->redirect('/');
        }
        if ($this->request->is('post')) {

            if ($_POST['as_clear'] == 1) {
                if ($this->Session->read('user_quesli')) {
                    $notification = 'Congrats , you gained new points! go to dashboard to check your new rank';
                } else {
                    $msg = "<br> New Annonymus article is added  :" . $answers[0]['Question']['last'];
                    $to = "heshamkadry.hk@gmail.com";
                    $subject = "Article edit";
                    mail($to, $subject, $msg);
                    $notification = 'Thank you for your contribution, register now to join our community of Speeliests';
                }

                $this->Session->setFlash($notification, 'default', array(), 'speeli_notification');

                for ($sort_num = 0; $sort_num < count($this->request->data['sort_value']); $sort_num++) {
                    $update = $this->Answer->updateAll(
                            array('Answer.sort' => $sort_num + 1), array('Answer.id' => $this->request->data['id'][$sort_num]));
                }
                if ($user_session) {
                    $userid = $user_session['User']['id'];
                    $userids = $user_session['User']['id'];
                    $userip = 0;
                } else {
                    $userid = $this->get_client_ip_server();
                    $userids = 0;
                    $userip = $this->get_client_ip_server();
                }
                $dt = new DateTime();
                $date = $dt->format('Y-m-d H:i:s');
                if (!empty($answers[0]['Question']['title_update'])) {
                    $explode_title = explode(',,,', $answers[0]['Question']['title_update']);
                    $last = end($explode_title);
                    $titles_check = end(unserialize(base64_decode($last)));
                } else {
                    $titles_check = $answers[0]['Question']['title'];
                }
                if (empty($answers[0]['Question']['last'])) {
                    $last = $this->request->data['art_title'][0];
                } else {
                    $last = $answers[0]['Question']['last'];
                }

                if (isset($this->request->data['ref']) && !empty($this->request->data['ref'])) {
                    $refs = explode(',,,,', $answers[0]['Question']['reference']);
                    $ref = '';
                    for ($ref_count = 0; $ref_count < count($this->request->data['ref']); $ref_count++) {
                        $ref_url = explode(',,', $refs[$ref_count]);
                        if (($this->request->data['ref_name'][$ref_count] == $ref_url[0]) && ($this->request->data['ref'][$ref_count] == $ref_url[1])) {
                            echo $i;
                            if ($ref_count == 0) {
                                $ref.=$this->request->data['ref_name'][$ref_count] . ',,' . $this->request->data['ref'][$ref_count];
                            } else {
                                $ref.=",,,," . $this->request->data['ref_name'][$ref_count] . ',,' . $this->request->data['ref'][$ref_count];
                            }
                        } else {
                            $log['Log']['user_id'] = $userid;
                            $log['Log']['question'] = $last;
                            $log['Log']['question_slug'] = $question_slug;
                            $log['Log']['user_ip'] = $userip;
                            $log['Log']['question_id'] = $this->request->data['art_id'][0];
                            $log['Log']['answer_id'] = $ref_count;
                            $log['Log']['title'] = 'reference has been updated';
                            $this->Log->Create();
                            $this->Log->save($log);
                            echo $i;
                            if ($ref_count == 0) {
                                $ref.=$this->request->data['ref_name'][$ref_count] . ',,' . $this->request->data['ref'][$ref_count];
                            } else {
                                $ref.=",,,," . $this->request->data['ref_name'][$ref_count] . ',,' . $this->request->data['ref'][$ref_count];
                            }
                        }
                    }
                    $ref = $this->escape_quotes($ref);
                } else {
                    $ref = '';
                }

                $question_id = $this->request->data['art_id'][0];
                $image_status = false;
                if ($this->request->params['form']['image_title']['size'][0] != 0) {
                    $date = date("Y-m-d") . '_' . rand(0, 999);
                    $name = $date . $this->request->params['form']['image_title']['name'][0];
                    $tmp = $this->request->params['form']['image_title']['tmp_name'][0];
                    if (!file_exists(WWW_ROOT . 'question' . DS . $question_id)) {
                        mkdir(WWW_ROOT . 'question' . DS . $question_id, 0777, true);
                    }
                    $filePath = WWW_ROOT . 'question' . DS . $question_id . DS . $name;
                    if (move_uploaded_file($tmp, $filePath)) {
                        $article_image = $name;
                        $image_status = true;
                    }
                } else {
                    
                }


                if (str_replace("&nbsp;", "", $titles_check) != $this->request->data['art_title'][0]) {
                    $array_new = array($userid, $date, $this->request->data['art_title'][0]);
                    $array_new = base64_encode(serialize($array_new));
                    //$last = str_replace('"', "'", $title);
                    $last = $this->escape_quotes($last);
                    if ($image_status) {
                        $article_image = $answers[0]['Question']['image_user'] . "____" . $article_image;
                        $log['Log']['user_id'] = $userid;
                        $log['Log']['question'] = $last;
                        $log['Log']['question_slug'] = $question_slug;
                        $log['Log']['user_ip'] = $userip;
                        $log['Log']['question_id'] = $this->request->data['art_id'][0];
                        $log['Log']['answer_id'] = 0;
                        $log['Log']['title'] = 'Image has been updated';
                        $this->Log->Create();
                        $this->Log->save($log);
                        $update = $this->Question->updateAll(
                                array('Question.last' => '"' . $last . '"', 'Question.image_user' => "'" . $article_image . "'", 'Question.title_update' => "'" . $array_new . "'", 'Question.reference' => '"' . $ref . '"'), array('Question.id' => $this->request->data['art_id'][0]));
                    } else {
                        $update = $this->Question->updateAll(
                                array('Question.last' => '"' . $last . '"', 'Question.title_update' => "'" . $array_new . "'", 'Question.reference' => '"' . $ref . '"'), array('Question.id' => $this->request->data['art_id'][0]));
                    }
                    $log['Log']['user_id'] = $userid;
                    $log['Log']['question'] = $last;
                    $log['Log']['question_slug'] = $question_slug;
                    $log['Log']['user_ip'] = $userip;
                    $log['Log']['question_id'] = $this->request->data['art_id'][0];
                    $log['Log']['answer_id'] = 0;
                    $log['Log']['title'] = 'Question title updated';
                    $this->Log->Create();
                    $this->Log->save($log);
                } else {
                    $last = $this->escape_quotes($last);
                    if ($image_status) {
                        $article_image = $answers[0]['Question']['image_user'] . "____" . $article_image;
                        $update = $this->Question->updateAll(
                                array('Question.last' => '"' . $last . '"',
                            'Question.image_user' => "'" . $article_image . "'",
                            'Question.reference' => '"' . $ref . '"'), array('Question.id' => $this->request->data['art_id'][0]));
                        $log['Log']['user_id'] = $userid;
                        $log['Log']['question'] = $last;
                        $log['Log']['question_slug'] = $question_slug;
                        $log['Log']['user_ip'] = $userip;
                        $log['Log']['question_id'] = $this->request->data['art_id'][0];
                        $log['Log']['answer_id'] = 0;
                        $log['Log']['title'] = 'Image has been updated';
                        $this->Log->Create();
                        $this->Log->save($log);
                    } else {
                        $update = $this->Question->updateAll(array('Question.last' => '"' . $last . '"', 'Question.reference' => '"' . $ref . '"'), array('Question.id' => $this->request->data['art_id'][0]));
                    }
                }
                $i = 0;
                foreach ($answers as $answer) {
                    if ($this->request->params['form']['image']['size'][$i] != 0) {
                        if (preg_match("/image/", $this->request->params['form']['image']['type'][$i])) {
                            $explode = explode('.', $this->request->params['form']['image']['name'][$i]);
                            $date = date("Y-m-d") . '_' . rand(0, 99999);
                            $name = $date . "." . $explode[1];
                            $tmp = $this->request->params['form']['image']['tmp_name'][$i];
                            if (!file_exists(WWW_ROOT . 'question' . DS . $question_id)) {
                                mkdir(WWW_ROOT . 'question' . DS . $question_id, 0777, true);
                            }
                            $filePath = WWW_ROOT . 'question' . DS . $question_id . DS . $name;
                            if (move_uploaded_file($tmp, $filePath)) {
                                $update = $this->Answer->updateAll(array('Answer.image' => '"' . $name . '"'), array('Answer.question_id' => $this->request->data['art_id'][0], 'Answer.id' => $this->request->data['id'][$i]));
                                $log['Log']['user_id'] = $userid;
                                $log['Log']['question_slug'] = $question_slug;
                                $log['Log']['question'] = $last;
                                $log['Log']['answer_id'] = $i + 1;
                                $log['Log']['user_ip'] = $userip;
                                $log['Log']['question_id'] = $this->request->data['art_id'][0];

                                $log['Log']['title'] = 'new image added';
                                $this->Log->Create();
                                $this->Log->save($log);
                            }
                        }
                    }
                    if (!empty($this->request->data['title'][$i])) {
                        if (empty($answer['Answer']['last_title'])) {
                            $update = $this->Answer->updateAll(array('Answer.last_title' => '"' . $this->request->data['title'][$i] . '"'), array('Answer.question_id' => $this->request->data['art_id'][0], 'Answer.id' => $this->request->data['id'][$i]));
                        } else {
                            if ($answer['Answer']['last_title'] != $this->request->data['title'][$i]) {
                                if ($answer['Answer']['last_title'] != $this->request->data['title'][$i]) {
                                    if (!empty($answer['Answer']['title_update'])) {
                                        $explode_title = explode(',,,', $answer['Answer']['title_update']);
                                        $body = $answer['Answer']['last_title'];
                                        $body = str_replace('"', "'", $body);
                                        //$body_old=$answer['Answer']['last_title'];
                                        $array_new = array($userid, $date, $body);
                                        $array_new = base64_encode(serialize($array_new));
                                        $array_new = $answer['Answer']['title_update'] . ",,," . $array_new;
                                        $update = $this->Answer->updateAll(array('Answer.last_title' => '"' . $this->request->data['title'][$i] . '"', 'Answer.title_update' => "'" . $array_new . "'"), array('Answer.question_id' => $this->request->data['art_id'][0], 'Answer.id' => $this->request->data['id'][$i]));
                                        $log['Log']['user_id'] = $userid;
                                        $log['Log']['question_slug'] = $question_slug;
                                        $log['Log']['question'] = $last;
                                        $log['Log']['answer_id'] = $i + 1;
                                        $log['Log']['user_ip'] = $userip;
                                        $log['Log']['question_id'] = $this->request->data['art_id'][0];

                                        $log['Log']['title'] = 'point title updated';
                                        $this->Log->Create();
                                        $this->Log->save($log);
                                    } else {
                                        if ($this->request->data['title'][$i] != $answer['Answer']['title']) {
                                            $this->request->data['title'][$i] = str_replace('"', "'", $this->request->data['title'][$i]); //for text formatting
                                            $answer['Answer']['last_title'] = str_replace('"', "'", $answer['Answer']['last_title']);
                                            $array_new = array($userid, $date, $answer['Answer']['last_title']);
                                            $array_new = base64_encode(serialize($array_new));
                                            $update = $this->Answer->updateAll(array('Answer.last_title' => '"' . $this->request->data['title'][$i] . '"', 'Answer.title_update' => "'" . $array_new . "'"), array('Answer.question_id' => $this->request->data['art_id'][0], 'Answer.id' => $this->request->data['id'][$i]));
                                            $log['Log']['user_id'] = $userid;
                                            $log['Log']['question'] = $last;
                                            $log['Log']['answer_id'] = $i + 1;
                                            $log['Log']['question_slug'] = $question_slug;
                                            $log['Log']['user_ip'] = $userip;
                                            $log['Log']['question_id'] = $this->request->data['art_id'][0];

                                            $log['Log']['title'] = 'point title updated';
                                            $this->Log->Create();
                                            $this->Log->save($log);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (!empty($this->request->data['body'][$i])) {
                        if (empty($answer['Answer']['last_body'])) {
                            $update = $this->Answer->updateAll(array('Answer.last_body' => '"' . $this->request->data['body'][$i] . '"'), array('Answer.question_id' => $this->request->data['art_id'][0], 'Answer.id' => $this->request->data['id'][$i]));
                        } else {
                            if ($answer['Answer']['last_body'] != $this->request->data['body'][$i]) {
                                $output = strcasecmp(strip_tags(trim($answer['Answer']['last_body'])), strip_tags(trim($this->request->data['body'][$i])));
                                if ($output != 0) {
                                    if (!empty($answer['Answer']['body_update'])) {
                                        $body = str_replace('"', "'", $this->request->data['body'][$i]); //for text formatting                                        
                                        $body = str_replace('"', "'", $answer['Answer']['last_body']);
                                        $array_new = array($userid, $date, $body);
                                        $array_new = base64_encode(serialize($array_new));
                                        $array_new = $answer['Answer']['body_update'] . ",,," . $array_new;
                                        $body = $answer['Answer']['last_body'];
                                        $update = $this->Answer->updateAll(array('Answer.last_body' => '"' . $this->request->data['body'][$i] . '"', 'Answer.body_update' => "'" . $array_new . "'"), array('Answer.question_id' => $this->request->data['art_id'][0], 'Answer.id' => $this->request->data['id'][$i]));
                                        $log['Log']['user_id'] = $userid;
                                        $log['Log']['question_slug'] = $question_slug;
                                        $log['Log']['answer_id'] = $i + 1;
                                        $log['Log']['question'] = $last . " point $body";
                                        $log['Log']['user_ip'] = $userip;
                                        $log['Log']['question_id'] = $this->request->data['art_id'][0];

                                        $log['Log']['title'] = 'point body updated';
                                        $this->Log->Create();
                                        $this->Log->save($log);
                                    } else {
                                        if ($this->request->data['body'][$i] != $answer['Answer']['body']) {
                                            $array_new = array($userid, $date, $answer['Answer']['last_body']);
                                            $this->request->data['body'][$i] = str_replace('"', "'", $this->request->data['body'][$i]);
                                            $array_new = base64_encode(serialize($array_new));
                                            $update = $this->Answer->updateAll(array('Answer.last_body' => '"' . $this->request->data['body'][$i] . '"', 'Answer.body_update' => "'" . $array_new . "'"), array('Answer.question_id' => $this->request->data['art_id'][0], 'Answer.id' => $this->request->data['id'][$i]));
                                            $log['Log']['user_id'] = $userid;
                                            $log['Log']['question_slug'] = $question_slug;
                                            $log['Log']['answer_id'] = $i + 1;
                                            $log['Log']['user_ip'] = $userip;
                                            $log['Log']['question'] = $last . " point $body";
                                            $log['Log']['question_id'] = $this->request->data['art_id'][0];

                                            $log['Log']['title'] = 'point body updated';
                                            $this->Log->Create();
                                            $this->Log->save($log);
                                        }
                                    }
                                }
                            }
                        }
                    }
                    $i++;
                }
                $i = 0;
                if (isset($this->request->data['body_new']) && !empty($this->request->data['body_new'])) {
                    $j = 0;
                    if (!empty($this->request->data['body_new'])) {
                        foreach ($this->request->data['body_new'] as $points) {
                            $question_id = $this->request->data['art_id'][0];
                            if (!empty($points)) {
                                if (isset($this->request->params['form']['image_new']['name'][$j])) {
                                    if ($this->request->params['form']['image_new']['size'][$j] != 0) {
                                        $date = date("Y-m-d") . '_' . rand(0, 999);
                                        $name = $date . $this->request->params['form']['image_new']['name'][$j];
                                        $tmp = $this->request->params['form']['image_new']['tmp_name'][$j];
                                        if (!file_exists(WWW_ROOT . 'question' . DS . $question_id)) {
                                            mkdir(WWW_ROOT . 'question' . DS . $question_id, 0777, true);
                                        }
                                        $filePath = WWW_ROOT . 'question' . DS . $question_id . DS . $name;
                                        if (move_uploaded_file($tmp, $filePath)) {
                                            $answer_new[$i]['Answer']['image'] = $name;
                                        }
                                    }
                                }

                                $answer_new[$i]['Answer']['last_title'] = str_replace('"', "'", $this->request->data['title_new'][$i]);
                                $answer_new[$i]['Answer']['last_body'] = str_replace('"', "'", $this->request->data['body_new'][$i]);
                                $answer_new[$i]['Answer']['title'] = str_replace('"', "'", $this->request->data['title_new'][$i]);
                                $answer_new[$i]['Answer']['body'] = str_replace('"', "'", $this->request->data['body_new'][$i]);
                                $answer_new[$i]['Answer']['sort'] = count($answers) + 1 + $i;
                                $answer_new[$i]['Answer']['question_id'] = $question_id;
                                if ($user_session) {
                                    $userid = $user_session['User']['id'];
                                } else {
                                    $userid = 0;
                                    $answer_new[$i]['Answer']['user_ip'] = $this->get_client_ip_server();
                                }
                                $answer_new[$i]['Answer']['user_id'] = $userid;
                                $i++;
                            }
                            $j++;
                        }
                        if (!empty($answer_new)) {

                            if ($this->Answer->saveAll($answer_new)) {
                                $log['Log']['user_id'] = $userid;
                                $log['Log']['question'] = $last;

                                $log['Log']['question_slug'] = $question_slug;
                                $log['Log']['user_ip'] = $userip;
                                $log['Log']['question_id'] = $this->request->data['art_id'][0];
                                $log['Log']['answer_id'] = 0;
                                $log['Log']['title'] = 'new point added';

                                $this->Log->Create();
                                $this->Log->save($log);
                            }
                        }
                    }
                    $this->redirect(array('controller' => 'articles', 'action' => 'view', $answers[0]['Question']['slug']));
                } else {

                    $this->redirect(array('controller' => 'articles', 'action' => 'view', $answers[0]['Question']['slug']));
                }
            }
        }
    }

    public function select_home() {
        $this->request->onlyAllow('ajax');
        $this->layout = $this->autoRender = false;
        $this->loadModel('Selected');
        $this->loadModel('Question');
        if (isset($_POST['question_id'])) {
            if ($_POST['status'] == 1) {
                $data['Selected']['question_id'] = $_POST['question_id'];
                $data['Selected']['slug'] = $_POST['slug'];
                $data['Selected']['image'] = $_POST['article_image'];
                $data['Selected']['title'] = $_POST['title'];
                $this->Selected->Create();
                $this->Selected->save($data);
                $this->Question->updateAll(array('Question.selected' => 1), array('Question.id' => $_POST['question_id']));
            } else {
                if ($this->Selected->deleteAll(array('Selected.question_id' => $_POST['question_id']))) {
                    $this->Question->updateAll(array('Question.selected' => 0), array('Question.id' => $_POST['question_id']));
                }
            }
        }
    }

    public function point($aid) {
        $this->loadModel('Answer');
        $this->loadModel('Question');
        $answers = $this->Answer->find('all', array(
            'fields' => array('Answer.image_user,Question.last,Answer.last_title,Answer.last_body,Answer.title_update,Answer.body_update', 'Answer.image', 'Answer.title', 'Question.id,Question.title,Question.slug,Answer.id,Answer.body,User.username,User.id'),
            'conditions' => array('Answer.id' => $aid),
        ));
        $this->set('answers', $answers);
    }

    function update() {
        $this->layout = $this->autoRender = false;

        $user = $this->Session->read('user_quesli');
        $this->loadmodel('Answer');
        if (!empty($_POST['quesuserd'])) {

            $this->Answer->create();
            $answer = $this->Answer->find('first', array(
                'fields' => array('Answer.title_update,Answer.image_update,Answer.body_update'),
                'conditions' => array('Answer.question_id' => $_POST['quesuserarticle_i'], 'Answer.id' => $_POST['quesuseri'])));
            if ($user) {
                $userid = $user['User']['id'];
            } else {
                $userid = $this->get_client_ip_server();
            }
            Debug($answer);
            $dt = new DateTime();
            $date = $dt->format('Y-m-d H:i:s');
            $body = $_POST['quesuserv'];
            $array_new = array($userid, $date, $body);
            $array_new = base64_encode(serialize($array_new));
            // make sure to encode the serialized object

            if ($_POST['quesuserd'] == 't') {
                $field = 'title_update';
                $array = $answer['Answer']['title_update'] . ",,," . $array_new;
            } elseif ($_POST['quesuserd'] == 'b') {
                $field = 'body_update';
                $array = $answer['Answer']['body_update'] . ",,," . $array_new;
            }
            $update = $this->Answer->updateAll(array('Answer.' . $field => "'" . $array . "'"), array('Answer.question_id' => $_POST['quesuserarticle_i'], 'Answer.id' => $_POST['quesuseri']));
        }
    }

    function update_article() {
        $this->layout = $this->autoRender = false;

        $user = $this->Session->read('user_quesli');
        $this->loadmodel('Answer');
        if (!empty($_POST['quesuserd'])) {

            $this->Question->create();
            $answer = $this->Question->find('first', array(
                'fields' => array('Question.title_update'),
                'conditions' => array('Question.id' => $_POST['quesuserarticle_i'])));
            if ($user) {
                $userid = $user['User']['id'];
            } else {
                $userid = $this->get_client_ip_server();
            }
            Debug($answer);
            $dt = new DateTime();
            $date = $dt->format('Y-m-d H:i:s');
            $body = $_POST['quesuserv'];
            $array_new = array($userid, $date, $body);
            $array_new = base64_encode(serialize($array_new));
            // make sure to encode the serialized object

            if ($_POST['quesuserd'] == 't') {
                $field = 'title_update';
                $array = $answer['Answer']['title_update'] . ",,," . $array_new;
            }
            $update = $this->Question->updateAll(array('Question.' . $field => "'" . $array . "'"), array('Question.id' => $_POST['quesuserarticle_i']));
        }
    }
 public function un_category($category=false) {
        $this->loadModel('Question');
        if($category){
			 $this->loadModel('MainCategory');
            $categories=$this->MainCategory->find('list');
            $this->set('categories',$categories);
        $this->paginate = array(
            'fields' => array('Question.id,Question.category_id,Question.links,Question.last,Question.edited,Question.quality,Question.expand', 'Question.views,Question.created,Question.image,Question.title_update,Question.title', 'Question.slug,Question.id,Question.status'),
            'limit' => 50,
            'conditions'=>array('Question.category_id'=>0),
            'order' => 'Question.id DESC');    
        $this->set('category',1);
        }else{
        $this->paginate = array(
            'fields' => array('Question.id,Question.category_id,Question.links,Question.last,Question.edited,Question.quality,Question.expand', 'Question.views,Question.created,Question.image,Question.title_update,Question.title', 'Question.slug,Question.id,Question.status'),
            'limit' => 50,
            'conditions'=>array('Question.summary_id'=>NULL),
            'order' => 'Question.id DESC');   
        $this->set('category',0);
        }
       
        $questions = $this->Paginator->paginate('Question');
        $this->set('questions', $questions);
    }
    public function show_news() {
        $this->loadModel('Question');
        $this->paginate = array(
            'fields' => array('Question.id,Question.category_id,Question.links,Question.last,Question.edited,Question.locked,Question.share_count,Question.quality,Question.expand', 'Question.views,Question.created,Question.image,Question.title_update,Question.title', 'Question.slug,Question.id,Question.status'),
            'limit' => 200,
            'order' => 'Question.id DESC');
        $questions = $this->Paginator->paginate('Question');
        $this->set('questions', $questions);
    }
	 public function art_lock() {
        $this->request->onlyAllow('ajax');
        $this->layout = $this->autoRender = false;
        $this->loadModel('Question');
        if (isset($_POST['art_id'])) {
            if ($_POST['art_status'] == 0) {
                if ($this->Question->updateAll(array('Question.locked' => 0),
                        array('Question.id' => $_POST['art_id']))) {
                    
                }
            } else {
                if ($this->Question->updateAll(array('Question.locked' => 1), array('Question.id' => $_POST['art_id']))) {
                    
                }
            }
        }
    }
	
    public function linked() {
        $this->request->onlyAllow('ajax');
        $this->layout = $this->autoRender = false;
        $this->loadModel('Question');
        if (isset($_POST['art_id'])) {
            if ($_POST['art_status'] == 0) {
                if ($this->Question->updateAll(array('Question.links' => 0), array('Question.id' => $_POST['art_id']))) {
                    
                }
            } else {
                if ($this->Question->updateAll(array('Question.links' => 1), array('Question.id' => $_POST['art_id']))) {
                    
                }
            }
        }
    }

    public function expand() {
        $this->request->onlyAllow('ajax');
        $this->layout = $this->autoRender = false;
        $this->loadModel('Question');

        if (isset($_POST['art_id'])) {
            if ($_POST['art_status'] == 0) {
                if ($this->Question->updateAll(array('Question.expand' => 0), array('Question.id' => $_POST['art_id']))) {
                    
                }
            } else {
                if ($this->Question->updateAll(array('Question.expand' => 1), array('Question.id' => $_POST['art_id']))) {
                    
                }
            }
        }
    }

    public function quality() {
        $this->request->onlyAllow('ajax');
        $this->layout = $this->autoRender = false;
        $this->loadModel('Question');
        if (isset($_POST['art_id'])) {
            if ($_POST['art_status'] == 0) {
                if ($this->Question->updateAll(array('Question.quality' => 0), array('Question.id' => $_POST['art_id']))) {
                    
                }
            } else {
                if ($this->Question->updateAll(array('Question.quality' => 1), array('Question.id' => $_POST['art_id']))) {
                    
                }
            }
        }
    }

    public function art_edit() {
        $this->request->onlyAllow('ajax');
        $this->layout = $this->autoRender = false;
        $this->loadModel('Question');
        if (isset($_POST['art_id'])) {
            if ($_POST['art_status'] == 0) {
                if ($this->Question->updateAll(array('Question.edited' => 0), array('Question.id' => $_POST['art_id']))) {
                    
                }
            } else {
                if ($this->Question->updateAll(array('Question.edited' => 1), array('Question.id' => $_POST['art_id']))) {
                    
                }
            }
        }
    }

    public function check_question() {
        $this->loadModel('Question');
        $this->autoRender = false;
        $this->request->onlyAllow('ajax');
        $this->layout = $this->autoRender = false;
        if (isset($_POST['questitle']) && !empty($_POST['questitle'])) {
            $_POST['questitle'] = str_replace('"', "'", $_POST['questitle']);
            $seperates = $this->Question->spilt_title($_POST['questitle']);
            $seperate_coma = '';
            foreach ($seperates as $seperate) {
                $seperate_coma.="(+" . $seperate . "*) ";
            }

            $this->loadModel('Tag');
            $this->loadModel('Category');
            $this->loadModel('Question');

            $questions = $this->Question->find('all', array(
                'fields' => array('Question.title_update , Question.id,Question.title,  MATCH(Question.title)  AGAINST(" ' . $seperate_coma . ' " IN BOOLEAN MODE) AS releveance ,Question.slug'),
                'conditions' => array('Question.status'=>1,'OR' => array('MATCH(Question.title)  AGAINST(" ' . $seperate_coma . ' " IN BOOLEAN MODE)')),
                'order' => 'releveance DESC',
                'limit' => 6));

            if (!empty($questions)) {
                $this->set('questions', $questions);
            }
        }
        $this->render('check_question');
    }

    public function history($answer_id) {

        $this->loadModel('Answer');
        $this->layout = false;
        $this->layout = 'ajax';
        if (isset($answer_id)) {
            $history['Answer'] = $this->Answer->find('first', array('fields' => array('Answer.id,Answer.title_update,Answer.body_update')
                , 'conditions' => array('Answer.id' => $answer_id)));
            if (!empty($answer_id)) {
                $this->set('histories', $history['Answer']);
            }
        }
    }

    public function add_new_cat() {
        $this->layout = $this->autoRender = false;
        $this->layout = false;
        $this->loadModel('Category');
        $this->request->onlyAllow('ajax');
        if (isset($_POST['add_new_cat']) && !empty($_POST['add_new_cat'])) {
            $check_first = $this->Category->find('first', array('fields' => array('Category.id', 'Category.name'),
                'recursive' => -1,
                'conditions' => array('Category.name LIKE' => trim($_POST['add_new_cat']))));
            if (isset($check_first) && !empty($check_first['Category']['name'])) {
                return $category_id = $check_first['Category']['id'];
            } else {
                $data['Category']['name'] = trim($_POST['add_new_cat']);
                $data['Category']['slug'] = strip_tags(trim($_POST['add_new_cat']));
                $data['Category']['slug'] = str_replace(' ', '-', $data['Category']['slug']);
                $this->loadModel('Category');
                $this->Category->Create();
                $this->Category->save($data);
                return $category_id = $this->Category->getLastInsertId();
            }
        }
    }

    public function add_new_cat_edit() {
        $this->layout = $this->autoRender = false;
        $this->loadModel('Category');
        $this->request->onlyAllow('ajax');
        if (isset($_POST['add_new_cat']) && !empty($_POST['add_new_cat'])) {
            $check_first = $this->Category->find('first', array('fields' => array('Category.id', 'Category.name'),
                'recursive' => -1,
                'conditions' => array('Category.name LIKE' => trim($_POST['add_new_cat']))));

            if (isset($check_first) && !empty($check_first['Category']['name'])) {
                $category_id = $check_first['Category']['id'];
                $data['Category']['name'] = $_POST['add_new_cat'];
            } else {
                $data['Category']['name'] = trim($_POST['add_new_cat']);
                $data['Category']['slug'] = strip_tags(trim($_POST['add_new_cat']));
                $data['Category']['slug'] = str_replace(' ', '-', $data['Category']['slug']);
                $this->loadModel('Category');
                $this->Category->Create();
                $this->Category->save($data);
                $category_id = $this->Category->getLastInsertId();
            }
            $this->loadModel('QuestionCategory');
            $data['QuestionCategory']['category_id'] = $category_id;
            $data['QuestionCategory']['question_id'] = $_POST['art_id'];
            $this->QuestionCategory->Create();
            $this->QuestionCategory->save($data);
            $response['cat_id'] = $category_id;
            $response['art_id'] = $_POST['art_id'];
            $response['cat_name'] = $data['Category']['name'];
            $response['cat_slug'] = strip_tags(trim($data['Category']['name']));
            $response['cat_slug'] = str_replace(' ', '-', $response['cat_slug']);
            $this->set('response', $response);
            $this->render('add_new_cat_edit');
        }
    }

    public function search_cat() {
        $this->loadModel('Question');
        $this->loadModel('Category');
        $this->autoRender = false;
        $this->request->onlyAllow('ajax');
        $this->layout = $this->autoRender = false;
        if (isset($_POST['data_send']) && !empty($_POST['data_send'])) {
            $data = $_POST['data_send'];
            $seperates = $this->Question->spilt_title($_POST['data_send']);
            $seperate_coma = '';
            foreach ($seperates as $seperate) {
                $seperate_coma.="(+" . $seperate . "*) ";
            }
            $this->loadModel('Category');
            $tag = $this->Category->find('all', array(
			'group' => array('Category.name'),
                'fields' => array('Category.id', 'Category.name'),
                'conditions' => array("Category.name LIKE" => "%" . $data . "%"),
                //'conditions' => array("MATCH(Category.name) AGAINST('$data' IN BOOLEAN MODE)"),
                'recursive' => -1,
                'limit' => 10)
            );
            if (!empty($tag)) {
                $this->set('categories', $tag);
            }
        }
        $this->render('search_cat');
    }

    public function search_cat_edit() {
        $this->loadModel('Question');
        $this->loadModel('Category');
        $this->autoRender = false;
        $this->request->onlyAllow('ajax');
        $this->layout = $this->autoRender = false;
        if (isset($_POST['data_send']) && !empty($_POST['data_send'])) {
            $seperates = $this->Question->spilt_title($_POST['data_send']);
            $seperate_coma = '';
            foreach ($seperates as $seperate) {
                $seperate_coma.="(+" . $seperate . "*) ";
            }
            $this->loadModel('Category');
            $tag = $this->Category->find('all', array(
                'fields' => array('Category.id', 'Category.name'),
                'conditions' => array("MATCH(Category.name) AGAINST('$seperate_coma' IN BOOLEAN MODE)"),
                'recursive' => -1,
                'limit' => 10)
            );
            if (!empty($tag)) {
                $this->set('categories', $tag);
            }
        }
        $this->render('search_cat_edit');
    }

    public function edit_art_add_cat() {
        $this->request->onlyAllow('ajax');
        $this->layout = $this->autoRender = false;
        if (isset($_POST['art_id'])) {
            $this->loadModel('QuestionCategory');
            $data['QuestionCategory']['category_id'] = $_POST['cat_id'];
            $data['QuestionCategory']['question_id'] = $_POST['art_id'];
            $this->QuestionCategory->Create();
            $this->QuestionCategory->save($data);
            $response['cat_id'] = $_POST['cat_id'];
            $response['art_id'] = $_POST['art_id'];
            $response['cat_name'] = $_POST['cat_name'];
            $response['cat_slug'] = strip_tags(trim($_POST['cat_name']));
            $response['cat_slug'] = str_replace(' ', '-', $response['cat_slug']);
            $this->set('response', $response);
            $this->render('edit_art_add_cat');
        }
    }

    public function edit_art_delete_cat() {
        $this->request->onlyAllow('ajax');
        $this->autoRender = false;
        $this->layout = $this->autoRender = false;
        if (isset($_POST['art_id'])) {
            $this->loadModel('QuestionCategory');
            $data['QuestionCategory']['category_id'] = $_POST['cat_id'];
            $data['QuestionCategory']['question_id'] = $_POST['art_id'];
            $this->QuestionCategory->deleteAll(array('QuestionCategory.question_id' => $_POST['art_id'], 'QuestionCategory.category_id' => $_POST['cat_id']));
        }
    }

    public function get_cat() {
        $this->loadModel('Question');
        $this->loadModel('Category');
        $this->autoRender = false;
        $this->request->onlyAllow('ajax');
        $this->layout = $this->autoRender = false;
        if (isset($_POST['questitle']) && !empty($_POST['questitle'])) {
            $title_article = strip_tags($_POST['questitle']);
            $title_article = html_entity_decode($title_article);
            $title_article = preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', $title_article);
            $title_article = preg_replace('/&nbsp;/', ' ', $title_article);
            $seperates = $this->Question->spilt_title($title_article);
            $seperate_coma = '';
            foreach ($seperates as $seperate) {
                $seperate_coma.=" " . $seperate . " ";
            }

            $this->loadModel('Tag');
            $this->loadModel('Category');
            $this->loadModel('Question');
            $this->loadModel('QuestionCategory');
            $tag = $this->Category->find('all', array(
			'group' => array('Category.name'),
                'fields' => array('Category.id', 'Category.name'),
                'conditions' => array('MATCH(Category.name) AGAINST("' . $seperate_coma . '" IN BOOLEAN MODE)'),
                'recursive' => -1,
                'limit' => 4));

            if (!empty($tag)) {
                $this->set('categories', $tag);
            }
        }
        $this->render('get_cat');
    }

    public function vie($question_slug = false) {
        if ($question_slug != FALSE) {
            $this->loadModel('QuestionCategory');
            $this->loadModel('Answer');
            $this->loadModel('Question');
            $answers = $this->Answer->find('all', array(
                'fields' => array('Answer.sort,Question.selected,Question.views,Question.image,Question.status,Question.reference,Question.user_id,Question.title_update,Answer.title_update,Answer.body_update,Question.title_update', 'Answer.image', 'Answer.title', 'Question.id,Question.title,Question.slug,Answer.id,Answer.body,User.username,User.id'),
                'order' => array('Answer.sort ASC'),
                'conditions' => array('Question.slug' => $question_slug),
            ));
            if ($answers) {
                $countur = (int) $answers[0]['Question']['views'] + 1;
                $update = $this->Question->updateAll(
                        array('Question.views' => $countur), array('Question.id' => $answers[0]['Question']['id']));

                $category = $this->QuestionCategory->find('list', array(
                    'fields' => 'QuestionCategory.category_id,QuestionCategory.category_id',
                    'conditions' => array('QuestionCategory.question_id' => $answers[0]['Question']['id'])));

                $tags = $this->QuestionCategory->find('all', array(
                    'fields' => 'Category.id,Category.slug,Category.name',
                    'conditions' => array('QuestionCategory.question_id' => $answers[0]['Question']['id'])));
                $categories_all = $this->QuestionCategory->find('all', array(
                    'fields' => array('DISTINCT(Question.id), Question.title_update , Question.slug', 'Question.title'),
                    'limit' => 5,
                    'conditions' => array('Category.id' => $category, "NOT" => array('QuestionCategory.question_id' => $answers[0]['Question']['id']))));

                $this->loadModel('User');
                $user = $this->User->find('first', array('fields' => array('User.id,User.username'), 'conditions' => array('User.id' => $answers[0]['Question']['user_id'])));
                if (!empty($user)) {
                    $this->set('question_write', $user);
                } else {
                    $this->set('question_write', NULL);
                }
                $this->set('categories', $categories_all);
                $this->set('answers', $answers);
                $this->set('tags', $tags);
            } else {
                
            }
        }
    }

    public function edits($question_slug) {
        $user_session = $this->Session->read('user_quesli');

        $this->loadModel('QuestionCategory');
        $this->loadModel('Question');
        $this->loadModel('Answer');
        $answers = $this->Answer->find('all', array(
            'fields' => array('Answer.sort,Question.selected,Question.views,Question.image,Question.status,Question.reference,Question.user_id,Question.title_update,Answer.title_update,Answer.body_update,Question.title_update', 'Answer.image', 'Answer.title', 'Question.id,Question.title,Question.slug,Answer.id,Answer.body,User.username,User.id'),
            'order' => array('Answer.sort ASC'), 'conditions' => array('Question.slug' => $question_slug),
        ));
        if ($answers) {

            $countur = (int) $answers[0]['Question']['views'] + 1;
            $update = $this->Question->updateAll(
                    array('Question.views' => $countur), array('Question.id' => $answers[0]['Question']['id']));
            $category = $this->QuestionCategory->find('list', array(
                'fields' => 'QuestionCategory.question_id,QuestionCategory.category_id',
                'conditions' => array('QuestionCategory.question_id' => $answers[0]['Question']['id'])));
            $categories_all = $this->QuestionCategory->find('all', array(
                'fields' => array('Question.status,Question.type,Question.id,Question.title,Question.slug,Question.user_id'),
                'conditions' => array('QuestionCategory.category_id' => $category, 'NOT' => array('QuestionCategory.question_id' => $answers[0]['Question']['id']))));
            $tags = $this->QuestionCategory->find('all', array(
                'fields' => 'Category.id,Category.slug,Category.name',
                'conditions' => array('QuestionCategory.question_id' => $answers[0]['Question']['id'])));

            $this->loadModel('User');
            $user = $this->User->find('first', array('fields' => array('User.id,User.username'), 'conditions' => array('User.id' => $answers[0]['Question']['user_id'])));
            if (!empty($user)) {
                $this->set('question_write', $user);
            } else {
                $this->set('question_write', NULL);
            }
            $this->set('categories', $categories_all);
            $this->set('answers', $answers);
            $this->set('tags', $tags);
        } else {
            $this->redirect('/');
        }
        if ($this->request->is('post')) {

            for ($sort_num = 0; $sort_num < count($this->request->data['sort_value']); $sort_num++) {
                $update = $this->Answer->updateAll(
                        array('Answer.sort' => $sort_num + 1), array('Answer.id' => $this->request->data['id'][$sort_num]));
            }
            if ($user_session) {
                $userid = $user_session['User']['id'];
            } else {
                $userid = $this->get_client_ip_server();
            }
            $dt = new DateTime();
            $date = $dt->format('Y-m-d H:i:s');
            if (isset($this->request->data['ref']) && !empty($this->request->data['ref'])) {
                if (!empty($answers[0]['Question']['reference'])) {
                    $ref = $answers[0]['Question']['reference'] . ",,,,";
                } else {
                    $ref = '';
                }
                for ($ref_count = 0; $ref_count < count($this->request->data['ref']); $ref_count++) {
                    if ($ref_count == 0) {
                        $ref.=$this->request->data['ref_name'][$ref_count] . ',,' . $this->request->data['ref'][$ref_count];
                    } else {
                        $ref.=",,,," . $this->request->data['ref_name'][$ref_count] . ',,' . $this->request->data['ref'][$ref_count];
                    }
                }
            } else {
                $ref = '';
            }
            $question_id = $this->request->data['art_id'][0];
            $image_status = false;
            if ($this->request->params['form']['image_title']['size'][0] != 0) {
                $date = date("Y-m-d") . '_' . rand(0, 999);
                $name = $date . $this->request->params['form']['image_title']['name'][0];
                $tmp = $this->request->params['form']['image_title']['tmp_name'][0];
                if (!file_exists(WWW_ROOT . 'question' . DS . $question_id)) {
                    mkdir(WWW_ROOT . 'question' . DS . $question_id, 0777, true);
                }
                $filePath = WWW_ROOT . 'question' . DS . $question_id . DS . $name;
                if (move_uploaded_file($tmp, $filePath)) {
                    $article_image = $name;
                    $image_status = true;
                }
            } else {
                
            }
            if (!empty($answers[0]['Question']['title_update'])) {
                $explode_title = explode(',,,', $answers[0]['Question']['title_update']);
                $last = end($explode_title);
                $titles_check = end(unserialize(base64_decode($last)));
            } else {
                $titles_check = $answers[0]['Question']['title'];
            }
            if (str_replace("&nbsp;", "", $titles_check) != $this->request->data['art_title'][0]) {
                $array_new = array($userid, $date, $this->request->data['art_title'][0]);
                $array_new = base64_encode(serialize($array_new));
                if ($image_status) {
                    $update = $this->Question->updateAll(
                            array('Question.image' => "'" . $article_image . "'", 'Question.title_update' => "'" . $array_new . "'", 'Question.reference' => "'" . $ref . "'"), array('Question.id' => $this->request->data['art_id'][0]));
                } else {
                    $update = $this->Question->updateAll(
                            array('Question.title_update' => "'" . $array_new . "'", 'Question.reference' => "'" . $ref . "'"), array('Question.id' => $this->request->data['art_id'][0]));
                }
            } else {
                if ($image_status) {
                    $update = $this->Question->updateAll(array('Question.image' => "'" . $article_image . "'", 'Question.reference' => "'" . $ref . "'"), array('Question.id' => $this->request->data['art_id'][0]));
                } else {
                    $update = $this->Question->updateAll(array('Question.reference' => "'" . $ref . "'"), array('Question.id' => $this->request->data['art_id'][0]));
                }
            }
            $i = 0;
            foreach ($answers as $answer) {
                if (!empty($answer['Answer']['title_update'])) {
                    $explode_title = explode(',,,', $answer['Answer']['title_update']);
                    for ($t = 0; $t < count($explode_title); $t++) {
                        $title = unserialize(base64_decode($explode_title[$t]));
                        if (str_replace("&nbsp;", "", $title[2]) != $this->request->data['title'][$i]) {
                            $body = $this->request->data['title'][$i];
                            $array_new = array($userid, $date, $body);
                            $array_new = base64_encode(serialize($array_new));
                            $array_new = $answer['Answer']['title_update'] . ",,," . $array_new;
                            $update = $this->Answer->updateAll(array('Answer.title_update' => "'" . $array_new . "'"), array('Answer.question_id' => $this->request->data['art_id'][0], 'Answer.id' => $this->request->data['id'][$i]));
                        }
                    }
                } else {
                    if ($this->request->data['title'][$i] != $answer['Answer']['title']) {
                        $array_new = array($userid, $date, $this->request->data['title'][$i]);
                        $array_new = base64_encode(serialize($array_new));
                        $update = $this->Answer->updateAll(array('Answer.title_update' => "'" . $array_new . "'"), array('Answer.question_id' => $this->request->data['art_id'][0], 'Answer.id' => $this->request->data['id'][$i]));
                    }
                }
                if (!empty($answer['Answer']['body_update'])) {
                    $explode_body = explode(',,,', $answer['Answer']['body_update']);
                    for ($b = 0; $b < count($explode_body); $b++) {
                        $body = unserialize(base64_decode($explode_body[$b]));
                        if (str_replace("&nbsp;", "", $body[2]) != $this->request->data['body'][$i]) {
                            $body = $this->request->data['body'][$i];
                            $array_new = array($userid, $date, $body);
                            $array_new = base64_encode(serialize($array_new));
                            $array_new = $answer['Answer']['body_update'] . ",,," . $array_new;
                            $update = $this->Answer->updateAll(array('Answer.body_update' => "'" . $array_new . "'"), array('Answer.question_id' => $this->request->data['art_id'][0], 'Answer.id' => $this->request->data['id'][$i]));
                        }
                    }
                } else {
                    if ($this->request->data['body'][$i] != $answer['Answer']['body']) {
                        $array_new = array($userid, $date, $this->request->data['body'][$i]);
                        $array_new = base64_encode(serialize($array_new));
                        $update = $this->Answer->updateAll(array('Answer.body_update' => "'" . $array_new . "'"), array('Answer.question_id' => $this->request->data['art_id'][0], 'Answer.id' => $this->request->data['id'][$i]));
                    }
                }
                $i++;
            }
            $i = 0;
            if (isset($this->request->data['body_new']) && !empty($this->request->data['body_new'])) {
                $j = 0;
                if (!empty($this->request->data['body_new'])) {
                    foreach ($this->request->data['body_new'] as $points) {
                        $question_id = $this->request->data['art_id'][0];
                        if (!empty($points)) {
                            if (isset($this->request->params['form']['image_new']['name'][$j])) {
                                if ($this->request->params['form']['image_new']['size'][$j] != 0) {
                                    $date = date("Y-m-d") . '_' . rand(0, 999);
                                    $name = $date . $this->request->params['form']['image_new']['name'][$j];
                                    $tmp = $this->request->params['form']['image_new']['tmp_name'][$j];
                                    if (!file_exists(WWW_ROOT . 'question' . DS . $question_id)) {
                                        mkdir(WWW_ROOT . 'question' . DS . $question_id, 0777, true);
                                    }
                                    $filePath = WWW_ROOT . 'question' . DS . $question_id . DS . $name;
                                    if (move_uploaded_file($tmp, $filePath)) {
                                        $answer_new[$i]['image'] = $name;
                                    }
                                }
                            }

                            $answer_new[$i]['Answer']['title'] = $this->request->data['title_new'][$i];
                            $answer_new[$i]['Answer']['body'] = $this->request->data['body_new'][$i];
                            echo $answer_new[$i]['Answer']['sort'] = 15;
                            $answer_new[$i]['Answer']['question_id'] = $question_id;
                            if ($user_session) {
                                $userid = $user_session['User']['id'];
                            } else {
                                $userid = 0;
                                $answer_new[$i]['Answer']['user_ip'] = $this->get_client_ip_server();
                            }
                            $answer_new[$i]['Answer']['user_id'] = $userid;
                            $i++;
                        }
                        $j++;
                    }
                    if (!empty($answer_new)) {
                        if ($this->Answer->saveAll($answer_new)) {
                            
                        }
                    }
                }

                $this->redirect(array('controller' => 'articles', 'action' => 'view', $answers[0]['Question']['slug']));
            } else {

                $this->redirect(array('controller' => 'articles', 'action' => 'view', $answers[0]['Question']['slug']));
            }
        }
    }

    public function publish($question_slug) {
        $this->loadModel('QuestionCategory');
        $this->loadModel('Answer');
        $answers = $this->Answer->find('all', array(
            'fields' => array('Question.user_id,Question.title_update,Answer.title_update,Answer.body_update,Question.title_update', 'Answer.image', 'Answer.title', 'Question.id,Question.title,Question.slug,Answer.id,Answer.body,User.username,User.id'),
            'conditions' => array('Question.slug' => $question_slug),
        ));

        $category = $this->QuestionCategory->find('list', array(
            'fields' => 'QuestionCategory.question_id,QuestionCategory.category_id',
            'conditions' => array('QuestionCategory.question_id' => $answers[0]['Question']['id'])));

        $categories_all = $this->QuestionCategory->find('all', array(
            'fields' => array('Question.id,Question.title,Question.slug,Question.user_id'),
            'conditions' => array('QuestionCategory.category_id' => $category,
                'NOT' => array('QuestionCategory.question_id' => $answers[0]['Question']['id']))));

        $this->loadModel('User');
        $user = $this->User->find('first', array('fields' => array('User.id,User.username'), 'conditions' => array('User.id' => $answers[0]['Question']['user_id'])));
        if (!empty($user)) {
            $this->set('question_write', $user);
        } else {
            $this->set('question_write', NULL);
        }
        $this->set('categories', $categories_all);
        $this->set('answers', $answers);
    }

    public function filter() {
        $title_article = $_POST['questitle'];
        $this->request->onlyAllow('ajax');
        $this->layout = $this->autoRender = false;
        $title_article = strip_tags($title_article);
        $title_article = html_entity_decode($title_article);
        $title_article = preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', $title_article);
        $title_article = preg_replace('/&nbsp;/', ' ', $title_article);
        return $title_article;
    }

    public function drag($question_slug) {
        $this->loadModel('Log');
        $this->loadModel('QuestionCategory');
        $this->loadModel('Question');
        $this->loadModel('Answer');
        $answers = $this->Answer->find('all', array(
            'fields' => array('Answer.last_title,Answer.sort,Question.last,Question.views,Question.title_update,Answer.title_update,Question.title_update', 'Answer.title', 'Question.id,Question.title,Question.slug,Answer.id'),
            'order' => array('Answer.sort ASC'), 'conditions' => array('Question.slug' => $question_slug),
        ));
        if ($answers) {
            $this->set('answers', $answers);
        } else {
            $this->redirect('/');
        }
        if ($this->request->is('post')) {

            for ($sort_num = 0; $sort_num < count($this->request->data['sort_value']); $sort_num++) {
                $update = $this->Answer->updateAll(
                        array('Answer.sort' => $sort_num + 1), array('Answer.id' => $this->request->data['id'][$sort_num]));
            }
            $this->redirect(array('controller' => 'articles', 'action' => 'view', $answers[0]['Question']['slug']));
        }
    }

    public function add($requested = false) {
        $this->loadModel('Block');

        $test_ip = $this->get_client_ip_server();
        $block = $this->Block->find('first', array('fields' => array('Block.user_ip'), 'conditions' => array('Block.user_ip LIKE ' => "%$test_ip%")));
        if (!empty($block)) {
            $this->redirect('/');
        }
        if ($block['Block']['user_ip'] == $test_ip) {
            $this->redirect('/');
        }
        if ($requested == TRUE) {
            $this->loadModel('Request');
            $request = $this->Request->find('first', array('fields' => array('Request.id', 'Request.title'), 'conditions' => array('Request.id' => $requested)));
            $request_check = 1;
            $this->set('request', $request);
            $this->set('request_check', $request_check);
        } else {
            $request_check = 0;
            $this->set('request_check', $request_check);
            $request = '';
            $this->set('request', $request);
        }
        $this->loadModel('Tag');
        $this->loadModel('Category');
        $this->loadModel('Question');
        $this->loadModel('QuestionCategory');
        $this->loadModel('Answer');
        $tag_check = false;
        $user = $this->Session->read('user_quesli');

        if ($this->request->is('post')) {
            if ($_POST['as_clear'] == 1) {

                if (isset($user) && !empty($user)) {
                    $notification = 'Congrats , you gained new points! go to dashboard to check your new rank';
                } else {
                    $msg = "<br> New Annonymus article is added  :" . $this->request->data['Question']['title'];
                    $to = "heshamkadry.hk@gmail.com";
                    $subject = "Article added";
                    mail($to, $subject, $msg);
                    $notification = 'Thank you for your contribution, register now to join our community of Speeliests';
                }
                $this->Session->setFlash($notification, 'default', array(), 'speeli_notification');
                if ($this->request->data['Question']['request_check'] == 1) {
                    $update = $this->Request->updateAll(
                            array('Request.checked' => 2), array('Request.id' => $this->request->data['Question']['request_id']));
                }
                $ref = '';
                if (isset($this->request->data['ref']) && !empty($this->request->data['ref'][0])) {

                    for ($ref_count = 0; $ref_count < count($this->request->data['ref']); $ref_count++) {
                        if ($ref_count == 0) {
                            $ref.=$this->request->data['ref_name'][$ref_count] . ',,' . $this->request->data['ref'][$ref_count];
                        } else {
                            $ref.=",,,," . $this->request->data['ref_name'][$ref_count] . ',,' . $this->request->data['ref'][$ref_count];
                        }
                    }
                } else {
                    $ref = '';
                }
                $this->request->data['Question']['reference'] = $ref;
                $title_article = preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', $this->request->data['Question']['title']);
                $title_article = preg_replace('/&nbsp;/', ' ', $title_article);
                $title_article = strip_tags($title_article);
                /* check for references */
                $this->request->data['Question']['reference'] = $ref;
                $ref = preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', $this->request->data['Question']['reference']);
                $ref = preg_replace('/&nbsp;/', ' ', $ref);
                $ref = strip_tags($ref);
                $ref = htmlspecialchars(trim($ref));
                $this->request->data['Question']['reference'] = $ref;
                /* end references */
                //$seperate = $this->Question->spilt_title($this->request->data['Question']['title']);
//   $this->request->data['Question']['body']=  implode(',,,', $this->request->data['body']);
                $this->request->data['Question']['title'] = htmlspecialchars(trim($title_article));
                $slug = preg_replace("/[^A-Za-z0-9 ]/", '', $title_article);
                $this->request->data['Question']['slug'] = strip_tags(trim(implode("-", $this->Question->remove_Space($slug))));
                if ($user) {
                    $userid = $user['User']['id'];
                    $this->request->data['Question']['user_ip'] = $this->get_client_ip_server();
                } else {
                    $this->request->data['Question']['user_ip'] = $this->get_client_ip_server();
                    $userid = 0;
                }
                $date = date("Y-m-d") . '_' . rand(0, 999);
                $last_title_article = array($userid, $date, $this->request->data['Question']['title']);
                $this->request->data['Question']['title_update'] = base64_encode(serialize($last_title_article));

                $this->request->data['Question']['user_id'] = (int) $userid;
                if ($this->Question->save($this->request->data)) {
                    $question_id = $this->Question->getLastInsertId();
                    $this->loadModel('Log');
                    $question_id = $this->Question->getLastInsertId();
                    $log['Log']['user_id'] = $userid;
                    $log['Log']['question'] = $this->request->data['Question']['title'];
                    $log['Log']['question_slug'] = $this->request->data['Question']['slug'];
                    $log['Log']['user_ip'] = $this->request->data['Question']['user_ip'];
                    $log['Log']['question_id'] = $question_id;
                    $log['Log']['answer_id'] = 0;
                    $log['Log']['title'] = 'New Question Added';
                    $this->Log->Create();
                    $this->Log->save($log);
                    $first_tags = 0;
                    $both_tags = 0;
                    /* Save old category question end */
                    $b = 0;
                    /* Save new category question */
                    if (isset($this->request->data['new']) && !empty($this->request->data['new'])) {
                        $both_tags = (count($this->request->data['new']) + $first_tags);
                        for ($i = $first_tags; $i < $both_tags; $i++) {
                            $category[$i]['QuestionCategory']['category_id'] = $this->request->data['new'][$b];
                            $category[$i]['QuestionCategory']['question_id'] = $question_id;
                            $category[$i]['QuestionCategory']['user_id'] = $userid;
                            $b++;
                        }
                        if ($this->QuestionCategory->saveAll($category)) {
                            
                        }
                    }
                    /* Save new category question end */
                    $this->Tag->create();
                    $tag = FALSE;
                    $both_tags = 0;
                    $b = 0;
                    /* Save new category with tags */

                    /* Save new category with tags end */
                    $j = 0;
                    if ($user) {
                        $userip = 0;
                        $userid = $user['User']['id'];
                    } else {
                        $userip = $this->get_client_ip_server();
                        $userid = 0;
                    }
                    if ($this->request->params['form']['image_title']['size'][0] != 0) {
                        if (preg_match("/image/", $this->request->params['form']['image_title']['type'][0])) {
                            $date = date("Y-m-d") . '_' . rand(0, 999);
                            $name = $date . $this->request->params['form']['image_title']['name'][0];
                            $tmp = $this->request->params['form']['image_title']['tmp_name'][0];
                            if (!file_exists(WWW_ROOT . 'question' . DS . $question_id)) {
                                mkdir(WWW_ROOT . 'question' . DS . $question_id, 0777, true);
                            }
                            $filePath = WWW_ROOT . 'question' . DS . $question_id . DS . $name;
                            if (move_uploaded_file($tmp, $filePath)) {
                                $article_image = $name;
                            }
                            $update = $this->Question->updateAll(
                                    array('Question.image' => "'" . $article_image . "'"), array('Question.id' => $question_id));
                        }
                    }

                    foreach ($this->request->data['body'] as $points) {
                        if ($this->request->params['form']['image']['size'][$j] != 0) {
                            if (preg_match("/image/", $this->request->params['form']['image']['type'][$j])) {
                                $date = date("Y-m-d") . '_' . rand(0, 999);
                                $name = $date . $this->request->params['form']['image']['name'][$j];
                                $tmp = $this->request->params['form']['image']['tmp_name'][$j];
                                if (!file_exists(WWW_ROOT . 'question' . DS . $question_id)) {
                                    mkdir(WWW_ROOT . 'question' . DS . $question_id, 0777, true);
                                }
                                $filePath = WWW_ROOT . 'question' . DS . $question_id . DS . $name;
                                if (move_uploaded_file($tmp, $filePath)) {
                                    $answer[$j]['image'] = $name;
                                }
                            }
                        }
                        $answer[$j]['title_update'] = array($userid, $date, $this->request->data['title'][$j]);
                        $answer[$j]['title_update'] = base64_encode(serialize($answer[$j]['title_update']));
                        //
                        $answer[$j]['body_update'] = array($userid, $date, $points);
                        $answer[$j]['body_update'] = base64_encode(serialize($answer[$j]['body_update']));

                        $answer[$j]['title'] = $this->request->data['title'][$j];
                        $answer[$j]['body'] = $points;
                        $answer[$j]['question_id'] = $question_id;
                        $answer[$j]['user_id'] = $userip;
                        $answer[$j]['user_id'] = $userid;
                        $j++;
                    }

                    if ($this->Answer->saveAll($answer)) {

                        $this->redirect(array('controller' => 'articles', 'action' => 'view', $this->request->data['Question']['slug']));
                    }
                }
            }
        }
    }

    public function delete_article() {
        $this->request->onlyAllow('ajax');
        $this->layout = $this->autoRender = false;
        $this->loadModel('Question');
        $this->loadModel('Log');
        $this->loadModel('QuestionCategory');
        $this->loadModel('Answer');
        if (isset($_POST['target'])) {
            if ($_POST['target'] == 'a') {
                if ($this->Question->deleteAll(array('Question.id' => $_POST['ques_art']))) {
                    if ($this->Answer->deleteAll(array('Answer.question_id' => $_POST['ques_art']))) {
                        if ($this->QuestionCategory->deleteAll(array('QuestionCategory.question_id' => $_POST['ques_art']))) {
                            if ($this->Log->deleteAll(array('Log.question_id' => $_POST['ques_art']))) {
                                $this->redirect('/');
                            }
                        }
                    }
                }
            } else {
                echo $_POST['ques_art'];
                if ($this->Answer->deleteAll(array('Answer.id' => $_POST['ques_art']))) {
                    $this->redirect('/');
                }
            }
        }
    }

    public function delete_image() {
        $this->request->onlyAllow('ajax');
        $this->layout = $this->autoRender = false;
        $this->loadModel('Question');
        $this->loadModel('Log');
        $this->loadModel('Answer');
        if (isset($_POST['target'])) {
            if ($_POST['target'] == 'a') {
                if ($this->Question->updateAll(array('Question.image_user' => 1), array('Question.id' => $_POST['ques_art']))) {
                    
                }
            } else {
                if ($this->Answer->updateAll(array('Answer.image_user' => 1), array('Answer.id' => $_POST['ques_art']))) {
                    
                }
            }
        }
    }

    public function update_status() {
        $this->request->onlyAllow('ajax');
        $this->layout = $this->autoRender = false;
        $this->loadModel('Question');
        $this->loadModel('Answer');
        if (isset($_POST['target'])) {
            if ($_POST['target'] == 'a') {
                if ($this->Question->updateAll(array('Question.status' => $_POST['status']), array('Question.id' => $_POST['ques_art']))) {
                    
                }
            } else {
                if ($this->Answer->updateAll(array('Answer.status' => $_POST['status']), array('Answer.id' => $_POST['ques_art']))) {
                    
                }
            }
        }
    }
	 public function main_photo() {
        $this->request->onlyAllow('ajax');
        $this->layout = $this->autoRender = false;
        $this->loadModel('Question');

        if (isset($_POST['photo_id'])) {

            $return = $this->Question->updateAll(array('Question.image' => "'" . $_POST['photo_id'] . "'"), array('Question.id' => $_POST['article_id']));
            if ($return == true) {
                return true;
            } else {
                return false;
            }
        }
    }
	public function blank() {
        $this->request->onlyAllow('ajax');
        $this->layout = $this->autoRender = false;
        $this->loadModel('Question');

        if (isset($_POST['article_id'])) {
            $return = $this->Question->updateAll(array('Question.image' =>NULL), array('Question.id' => $_POST['article_id']));
            if ($return == true) {
                return true;
            } else {
                return false;
            }
        }
    }
	 public function share_count() {
        $this->request->onlyAllow('ajax');
        $this->layout = $this->autoRender = false;

        $this->loadModel('Question');
        if (isset($_POST['question_id'])) {
            $update = $this->Question->updateAll(array('Question.share_count '=>'Question.share_count+1'), array('Question.id' => $_POST['question_id']));
        }
    }
	
	

}

?>