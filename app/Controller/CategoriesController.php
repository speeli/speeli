<?php

class CategoriesController extends AppController {

    public $name = 'Categories';
    public $components = array('Paginator', 'RequestHandler', 'Email', 'Session');
    public $helpers = array('Js', 'Paginator', 'Html', 'Form', 'Session');

    /**
     * This controller does not use a model
     *
     * @var array
     */
    public $uses = array();

    public function beforeFilter() {
        parent::beforeFilter();
        // Allow users to register and logout.
    }

    public function index() {
        
    }
    public function udpate_cat_views (){
         $this->layout = $this->autoRender = false;
        $this->layout = false;
        
        $this->loadModel('MainCategory');
        $this->MainCategory->updateAll(array('MainCategory.views'=>'MainCategory.views+1'),array('MainCategory.slug'=>$_POST['data']));
    }
    public function add_new_cat() {
        $this->layout = $this->autoRender = false;
        $this->layout = false;
        $this->loadModel('Category');
        $this->request->onlyAllow('ajax');
        if (isset($_POST['add_new_cat']) && !empty($_POST['add_new_cat'])) {
            $check_first = $this->Category->find('first', array('fields' => array('Category.id', 'Category.name'),
                'recursive' => -1,
                'conditions' => array('Category.name LIKE' => trim($_POST['add_new_val']))));
            if (isset($check_first) && !empty($check_first['Category']['name'])) {
                $update = $this->Category->updateAll(
                        array('Category.parent_id' => $check_first['Category']['id']), array('Category.id' => $_POST['cat_id']));
                return $category_id = $check_first['Category']['id'];
            } else {
                $data['Category']['type'] = '1';
                $data['Category']['name'] = trim($_POST['add_new_cat']);
                $data['Category']['slug'] = strip_tags(trim($_POST['add_new_cat']));
                $data['Category']['slug'] = str_replace(' ', '-', $data['Category']['slug']);
                print_r($data);
                $this->Category->Create();
                $this->Category->save($data);
                $category_id = $this->Category->getLastInsertId();
                $update = $this->Category->updateAll(
                        array('Category.parent_id' => $category_id), array('Category.id' => $_POST['cat_id']));
                return $category_id;
            }
        }
    }

  

    public function summary($flag) {
        $this->loadModel('Question');
        $array = array(1 => 'Answers-Summary', 2 => 'Video-Summary', 3 => 'Book-Summary',
            4 => 'Study-Summary', 5 => 'Episode-Summary', 6 => 'Event-Summary');
        $summary = array_search($flag, $array);
        
        
        $this->paginate = array(
            'fields' => array('MainCategory.name,MainCategory.slug,Question.id,Question.views,Question.image,Question.last,Question.title,Question.slug'),
            'limit' => 50,
            'conditions' => array('Question.category_id >'=>0,'Question.status' => 1, 'Question.summary_id' => $summary),
            'group' => 'Question.id',
            'order' => array('Question.id DESC', 'Question.views DESC'));
        $categories_all = $this->paginate('Question');
        debug($categories_all);
        $this->set('categories_all', $categories_all);

        $explode = explode('-', $flag);
        $update_title = "Speeli " . $explode[0] . " Summary";
        $this->set('update_title', $update_title);
        $this->set('questions', $categories_all);
    }

    public function category($flag) {
        $this->loadModel('MainCategory');
        $this->loadModel('Question');
        $c_id=$this->MainCategory->find('first',array('conditions'=>array('MainCategory.slug'=>$flag)));
        $this->Question->unBindModel(array(hasMany => array('QuestionCategory')));
        $this->paginate = array(
            'fields' => array('Summary.name,Summary.icons,Summary.slug,Question.id,Question.views,Question.image,Question.last,Question.title,Question.slug'),
            'limit' => 25,
            'recursive'=>0,
            'conditions' => array('Question.status' => 1, 'Question.category_id' => $c_id['MainCategory']['id']),
            'group' => 'Question.id',
            'order' => array('Question.id DESC', 'Question.views DESC'));
        
        $categories_all = $this->paginate('Question');
      
        $this->set('categories_all', $categories_all);
        $this->set('c_id',$c_id);
        $this->set('questions', $categories_all);
    }

    public function add_new_cat_parent() {
        $this->layout = $this->autoRender = false;
        $this->layout = false;
        $this->loadModel('Category');
        $this->loadModel('Question');
        $this->request->onlyAllow('ajax');
        if (isset($_POST['add_new_cat']) && !empty($_POST['add_new_cat'])) {
            $check_first = $this->Category->find('first', array('fields' => array('Category.id', 'Category.name'),
                'recursive' => -1,
                'conditions' => array('Category.type' => 1, 'Category.name LIKE' => trim($_POST['add_new_cat']))));
            if (isset($check_first) && !empty($check_first['Category']['name'])) {
                $update = $this->Question->updateAll(
                        array('Question.category_id' => $check_first['Category']['id']), array('Question.id' => $_POST['cat_id']));
                return $category_id = $check_first['Category']['id'];
            } else {
                $data['Category']['type'] = '1';
                $data['Category']['name'] = trim($_POST['add_new_cat']);
                $data['Category']['slug'] = strip_tags(trim($_POST['add_new_cat']));
                $data['Category']['slug'] = str_replace(' ', '-', $data['Category']['slug']);
                print_r($data);
                $this->Category->Create();
                $this->Category->save($data);
                $category_id = $this->Category->getLastInsertId();
                $update = $this->Question->updateAll(
                        array('Question.category_id' => $category_id), array('Question.id' => $_POST['cat_id']));

                return $category_id;
            }
        }
    }

    public function update_cat() {
        $this->layout = $this->autoRender = false;
        $this->layout = false;
        $this->loadModel('Question');
        $this->request->onlyAllow('ajax');
        if (isset($_POST['add_new_cat']) && !empty($_POST['add_new_cat'])) {

            $update = $this->Question->updateAll(
                    array('Question.category_id' => $_POST['add_new_cat']), array('Question.id' => $_POST['cat_id']));
        }
    }

    public function search_parent() {
        Configure::write('debug', 2);

        $this->loadModel('Question');
        $this->loadModel('Category');
        $this->autoRender = false;
        $this->request->onlyAllow('ajax');
        $this->layout = $this->autoRender = false;
        if (isset($_POST['data_send']) && !empty($_POST['data_send'])) {
            $data = $_POST['data_send'];
            $seperates = $this->Question->spilt_title($_POST['data_send']);
            $seperate_coma = '';
            foreach ($seperates as $seperate) {
                $seperate_coma.="(+" . $seperate . "*) ";
            }
            $this->loadModel('Category');
            $tag = $this->Category->find('all', array(
                'fields' => array('Category.id', 'Category.name'),
                'conditions' => array('Category.type' => 1, "Category.name LIKE" => "%" . $data . "%"),
                'recursive' => -1,
                'limit' => 10)
            );
            if (!empty($tag)) {
                $this->set('cat_id', $_POST['cat_id']);
                $this->set('categories', $tag);
            }
            $this->set('cat_id', $_POST['cat_id']);
        }
        $this->render('search_parent');
    }

    public function search_category() {
        $this->loadModel('Question');
        $this->loadModel('MainCategory');
        $this->autoRender = false;
        $this->request->onlyAllow('ajax');
        $this->layout = $this->autoRender = false;
        if (isset($_POST['data_send']) && !empty($_POST['data_send'])) {
            $data = $_POST['data_send'];
            $seperates = $this->Question->spilt_title($_POST['data_send']);
            $seperate_coma = '';
            foreach ($seperates as $seperate) {
                $seperate_coma.="(+" . $seperate . "*) ";
            }
            $this->loadModel('MainCategory');
            $tag = $this->MainCategory->find('all', array(
                'fields' => array('MainCategory.id', 'MainCategory.name'),
                'conditions' => array("MainCategory.name LIKE" => "%" . $data . "%"),
                'recursive' => -1,
                'limit' => 10)
            );
            if (!empty($tag)) {
                $this->set('cat_id', $_POST['cat_id']);
                $this->set('categories', $tag);
            }
            $this->set('cat_id', $_POST['cat_id']);
        }
        $this->render('search_category');
    }

    public function all($type = Null) {
        $this->loadModel('QuestionCategory');
        $this->loadModel('Category');
        $user = $this->Session->read('user_quesli');
        if ($user['User']['admin'] == 1) {


            $this->paginate = array(
                'group' => array('Category.name'),
                'fields' => array('DISTINCT Category.name ,Category.id,Category.slug,Category.created,Category.views'),
                'limit' => 15);
            $categories = $this->paginate('Category');
            $i = 0;
            foreach ($categories as $category) {

                $categories[$i]['Category']['count'] = $this->QuestionCategory->find('all', array(
                    'conditions' => array('QuestionCategory.category_id' => $category['Category']['id']),
                    'fields' => array('COUNT(Question.id) as q_count,SUM(Question.views) as q_views')));
                $i++;
            }

            $this->set('categories', $categories);
        } else {
            $this->redirect('/');
        }
        /* if ($user['User']['admin'] == 1) {
          $this->paginate = array(
          'fields' => array('DISTINCT (Category.name),Category.views,Category.image,Category.id,Category.slug,Category.created'),
          'conditions' => array('Category.type' => 0),
          'limit' => 15);
          $this->set('categories', $this->paginate('Category'));
          }
          if($type){
          if($type==0){
          $this->paginate = array(
          'fields' => array('DISTINCT (Category.name),Category.views,Category.image,Category.id,Category.slug,Category.created'),
          'conditions' => array('Category.type' => 0,'Category.parent_id ='=> 0),
          'limit' => 15);
          }else{
          $this->paginate = array(
          'fields' => array('DISTINCT (Category.name),Category.views,Category.image,Category.id,Category.slug,Category.created'),
          'conditions' => array('Category.type' => 0,'Category.parent_id <>'=>0),
          'limit' => 5);
          }

          $this->set('categories', $this->paginate('Category'));
          } */
    }

    public function updates() {
        $this->loadModel('QuestionCategory');
        if (isset($_POST['target']) && is_numeric($_POST['target'])) {
            if ($_POST['target'] == 0) {
                $this->loadModel('QuestionCategory');
                $this->paginate = array('fields' => array('DISTINCT(QuestionCategory.question_id),Question.views,Question.status,Question.last,Question.title,Question.slug,Question.title_update,Question.image'),
                    'group' => 'QuestionCategory.question_id', 'limit' => 50, 'order' => 'Question.views DESC');
                $this->set('questions', $this->paginate('QuestionCategory'));
                $update_title = 'Top Speeli articles';
                $this->set('update_title', $update_title);
            } elseif ($_POST['target'] == 1) {
                $this->paginate = array(
                    'fields' => array('DISTINCT(QuestionCategory.question_id),Question.views,Question.status,Question.title,Question.slug,Question.last,Question.title_update,Question.image'),
                    'group' => 'QuestionCategory.question_id', 'limit' => 50,
                    'order' => 'Question.id DESC');
                $this->set('questions', $this->paginate('QuestionCategory'));
                $update_title = 'Latest Speeli articles';
                $this->set('update_title', $update_title);
            } elseif ($_POST['target'] == 2) {
                $this->paginate = array(
                    'fields' => array('DISTINCT(QuestionCategory.question_id),Category.id,Category.views,Question.views', 'Category.name,Question.title,Question.last,Question.slug,COUNT(QuestionCategory.category_id) AS cat'),
                    'group' => 'QuestionCategory.question_id',
                    'order' => 'cat DESC');
                $this->set('questions', $this->paginate('QuestionCategory'));
                $update_title = 'Trending Speeli topics';
                $this->set('update_title', $update_title);
            } elseif ($_POST['target'] == 3) {
                $this->paginate = array(
                    'fields' => array('DISTINCT(QuestionCategory.question_id),Category.id,Category.views,Question.views', 'Category.name,Question.last,Question.title,Question.slug,Question.title_update,'),
                    'group' => 'QuestionCategory.question_id', 'order' => 'Category.views DESC');
                $this->set('questions', $this->paginate('QuestionCategory'));
                $update_title = 'Most viewed topics';
                $this->set('update_title', $update_title);
            } else {
                
            }
        }
    }

    public function search() {
        $this->loadModel('Category');
        $this->loadModel('Question');
        $this->autoRender = false;
        $this->request->onlyAllow('ajax');
        $this->layout = $this->autoRender = false;
        if (isset($_POST['questitle']) && !empty($_POST['questitle'])) {
            $seperates = $this->Question->spilt_title($_POST['questitle']);
            $seperate_coma = '';
            foreach ($seperates as $seperate) {
                $conditions['or'][] = array('Category.name LIKE' => "%$seperate%");
            }
            $questions = $this->Category->find('all', array(
                'fields' => array('DISTINCT(Category.name),Category.slug,Category.views'),
                'conditions' => $conditions,
                'limit' => 15));

            $this->set('questions', $questions);
        }
        $this->render('search');
    }

    public function topics() {
		echo Configure::write('debug',0);
        $this->loadModel('QuestionCategory');
        $this->loadModel('Category');
        $categories_all = $this->QuestionCategory->find('all', array(
            'fields' => array('DISTINCT (QuestionCategory.question_id),Question.views,Category.views', 'Category.name,Question.title,Question.slug'),
            'limit' => 50,
            'group' => 'QuestionCategory.question_id',
            'order' => 'Question.views DESC'));
        $update_title = 'Top Speeli articles';
        
        
        $this->set('update_title', $update_title);
        $this->set('questions', $categories_all);
    }

    public function view($category_slug) {
        $this->loadModel('QuestionCategory');
        $this->loadModel('Category');
        $this->paginate = array('fields' => array('DISTINCT(Question.id),'
                . 'Question.last,Question.title_update,Question.status,Question.views,Question.created,Question.title,Question.slug,Question.image'),
            'limit' => 50, 'order' => 'Question.id DESC',
            'conditions' => array('OR' => array('Category.slug' => $category_slug)));
        $this->set('categories', $this->paginate('QuestionCategory'));

        $unslug = str_replace('-', ' ', $category_slug);
        $this->set('category_name', $unslug);
        $update = $this->Category->updateAll(
                array('Category.views' => 'views +1'), array('Category.slug' => $category_slug));
    }

    public function view_test($category_slug) {
        $this->loadModel('QuestionCategory');
        $this->loadModel('Category');
        $categories_all = $this->QuestionCategory->find('all', array(
            'fields' => array('Category.views', 'Category.name,Question.title,Question.slug'),
            'conditions' => array('Category.slug' => $category_slug)));
        $countur = (int) $categories_all[0]['Category']['views'] + 1;
        debug($countur);
        $update = $this->Category->updateAll(
                array('Category.views' => $countur), array('Category.slug' => $category_slug));
        $this->set('categories', $categories_all);
    }

    public function add() {
        $this->loadModel('Tag');
        $this->loadModel('Category');
        $this->loadModel('Question');
        $this->loadModel('QuestionCategory');
        $this->loadModel('Answer');
        $tag_check = false;
        $user = $this->Session->read('user_quesli');
        if ($this->request->is('post')) {

            $seperate = $this->Question->spilt_title($this->request->data['Question']['title']);
            //   $this->request->data['Question']['body']=  implode(',,,', $this->request->data['body']);
            $this->request->data['Question']['slug'] = implode("-", $this->Question->remove_Space($this->request->data['Question']['title']));
            $this->request->data['Question']['user_id'] = $user['User']['id'];
            if ($this->Question->save($this->request->data)) {
                $question_id = $this->Question->getLastInsertId();
                for ($i = 0; $i < count($this->request->data['tags']); $i++) {
                    $category[$i]['QuestionCategory']['category_id'] = $this->request->data['tags'][$i];
                    $category[$i]['QuestionCategory']['question_id'] = $question_id;
                    $category[$i]['QuestionCategory']['user_id'] = $user['User']['id'];
                }
                if ($this->QuestionCategory->saveAll($category)) {
                    
                }
                $this->Tag->create();
                $tag = FALSE;
                for ($i = 0; $i < count($this->request->data['tags']); $i++) {
                    $tags = $this->Tag->find('first', array(
                        'fields' => array('Tag.category', 'Tag.body'),
                        'conditions' => array('Tag.category' => $this->request->data['tags'][$i])));
                    if (!empty($tags)) {
                        $explode = explode(" ", $tags['Tag']['body']);
                        $tags_body_new = array_unique(array_merge($explode, $seperate));
                        $tags_body_new = implode(" ", $tags_body_new);
                        $tags_body_new = trim($tags_body_new);
                        $tag_update = $this->Tag->updateAll(array('Tag.body' => "'" . $tags_body_new . "'"), array('Tag.category' => $tags['Tag']['category']));
                    } else {
                        $tag_new[$i]['Tag']['category'] = $this->request->data['tags'][$i];
                        $tag_new[$i]['Tag']['body'] = implode(" ", $seperate);
                        $tag_check = true;
                    }
                }
                if ($tag_check) {
                    if ($this->Tag->saveAll($tag_new)) {
                        
                    }
                }
                $j = 0;
                foreach ($this->request->data['body'] as $points) {
                    if ($this->request->params['form']['image']['size'][$j] != 0) {
                        $date = date("Y-m-d") . '_' . rand(0, 999);
                        $name = $date . $this->request->params['form']['image']['name'][$j];
                        $tmp = $this->request->params['form']['image']['tmp_name'][$j];
                        if (!file_exists(WWW_ROOT . 'question' . DS . $question_id)) {
                            mkdir(WWW_ROOT . 'question' . DS . $question_id, 0777, true);
                        }
                        $filePath = WWW_ROOT . 'question' . DS . $question_id . DS . $name;
                        if (move_uploaded_file($tmp, $filePath)) {
                            $answer[$j]['image'] = $name;
                        }
                    }
                    $answer[$j]['title'] = $this->request->data['title'][$j];
                    $answer[$j]['body'] = $points;
                    $answer[$j]['question_id'] = $question_id;
                    $answer[$j]['user_id'] = $user['User']['id'];
                    $j++;
                }

                $this->Answer->saveAll($answer);
            }
        }
    }

    function get_all_categories() {
        Configure::write('debug', 0);
        $this->layout = $this->autoRender = false;
        $subcategories = $this->Question->getTags();
        $submenus['submenus'] = $subcategories;
        echo json_encode($submenus);
    }

    public function edit($id) {
        if ($id) {
            $user = $this->Session->read('user_quesli');
            if ($user['User']['admin'] == 1) {
                $this->loadModel('Category');
                $categories = $this->Category->findById($id);
                if ($this->request->is('post') || $this->request->is('put')) {
                    $this->Category->id = $id;

                    if ($this->data['Category']['image']['size'] != 0) {
                        $name = $this->data['Category']['image']['name'];
                        $tmp = $this->data['Category']['image']['tmp_name'];
                        if (!file_exists(WWW_ROOT . 'categories' . DS . date('n'))) {
                            mkdir(WWW_ROOT . 'categories' . DS . date('n'), 0777, true);
                        }
                        $filePath = WWW_ROOT . DS . 'categories' . DS . date('n') . DS . $name;
                        if (move_uploaded_file($tmp, $filePath)) {
                            $this->request->data['Category']['image'] = $name;
                        }
                    } else {
                        $this->request->data['Category']['image'] = $categories['Category']['image'];
                    }

                    if ($this->Category->save($this->request->data)) {
                        return $this->redirect(array('controller' => 'categories', 'action' => 'all'
                        ));
                    }
                }
                if (!$this->request->data) {
                    $this->request->data = $categories;
                }
            }
        }
    }

    public function search_summary() {
        echo Configure::write('debug', 2);

        $this->loadModel('Question');
        $this->loadModel('Summary');
        $this->autoRender = false;
        $this->request->onlyAllow('ajax');
        $this->layout = $this->autoRender = false;
        if (isset($_POST['data_send']) && !empty($_POST['data_send'])) {
            $data = $_POST['data_send'];
            $seperates = $this->Question->spilt_title($_POST['data_send']);
            $seperate_coma = '';
            foreach ($seperates as $seperate) {
                $seperate_coma.="(+" . $seperate . "*) ";
            }
            $this->loadModel('Summary');
            $tag = $this->Summary->find('all', array(
                'fields' => array('Summary.id', 'Summary.name'),
                'conditions' => array("Summary.name LIKE" => "%" . $data . "%"),
                'recursive' => -1,
                'limit' => 6)
            );
            if (!empty($tag)) {
                $this->set('cat_id', $_POST['cat_id']);
                $this->set('summaries', $tag);
            }
            $this->set('cat_id', $_POST['cat_id']);
        }
        $this->render('search_summary');
    }

    public function update_sum() {
        $this->layout = $this->autoRender = false;
        $this->layout = false;
        $this->loadModel('Question');
        $this->request->onlyAllow('ajax');
        if (isset($_POST['add_new_cat']) && !empty($_POST['add_new_cat'])) {
            echo $_POST['add_new_cat'];
            echo $_POST['cat_id'];
            $update = $this->Question->updateAll(
                    array('Question.summary_id' => $_POST['add_new_cat']), array('Question.id' => $_POST['cat_id']));
        }
    }

    public function delete() {
        $this->request->onlyAllow('ajax');
        $this->layout = $this->autoRender = false;
        $this->loadModel('Category');
        $this->loadModel('QuestionCategory');
        $user = $this->Session->read('user_quesli');
        if ($user['User']['admin'] == 1) {
            if (isset($_POST['target'])) {
                if ($_POST['target']) {
                    if ($this->Category->deleteAll(array('Category.id' => $_POST['target']))) {
                        if ($this->QuestionCategory->deleteAll(array('QuestionCategory.category_id' => $_POST['target']))) {
                            
                        }
                    }
                }
            }
        } else {
            return $this->redirect(array(
                        'action' => '/'
            ));
        }
    }

}

?>