<?php

class HomesController extends AppController {

    public $name = 'Homes';
    public $components = array('Paginator', 'RequestHandler', 'Email', 'Session');
    public $helpers = array('Js', 'Paginator', 'Html', 'Form', 'Session');

    /**
     * This controller does not use a model
     *
     * @var array
     */
    public $uses = array();

    public function beforeFilter() {
        parent::beforeFilter();
        // Allow users to register and logout.
    }
    public function about() {
        
            $this->loadModel('QuestionCategory');
            $this->loadModel('Answer');
            $this->loadModel('Question');
            $answers = $this->Answer->find('all', array(
                'fields' => array('Answer.sort,Question.selected,Question.views,Question.image,Question.status,Question.reference,Question.user_id,Question.title_update,Answer.title_update,Answer.body_update,Question.title_update', 'Answer.image', 'Answer.title', 'Question.id,Question.title,Question.slug,Answer.id,Answer.body,User.username,User.id'),
                'order' => array('Answer.sort ASC'),
                'conditions' => array('Question.id' => 123),
            ));
            if ($answers) {
                $countur = (int) $answers[0]['Question']['views'] + 1;
                $update = $this->Question->updateAll(
                        array('Question.views' => $countur), array('Question.id' => $answers[0]['Question']['id']));
                $tags = $this->QuestionCategory->find('all', array(
                    'fields' => 'Category.id,Category.slug,Category.name',
                    'conditions' => array('QuestionCategory.question_id' => $answers[0]['Question']['id'])));
               
                $this->set('categories', $categories_all);
                $this->set('answers', $answers);
                $this->set('tags', $tags);
            }
        
    }
    public function index($article=false) {
      $this->loadModel('Selected');
      $selecteds=$this->Selected->find('all',array('fields'=>array('Selected.slug,Selected.title,Selected.image,Selected.question_id')));
      $this->set('selecteds',$selecteds);
    }

    public function contact() {
        
      $this->layout=false;
      
       if ($this->request->is('post')) {
            $msg='First name: '. $$this->request->data['first']. "<br> Email: " .$this->request->data['email']. "<br> Message :" . $this->request->data['msg'];
            
          mail("support@speeli.com","Speeli ",$msg);
             $this->redirect('/');
        }
        
        
    }
     public function search() {
        $this->loadModel('Question');
        $this->autoRender = false;
        $this->request->onlyAllow('ajax');
        $this->layout = $this->autoRender = false;
        if (isset($_POST['questitle']) && !empty($_POST['questitle'])) {
            $seperates = $this->Question->spilt_title($_POST['questitle']);
            $seperate_coma = '';
            foreach ($seperates as $seperate) {
                $seperate_coma.="(+" . $seperate . "*) ";
            }
            $this->loadModel('Category');
            $this->loadModel('Question');
            
            $tag = array();
            $questions = $this->Question->find('all', array(
                'fields' => array('Question.title_update,Question.id,Question.title ,Question.slug'),
                'conditions' => array('OR' => array('MATCH(Question.title)  AGAINST(" ' . $seperate_coma . ' " IN BOOLEAN MODE)')),
                'limit' => 6));
             $this->set('questions', $questions);
        }
        $this->render('search');
    }
    
}

?>