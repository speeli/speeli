<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

    public $components = array(
        'Session', 'Cookie');
    public $helpers = array('Html' => array('className' => 'MyHtml'));

    public function beforeFilter() {
        App::import('Vendor', 'facebook-php-sdk-master/src/facebook');
        if (isset($_SERVER) && isset($_SERVER['SERVER_NAME'])) {
            //use live facebook config
            $this->Facebook = new Facebook(array(
                'appId' => '835557989856782',
                'secret' => '105f5d52f045b0e806f1360e14a1141b',
                'fileUpload' => true
            ));
        } else {
            //in development mode use local app local app
            $this->Facebook = new Facebook(array(
                'appId' => '220578118121406',
                'secret' => '2188fc10183358a1b6f6ec11d66f9b11',
                'fileUpload' => true
            ));
        }
        $messages = $this->check_message();
        $this->set('message_count', $messages);
       $this->Cookie->name = 'user_speeli';
        $this->Cookie->time = '2 weeks';  // or '7 day'
        $this->Cookie->key = 'qSI232qs*&sXOw!adre@34SAv!@*(XSL#$%)asGb$@11~_+!@#HKis~#^';
        $this->Cookie->type('aes');
        if ($this->Cookie->read('user_speeli')) {
            $user = $this->Cookie->read('user_speeli');
            $this->Session->write('user_quesli', $user);
        }
    }

    public function beforeRender() {
        parent::beforeRender();
        $this->set('fb_login_url', $this->Facebook->getLoginUrl(array('redirect_uri' => Router::url(array('controller' => 'users', 'action' => 'login'), true))));
    }

    function get_client_ip_server() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if (getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if (getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if (getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if (getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if (getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    /* function get_client_ip_server() {
      $client = @$_SERVER['HTTP_CLIENT_IP'];
      $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
      $remote = $_SERVER['REMOTE_ADDR'];
      $ip = '';
      if (filter_var($client, FILTER_VALIDATE_IP)) {
      $ip = $client;
      } elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
      $ip = $forward;
      } else {
      $ip = $remote;
      }

      return $ip;
      } */

    function check_message() {
        $this->loadModel('Message');
        $user = $this->Session->read('user_quesli');
        if (!empty($user)) {
            $messages = $this->Message->find('count', array(
                'conditions' => array('Message.user_recieve' => $user['User']['id'], 'Message.seen' => 0)
            ));
            return $messages;
        }
    }

}
