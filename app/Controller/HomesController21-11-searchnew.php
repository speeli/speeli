﻿<?php

class HomesController extends AppController {

    public $name = 'Homes';
    public $components = array('Paginator', 'RequestHandler', 'Email', 'Session');
    public $helpers = array('Js', 'Paginator', 'Html', 'Form', 'Session');

    /**
     * This controller does not use a model
     *
     * @var array
     */
    public $uses = array();

    public function beforeFilter() {
        parent::beforeFilter();
        // Allow users to register and logout.
    }
    public function dash_admin() {
        $user = $this->Session->read('user_quesli');
        if ($user['User']['admin'] == 1) {
            
        } else {
            $this->redirect('/');
        }
    }
public function directory(){
         $this->loadModel('Question');
        if ($id) {
            $category_id = array(15, 8, 13, 1, 5, 7, 11, 9); //15 8 13 1 5   7 11 9    
        } else {
            $category_id = array(15, 8, 13, 1, 5); //15 8 13 1 5   7 11 9    
        }
        for ($cid = 0; $cid < count($category_id); $cid++) {
            if ($cid == 0) {
                $seperate_coma = "Question.category_id = " . $category_id[$cid];
            } else {

                $seperate_coma.=" OR Question.category_id = " . $category_id[$cid];
            }
        }
        $grids = $this->Question->find('all', array('recursive' => 0,
            'fields' => array('SUM(Question.views) AS v', 'MainCategory.slug,MainCategory.id,MainCategory.name ,COUNT(MainCategory.id) AS co')
            , 'group' => 'MainCategory.id'));

        $this->set('grids', $grids);
    }
	public function feed_new() {
        $user = $this->Session->read('user_quesli');
        $this->loadModel('UserCategory');
        $this->loadModel('QuestionCategory');
        $this->loadModel('MainCategory'); //1  6 8 9 11
        $category_id = array(1, 6, 8, 9, 11, 16, 15);
        for ($cid = 0; $cid < count($category_id); $cid++) {
            if ($cid == 0) {
                $seperate_coma = "MainCategory.id = " . $category_id[$cid];
            } else {
                $seperate_coma.=" OR MainCategory.id = " . $category_id[$cid];
            }
        }
        $main_categories = $this->MainCategory->find('list', array('conditions' => array('OR' => array($seperate_coma)), 'fields' => 'MainCategory.name,MainCategory.slug'));

        $this->set('main_categories', $main_categories);
        if ($user) {
           

            $this->loadModel('Answer');
            $questions = $this->Answer->find('all', array(
                'contain' => array('Question'),
                'limit' => 15,
                'fields' => array('Question.image_user,Answer.image_user,Question.locked,Question.last',
                    'Answer.last_title,Answer.last_body,Answer.sort,Question.selected',
                    'Question.views,Question.image,Question.status,Question.reference,Question.user_id',
                    'Question.title_update,Answer.title_update,Answer.body_update,Question.title_update',
                    'Answer.image', 'Answer.title', 'Question.id,Question.title,Question.slug',
                    'Answer.id,Answer.body,User.username,User.id,User.image,User.gender'),
                'conditions' => array('Answer.sort' => 1, 'Question.status' => 1),
                'order' => 'Question.id DESC'));


            $this->set('questions', $questions);
        } else {
            $this->loadModel('Answer');
            $questions = $this->Answer->find('all', array(
                'contain' => array('Question'),
                'limit' => 15,
                'fields' => array('Question.image_user,Answer.image_user,Question.locked,Question.last',
                    'Answer.last_title,Answer.last_body,Answer.sort,Question.selected',
                    'Question.views,Question.image,Question.status,Question.reference,Question.user_id',
                    'Question.title_update,Answer.title_update,Answer.body_update,Question.title_update',
                    'Answer.image', 'Answer.title', 'Question.id,Question.title,Question.slug',
                    'Answer.id,Answer.body,User.username,User.id,User.image,User.gender'),
                'conditions' => array('Answer.sort' => 1, 'Question.status' => 1),
                'order' => 'Question.id DESC'));


            $this->set('questions', $questions);
            $this->paginate = array(
                'fields' => array('DISTINCT(QuestionCategory.question_id),Question.id,Question.views,Question.status,Question.title,Question.slug,Question.title_update,Question.image'),
                'limit' => 30, 'order' => 'Question.id DESC', 'conditions' => array('Question.expand' => 1));
            $this->set('expands', $this->paginate('QuestionCategory'));
        }
    }
    public function requested() {
        $user = $this->Session->read('user_quesli');
        if ($user['User']['admin'] == 1) {
        $this->loadModel('Request');
        $user = $this->Session->read('user_quesli');
        
        $this->paginate = array(
            'fields' => array('Request.id,Request.title,Request.created,Request.checked'),
            'limit' => 40,
            'order' => 'Request.id DESC');
        $questions = $this->Paginator->paginate('Request');
        $this->set('questions', $questions);
        }else{
            $this->redirect('/');
        }
    }

    public function request_check() {
        $this->request->onlyAllow('ajax');
        $this->layout = $this->autoRender = false;
        $this->loadModel('Request');
        if (isset($_POST['art_id'])) {
            if ($this->Request->updateAll(array('Request.checked' =>$_POST['art_status']), array('Request.id' => $_POST['art_id']))) {
                
            }
        }
    }
 public function summ() {
        $this->autoRender = false;
        $this->layout = $this->autoRender = false;
        //$seperates = array('Video');
	 $seperates = array('Why','Who' ,'What','How','When','Where');
        $seperate_coma = '';
        $seperate_exact = "";
        $seperate_false = "";

        $s = 0;
        foreach ($seperates as $seperate) {
            if ($s == 0) {
                $seperate_exact.="" . $seperate . " ";
                $seperate_coma.="Question.title LIKE '%$seperate%' ";
             
            } else {
                
                $seperate_coma.=" OR Question.title LIKE '%$seperate%' ";
                
            }
            $s++;
        }
        $this->loadModel('Question');
        $this->paginate = array(
            'recursive'=>-1,
            'fields' => array('Question.title_update,Question.id,Question.title ,Question.slug'),
            'conditions' => array('OR' => array($seperate_coma)),
            'limit' => 50);
       $paginate= $this->Question->find('list',array('fields'=>array('Question.id'),'limit'=>50,'conditions' => array('AND' => array('Question.summary_id'=>NULL,'OR'=>array($seperate_coma)))));
       debug($paginate);
       $this->Question->updateAll(array('Question.summary_id' => 1), array('Question.id' => $paginate));
        
        echo count($paginate);
        //page:17
        die();
    }
public function grid($id=false) {
        $this->loadModel('Question');
        if($id) {
            
        $category_id = array(15, 8,13,1, 5,7,11,9);//15 8 13 1 5   7 11 9    
        } else {
        $category_id = array(15, 8,13,1, 5);//15 8 13 1 5   7 11 9    
        }
        for($cid=0;$cid<count($category_id);$cid++) {
            if ($cid == 0) {
                $seperate_coma="Question.category_id = ".$category_id[$cid];
            } else {

                $seperate_coma.=" OR Question.category_id = ".$category_id[$cid];
            }
        }
        $grids = $this->Question->find('all', array('recursive' => 0,
            'fields' => array('SUM(Question.views) AS v', 'MainCategory.slug,MainCategory.id,MainCategory.name ,COUNT(MainCategory.id) AS co')
            , 'group' => 'MainCategory.id', 'conditions' => array('OR'=>array($seperate_coma))));
        
        $this->set('grids', $grids);
    }

     public function request_edit() {
        $this->request->onlyAllow('ajax');
        $this->layout = $this->autoRender = false;
        $this->loadModel('Request');
        if (isset($_POST['cat_id'])) {
            if ($this->Request->updateAll(array('Request.title' =>"'".$_POST['data_send']."'"), array('Request.id' => $_POST['cat_id']))) {
                
            }
        }
    }
     public function requested_article() {
        $this->loadModel('Request');
        $this->paginate = array(
            'fields' => array('Request.id,Request.title'),
            'limit' => 6,
            'conditions'=>array('Request.checked'=>1),
            'order' => 'Request.id DESC');
        $questions = $this->Paginator->paginate('Request');
        $this->set('questions', $questions);
    }
     public function requested_articles() {
        $this->loadModel('Request');
        $this->paginate = array(
            'fields' => array('Request.id,Request.title'),
            'limit' => 80,
            'conditions' => array('Request.checked' => 1),
            'order' => 'Request.id DESC');
        $questions = $this->Paginator->paginate('Request');
        $this->set('questions', $questions);
    }

    function ranking($points) {
        $ranking = '';
        $points+=50;

        if ($points < 60) {
            $image = 'first.jpg';
            $ranking = 'Level 1 - Baby Speelian';
            $remain = 120 - $points;

            $remain_dash = $points . ' / 120';
            $next = 'Level 2 - Potential Genius';
            $percent = (($remain * 100) / 120);
        } else if ($points >= 60 && $points <= 120) {
            $image = 'second.jpg';
            $next = 'Level 3 - Whizkid';
            $ranking = 'Level 2 - Potential Genius';
            $remain = 240 - $points;
            $percent = (($remain * 100) / 240);
            $remain_dash = $points . ' / 240';
        } else if ($points >= 120 && $points <= 240) {
            $image = 'third.jpg';
            $ranking = 'Level 3 - Whizkid';
            $next = 'Level 4 - keyboard Ninja';
            $remain = 500 - $points;
            $remain_dash = $points . ' / 500';
            $percent = (($remain * 100) / 500);
        } else if ($points >= 240 && $points <= 500) {

            $image = 'fourth.jpg';
            $next = 'Level 5 - Wizzard';
            $ranking = 'Level 4 - Keyboard Ninja';
            $remain = 2000 - $points;
            $percent = (($remain * 100) / 2000);
            $remain_dash = $points . ' / 2000';
        } else if ($points >= 500 && $points <= 2000) {
            $remain = 6000 - $points;
            $next = 'Level 6 - Chuck Norris';
            $remain_dash = $points . ' / 6000';
            $image = 'fifth.jpg';
            $ranking = 'Level 5 - Wizzard';
            $percent = (($remain * 100) / 6000);
        } else if ($points >= 2000 && $points <= 6000) {
            $image = 'six.jpg';
            $ranking = 'Level 6 - Chuck Norris';
            $next = 'Level 7 - Batman';
            $remain = 12000 - $points;
            $remain_dash = $points . ' / 12000';
            $percent = (($remain * 100) / 12000);
        } else if ($points >= 6000 && $points <= 12000) {
            $image = 'seven.jpg';
            $next = 'Level 8 - Extraterrestrial Being';
            $ranking = 'Level 7 - Batman';
            $remain = 20000 - $points;
            $remain_dash = $points . ' / 20000';
            $percent = (($remain * 100) / 20000);
        } else if ($points >= 12000 && $points <= 20000) {
            $image = 'eight.jpg';
            $next = 'Level 9 - Spiritual Entity';
            $ranking = 'Level 8 - Extraterrestrial Being';
            $remain = 40000 - $points;
            $remain_dash = $points . ' / 40000';
            $percent = (($remain * 100) / 40000);
        } else if ($points >= 20000 && $points <= 40000) {
            $remain = 100000 - $points;
            $next = 'Level 10 - Infinity';
            $image = 'nine.jpg';
            $remain_dash = $points . ' / 100000';
            $ranking = 'Level 9 - spiritual Entity';
            $percent = (($remain * 100) / 100000);
        } else if ($points >= 40000 && $points <= 100000) {
            $remain = 100000 - $points;
            $next = 'You are in the top level ';
            $image = 'ten.jpg';
            $remain_dash = $points . ' / 100000';
            $percent = (($remain * 100) / 100000);
            $ranking = 'Level 10- Infinity';
        } else if ($points >= 100000) {
            $remain = 0;
            $remain_dash = 'completed';
            $image = 'ten.jpg';
            $next = 'You are in the top level ';
        }



        $rankings = array('remain_dash' => $remain_dash, 'next' => $next, 'image' => $image, 'percent' => intval ($percent), 'rank' => $ranking, 'remain' => $remain);
        $ranking = $rankings;

        return $ranking;
    }
     public function edit_article() {
        $this->loadModel('Question');
        $this->paginate = array(
            'fields' => array('Question.id,Question.title_update,Question.last,Question.category_id,Question.links,Question.last,Question.edited,Question.quality,Question.expand', 'Question.views,Question.created,Question.image,Question.title_update,Question.title', 'Question.slug,Question.id,Question.status'),
            'limit' => 6,
            'conditions'=>array('Question.edited'=>0),
            'order' => 'Question.id DESC');
        $questions = $this->Paginator->paginate('Question');
        $this->set('questions', $questions);
    }
  public function edit_articles() {
        $this->loadModel('Question');
        $this->paginate = array(
            'fields' => array('Question.id,Question.title_update,Question.last,Question.category_id,Question.links,Question.last,Question.edited,Question.quality,Question.expand', 'Question.views,Question.created,Question.image,Question.title_update,Question.title', 'Question.slug,Question.id,Question.status'),
            'limit' => 60,
            'conditions' => array('Question.edited' => 0),
            'order' => 'Question.id DESC');
        $questions = $this->Paginator->paginate('Question');
        $this->set('questions', $questions);
    }
    public function expanded_article() {
        $this->loadModel('Question');
        $this->paginate = array(
            'fields' => array('Question.id,Question.title_update,Question.last,Question.category_id,Question.links,Question.last,Question.edited,Question.quality,Question.expand', 'Question.views,Question.created,Question.image,Question.title_update,Question.title', 'Question.slug,Question.id,Question.status'),
            'limit' => 6,
            'conditions'=>array('Question.expand'=>1),
            'order' => 'Question.id DESC');
        $questions = $this->Paginator->paginate('Question');
        $this->set('questions', $questions);
    }
    public function expanded_articles() {
        $this->loadModel('Question');
        $this->paginate = array(
            'fields' => array('Question.id,Question.title_update,Question.last,Question.category_id,Question.links,Question.last,Question.edited,Question.quality,Question.expand', 'Question.views,Question.created,Question.image,Question.title_update,Question.title', 'Question.slug,Question.id,Question.status'),
            'limit' => 50,
            'conditions' => array('Question.expand' => 1),
            'order' => 'Question.id DESC');
        $questions = $this->Paginator->paginate('Question');
        $this->set('questions', $questions);
    }
   public function dashboard() {
        $this->loadModel('Question');
        $this->loadModel('Log');
        $session = $this->Session->read('user_quesli');
        if ($session) {
            $points = 0;
            $count = $this->Question->find('count', array('conditions' => array('Question.user_id' => $session['User']['id'])));
            $points = $count * 30;
            $count_edit = $this->Log->find('all', array('order' => 'Log.question_id',
                'fields' => array('Log.question_id'),
                'conditions' => array('Log.user_id' => $session['User']['id'])));
            $count_edit = count($count_edit);
            echo $count_edit;
            $points+=$count_edit * 10;
            
            $this->paginate = array(
                'fields' => array('Question.id,Question.title_update,Question.last,Question.category_id,Question.links,Question.last,Question.edited,Question.quality,Question.expand', 'Question.views,Question.created,Question.image,Question.title_update,Question.title', 'Question.slug,Question.id,Question.status'),
                'limit' => 6,
                'order' => 'Question.id DESC');
            $questions = $this->Paginator->paginate('Question');
            $ranking = $this->ranking($points);
            $this->set('questions', $questions);

            $this->set(compact('points', 'ranking'));
        } else {
            $this->redirect('/');
        }
    }
public function logs_updates() {
        $this->loadModel('Log');
        if (isset($_POST['target']) && is_numeric($_POST['target'])) {
           if (isset($_POST['target']) && is_numeric($_POST['target'])&&($_POST['target']<5)) {
                $this->loadModel('QuestionCategory');
              $this->paginate = array('fields' => array('Log.user_ip,Log.title,Log.question,Log.question_slug,Log.answer_id,Log.question_id,Log.created,User.username'),
                'limit' => 100, 'order' => 'Log.id DESC', 'conditions' => array('Log.title' => $_POST['status_log_send']));
            $this->set('logs', $this->paginate('Log'));
            }
            elseif ($_POST['target'] == 5) {
                
                if($_POST['status_log_send']==="0"){
               $this->paginate = array('fields' => array('Log.user_ip,Log.title,Log.question,Log.question_slug,Log.answer_id,Log.question_id,Log.created,User.username'),
                'limit' => 100, 'order' => 'Log.id DESC', 'conditions' => array('Log.user_id'=>0));
                }else{
                    $this->paginate = array('fields' => array('Log.user_ip,Log.title,Log.question,Log.question_slug,Log.answer_id,Log.question_id,Log.created,User.username'),
                'limit' => 100, 'order' => 'Log.id DESC', 'conditions' => array('Log.title' => $_POST['status_log_send'],'Log.user_id'=>0));          
                }
                
           $this->set('logs', $this->paginate('Log'));
            }
            elseif ($_POST['target'] == 6) {
                
                if($_POST['status_log_send']==="0"){
               $this->paginate = array('fields' => array('Log.user_ip,Log.title,Log.question,Log.question_slug,Log.answer_id,Log.question_id,Log.created,User.username'),
                'limit' => 100, 'order' => 'Log.id DESC', 'conditions' => array('Log.user_id >'=>0));         
                }else{
                    $this->paginate = array('fields' => array('Log.user_ip,Log.title,Log.question,Log.question_slug,Log.answer_id,Log.question_id,Log.created,User.username'),
                'limit' => 100, 'order' => 'Log.id DESC', 'conditions' => array('Log.title' => $_POST['status_log_send'],'Log.user_id >'=>0));
                }
            $this->set('logs', $this->paginate('Log'));
            }
            else {
                
            }
        }
    }
   public function request() {
        
        $this->layout = false;
        $this->loadModel('Request');
        $user_session = $this->Session->read('user_quesli');
        if ($this->request->is('post')) {
            $this->request->data['Request']['user_ip']=$this->get_client_ip_server();
            if(!empty($user_session)){
            $this->request->data['Request']['user_id'] = $user_session['User']['id'];    
            }
            $this->Request->Create();
            $this->Request->save($this->request->data);
            $msg = "<br> Message :" . $this->request->data['Request']['title'];
            $to = "support@speeli.com";
            $subject = "Request";
           
                    mail($to, $subject, $msg);
            $this->redirect('/');
        }
        $this->set('session', $user_session);
    }
    public function logs_search() {
        $this->loadModel('Log');
        $this->loadModel('Question');
        $this->loadModel('User');
        $this->autoRender = false;
        $this->request->onlyAllow('ajax');
        $this->layout = $this->autoRender = false;
        if (isset($_POST['questitle']) && !empty($_POST['questitle'])) {
            if($_POST['log_type']==1){
                 $seperates = $this->Question->spilt_title($_POST['questitle']);
            $seperate_coma = '';
            foreach ($seperates as $seperate) {
                $conditions['or'][] = array('Log.question LIKE' => "%$seperate%");
            }
           
            $this->paginate = array('fields' => array('Log.user_ip,Log.title,Log.question,Log.question_slug,Log.answer_id,Log.question_id,Log.created,User.username'),
                'limit' => 15, 'order' => 'Log.id DESC', 'conditions' => $conditions);
            }elseif($_POST['log_type']==2){
                $seperates = $this->Question->spilt_title($_POST['questitle']);
            $seperate_coma = '';
            foreach ($seperates as $seperate) {
                $conditions['or'][] = array('User.username LIKE' => "%$seperate%");
            }
            $this->paginate = array('fields' => array('Log.user_ip,Log.title,Log.question,Log.question_slug,Log.answer_id,Log.question_id,Log.created,User.username'),
                'limit' => 15, 'order' => 'Log.id DESC', 'conditions' => $conditions); 
            }
            elseif($_POST['log_type']==3){
                  $seperates = $this->Question->spilt_title($_POST['questitle']);
            $seperate_coma = '';
            foreach ($seperates as $seperate) {
                $conditions['or'][] = array('Log.user_ip LIKE' => "%$seperate%");
            }
            $this->paginate = array('fields' => array('Log.user_ip,Log.title,Log.question,Log.question_slug,Log.answer_id,Log.question_id,Log.created,User.username'),
                'limit' => 15, 'order' => 'Log.id DESC', 'conditions' => $conditions); 
            }
           
           $logs= $this->paginate('Log');
           
            $this->set('logs', $this->paginate('Log'));
        }
        $this->render('logs_search');
    }
    public function logs() {
        $user = $this->Session->read('user_quesli');
        if ($user['User']['admin'] == 1) {
            $this->loadModel('Log');
            $this->paginate = array('fields' => array('User.id,Log.user_ip,Log.title,Log.question,Log.question_slug,Log.answer_id,Log.question_id,Log.created,User.username'),
                'limit' => 100, 'order' => 'Log.id DESC', 'conditions' => array('Log.status' => 0));
            $this->set('logs', $this->paginate('Log'));
        } else {
            $this->redirect('/');
        }
    }

    public function about() {
        $this->loadModel('QuestionCategory');
        $this->loadModel('Answer');
        $this->loadModel('Question');
        $answers = $this->Answer->find('all', array(
            'fields' => array('Answer.sort,Question.selected,Question.views,Question.image,Question.status,Question.reference,Question.user_id,Question.title_update,Answer.title_update,Answer.body_update,Question.title_update', 'Answer.image', 'Answer.title', 'Question.id,Question.title,Question.slug,Answer.id,Answer.body,User.username,User.id'),
            'order' => array('Answer.sort ASC'),
            'conditions' => array('Question.id' => 123),
        ));
        if ($answers) {
            $countur = (int) $answers[0]['Question']['views'] + 1;
            $update = $this->Question->updateAll(
                    array('Question.views' => $countur), array('Question.id' => $answers[0]['Question']['id']));
            $tags = $this->QuestionCategory->find('all', array(
                'fields' => 'Category.id,Category.slug,Category.name',
                'conditions' => array('QuestionCategory.question_id' => $answers[0]['Question']['id'])));

            $this->set('categories', $categories_all);
            $this->set('answers', $answers);
            $this->set('tags', $tags);
        }
    }
     public function feed($category=false) {
        $user = $this->Session->read('user_quesli');
        $this->loadModel('UserCategory');
        $this->loadModel('QuestionCategory');
        $this->loadModel('MainCategory'); //1  6 8 9 11
        $category_id = array(1, 6, 8, 9, 11, 16);
        for ($cid = 0; $cid < count($category_id); $cid++) {
            if ($cid == 0) {
                $seperate_coma = "MainCategory.id = " . $category_id[$cid];
            } else {
                $seperate_coma.=" OR MainCategory.id = " . $category_id[$cid];
            }
        }
        $category_filter=array(1=>'Technology',6=>'Science',8=>'Business',9=>'Politics',11=>'Games',15=>'Sports');
        
        $main_categories = $this->MainCategory->find('list', array('conditions' => array('OR' => array($seperate_coma)), 'fields' => 'MainCategory.name,MainCategory.slug'));
        
        $this->set('main_categories', $main_categories);
        if ($user) {
            /*
              $this->loadModel('UserCategory');
              $this->loadModel('QuestionCategory');
              $user_cat = $this->UserCategory->find('list', array('fields' => array('UserCategory.category_id'), 'conditions' => array('UserCategory.user_id' => $user['User']['id'])));
              if (!empty($user_cat)) {
              $this->paginate = array('fields' => array('DISTINCT(Question.id),Question.last,Question.title_update,Question.status,Question.title,Question.slug,Question.image'),
              'limit' => 50, 'order' => 'Question.id DESC', 'conditions' => array('OR' => array('QuestionCategory.category_id' => $user_cat)));
              $this->set('questions', $this->paginate('QuestionCategory'));
              } else {
              $this->paginate = array(
              'fields' => array('DISTINCT(QuestionCategory.question_id),Question.id,Question.views,Question.status,Question.title,Question.slug,Question.title_update,Question.image'),
              'group' => 'QuestionCategory.question_id', 'limit' => 50,
              'order' => 'Question.id DESC');
              $this->set('questions', $this->paginate('QuestionCategory'));
              } */

           
            
        } else {
           
        }
         $this->loadModel('Answer');
            if ($category) {
                $array=array('Question.category_id'=>$category_filter[$category],'Question.image <>' => '', 'Answer.sort' => 1, 'Question.status' => 1);
            }else{
                $array=array('Question.image <>' => '', 'Answer.sort' => 1, 'Question.status' => 1);
            }
            $this->paginate = array(
                'contain' => array('Question'),
                'limit' => 15,
                'fields' => array('Question.image_user,Answer.image_user,Question.locked,Question.last',
                    'Answer.last_title,Answer.last_body,Answer.sort,Question.selected',
                    'Question.views,Question.image,Question.status,Question.reference,Question.user_id',
                    'Question.title_update,Answer.title_update,Answer.body_update,Question.title_update',
                    'Answer.image', 'Answer.title', 'Question.id,Question.title,Question.slug',
                    'Answer.id,Answer.body,User.username,User.id,User.image,User.gender'),
                'conditions' => $array,
                'order' => 'Question.id DESC');

            $this->set('questions', $this->paginate('Answer'));
    }
/* oldfeed and update on 29-10-2016
   public function feed() {
        $user = $this->Session->read('user_quesli');
        $this->loadModel('UserCategory');
        $this->loadModel('QuestionCategory');
        if ($user) {
            
                $this->loadModel('UserCategory');
                $this->loadModel('QuestionCategory');
                $user_cat = $this->UserCategory->find('list', array('fields' => array('UserCategory.category_id'), 'conditions' => array('UserCategory.user_id' => $user['User']['id'])));
                if(!empty($user_cat)){
                $this->paginate = array('fields' => array('DISTINCT(Question.id),Question.last,Question.title_update,Question.status,Question.title,Question.slug,Question.image'),
                    'limit' => 50, 'order' => 'Question.id DESC', 'conditions' => array('OR' => array('QuestionCategory.category_id' => $user_cat)));
                $this->set('questions', $this->paginate('QuestionCategory'));
                   
                }else{
                   $this->paginate = array(
                'fields' => array('DISTINCT(QuestionCategory.question_id),Question.id,Question.views,Question.status,Question.title,Question.slug,Question.title_update,Question.image'),
                'group' => 'QuestionCategory.question_id', 'limit' => 50,
                'order' => 'Question.id DESC');
            $this->set('questions', $this->paginate('QuestionCategory'));  
                }
                 
                $this->paginate = array('fields' => array('DISTINCT(QuestionCategory.question_id),Question.id,Question.last,Question.title_update,Question.status,Question.title,Question.slug,Question.image'),
                    'limit' => 10, 'order' => 'Question.id DESC', 'conditions' => array('Question.expand' => 1, 'OR' => array('QuestionCategory.category_id' => $user_cat)));
                $this->set('expands', $this->paginate('QuestionCategory'));
                if(count($this->paginate('QuestionCategory'))<11){
                  $this->paginate = array('fields' => array('DISTINCT(QuestionCategory.question_id),Question.id,Question.last,Question.title_update,Question.status,Question.title,Question.slug,Question.image'),
                    'limit' => 10, 'order' => 'Question.id DESC', 'conditions' => array('Question.expand' => 1, 'NOT' => array('QuestionCategory.category_id' => $user_cat)));  
                $second_expand=$this->paginate('QuestionCategory');
                  $this->set('second_expand', $second_expand);
                }
            
        } else {
            
            $this->paginate = array(
                'fields' => array('DISTINCT(QuestionCategory.question_id),Question.id,Question.views,Question.status,Question.title,Question.slug,Question.title_update,Question.image'),
                'group' => 'QuestionCategory.question_id', 'limit' => 50,
                'conditions'=>array('Question.status'=>1),
                'order' => 'Question.id DESC');
            $this->set('questions', $this->paginate('QuestionCategory'));
            $this->paginate = array(
                'fields' => array('DISTINCT(QuestionCategory.question_id),Question.id,Question.views,Question.status,Question.title,Question.slug,Question.title_update,Question.image'),
                'limit' => 30, 'order' => 'Question.id DESC', 'conditions' => array('Question.expand' => 1));
            $this->set('expands', $this->paginate('QuestionCategory'));
        }
    }*/
    public function blocks() {
          $this->autoRender = false;
        $this->request->onlyAllow('ajax');
        $this->loadModel('Block');
        $this->layout = $this->autoRender = false;
        if(isset($_POST)){
            if(isset($_POST['user_ip'])&&!empty($_POST['user_ip'])){
                $block['Block']['user_ip'] = $_POST['user_ip'];
                $this->Block->Create();
                $this->Block->save($block);
            }
        }
    }
    
    public function index($article = false) {
        $this->loadModel('Question');
        $selecteds = $this->Question->find('all', array('fields' =>
            array('Question.slug,Question.title,Question.title_update,Question.image,Question.id'),
            'limit'=>6,'conditions'=>array('Question.selected'=>1)));
        $this->set('selecteds', $selecteds);
   $later = date("Y-m-d H:i:s", strtotime("-20 week"));
        $this->paginate = array(
            'fields' => array('Question.id,Question.category_id,Question.links,'
                . 'Question.last,Question.edited,Question.quality,Question.expand', 'Question.views,'
                . 'Question.created,Question.image,Question.title_update,Question.title',
                    'Question.slug,Question.id,Question.status','Summary.id','Summary.name'),
            'limit' => 6,
            'conditions' => array('Question.summary_id >'=>0,'Question.created >=' => $later),
            'order' => 'Question.views DESC');
        $latests = $this->Paginator->paginate('Question');
        $this->set('latests', $latests);
    }
    public function index1($article = false) {
        $this->loadModel('Selected');
        $selecteds = $this->Selected->find('all', array('fields' => array('Selected.slug,Selected.title,Selected.image,Selected.question_id')));
        $this->set('selecteds', $selecteds);
    }

    public function contact() {

        $this->layout = false;
        if ($this->request->is('post')) {
            $msg = 'First name: ' . $$this->request->data['first'] . "<br> Email: " . $this->request->data['email'] . "<br> Message :" . $this->request->data['msg'];
            mail("support@speeli.com", "Speeli ", $msg);
            $this->redirect('/');
        }
    }
public function search() {
        $this->loadModel('Question');
        $this->autoRender = false;
        $this->request->onlyAllow('ajax');
        $this->layout = $this->autoRender = false;
        $_POST['questitle'] = $_GET['questitle'];
        if (isset($_POST['questitle']) && !empty($_POST['questitle'])) {
            $seperates = $this->Question->spilt_title($_POST['questitle']);
            $seperate_coma = '';
            $seperate_exact = "";
            $seperate_false = "";
            if (isset($this->params->query['page'])) {
                $page = $this->params->query['page'];
            } else {
                $page = 1;
            }
            $s = 0;

            //first step to find exact match
 $this->paginate = array(
                'fields' => array('Question.title_update,Question.id,Question.title ,Question.slug'),
                'conditions' => array('Question.status' => 1, "Question.last LIKE " => $_POST['questitle']),
                'limit' => 10);
            $paginate = $this->paginate('Question');
            if (count($paginate) < 1) {
                $this->paginate = array(
                    'fields' => array('Question.title_update,Question.id,Question.title ,Question.slug'),
                    'conditions' => array('Question.status' => 1, "Question.last LIKE " => $_POST['questitle'] . "%"),
                    'limit' => 10);
                $paginate = $this->paginate('Question');
                if (count($paginate) < 1) {
                    $this->paginate = array(
                        'fields' => array('Question.title_update,Question.id,Question.title ,Question.slug'),
                        'conditions' => array('Question.status' => 1, "Question.last LIKE " => "%" . $_POST['questitle']),
                        'limit' => 10);
                    $paginate = $this->paginate('Question');
                    if (count($paginate) < 1) {
                        $this->paginate = array(
                            'fields' => array('Question.title_update,Question.id,Question.title ,Question.slug'),
                            'conditions' => array('Question.status' => 1, "Question.last LIKE " => "%" . $_POST['questitle'] . "%"),
                            'limit' => 10);
                        $paginate = $this->paginate('Question');
                        if (count($paginate) < 1) {
                            $this->paginate = array(
                                'fields' => array('Question.title_update,Question.id,Question.title ,Question.slug'),
                                'conditions' => array('Question.status' => 1, "Question.last LIKE " => "%" . $_POST['questitle'] . "%"),
                                'limit' => 10);
                            $paginate = $this->paginate('Question');
                        }
                    }
                }
            }
            if (count($paginate) < 1) {
                //second step to find with and 
                foreach ($seperates as $seperate) {
                    if ($s == 0) {
                        $seperate_exact.="" . $seperate . " ";
                        $seperate_coma.="Question.title LIKE '$seperate%' ";
                        $seperate_false.="Question.title LIKE '%$seperate%' ";
                         $seperate_OR.="Question.title LIKE '%$seperate%' ";
                    } else {
                        $seperate_false.=" AND Question.title LIKE '%$seperate%' ";
                        $seperate_OR.=" OR Question.title LIKE '%$seperate%' ";
                        $seperate_coma.=" AND Question.title LIKE '$seperate%' ";
                        $seperate_exact.="" . $seperate . " ";
                    }
                    $s++;
                }
                $this->loadModel('Category');
                $this->loadModel('Question');
                if (strlen(trim($_POST['questitle'])) <= 3) {
                    $this->paginate = array(
                        'fields' => array('Question.title_update,Question.id,Question.title ,Question.slug'),
                        'conditions' => array('Question.status' => 1, 'OR' => array($seperate_coma)),
                        'limit' => 10);
                    $paginate = $this->paginate('Question');
                } else {
                    
                }
                $this->paginate = array(
                    'fields' => array('Question.title_update,Question.id,Question.title ,Question.slug'),
                    'conditions' => array('Question.status' => 1, 'AND' => array($seperate_false)),
                    'limit' => 10);
                $paginate = $this->paginate('Question');
                if ($paginate) {
                    $paginate = $this->paginate('Question');
                    $count = count($paginate);
                    $count = 10 - $count;
                    if ($count > 0) {
                        $id = array();
                        foreach ($paginate as $question) {
                            if (count($paginate) == 1) {
                                $id = $question['Question']['id'];
                            } else {
                                array_push($id, $question['Question']['id']);
                            }
                        }
                        $this->paginate = array(
                            'fields' => array('Question.title_update,Question.id,Question.title ,Question.slug'),
                            'conditions' => array('Question.status' => 1, 'Question.id <>' => $id, 'OR' => array($seperate_OR)),
                            'limit' => $count);
                        $paginate1 = $this->paginate('Question');
                        $pageinate_all = array_merge($paginate, $paginate1);
                        $paginate = $pageinate_all;
                    }
                }
                // third step to find with or not and
                if (count($paginate) < 1) {
                    foreach ($seperates as $seperate) {
                        if ($s == 0) {
                            $seperate_exact.="" . $seperate . " ";
                            $seperate_coma.="Question.title LIKE '$seperate%' ";
                            $seperate_false.="Question.title LIKE '%$seperate%' ";
                        } else {
                            $seperate_false.=" OR Question.title LIKE '%$seperate%' ";
                            $seperate_coma.=" OR Question.title LIKE '$seperate%' ";
                            $seperate_exact.="" . $seperate . " ";
                        }
                        $s++;
                    }
                    $this->loadModel('Category');
                    $this->loadModel('Question');
                    if (strlen(trim($_POST['questitle'])) <= 3) {
                        $this->paginate = array(
                            'fields' => array('Question.title_update,Question.id,Question.title ,Question.slug'),
                            'conditions' => array('Question.status' => 1, 'OR' => array($seperate_coma)),
                            'limit' => 10);
                        $paginate = $this->paginate('Question');
                    } else {
                        
                    }
                    if ($paginate) {

                        $count = count($paginate);
                        $count = 10 - $count;
                        if ($count > 0) {
                            $id = array();
                            foreach ($paginate as $question) {
                                if (count($paginate) == 1) {
                                    $id = $question['Question']['id'];
                                } else {
                                    array_push($id, $question['Question']['id']);
                                }
                            }
                            $this->paginate = array(
                                'fields' => array('Question.title_update,Question.id,Question.title ,Question.slug'),
                                'conditions' => array('Question.status' => 1, 'Question.id <>' => $id, 'OR' => array($seperate_false)),
                                'limit' => $count);
                            $paginate1 = $this->paginate('Question');
                            $pageinate_all = array_merge($paginate1);
                            $paginate = $pageinate_all;
                        }
                    } else {
                        $this->paginate = array(
                            'fields' => array('Question.title_update,Question.id,Question.title ,Question.slug'),
                            'conditions' => array('Question.status' => 1, 'OR' => array($seperate_false)),
                            'limit' => 10);
                        $paginate = $this->paginate('Question');
                    }
                }
            } else {
                
            }

            $this->set('word', $_POST['questitle']);
            $this->set('questions', $paginate);
        } else {
            if ($this->RequestHandler->isAjax()) {
                
            }
        }
        $this->render('search');
    }
    
}

?>