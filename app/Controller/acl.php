
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:og="http://ogp.me/ns#">
    <head>
        <link rel="shortcut icon" href="<?php echo $this->webroot . 'img/ico.ico'; ?>" type="image/x-icon">
            <meta charset="UTF-8">
                <title>

                    <?php if (isset($page_title) && !empty($page_title)) { ?>
                        <?php echo $page_title; ?>
                        <?php
                    } else {
                        echo $setting['Setting']['system_name'];
                    }
                    ?>
                </title>

                <meta charset="UTF-8">
                    <meta name="viewport" content="width=device-width, initial-scale=1.0">
                        <?php
                        echo $this->Html->css(array('bootstrap.min', 'font-awesome.min',
                            'main'));
                        echo $this->Html->script(array('jquery-1.11.3.min', 'bootstrap.min', 'theme'));

                        echo $this->fetch('css');
                        echo $this->fetch('script');
                        ?>


                        </head>
                        <?php
                        $lang = $this->Session->read('Config.language');
                        $lang = ($lang == 'ar') ? 'text-align:right' : 'text-align:left';
                        ?>
                        <body class="home page page-id-13 page-template-default header-header-default 
                              header-fixed-on header-transparent chrome osx wpb-js-composer js-comp-ver-4.6.2 vc_responsive esg-blurlistenerexists" 
                              style="<?php echo $lang; ?>">
                            <div id="page" class="hfeed site"><a class="skip-link screen-reader-text" href="#content">Skip to content</a>
                                <header id="masthead" class="site-header fixed-on" role="banner">
                                    <div class="header-wrap">
                                        <div class="container">
                                            <div class="site-branding">
                                                <a href="<?php echo Router::url('/'); ?>" title="Construction Theme" rel="home"> 
                                                    <img src="http://demo.wpcharming.com/construction/wp-content/themes/construction/assets/images/logo_transparent.png" alt=""> </a>
                                            </div>
                                            <div class="header-right-wrap clearfix">
                                                <div class="header-widget">
                                                    <div class="header-right-widgets clearfix">
                                                        <div class="header-extract clearfix">
                                                            <div class="extract-element">
                                                                <div class="header-social"> <a target="_blank" href="http://twitter.com" title="Twitter"><i class="fa fa-twitter"></i></a> <a target="_blank" href="#" title="Linkedin"><i class="fa fa-linkedin-square"></i></a> <a target="_blank" href="#" title="Pinterest"><i class="fa fa-pinterest"></i></a> <a target="_blank" href="#" title="Google Plus"><i class="fa fa-google-plus"></i></a> <a href="mailto:name@email.com" title="Email"><i class="fa fa-envelope"></i></a></div>
                                                            </div>
                                                            <div class="extract-element"> <span class="header-text"></span>
                                                                <span class="phone-text primary-color"><a href="tel:1.800.123.4567"><?php echo $setting['Setting']['phone'] ?></a></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <nav id="site-navigation" class="main-navigation" role="navigation">
                                                    <div id="nav-toggle"><i class="fa fa-bars"></i></div>
                                                    <ul class="wpc-menu">
                                                        <li id="menu-item-37" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-27">
                                                            <a href="<?php echo Router::url('/'); ?>"><?php echo __('Home'); ?></a>
                                                        </li>


                                                        <li id="menu-item-28" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-28">
                                                            <a href="<?php echo Router::url('/about'); ?>"><?php echo __('About'); ?></a>
                                                        </li>
                                                        <li id="menu-item-32" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-32">
                                                            <div class="nav-toggle-subarrow"><i class="fa fa-angle-down"></i></div>
                                                            <a href="<?php echo Router::url('/people'); ?>"><?php echo __('People'); ?></a>

                                                        </li>
                                                        <li id="menu-item-377" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-377">

                                                            <a href="<?php echo Router::url('/projects'); ?>"><?php echo __('Projects'); ?></a>

                                                        </li>
                                                         <li id="menu-item-377" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-377">

                                                            <a href="<?php echo Router::url('/image'); ?>"><?php echo __('Images'); ?></a>

                                                        </li>
                                                        <li id="menu-item-274" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-274"><a href="<?php echo Router::url(array('controller' => 'homes', 'action' => 'lang', 'ar')); ?>"><?php echo __('Ar'); ?></a></li>
                                                        <li id="menu-item-275" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-275"><a href="<?php echo Router::url(array('controller' => 'homes', 'action' => 'lang', 'en')); ?>"><?php echo __('En'); ?></a></li>
                                                        <li id="menu-item-31" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-31"><a href="<?php echo Router::url('/articles'); ?>"><?php echo __('News'); ?></a></li>
                                                        <li id="menu-item-29" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-29"><a href="<?php echo Router::url('/contact'); ?>"><?php echo __('Contact Us'); ?></a></li>
                                                    </ul>
                                                </nav>
                                            </div>
                                        </div>
                                    </div>
                                </header>
                                <?php
                                echo $this->Html->css(array('owl.carousel'));
                                ?>

                                <?php
                                echo $this->Html->script(array('owl.carousel.min'));
                                ?>
                                <?php
                                echo $this->fetch('content');
                                ?>
                                <footer id="colophon" class="site-footer" role="contentinfo">
                                    <div class="footer-connect">
                                        <div class="container">
                                            <div class="footer-social">
                                                <label class="font-heading" for=""><?php echo __('Follow us'); ?></label> <a target="_blank" href="http://twitter.com" title="Twitter"><i class="fa fa-twitter"></i></a> <a target="_blank" href="#" title="Facebook"><i class="fa fa-facebook"></i></a> <a target="_blank" href="#" title="Linkedin"><i class="fa fa-linkedin-square"></i></a> <a target="_blank" href="#" title="Google Plus"><i class="fa fa-google-plus"></i></a> <a target="_blank" href="#" title="Instagram"><i class="fa fa-instagram"></i></a></div>
                                        </div>
                                    </div>
                                    <div class="container">
                                        <div class="footer-widgets-area">
                                            <div class="sidebar-footer footer-columns footer-4-columns clearfix">
                                                <div id="footer-1" class="footer-1 footer-column widget-area" role="complementary">
                                                    <aside id="text-4" class="widget widget_text">
                                                        <h3 class="widget-title"><?php echo $setting['Setting']['system_name']; ?></h3>
                                                        <div class="textwidget">
                                                            <?php
                                                            
                                                            $about=($lang == 'ar') ? $setting['Setting']['about_ar'] : $setting['Setting']['about'];
                                                            ?>
                                                            <?php echo (strlen($about) > 270) ? substr($about, 0, 270)  : $about; ?>
                                                        </div>
                                                    </aside>
                                                </div>
                                                <div id="footer-2" class="footer-2 footer-column widget-area" role="complementary">
                                                    <aside id="nav_menu-3" class="widget widget_nav_menu">
                                                        <h3 class="widget-title"><?php echo $setting['Setting']['system_name']; ?></h3>
                                                        <div class="menu-footer-widget-menu-container">
                                                            <ul id="menu-footer-widget-menu" class="menu">
                                                                <li id="menu-item-492" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-13 current_page_item menu-item-492"><a href="<?php echo Router::url('/'); ?>"><?php echo __('Home'); ?> </a></li>
                                                                <li id="menu-item-490" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-490"><a href="<?php echo Router::url('/about'); ?>"><?php echo __('About'); ?> </a></li>
                                                                <li id="menu-item-486" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-486"><a href="<?php echo Router::url('/articles'); ?>"><?php echo __('News'); ?> </a></li>
                                                                <li id="menu-item-487" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-487"><a href="#"><?php echo __('Services'); ?> </a></li>
                                                                <li id="menu-item-489" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-489"><a href="<?php echo Router::url('/projects'); ?>"><?php echo __('Projects'); ?> </a></li>
                                                                <li id="menu-item-485" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-485"><a href="<?php echo Router::url('/contact'); ?>"><?php echo __('Contact Us'); ?> </a></li>
                                                            </ul>
                                                        </div>
                                                    </aside>
                                                </div>
                                                <div id="footer-3" class="footer-3 footer-column widget-area" role="complementary">
                                                    <aside id="text-5" class="widget widget_text">
                                                        <h3 class="widget-title"><?php echo $setting['Setting']['system_name']; ?></h3>
                                                        <div class="textwidget">
                                                            <div class="contact-info-box">
                                                                <div class="contact-info-item">
                                                                    <div class="contact-text"><i class="fa fa-map-marker"></i></div>
                                                                    <div class="contact-value"><?php echo $setting['Setting']['address']; ?></div>
                                                                </div>
                                                                <div class="contact-info-item">
                                                                    <div class="contact-text"><i class="fa fa-phone"></i></div>
                                                                    <div class="contact-value"><?php echo $setting['Setting']['phone']; ?></div>
                                                                </div>
                                                                <div class="contact-info-item">
                                                                    <div class="contact-text"><i class="fa fa-envelope"></i></div>
                                                                    <div class="contact-value"><a href="<?php echo $setting['Setting']['email']; ?>"><?php echo $setting['Setting']['email']; ?></a></div>
                                                                </div>
                                                                <div class="contact-info-item">
                                                                    <div class="contact-text"><i class="fa fa-fax"></i></div>
                                                                    <div class="contact-value">FAX: <?php echo $setting['Setting']['fax']; ?></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </aside>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                    <div class="site-info-wrapper">
                                        <div class="container">
                                            <div class="site-info clearfix">

                                                <div class="footer-menu">
                                                    <div class="menu-footer-container">
                                                        <ul id="menu-footer" class="menu">
                                                            <li id="menu-item-501" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-501"><a href="#"><?php echo __('Services'); ?></a></li>
                                                            <li id="menu-item-502" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-502"><a href="<?php echo Router::url('/projects'); ?>"><?php echo __('Projects'); ?></a></li>
                                                            <li id="menu-item-500" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-500"><a href="<?php echo Router::url('/contact'); ?>"><?php echo __('Contact Us'); ?></a></li>

                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </footer>
                            </div>





                        </body>

                        </html>
