<?php

class ArticlesController extends AppController {

    public $name = 'Articles';
    public $components = array('Paginator', 'RequestHandler', 'Email', 'Session');
    public $helpers = array('Js', 'Paginator', 'Html', 'Form', 'Session');

    /**
     * This controller does not use a model
     *
     * @var array
     */
    public $uses = array();

    public function beforeFilter() {
        parent::beforeFilter();
        // Allow users to register and logout.
    }

    public function interest() {
        if ($_POST['target']) {
            $user = $this->Session->read('user_quesli');
            if ($user) {
                if ($_POST['target'] == 'interest') {
                    $this->loadModel('UserCategory');
                    $this->loadModel('QuestionCategory');
                    $user_cat = $this->UserCategory->find('list', array('fields' => array('UserCategory.category_id'), 'conditions' => array('UserCategory.user_id' => $user['User']['id'])));
                    $this->paginate = array('fields' => array('Question.status,Question.title,Question.slug,Question.title_update,Question.image'),
                        'limit' => 15, 'order' => 'Question.id DESC', 'conditions' => array('OR' => array('QuestionCategory.category_id' => $user_cat)));
                    $this->set('questions', $this->paginate('QuestionCategory'));
                } else {
                    $this->loadModel('UserCategory');
                    $this->loadModel('QuestionCategory');
                    $this->paginate = array('fields' => array('Question.id,Question.status,Question.title,Question.slug,Question.title_update,Question.image'),
                        'limit' => 30, 'order' => 'Question.id DESC');
                    $this->set('questions', $this->paginate('QuestionCategory'));
                }
            } else {
                $this->loadModel('UserCategory');
                $this->loadModel('QuestionCategory');
                $user_cat = $this->UserCategory->find('list', array('fields' => array('UserCategory.category_id'), 'conditions' => array('UserCategory.user_id' => $user['User']['id'])));
                debug($user_cat);
                $this->paginate = array('fields' => array('Question.status,Question.title,Question.slug,Question.title_update,Question.image'),
                    'limit' => 30, 'order' => 'Question.id DESC');
                $this->set('questions', $this->paginate('QuestionCategory'));
            }
        }
    }

    public function index() {
        $user = $this->Session->read('user_quesli');
        if ($user) {
            $this->loadModel('UserCategory');
            $this->loadModel('QuestionCategory');
            $user_cat = $this->UserCategory->find('list', array('fields' => array('UserCategory.category_id'), 'conditions' => array('UserCategory.user_id' => $user['User']['id'])));
            debug($user_cat);
            $this->paginate = array('fields' => array('Question.expand,Question.id,Question.status,Question.title,Question.slug,Question.title_update,Question.image'),
                'limit' => 20, 'order' => 'Question.id DESC', 'conditions' => array('OR' => array('QuestionCategory.category_id' => $user_cat)));
            $this->set('questions', $this->paginate('QuestionCategory'));
        } else {
            
        }

        $this->redirect('/');
    }

    public function select_home() {
        $this->request->onlyAllow('ajax');
        $this->layout = $this->autoRender = false;
        $this->loadModel('Selected');
        $this->loadModel('Question');
        if (isset($_POST['question_id'])) {
            if ($_POST['status'] == 1) {
                $data['Selected']['question_id'] = $_POST['question_id'];
                $data['Selected']['slug'] = $_POST['slug'];
                $data['Selected']['image'] = $_POST['article_image'];
                $data['Selected']['title'] = $_POST['title'];
                $this->Selected->Create();
                $this->Selected->save($data);
                $this->Question->updateAll(array('Question.selected' => 1), array('Question.id' => $_POST['question_id']));
            } else {
                if ($this->Selected->deleteAll(array('Selected.question_id' => $_POST['question_id']))) {
                    $this->Question->updateAll(array('Question.selected' => 0), array('Question.id' => $_POST['question_id']));
                }
            }
        }
    }

    function update() {
        $this->layout = $this->autoRender = false;
        $user = $this->Session->read('user_quesli');
        $this->loadmodel('Answer');
        if (!empty($_POST['quesuserd'])) {
            $this->Answer->create();
            $answer = $this->Answer->find('first', array(
                'fields' => array('Answer.title_update,Answer.image_update,Answer.body_update'),
                'conditions' => array('Answer.question_id' => $_POST['quesuserarticle_i'], 'Answer.id' => $_POST['quesuseri'])));
            if ($user) {
                $userid = $user['User']['id'];
            } else {
                $userid = $this->get_client_ip_server();
            }
            Debug($answer);
            $dt = new DateTime();
            $date = $dt->format('Y-m-d H:i:s');
            $body = $_POST['quesuserv'];
            $array_new = array($userid, $date, $body);
            $array_new = base64_encode(serialize($array_new));
            // make sure to encode the serialized object

            if ($_POST['quesuserd'] == 't') {
                $field = 'title_update';
                $array = $answer['Answer']['title_update'] . ",,," . $array_new;
            } elseif ($_POST['quesuserd'] == 'b') {
                $field = 'body_update';
                $array = $answer['Answer']['body_update'] . ",,," . $array_new;
            }
            $update = $this->Answer->updateAll(array('Answer.' . $field => "'" . $array . "'"), array('Answer.question_id' => $_POST['quesuserarticle_i'], 'Answer.id' => $_POST['quesuseri']));
        }
    }

    function update_article() {
        $this->layout = $this->autoRender = false;

        $user = $this->Session->read('user_quesli');
        $this->loadmodel('Answer');
        if (!empty($_POST['quesuserd'])) {

            $this->Question->create();
            $answer = $this->Question->find('first', array(
                'fields' => array('Question.title_update'),
                'conditions' => array('Question.id' => $_POST['quesuserarticle_i'])));
            if ($user) {
                $userid = $user['User']['id'];
            } else {
                $userid = $this->get_client_ip_server();
            }
            Debug($answer);
            $dt = new DateTime();
            $date = $dt->format('Y-m-d H:i:s');
            $body = $_POST['quesuserv'];
            $array_new = array($userid, $date, $body);
            $array_new = base64_encode(serialize($array_new));
            // make sure to encode the serialized object

            if ($_POST['quesuserd'] == 't') {
                $field = 'title_update';
                $array = $answer['Answer']['title_update'] . ",,," . $array_new;
            }
            $update = $this->Question->updateAll(array('Question.' . $field => "'" . $array . "'"), array('Question.id' => $_POST['quesuserarticle_i']));
        }
    }

    public function show_news() {

        $this->loadModel('Question');
        $questions = $this->Question->find('all', array('fields' => array('Question.expand', 'Question.title_update,Question.title', 'Question.slug,Question.id,Question.status'),
            'limit' => 300, 'order' => array('Question.id DESC')));
        if (!($this->request->is('post'))) {
            if (empty($answers[0]['Question']['last'])) {
                if (!empty($answers[0]['Question']['title_update'])) {
                    $explode_title = explode(',,,', $answers[0]['Question']['title_update']);
                    $last = end($explode_title);
                    $titles = end(unserialize(base64_decode($last)));
                } else {
                    $titles = $answers[0]['Question']['title'];
                }
                $update = $this->Question->updateAll(
                        array('Question.last' => "'" . $titles . "'"), array('Question.id' => $answers[0]['Question']['id']));
                foreach ($answers as $answer) {
                    if (empty($answer['Answer']['last_body'])) {
                        if (!empty($answer['Answer']['title_update'])) {
                            $explode_title = explode(',,,', $answer['Answer']['title_update']);
                            $last = end($explode_title);

                            $title = end(unserialize(base64_decode($last)));
                        } else {
                            $title = $answer['Answer']['title'];
                        }
                        if (!empty($answer['Answer']['body_update'])) {
                            $explode_body = explode(',,,', $answer['Answer']['body_update']);
                            $last = end($explode_body);
                            $body = end(unserialize(base64_decode($last)));
                        } else {
                            $body = $answer['Answer']['body'];
                        }
                        $update = $this->Answer->updateAll(
                                array('Answer.last_title' => "'" . $title . "'", 'Answer.last_body' => "'" . $body . "'"), array('Answer.id' => $answer['Answer']['id']));
                    }
                }
            }
        }
        $this->set('questions', $questions);
    }

    public function check_question() {
        $this->loadModel('Question');
        $this->autoRender = false;
        $this->request->onlyAllow('ajax');
        $this->layout = $this->autoRender = false;
        if (isset($_POST['questitle']) && !empty($_POST['questitle'])) {
            $seperates = $this->Question->spilt_title($_POST['questitle']);
            $seperate_coma = '';
            foreach ($seperates as $seperate) {
                $seperate_coma.="(+" . $seperate . "*) ";
            }
            $this->loadModel('Tag');
            $this->loadModel('Category');
            $this->loadModel('Question');

            $questions = $this->Question->find('all', array(
                'fields' => array('Question.title_update , Question.id,Question.title,  MATCH(Question.title)  AGAINST(" ' . $seperate_coma . ' " IN BOOLEAN MODE) AS releveance ,Question.slug'),
                'conditions' => array('OR' => array('MATCH(Question.title)  AGAINST(" ' . $seperate_coma . ' " IN BOOLEAN MODE)')),
                'order' => 'releveance DESC',
                'limit' => 6));

            if (!empty($questions)) {
                $this->set('questions', $questions);
            }
        }
        $this->render('check_question');
    }

    public function history($answer_id) {
        if ($this->request->onlyAllow('ajax') == false) {
            $this->redirect('/');
        }
        $this->loadModel('Answer');
        $this->layout = false;
        $this->layout = 'ajax';
        if (isset($answer_id)) {
            $history['Answer'] = $this->Answer->find('first', array('fields' => array('Answer.id,Answer.title_update,Answer.body_update')
                , 'conditions' => array('Answer.id' => $answer_id)));
            if (!empty($answer_id)) {
                $this->set('histories', $history['Answer']);
            }
        }
    }

    public function reverse() {
        $this->layout = $this->autoRender = false;
        $this->layout = false;
        $this->request->onlyAllow('ajax');
        
        if (isset($_POST['action'])) {
             $_POST['action'];
            $this->loadModel('Answer');
            $history = $this->Answer->find('first', array('fields' => array('Answer.id,Answer.title_update,Answer.body_update')
                , 'conditions' => array('Answer.id' => $_POST['speel_ans'])));
            if($_POST['speel_type']=="0"){
               echo "dsa";
                $explode = explode(',,,', $history['Answer']['title_update']);
            }else{
                
                $explode = explode(',,,', $history['Answer']['body_update']);
            }
                    echo count($explode)."dfd";
                    for ($i = 0; $i < count($explode); $i++) {
                        
                        if($i==(int)$_POST['action']){
                            
                            if($_POST['speel_type']=="0"){
                                 $array = unserialize(base64_decode($explode[$i]));
                                  $update = $this->Answer->updateAll(
                           array('Answer.last_title' => "'" . $array[2] . "'"),
                            array('Answer.id' => $history['Answer']['id']));
                            }else{
                            $array = unserialize(base64_decode($explode[$i]));
                                  $update = $this->Answer->updateAll(
                           array('Answer.last_body' => "'" . $array[2] . "'"),
                            array('Answer.id' => $history['Answer']['id']));
                            }
                             break;
                        }
                       
                    }
                
            
            
        }
    }

    public function add_new_cat() {
        $this->layout = $this->autoRender = false;
        $this->layout = false;
        $this->loadModel('Category');
        $this->request->onlyAllow('ajax');
        if (isset($_POST['add_new_cat']) && !empty($_POST['add_new_cat'])) {
            $check_first = $this->Category->find('first', array('fields' => array('Category.id', 'Category.name'), 'conditions' => array('Category.name' => $_POST['add_new_cat'])));
            if (isset($check_first) && !empty($check_first['Category']['name'])) {
                return $category_id = $check_first['Category']['id'];
            } else {
                $data['Category']['name'] = trim($_POST['add_new_cat']);
                $data['Category']['slug'] = strip_tags(trim($_POST['add_new_cat']));
                $data['Category']['slug'] = str_replace(' ', '-', $data['Category']['slug']);
                $this->loadModel('Category');
                $this->Category->Create();
                $this->Category->save($data);
                return $category_id = $this->Category->getLastInsertId();
            }
        }
    }

    public function add_new_cat_edit() {
        $this->layout = $this->autoRender = false;
        $this->loadModel('Category');
        $this->request->onlyAllow('ajax');
        if (isset($_POST['add_new_cat']) && !empty($_POST['add_new_cat'])) {
            $check_first = $this->Category->find('first', array('fields' => array('Category.id', 'Category.name'), 'conditions' => array('Category.name' => $_POST['add_new_cat'])));
            if (isset($check_first) && !empty($check_first['Category']['name'])) {
                $category_id = $check_first['Category']['id'];
            } else {
                $data['Category']['name'] = trim($_POST['add_new_cat']);
                $data['Category']['slug'] = strip_tags(trim($_POST['add_new_cat']));
                $data['Category']['slug'] = str_replace(' ', '-', $data['Category']['slug']);
                $this->loadModel('Category');
                $this->Category->Create();
                $this->Category->save($data);
                $category_id = $this->Category->getLastInsertId();
            }
            $this->loadModel('QuestionCategory');
            $data['QuestionCategory']['category_id'] = $category_id;
            $data['QuestionCategory']['question_id'] = $_POST['art_id'];
            $this->QuestionCategory->Create();
            $this->QuestionCategory->save($data);
            $response['cat_id'] = $category_id;
            $response['art_id'] = $_POST['art_id'];
            $response['cat_name'] = $data['Category']['namFe'];
            $response['cat_slug'] = strip_tags(trim($data['Category']['name']));
            $response['cat_slug'] = str_replace(' ', '-', $response['cat_slug']);
            $this->set('response', $response);
            $this->render('add_new_cat_edit');
        }
    }

    public function search_cat() {
        $this->loadModel('Question');
        $this->loadModel('Category');
        $this->autoRender = false;
        $this->request->onlyAllow('ajax');
        $this->layout = $this->autoRender = false;
        if (isset($_POST['data_send']) && !empty($_POST['data_send'])) {
            $data = $_POST['data_send'];
            $seperates = $this->Question->spilt_title($_POST['data_send']);
            $seperate_coma = '';
            foreach ($seperates as $seperate) {
                $seperate_coma.="(+" . $seperate . "*) ";
            }
            $this->loadModel('Category');
            $tag = $this->Category->find('all', array(
                'fields' => array('Category.id', 'Category.name'),
                'conditions' => array("Category.name LIKE" => "%" . $data . "%"),
                //'conditions' => array("MATCH(Category.name) AGAINST('$data' IN BOOLEAN MODE)"),
                'recursive' => -1,
                'limit' => 10)
            );
            if (!empty($tag)) {
                $this->set('categories', $tag);
            }
        }
        $this->render('search_cat');
    }

    public function search_cat_edit() {
        $this->loadModel('Question');
        $this->loadModel('Category');
        $this->autoRender = false;
        $this->request->onlyAllow('ajax');
        $this->layout = $this->autoRender = false;
        if (isset($_POST['data_send']) && !empty($_POST['data_send'])) {
            $seperates = $this->Question->spilt_title($_POST['data_send']);
            $seperate_coma = '';
            foreach ($seperates as $seperate) {
                $seperate_coma.="(+" . $seperate . "*) ";
            }
            $this->loadModel('Category');
            $tag = $this->Category->find('all', array(
                'fields' => array('Category.id', 'Category.name'),
                'conditions' => array("MATCH(Category.name) AGAINST('$seperate_coma' IN BOOLEAN MODE)"),
                'recursive' => -1,
                'limit' => 10)
            );
            if (!empty($tag)) {
                $this->set('categories', $tag);
            }
        }
        $this->render('search_cat_edit');
    }

    public function edit_art_add_cat() {
        $this->request->onlyAllow('ajax');
        $this->layout = $this->autoRender = false;
        if (isset($_POST['art_id'])) {
            $this->loadModel('QuestionCategory');
            $data['QuestionCategory']['category_id'] = $_POST['cat_id'];
            $data['QuestionCategory']['question_id'] = $_POST['art_id'];
            $this->QuestionCategory->Create();
            $this->QuestionCategory->save($data);
            $response['cat_id'] = $_POST['cat_id'];
            $response['art_id'] = $_POST['art_id'];
            $response['cat_name'] = $_POST['cat_name'];
            $response['cat_slug'] = strip_tags(trim($_POST['cat_name']));
            $response['cat_slug'] = str_replace(' ', '-', $response['cat_slug']);
            $this->set('response', $response);
            $this->render('edit_art_add_cat');
        }
    }

    public function edit_art_delete_cat() {
        $this->request->onlyAllow('ajax');
        $this->autoRender = false;
        $this->layout = $this->autoRender = false;
        if (isset($_POST['art_id'])) {
            $this->loadModel('QuestionCategory');
            $data['QuestionCategory']['category_id'] = $_POST['cat_id'];
            $data['QuestionCategory']['question_id'] = $_POST['art_id'];
            $this->QuestionCategory->deleteAll(array('QuestionCategory.question_id' => $_POST['art_id'], 'QuestionCategory.category_id' => $_POST['cat_id']));
        }
    }

    public function get_cat() {
        $this->loadModel('Question');
        $this->loadModel('Category');
        $this->autoRender = false;
        $this->request->onlyAllow('ajax');
        $this->layout = $this->autoRender = false;
        if (isset($_POST['questitle']) && !empty($_POST['questitle'])) {
            $title_article = strip_tags($_POST['questitle']);
            $title_article = html_entity_decode($title_article);
            $title_article = preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', $title_article);
            $title_article = preg_replace('/&nbsp;/', ' ', $title_article);
            $seperates = $this->Question->spilt_title($title_article);
            $seperate_coma = '';
            foreach ($seperates as $seperate) {
                $seperate_coma.="(+" . $seperate . "*) ";
            }
            $this->loadModel('Tag');
            $this->loadModel('Category');
            $this->loadModel('Question');
            $this->loadModel('QuestionCategory');
            $tag = $this->Category->find('all', array(
                'fields' => array('Category.id', 'Category.name'),
                'conditions' => array('MATCH(Category.name) AGAINST("' . $seperate_coma . '" IN BOOLEAN MODE)'),
                'limit' => 4)
            );
            if (!empty($tag)) {
                $this->set('categories', $tag);
            }
        }
        $this->render('get_cat');
    }

    public function view($question_slug = false) {
        if ($question_slug != FALSE) {
            $this->loadModel('QuestionCategory');
            $this->loadModel('Answer');
            $this->loadModel('Question');
            $answers = $this->Answer->find('all', array(
                'fields' => array('Answer.last_title,Answer.last_body,Answer.sort,Question.selected,Question.views,Question.image,Question.status,Question.reference,Question.user_id,Question.title_update,Answer.title_update,Answer.body_update,Question.title_update', 'Answer.image', 'Answer.title', 'Question.id,Question.title,Question.slug,Answer.id,Answer.body,User.username,User.id'),
                'order' => array('Answer.sort ASC'),
                'conditions' => array('Question.slug' => $question_slug),
            ));
            if ($answers) {
                if (!($this->request->is('post'))) {
                    if (empty($answers[0]['Question']['last'])) {
                        if (!empty($answers[0]['Question']['title_update'])) {
                            $explode_title = explode(',,,', $answers[0]['Question']['title_update']);
                            $last = end($explode_title);
                            $titles = end(unserialize(base64_decode($last)));
                        } else {
                            $titles = $answers[0]['Question']['title'];
                        }
                        foreach ($answers as $answer) {
                            if (empty($answer['Answer']['last_body'])) {
                                if (!empty($answer['Answer']['title_update'])) {
                                    $explode_title = explode(',,,', $answer['Answer']['title_update']);
                                    $last = end($explode_title);

                                    $title = end(unserialize(base64_decode($last)));
                                } else {
                                    $title = $answer['Answer']['title'];
                                }
                                if (!empty($answer['Answer']['body_update'])) {
                                    $explode_body = explode(',,,', $answer['Answer']['body_update']);
                                    $last = end($explode_body);
                                    $body = end(unserialize(base64_decode($last)));
                                } else {
                                    $body = $answer['Answer']['body'];
                                }
                                $update = $this->Answer->updateAll(
                                        array('Answer.last_title' => "'" . $title . "'", 'Answer.last_body' => "'" . $body . "'"), array('Answer.id' => $answer['Answer']['id']));
                            }
                        }
                    }
                }
                $countur = (int) $answers[0]['Question']['views'] + 1;
                $update = $this->Question->updateAll(
                        array('Question.views' => $countur), array('Question.id' => $answers[0]['Question']['id']));

                $category = $this->QuestionCategory->find('list', array(
                    'fields' => 'QuestionCategory.category_id,QuestionCategory.category_id',
                    'conditions' => array('QuestionCategory.question_id' => $answers[0]['Question']['id'])));

                $tags = $this->QuestionCategory->find('all', array(
                    'fields' => 'Category.id,Category.slug,Category.name',
                    'conditions' => array('QuestionCategory.question_id' => $answers[0]['Question']['id'])));
                $categories_all = $this->QuestionCategory->find('all', array(
                    'fields' => array('DISTINCT(Question.id), Question.title_update , Question.slug', 'Question.title'),
                    'limit' => 5,
                    'conditions' => array('Category.id' => $category, "NOT" => array('QuestionCategory.question_id' => $answers[0]['Question']['id']))));

                $this->loadModel('User');
                $user = $this->User->find('first', array('fields' => array('User.id,User.username'), 'conditions' => array('User.id' => $answers[0]['Question']['user_id'])));
                if (!empty($user)) {
                    $this->set('question_write', $user);
                } else {
                    $this->set('question_write', NULL);
                }
                $this->set('categories', $categories_all);
                $this->set('answers', $answers);
                $this->set('tags', $tags);
            } else {
                
            }
        }
    }

    public function edit($question_slug) {
        $user_session = $this->Session->read('user_quesli');
        $this->loadModel('Log');
        $this->loadModel('QuestionCategory');
        $this->loadModel('Question');
        $this->loadModel('Answer');
        $answers = $this->Answer->find('all', array(
            'fields' => array('Answer.last_title,Answer.last_body,Answer.sort,Question.selected,Question.last,Question.views,Question.image,Question.status,Question.reference,Question.user_id,Question.title_update,Answer.title_update,Answer.body_update,Question.title_update', 'Answer.image', 'Answer.title', 'Question.id,Question.title,Question.slug,Answer.id,Answer.body,User.username,User.id'),
            'order' => array('Answer.sort ASC'), 'conditions' => array('Question.slug' => $question_slug),
        ));
        if ($answers) {
            if (!($this->request->is('post'))) {

                if (empty($answers[0]['Question']['last'])) {
                    if (!empty($answers[0]['Question']['title_update'])) {
                        $explode_title = explode(',,,', $answers[0]['Question']['title_update']);
                        $last = end($explode_title);
                        $titles = end(unserialize(base64_decode($last)));
                    } else {
                        $titles = $answers[0]['Question']['title'];
                    }
                    $update = $this->Question->updateAll(
                            array('Question.last' => "'" . $titles . "'"), array('Question.id' => $answers[0]['Question']['id']));
                    foreach ($answers as $answer) {
                        if (empty($answer['Answer']['last_body'])) {
                            if (!empty($answer['Answer']['title_update'])) {
                                $explode_title = explode(',,,', $answer['Answer']['title_update']);
                                $last = end($explode_title);

                                $title = end(unserialize(base64_decode($last)));
                            } else {
                                $title = $answer['Answer']['title'];
                            }
                            if (!empty($answer['Answer']['body_update'])) {
                                $explode_body = explode(',,,', $answer['Answer']['body_update']);
                                $last = end($explode_body);
                                $body = end(unserialize(base64_decode($last)));
                            } else {
                                $body = $answer['Answer']['body'];
                            }
                            $update = $this->Answer->updateAll(
                                    array('Answer.last_title' => '"' . $title . '"', 'Answer.last_body' => '"' . $body . '"'), array('Answer.id' => $answer['Answer']['id']));
                        }
                    }
                }
            }
            $countur = (int) $answers[0]['Question']['views'] + 1;
            $update = $this->Question->updateAll(
                    array('Question.views' => $countur), array('Question.id' => $answers[0]['Question']['id']));
            $category = $this->QuestionCategory->find('list', array(
                'fields' => 'QuestionCategory.question_id,QuestionCategory.category_id',
                'conditions' => array('QuestionCategory.question_id' => $answers[0]['Question']['id'])));
            $categories_all = $this->QuestionCategory->find('all', array(
                'fields' => array('Question.status,Question.type,Question.id,Question.title,Question.slug,Question.user_id'),
                'conditions' => array('QuestionCategory.category_id' => $category, 'NOT' => array('QuestionCategory.question_id' => $answers[0]['Question']['id']))));
            $tags = $this->QuestionCategory->find('all', array(
                'fields' => 'Category.id,Category.slug,Category.name',
                'conditions' => array('QuestionCategory.question_id' => $answers[0]['Question']['id'])));

            $this->loadModel('User');
            $user = $this->User->find('first', array('fields' => array('User.id,User.username'), 'conditions' => array('User.id' => $answers[0]['Question']['user_id'])));
            if (!empty($user)) {
                $this->set('question_write', $user);
            } else {
                $this->set('question_write', NULL);
            }
            $this->set('categories', $categories_all);
            $this->set('answers', $answers);
            $this->set('tags', $tags);
        } else {
            $this->redirect('/');
        }
        if ($this->request->is('post')) {

            for ($sort_num = 0; $sort_num < count($this->request->data['sort_value']); $sort_num++) {
                $update = $this->Answer->updateAll(
                        array('Answer.sort' => $sort_num + 1), array('Answer.id' => $this->request->data['id'][$sort_num]));
            }
            if ($user_session) {
                $userid = $user_session['User']['id'];
                $userids = $user_session['User']['id'];
                $userip = 0;
            } else {
                $userid = $this->get_client_ip_server();
                $userids = 0;
                $userip = $this->get_client_ip_server();
            }
            $dt = new DateTime();
            $date = $dt->format('Y-m-d H:i:s');
            if (isset($this->request->data['ref']) && !empty($this->request->data['ref'])) {
                if (!empty($answers[0]['Question']['reference'])) {
                    $ref = $answers[0]['Question']['reference'] . ",,,,";
                } else {
                    $ref = '';
                }
                for ($ref_count = 0; $ref_count < count($this->request->data['ref']); $ref_count++) {
                    if ($ref_count == 0) {
                        $ref.=$this->request->data['ref_name'][$ref_count] . ',,' . $this->request->data['ref'][$ref_count];
                    } else {
                        $ref.=",,,," . $this->request->data['ref_name'][$ref_count] . ',,' . $this->request->data['ref'][$ref_count];
                    }
                }
            } else {
                $ref = '';
            }
            $question_id = $this->request->data['art_id'][0];
            $image_status = false;
            if ($this->request->params['form']['image_title']['size'][0] != 0) {
                $date = date("Y-m-d") . '_' . rand(0, 999);
                $name = $date . $this->request->params['form']['image_title']['name'][0];
                $tmp = $this->request->params['form']['image_title']['tmp_name'][0];
                if (!file_exists(WWW_ROOT . 'question' . DS . $question_id)) {
                    mkdir(WWW_ROOT . 'question' . DS . $question_id, 0777, true);
                }
                $filePath = WWW_ROOT . 'question' . DS . $question_id . DS . $name;
                if (move_uploaded_file($tmp, $filePath)) {
                    $article_image = $name;
                    $image_status = true;
                }
            } else {
                
            }
            if (!empty($answers[0]['Question']['title_update'])) {
                $explode_title = explode(',,,', $answers[0]['Question']['title_update']);
                $last = end($explode_title);
                $titles_check = end(unserialize(base64_decode($last)));
            } else {
                $titles_check = $answers[0]['Question']['title'];
            }
            if (empty($answers[0]['Question']['last'])) {
                $last = $this->request->data['art_title'][0];
            } else {
                $last = $answers[0]['Question']['last'];
            }

            if (str_replace("&nbsp;", "", $titles_check) != $this->request->data['art_title'][0]) {
                $array_new = array($userid, $date, $this->request->data['art_title'][0]);
                $array_new = base64_encode(serialize($array_new));
                if ($image_status) {

                    $update = $this->Question->updateAll(
                            array('Question.last' => "'" . $last . "'", 'Question.image' => "'" . $article_image . "'", 'Question.title_update' => "'" . $array_new . "'", 'Question.reference' => "'" . $ref . "'"), array('Question.id' => $this->request->data['art_id'][0]));
                } else {
                    $update = $this->Question->updateAll(
                            array('Question.last' => "'" . $last . "'", 'Question.title_update' => "'" . $array_new . "'", 'Question.reference' => "'" . $ref . "'"), array('Question.id' => $this->request->data['art_id'][0]));
                }
                $log['Log']['user_id'] = $userid;
                $log['Log']['question'] = $last;
                $log['Log']['question_slug'] = $question_slug;
                $log['Log']['user_ip'] = $userip;
                $log['Log']['question_id'] = $this->request->data['art_id'][0];
                $log['Log']['answer_id'] = 0;
                $log['Log']['title'] = 'Question title updated';
                $this->Log->Create();
                $this->Log->save($log);
            } else {
                if ($image_status) {
                    $update = $this->Question->updateAll(array('Question.last' => "'" . $last . "'", 'Question.image' => "'" . $article_image . "'", 'Question.reference' => "'" . $ref . "'"), array('Question.id' => $this->request->data['art_id'][0]));
                } else {
                    $update = $this->Question->updateAll(array('Question.last' => "'" . $last . "'", 'Question.reference' => "'" . $ref . "'"), array('Question.id' => $this->request->data['art_id'][0]));
                }
            }
            $i = 0;
            foreach ($answers as $answer) {
                if (!empty($this->request->data['title'][$i])) {
                    if (empty($answer['Answer']['last_title'])) {
                        $update = $this->Answer->updateAll(array('Answer.last_title' => "'" . $this->request->data['title'][$i] . "'"), array('Answer.question_id' => $this->request->data['art_id'][0], 'Answer.id' => $this->request->data['id'][$i]));
                    } else {
                        if ($answer['Answer']['title'] != $this->request->data['title'][$i]) {
                            if ($answer['Answer']['last_title'] != $this->request->data['title'][$i]) {
                                if (!empty($answer['Answer']['title_update'])) {
                                    $explode_title = explode(',,,', $answer['Answer']['title_update']);
                                    $body = $this->request->data['title'][$i];
                                    $array_new = array($userid, $date, $body);
                                    $array_new = base64_encode(serialize($array_new));
                                    $array_new = $answer['Answer']['title_update'] . ",,," . $array_new;
                                    $update = $this->Answer->updateAll(array('Answer.last_title' => "'" . $body . "'", 'Answer.title_update' => "'" . $array_new . "'"), array('Answer.question_id' => $this->request->data['art_id'][0], 'Answer.id' => $this->request->data['id'][$i]));
                                    $log['Log']['user_id'] = $userid;
                                    $log['Log']['question_slug'] = $question_slug;
                                    $log['Log']['question'] = $last;
                                    $log['Log']['user_ip'] = $userip;
                                    $log['Log']['question_id'] = $this->request->data['art_id'][0];
                                    $log['Log']['answer_id'] = $this->request->data['id'][$i];
                                    $log['Log']['title'] = 'point title updated';
                                    $this->Log->Create();
                                    $this->Log->save($log);
                                } else {
                                    if ($this->request->data['title'][$i] != $answer['Answer']['title']) {
                                        $array_new = array($userid, $date, $this->request->data['title'][$i]);
                                        $array_new = base64_encode(serialize($array_new));
                                        $update = $this->Answer->updateAll(array('Answer.last_title' => "'" . $this->request->data['title'][$i] . "'", 'Answer.title_update' => "'" . $array_new . "'"), array('Answer.question_id' => $this->request->data['art_id'][0], 'Answer.id' => $this->request->data['id'][$i]));
                                        $log['Log']['user_id'] = $userid;
                                        $log['Log']['question'] = $last;
                                        $log['Log']['question_slug'] = $question_slug;
                                        $log['Log']['user_ip'] = $userip;
                                        $log['Log']['question_id'] = $this->request->data['art_id'][0];
                                        $log['Log']['answer_id'] = $this->request->data['id'][$i];
                                        $log['Log']['title'] = 'point title updated';
                                        $this->Log->Create();
                                        $this->Log->save($log);
                                    }
                                }
                            }
                        }
                    }
                }
                if (!empty($this->request->data['body'][$i])) {
                    if (empty($answer['Answer']['last_body'])) {
                        $update = $this->Answer->updateAll(array('Answer.last_body' => "'" . $this->request->data['body'][$i] . "'"), array('Answer.question_id' => $this->request->data['art_id'][0], 'Answer.id' => $this->request->data['id'][$i]));
                    } else {
                        if ($answer['Answer']['body'] != $this->request->data['body'][$i]) {
                            if ($answer['Answer']['last_body'] != $this->request->data['body'][$i]) {
                                if (!empty($answer['Answer']['body_update'])) {

                                    $body = $this->request->data['body'][$i];
                                    $array_new = array($userid, $date, $body);
                                    $array_new = base64_encode(serialize($array_new));
                                    $array_new = $answer['Answer']['body_update'] . ",,," . $array_new;
                                    $update = $this->Answer->updateAll(array('Answer.last_body' => "'" . $body . "'", 'Answer.body_update' => "'" . $array_new . "'"), array('Answer.question_id' => $this->request->data['art_id'][0], 'Answer.id' => $this->request->data['id'][$i]));
                                    $log['Log']['user_id'] = $userid;
                                    $log['Log']['question_slug'] = $question_slug;
                                    $log['Log']['question'] = $last;
                                    $log['Log']['user_ip'] = $userip;
                                    $log['Log']['question_id'] = $this->request->data['art_id'][0];
                                    $log['Log']['answer_id'] = $this->request->data['id'][$i];
                                    $log['Log']['title'] = 'point body updated';
                                    $this->Log->Create();
                                    $this->Log->save($log);
                                } else {
                                    if ($this->request->data['body'][$i] != $answer['Answer']['body']) {
                                        $array_new = array($userid, $date, $this->request->data['body'][$i]);
                                        $array_new = base64_encode(serialize($array_new));
                                        $update = $this->Answer->updateAll(array('Answer.last_body' => "'" . ".'", 'Answer.body_update' => "'" . $array_new . "'"), array('Answer.question_id' => $this->request->data['art_id'][0], 'Answer.id' => $this->request->data['id'][$i]));
                                        $log['Log']['user_id'] = $userid;
                                        $log['Log']['question_slug'] = $question_slug;
                                        $log['Log']['user_ip'] = $userip;
                                        $log['Log']['question_id'] = $this->request->data['art_id'][0];
                                        $log['Log']['answer_id'] = $this->request->data['id'][$i];
                                        $log['Log']['title'] = 'point body updated';
                                        $this->Log->Create();
                                        $this->Log->save($log);
                                    }
                                }
                            }
                        }
                    }
                }
                $i++;
            }
            $i = 0;
            if (isset($this->request->data['body_new']) && !empty($this->request->data['body_new'])) {
                $j = 0;
                if (!empty($this->request->data['body_new'])) {
                    foreach ($this->request->data['body_new'] as $points) {
                        $question_id = $this->request->data['art_id'][0];
                        if (!empty($points)) {
                            if (isset($this->request->params['form']['image_new']['name'][$j])) {
                                if ($this->request->params['form']['image_new']['size'][$j] != 0) {
                                    $date = date("Y-m-d") . '_' . rand(0, 999);
                                    $name = $date . $this->request->params['form']['image_new']['name'][$j];
                                    $tmp = $this->request->params['form']['image_new']['tmp_name'][$j];
                                    if (!file_exists(WWW_ROOT . 'question' . DS . $question_id)) {
                                        mkdir(WWW_ROOT . 'question' . DS . $question_id, 0777, true);
                                    }
                                    $filePath = WWW_ROOT . 'question' . DS . $question_id . DS . $name;
                                    if (move_uploaded_file($tmp, $filePath)) {
                                        $answer_new[$i]['image'] = $name;
                                    }
                                }
                            }

                            $answer_new[$i]['Answer']['last_title'] = $this->request->data['title_new'][$i];
                            $answer_new[$i]['Answer']['last_body'] = $this->request->data['body_new'][$i];
                            $answer_new[$i]['Answer']['title'] = $this->request->data['title_new'][$i];
                            $answer_new[$i]['Answer']['body'] = $this->request->data['body_new'][$i];
                            $answer_new[$i]['Answer']['sort'] = count($answers) + 1;
                            $answer_new[$i]['Answer']['question_id'] = $question_id;
                            if ($user_session) {
                                $userid = $user_session['User']['id'];
                            } else {
                                $userid = 0;
                                $answer_new[$i]['Answer']['user_ip'] = $this->get_client_ip_server();
                            }
                            $answer_new[$i]['Answer']['user_id'] = $userid;
                            $i++;
                        }
                        $j++;
                    }
                    if (!empty($answer_new)) {

                        if ($this->Answer->saveAll($answer_new)) {
                            $log['Log']['user_id'] = $userid;
                            $log['Log']['question'] = $last;
                            $log['Log']['question_slug'] = $question_slug;
                            $log['Log']['user_ip'] = $userip;
                            $log['Log']['question_id'] = $this->request->data['art_id'][0];
                            $log['Log']['answer_id'] = 0;
                            $log['Log']['title'] = 'new point added';

                            $this->Log->Create();
                            $this->Log->save($log);
                        }
                    }
                }
                $this->redirect(array('controller' => 'articles', 'action' => 'view', $answers[0]['Question']['slug']));
            } else {

                $this->redirect(array('controller' => 'articles', 'action' => 'view', $answers[0]['Question']['slug']));
            }
        }
    }

    public function publish($question_slug) {
        $this->loadModel('QuestionCategory');
        $this->loadModel('Answer');
        $answers = $this->Answer->find('all', array(
            'fields' => array('Question.user_id,Question.title_update,Answer.title_update,Answer.body_update,Question.title_update', 'Answer.image', 'Answer.title', 'Question.id,Question.title,Question.slug,Answer.id,Answer.body,User.username,User.id'),
            'conditions' => array('Question.slug' => $question_slug),
        ));

        $category = $this->QuestionCategory->find('list', array(
            'fields' => 'QuestionCategory.question_id,QuestionCategory.category_id',
            'conditions' => array('QuestionCategory.question_id' => $answers[0]['Question']['id'])));

        $categories_all = $this->QuestionCategory->find('all', array(
            'fields' => array('Question.id,Question.title,Question.slug,Question.user_id'),
            'conditions' => array('QuestionCategory.category_id' => $category, 'NOT' => array('QuestionCategory.question_id' => $answers[0]['Question']['id']))));

        $this->loadModel('User');
        $user = $this->User->find('first', array('fields' => array('User.id,User.username'), 'conditions' => array('User.id' => $answers[0]['Question']['user_id'])));
        if (!empty($user)) {
            $this->set('question_write', $user);
        } else {
            $this->set('question_write', NULL);
        }
        $this->set('categories', $categories_all);
        $this->set('answers', $answers);
    }

    public function filter() {
        $title_article = $_POST['questitle'];
        $this->request->onlyAllow('ajax');
        $this->layout = $this->autoRender = false;
        $title_article = strip_tags($title_article);
        $title_article = html_entity_decode($title_article);
        $title_article = preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', $title_article);
        $title_article = preg_replace('/&nbsp;/', ' ', $title_article);
        return $title_article;
    }

    public function add() {
        $this->loadModel('Tag');
        $this->loadModel('Category');
        $this->loadModel('Question');
        $this->loadModel('QuestionCategory');
        $this->loadModel('Answer');
        $tag_check = false;
        $user = $this->Session->read('user_quesli');

        if ($this->request->is('post')) {

            if (isset($this->request->data['ref']) && !empty($this->request->data['ref'])) {
                for ($ref_count = 0; $ref_count < count($this->request->data['ref']); $ref_count++) {
                    if ($ref_count == 0) {
                        $ref.=$this->request->data['ref_name'][$ref_count] . ',,' . $this->request->data['ref'][$ref_count];
                    } else {
                        $ref.=",,,," . $this->request->data['ref_name'][$ref_count] . ',,' . $this->request->data['ref'][$ref_count];
                    }
                }
            } else {
                $ref = '';
            }

            $this->request->data['Question']['reference'] = $ref;
            $title_article = preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', $this->request->data['Question']['title']);
            $title_article = preg_replace('/&nbsp;/', ' ', $title_article);
            $title_article = strip_tags($title_article);
            //$seperate = $this->Question->spilt_title($this->request->data['Question']['title']);
//   $this->request->data['Question']['body']=  implode(',,,', $this->request->data['body']);
            $this->request->data['Question']['title'] = htmlspecialchars(trim($title_article));
            $slug = preg_replace("/[^A-Za-z0-9 ]/", '', $title_article);
            $this->request->data['Question']['slug'] = strip_tags(trim(implode("-", $this->Question->remove_Space($slug))));
            if ($user) {
                $userid = $user['User']['id'];
            } else {
                $this->request->data['Question']['user_ip'] = $this->get_client_ip_server();
                $userid = 0;
            }
            $this->request->data['Question']['user_id'] = (int) $userid;
            if ($this->Question->save($this->request->data)) {
                $question_id = $this->Question->getLastInsertId();
                $first_tags = 0;
                $both_tags = 0;
                /* Save old category question end */
                $b = 0;
                /* Save new category question */
                if (isset($this->request->data['new']) && !empty($this->request->data['new'])) {
                    $both_tags = (count($this->request->data['new']) + $first_tags);
                    for ($i = $first_tags; $i < $both_tags; $i++) {
                        $category[$i]['QuestionCategory']['category_id'] = $this->request->data['new'][$b];
                        $category[$i]['QuestionCategory']['question_id'] = $question_id;
                        $category[$i]['QuestionCategory']['user_id'] = $userid;
                        $b++;
                    }
                    if ($this->QuestionCategory->saveAll($category)) {
                        
                    }
                }
                /* Save new category question end */
                $this->Tag->create();
                $tag = FALSE;
                $both_tags = 0;
                $b = 0;
                /* Save new category with tags */

                /* Save new category with tags end */
                $j = 0;
                if ($user) {
                    $userip = 0;
                    $userid = $user['User']['id'];
                } else {
                    $userip = $this->get_client_ip_server();
                    $userid = 0;
                }
                if ($this->request->params['form']['image_title']['size'][0] != 0) {
                    if (preg_match("/image/", $this->request->params['form']['image_title']['type'][0])) {
                        $date = date("Y-m-d") . '_' . rand(0, 999);
                        $name = $date . $this->request->params['form']['image_title']['name'][0];
                        $tmp = $this->request->params['form']['image_title']['tmp_name'][0];
                        if (!file_exists(WWW_ROOT . 'question' . DS . $question_id)) {
                            mkdir(WWW_ROOT . 'question' . DS . $question_id, 0777, true);
                        }
                        $filePath = WWW_ROOT . 'question' . DS . $question_id . DS . $name;
                        if (move_uploaded_file($tmp, $filePath)) {
                            $article_image = $name;
                        }
                        $update = $this->Question->updateAll(
                                array('Question.image' => "'" . $article_image . "'"), array('Question.id' => $question_id));
                    }
                }
                foreach ($this->request->data['body'] as $points) {
                    if ($this->request->params['form']['image']['size'][$j] != 0) {
                        if (preg_match("/image/", $this->request->params['form']['image']['type'][$j])) {
                            $date = date("Y-m-d") . '_' . rand(0, 999);
                            $name = $date . $this->request->params['form']['image']['name'][$j];
                            $tmp = $this->request->params['form']['image']['tmp_name'][$j];
                            if (!file_exists(WWW_ROOT . 'question' . DS . $question_id)) {
                                mkdir(WWW_ROOT . 'question' . DS . $question_id, 0777, true);
                            }
                            $filePath = WWW_ROOT . 'question' . DS . $question_id . DS . $name;
                            debug($filePath);
                            if (move_uploaded_file($tmp, $filePath)) {
                                $answer[$j]['image'] = $name;
                                debug($this->request->params['form']['image']['size']);
                            }
                        }
                    }

                    $answer[$j]['title'] = $this->request->data['title'][$j];
                    $answer[$j]['body'] = $points;
                    $answer[$j]['question_id'] = $question_id;
                    $answer[$j]['user_ip'] = $userip;
                    $answer[$j]['user_id'] = $userid;
                    $j++;
                }

                if ($this->Answer->saveAll($answer)) {

                    $this->redirect(array('controller' => 'articles', 'action' => 'view', $this->request->data['Question']['slug']));
                }
            }
        }
    }

    public function delete_article() {
        $this->request->onlyAllow('ajax');
        $this->layout = $this->autoRender = false;
        $this->loadModel('Question');
        $this->loadModel('QuestionCategory');
        $this->loadModel('Answer');
        if (isset($_POST['target'])) {
            if ($_POST['target'] == 'a') {
                if ($this->Question->deleteAll(array('Question.id' => $_POST['ques_art']))) {
                    if ($this->Answer->deleteAll(array('Answer.question_id' => $_POST['ques_art']))) {
                        if ($this->QuestionCategory->deleteAll(array('QuestionCategory.question_id' => $_POST['ques_art']))) {
                            return $this->redirect(array(
                                        'action' => 'show_news'
                            ));
                        }
                    }
                }
            } else {
                echo $_POST['ques_art'];
                if ($this->Answer->deleteAll(array('Answer.id' => $_POST['ques_art']))) {
                    
                }
            }
        }
    }

    public function expand() {
        $this->request->onlyAllowview('ajax');
        $this->layout = $this->autoRender = false;
        $this->loadModel('Question');

        if (isset($_POST['art_id'])) {
            if ($_POST['art_status'] == 0) {
                if ($this->Question->updateAll(array('Question.expand' => 0), array('Question.id' => $_POST['art_id']))) {
                    
                }
            } else {
                if ($this->Question->updateAll(array('Question.expand' => 1), array('Question.id' => $_POST['art_id']))) {
                    
                }
            }
        }
    }

    public function update_status() {
        $this->request->onlyAllow('ajax');
        $this->layout = $this->autoRender = false;
        $this->loadModel('Question');
        $this->loadModel('Answer');
        if (isset($_POST['target'])) {
            if ($_POST['target'] == 'a') {
                if ($this->Question->updateAll(array('Question.status' => $_POST['status']), array('Question.id' => $_POST['ques_art']))) {
                    
                }
            } else {
                if ($this->Answer->updateAll(array('Answer.status' => $_POST['status']), array('Answer.id' => $_POST['ques_art']))) {
                    
                }
            }
        }
    }
	 public function view1($question_slug = false) {
        if ($question_slug != FALSE) {
            $this->loadModel('QuestionCategory');
            $this->loadModel('Answer');
            $this->loadModel('Question');
            $answers = $this->Answer->find('all', array(
                'fields' => array('Answer.last_title,Answer.last_body,Answer.sort,Question.selected,Question.views,Question.image,Question.status,Question.reference,Question.user_id,Question.title_update,Answer.title_update,Answer.body_update,Question.title_update', 'Answer.image', 'Answer.title', 'Question.id,Question.title,Question.slug,Answer.id,Answer.body,User.username,User.id'),
                'order' => array('Answer.sort ASC'),
                'conditions' => array('Question.slug' => $question_slug),
            ));
            if ($answers) {
                if (!($this->request->is('post'))) {

                    if (empty($answers[0]['Question']['last'])) {
                        if (!empty($answers[0]['Question']['title_update'])) {
                            $explode_title = explode(',,,', $answers[0]['Question']['title_update']);
                            $last = end($explode_title);
                            $titles = end(unserialize(base64_decode($last)));
                        } else {
                            $titles = $answers[0]['Question']['title'];
                        }
                        foreach ($answers as $answer) {
                            if (empty($answer['Answer']['last_body'])) {
                                if (!empty($answer['Answer']['title_update'])) {
                                    $explode_title = explode(',,,', $answer['Answer']['title_update']);
                                    $last = end($explode_title);

                                    $title = end(unserialize(base64_decode($last)));
                                } else {
                                    $title = $answer['Answer']['title'];
                                }
                                if (!empty($answer['Answer']['body_update'])) {
                                    $explode_body = explode(',,,', $answer['Answer']['body_update']);
                                    $last = end($explode_body);
                                    $body = end(unserialize(base64_decode($last)));
                                } else {
                                    $body = $answer['Answer']['body'];
                                }
                                $update = $this->Answer->updateAll(
                                        array('Answer.last_title' => "'" . $title . "'", 'Answer.last_body' => "'" . $body . "'"), array('Answer.id' => $answer['Answer']['id']));
                            }
                        }
                    }
                }
                $countur = (int) $answers[0]['Question']['views'] + 1;
                $update = $this->Question->updateAll(
                        array('Question.views' => $countur), array('Question.id' => $answers[0]['Question']['id']));

                $category = $this->QuestionCategory->find('list', array(
                    'fields' => 'QuestionCategory.category_id,QuestionCategory.category_id',
                    'conditions' => array('QuestionCategory.question_id' => $answers[0]['Question']['id'])));

                $tags = $this->QuestionCategory->find('all', array(
                    'fields' => 'Category.id,Category.slug,Category.name',
                    'conditions' => array('QuestionCategory.question_id' => $answers[0]['Question']['id'])));
                $categories_all = $this->QuestionCategory->find('all', array(
                    'fields' => array('DISTINCT(Question.id), Question.title_update , Question.slug', 'Question.title'),
                    'limit' => 5,
                    'conditions' => array('Category.id' => $category, "NOT" => array('QuestionCategory.question_id' => $answers[0]['Question']['id']))));

                $this->loadModel('User');
                $user = $this->User->find('first', array('fields' => array('User.id,User.username'), 'conditions' => array('User.id' => $answers[0]['Question']['user_id'])));
                if (!empty($user)) {
                    $this->set('question_write', $user);
                } else {
                    $this->set('question_write', NULL);
                }
                $this->set('categories', $categories_all);
                $this->set('answers', $answers);
                $this->set('tags', $tags);
            } else {
                
            }
        }
    }
}

?>