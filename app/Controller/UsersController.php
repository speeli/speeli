﻿<?php

class UsersController extends AppController {

    public $name = 'Users';
    public $components = array('Paginator', 'RequestHandler', 'Email', 'Session', 'Cookie');
    public $helpers = array('Js', 'Paginator', 'Html', 'Form', 'Session');

    /**
     * This controller does not use a model
     *
     * @var array
     */
    public $uses = array();

    public function beforeFilter() {
        parent::beforeFilter();
        // Allow users to register and logout.
    }
public function login_face(){
        Configure::write('debug', 2);
       $this->autoLayout = false; 
       $this->layout = FALSE;
       $this->autoRender = false;
               debug( $_POST['email']);
               $user = $this->User->find('first',array('conditions'=>array('OR'=>array('email'=>$_POST['email'],'username'=>$_POST['email'])))) ;
               debug($user);
               if(empty($user)){
                $this->User->create();
                $this->request->data['User']['username'] = $_POST['email'];
                $this->request->data['User']['name'] = $_POST['name'];
                $this->request->data['User']['email'] = $_POST['email'];
                $this->request->data['User']['social'] = (int) $_POST['id'];
               
                if ($this->User->save($this->request->data)) {
                     $user = $this->User->find('first',array('conditions'=>array('username'=>$_POST['email']))) ;
                     $message = 'Welcome to Speeli  ' . $user['User']['username'] . '   :) <br>
                            Speeli is a website that summarizes anything into fast scan-able lists.<br>
                            Our mission is to help people find direct answers and summaries instead of wasting hours searching the internet or reading long text.<br>
                            <br>The ones who create speeli summaries are me and you. <br>
                            You can use Speeli to summarize anything by just clicking on the add Speeli Summary button.<br><br>
                            To find more about speeli you can check those summarized speeli posts: <a href="/articles/view/What-is-Speeli">What is Speeli?</a>  & 
                            <a href="/articles/view/The-guidelines-for-writing-a-Speeli-article"> Speeli Writing guidelines </a><br><br>
                            Feel free to reply back to me if you need any help<br>
                            looking forward for your contribution so that we can change the world together :)<br><br>
                            Farouk<br>
                            Senior community member <br>';
                        $message_group['MessageGroup']['user_send'] = 3;
                        $message_group['MessageGroup']['user_recieve'] = $user['User']['id'];
                        $this->loadModel('Message');
                        $this->loadModel('MessageGroup');
                        $this->MessageGroup->create();
                        if ($this->MessageGroup->save($message_group)) {
                            $group_id = $this->MessageGroup->getLastInsertId();
                            $this->request->data['Message']['user_send'] = 3;
                            $this->request->data['Message']['user_recieve'] = $user['User']['id'];
                            $this->request->data['Message']['message'] = $message;
                            $this->request->data['Message']['seen'] = 0;
                            $this->request->data['Message']['group_id'] = $group_id;
                            $this->Message->create();
                            if ($this->Message->save($this->request->data)) {
                                
                            }
                        }
                        $this->Session->write('user_quesli', $user);
                        setcookie('user_quesli', json_encode($user), time() + (86400 * 30), "/"); 
                    
                 }
                 }else{
                    $this->Session->write('user_quesli', $user);
                        setcookie('user_quesli', json_encode($user), time() + (86400 * 30), "/"); 
                        
                 }
    }
    function ranking($points) {
        $ranking = '';
        $points+=50;

        if ($points < 60) {
            $image = 'first.jpg';
            $ranking = 'Level 1 - Baby Speelian';
            $remain = 60 - $points;

            $remain_dash = $points . ' / 120';
            $next = 'Level 2 - Potential Genius';
            $percent = (($remain * 100) / 120);
        } else if ($points >= 60 && $points <= 120) {
            $image = 'second.jpg';
            $next = 'Level 3 - Whizkid';
            $ranking = 'Level 2 - Potential Genius';
            $remain = 150 - $points;
            $percent = (($remain * 100) / 240);
            $remain_dash = $points . ' / 240';
        } else if ($points >= 120 && $points <= 240) {
            $image = 'third.jpg';
            $ranking = 'Level 3 - Whizkid';
            $next = 'Level 4 - keyboard Ninja';
            $remain = 500 - $points;
            $remain_dash = $points . ' / 500';
            $percent = (($remain * 100) / 500);
        } else if ($points >= 240 && $points <= 500) {

            $image = 'fourth.jpg';
            $next = 'Level 5 - Wizzard';
            $ranking = 'Level 4 - Keyboard Ninja';
            $remain = 2000 - $points;
            $percent = (($remain * 100) / 2000);
            $remain_dash = $points . ' / 2000';
        } else if ($points >= 500 && $points <= 2000) {
            $remain = 6000 - $points;
            $next = 'Level 6 - Chuck Norris';
            $remain_dash = $points . ' / 6000';
            $image = 'fifth.jpg';
            $ranking = 'Level 5 - Wizzard';
            $percent = (($remain * 100) / 6000);
        } else if ($points >= 2000 && $points <= 6000) {
            $image = 'six.jpg';
            $ranking = 'Level 6 - Chuck Norris';
            $next = 'Level 7 - Batman';
            $remain = 12000 - $points;
            $remain_dash = $points . ' / 12000';
            $percent = (($remain * 100) / 12000);
        } else if ($points >= 6000 && $points <= 12000) {
            $image = 'seven.jpg';
            $next = 'Level 8 - Extraterrestrial Being';
            $ranking = 'Level 7 - Batman';
            $remain = 20000 - $points;
            $remain_dash = $points . ' / 20000';
            $percent = (($remain * 100) / 20000);
        } else if ($points >= 12000 && $points <= 20000) {
            $image = 'eight.jpg';
            $next = 'Level 9 - Spiritual Entity';
            $ranking = 'Level 8 - Extraterrestrial Being';
            $remain = 40000 - $points;
            $remain_dash = $points . ' / 40000';
            $percent = (($remain * 100) / 40000);
        } else if ($points >= 20000 && $points <= 40000) {
            $remain = 100000 - $points;
            $next = 'Level 10 - Infinity';
            $image = 'nine.jpg';
            $remain_dash = $points . ' / 100000';
            $ranking = 'Level 9 - spiritual Entity';
            $percent = (($remain * 100) / 100000);
        } else if ($points >= 40000 && $points <= 100000) {
            $remain = 100000 - $points;
            $next = 'You are in the top level ';
            $image = 'ten.jpg';
            $remain_dash = $points . ' / 100000';
            $percent = (($remain * 100) / 100000);
            $ranking = 'Level 10- Infinity';
        } else if ($points >= 100000) {
            $remain = 0;
            $remain_dash = 'completed';
            $image = 'ten.jpg';
            $next = 'You are in the top level ';
        }



        $rankings = array('remain_dash' => $remain_dash, 'next' => $next, 'image' => $image, 'percent' => intval($percent), 'rank' => $ranking, 'remain' => $remain);
        $ranking = $rankings;

        return $ranking;
    }

    public function check() {
        Configure::write('debug', 2);
        $this->autoRender = false;
        $this->request->onlyAllow('ajax');
        $this->layout = $this->autoRender = false;
        $this->loadModel('User');
        $user = $this->User->find('first', array(
            'conditions' => array('User.social' => $_POST['response']['id'])));

        if (isset($_POST['response'])) {
            if (empty($user['User']['social'])) {
                echo $_POST['response']['id'];
                $user['User']['social'] = (int) $_POST['response']['id'];
                $user['User']['name'] = $_POST['response']['name'];
                $user['User']['username'] = $_POST['response']['name'];
                $user['User']['email'] = $_POST['response']['email'];
                $user['User']['gender'] = $_POST['response']['gender'];
                $this->User->Create();
                $this->User->save($user);
                $user_id = $this->User->getLastInsertId();
                $user['User']['id'] = $user_id;
                print_r($user);
                $this->Session->write('user_quesli', $user);
                $this->Session->read('user_quesli');
            } else {
                print_r($user);
                $this->Session->write('user_quesli', $user);
                $this->Session->read('user_quesli');
            }
        }
    }

    public function contributors() {
        $this->loadModel('QuestionCategory');
        $this->paginate = array(
            'fields' => array('DISTINCT (QuestionCategory.question_id),User.id,User.username,User.name', 'User.image,User.knows,COUNT(QuestionCategory.user_id) AS user'),
            'group' => 'QuestionCategory.user_id',
            'order' => 'user DESC');
        $this->set('users', $this->paginate('QuestionCategory'));
    }

    public function search() {
        $this->loadModel('Question');
        $this->loadModel('QuestionCategory');
        $this->autoRender = false;
        $this->request->onlyAllow('ajax');
        $this->layout = $this->autoRender = false;
        if (isset($_POST['questitle']) && !empty($_POST['questitle'])) {
            $seperates = $this->Question->spilt_title($_POST['questitle']);
            $seperate_coma = '';
            foreach ($seperates as $seperate) {
                $conditions['or'][] = array('User.name LIKE' => "%$seperate%");
                $conditions['or'][] = array('User.username LIKE' => "%$seperate%");
            }
            $users = $this->QuestionCategory->find('all', array(
                'conditions' => $conditions,
                'fields' => array('DISTINCT (QuestionCategory.question_id),User.id,User.name,User.username', 'User.image,User.knows,COUNT(QuestionCategory.user_id) AS user'),
                'group' => 'QuestionCategory.user_id',
                'order' => 'user DESC',
                'limit' => 15));
            $this->set('users', $users);
        }
        $this->render('search');
    }

    public function message($id = FALSE) {
        $this->loadModel('MessageGroup');
        $this->loadModel('Message');

        $user = $this->Session->read('user_quesli');
        if (!empty($user)) {
            $empty = false;

            if ($this->request->is('post')) {
                $group = $this->MessageGroup->find('first', array('conditions' => array(
                        'OR' =>
                        array(
                            array('AND' => array(
                                    array('MessageGroup.user_send' => $user['User']['id']),
                                    array('MessageGroup.user_recieve' => $id)
                                )),
                            array('AND' => array(
                                    array('MessageGroup.user_send' => $id),
                                    array('MessageGroup.user_recieve' => $user['User']['id'])
                                )),
                        )
                )));
                if (isset($group) && !empty($group)) {
                    $this->request->data['Message']['user_send'] = $user['User']['id'];
                    $this->request->data['Message']['group_id'] = $group['MessageGroup']['id'];
                    $this->Message->create();
                    if ($this->Message->save($this->request->data)) {
                        
                    }
                } else {

                    $message_group['MessageGroup']['user_send'] = $user['User']['id'];
                    $message_group['MessageGroup']['user_recieve'] = $this->request->data['Message']['user_recieve'];

                    $this->MessageGroup->create();
                    if ($this->MessageGroup->save($message_group)) {
                        $group_id = $this->MessageGroup->getLastInsertId();
                        $this->request->data['Message']['user_send'] = $user['User']['id'];
                        $this->request->data['Message']['group_id'] = $group_id;
                        $this->Message->create();
                        if ($this->Message->save($this->request->data)) {
                            
                        }
                    }
                }
            }
            $message_all = $this->MessageGroup->find('all', array(
                'fields' => array('Sender.username,Sender.id,Sender.image,Receiver.username,Receiver.image,Receiver.id'),
                'conditions' => array('OR' =>
                    array(
                        array(array('MessageGroup.user_send' => $user['User']['id'])),
                        array(array('MessageGroup.user_recieve' => $user['User']['id'])),
                    )),
                'order' => 'MessageGroup.id DESC'
            ));
            if (isset($id) && !empty($id)) {

                $group = $this->MessageGroup->find('first', array('conditions' => array(
                        'OR' =>
                        array(
                            array('AND' => array(
                                    array('MessageGroup.user_send' => $user['User']['id']),
                                    array('MessageGroup.user_recieve' => $id)
                                )),
                            array('AND' => array(
                                    array('MessageGroup.user_send' => $id),
                                    array('MessageGroup.user_recieve' => $user['User']['id'])
                                )),
                        )
                )));

                if (!empty($group)) {
                    $messages = $this->Message->find('all', array('conditions' => array('Message.group_id' => $group['MessageGroup']['id']),
                        'order' => 'Message.id ASC'));
                    $empty = false;
                } else {
                    $messages = $this->Message->find('all', array(
                        'fields' => array('COUNT(Message.user_send) AS message_c,Sender.id,Sender.username,Sender.image,Message.created,Message.message'),
                        'conditions' => array('Message.seen' => 0, 'Message.user_recieve' => $user['User']['id']),
                        'group' => 'Sender.id',
                        'order' => 'Message.id ASC'));
                    $empty = true;
                }

                $this->Message->updateAll(array('Message.seen' => 1), array(
                    'Message.user_recieve' => $user['User']['id'],
                    'Message.group_id' => $group['MessageGroup']['id']));
                $unseen = false;
                $message_count = $this->Message->find('count', array(
                'conditions' => array('Message.user_recieve' => $user['User']['id'], 'Message.seen' => 0)));
            } else {
                $messages = $this->Message->find('all', array(
                    'fields' => array('COUNT(Message.user_send) AS message_c,Sender.id,Sender.username,Sender.image,Message.created,Message.message'),
                    'conditions' => array('Message.seen' => 0, 'Message.user_recieve' => $user['User']['id']),
                    'group' => 'Sender.id',
                    'order' => 'Message.id ASC'));
                $unseen = true;
            }
            $this->set(compact('user', 'messages', 'id', 'message_all', 'unseen','message_count'));
        }
    }

    public function add_new_cat_user() {
        $this->layout = $this->autoRender = false;
        $this->layout = false;
        $this->loadModel('Category');
        $this->request->onlyAllow('ajax');
        if (isset($_POST['add_new_cat']) && !empty($_POST['add_new_cat'])) {
            $check_first = $this->Category->find('first', array('fields' => array('Category.id', 'Category.name'),
                'recursive' => -1,
                'conditions' => array('MATCH(Category.name) AGAINST("' . $_POST['add_new_cat'] . '" IN BOOLEAN MODE)')));
            if (isset($check_first) && !empty($check_first['Category']['name'])) {
                $category_id = $check_first['Category']['id'];
            } else {
                $data['Category']['name'] = trim($_POST['add_new_cat']);
                $data['Category']['slug'] = strip_tags(trim($_POST['add_new_cat']));
                $data['Category']['slug'] = str_replace(' ', '-', $data['Category']['slug']);
                $this->loadModel('Category');
                $this->Category->Create();
                $this->Category->save($data);
                $category_id = $this->Category->getLastInsertId();
            }
        }
        if (isset($_POST['add_new_cat']) && !empty($_POST['add_new_cat'])) {
            $this->loadModel('UserCategory');
            $user = $this->Session->read('user_quesli');
            $user = $this->User->find('first', array('conditions' => array('User.id' => $user['User']['id'])));
            $this->loadModel('User');
            if ($user['User']['bio'] != "") {
                $old_bio = $user['User']['bio'] . " ,, " . $_POST['add_new_cat'] . "," . $category_id;
            } else {
                $old_bio = $_POST['add_new_cat'] . "," . $category_id;
            }
            $logo_update = $this->User->updateAll(array('User.bio' => "'" . $old_bio . "'"), array('User.id' => $user['User']['id']));
            $this->UserCategory->create();
            $user_cat['UserCategory']['user_id'] = $user['User']['id'];
            $user_cat['UserCategory']['category_id'] = $category_id;
            if ($this->UserCategory->save($user_cat)) {
                
            }
            if (!empty($tag)) {
                $this->set('categories', $tag);
            }
        }
        return $category_id;
    }

    public function search_cat() {
        $this->loadModel('Question');
        $this->loadModel('Category');
        $this->autoRender = false;
        $this->request->onlyAllow('ajax');
        $this->layout = $this->autoRender = false;
        if (isset($_POST['data_send']) && !empty($_POST['data_send'])) {
            $data = $_POST['data_send'];
            $seperates = $this->Question->spilt_title($_POST['data_send']);
            $seperate_coma = '';
            foreach ($seperates as $seperate) {
                $seperate_coma.="(+" . $seperate . "*) ";
            }
            $this->loadModel('Category');
            $tag = $this->Category->find('all', array(
                'fields' => array('Category.id', 'Category.name'),
                'conditions' => array("Category.name LIKE" => "%" . $data . "%"),
                //'conditions' => array("MATCH(Category.name) AGAINST('$data' IN BOOLEAN MODE)"),
                'recursive' => -1,
                'limit' => 10)
            );
            if (!empty($tag)) {
                $this->set('categories', $tag);
            }
        }
        $this->render('search_cat');
    }

    public function remove_skills() {
        $this->loadModel('UserCategory');
        $this->autoRender = false;
        $this->request->onlyAllow('ajax');
        $this->layout = $this->autoRender = false;
        if (isset($_POST['cat_id']) && !empty($_POST['cat_id'])) {
            $user = $this->Session->read('user_quesli');
            $user = $this->User->find('first', array('conditions' => array('User.id' => $user['User']['id'])));

            $this->loadModel('User');
            if ($user['User']['bio'] != "") {
                $explode = explode(',,', $user['User']['bio']);
                for ($i = 0; $i < count($explode); $i++) {
                    $sep = explode(',', $explode[$i]);
                    if ($sep[1] == $_POST['cat_id']) {
                        unset($explode[$i]);
                    }
                }
                $old_bio = implode(',,', $explode);
            } else {
                $old_bio = $_POST['cat_name'];
            }
            $logo_update = $this->User->updateAll(array('User.bio' => "'" . $old_bio . "'"), array('User.id' => $user['User']['id']));
            if ($this->UserCategory->deleteAll(array('UserCategory.user_id' => $user['User']['id'], 'UserCategory.category_id' => $_POST['cat_id']))) {
                
            }
        }
    }

    public function add_skills() {
        $this->loadModel('UserCategory');
        $this->autoRender = false;
        $this->request->onlyAllow('ajax');
        $this->layout = $this->autoRender = false;
        if (isset($_POST['cat_id']) && !empty($_POST['cat_id'])) {
            $user = $this->Session->read('user_quesli');
            $user = $this->User->find('first', array('conditions' => array('User.id' => $user['User']['id'])));
            $this->loadModel('User');
            if ($user['User']['bio'] != "") {
                $old_bio = $user['User']['bio'] . " ,, " . $_POST['cat_name'] . "," . $_POST['cat_id'];
            } else {
                $old_bio = $_POST['cat_name'] . "," . $_POST['cat_id'];
            }
            $logo_update = $this->User->updateAll(array('User.bio' => "'" . $old_bio . "'"), array('User.id' => $user['User']['id']));
            $this->UserCategory->create();
            $user_cat['UserCategory']['user_id'] = $user['User']['id'];
            $user_cat['UserCategory']['category_id'] = $_POST['cat_id'];
            if ($this->UserCategory->save($user_cat)) {
                
            }
            if (!empty($tag)) {
                $this->set('categories', $tag);
            }
        }
        $this->render('search_cat');
    }

    public function users_index() {
        $user = $this->Session->read('user_quesli');
        if ($user['User']['admin'] == 1) {
            //Configure::write('debug', 2);
            $this->loadModel('User');
            $this->paginate = array('fields' => array('User.id,User.name,User.gender,User.username,User.knows,User.email ,User.image,User.created'),
                'limit' => 15, 'order' => array('User.id'=>'DESC'));
            $this->set('users', $this->paginate('User'));
        } else {
            $this->redirect('/');
        }
    }

    public function index() {
        $this->redirect('/');
    }

    public function edit() {
        $user = $this->Session->read('user_quesli');
        $users = $this->User->findById($user['User']['id']);
        if ($this->request->is('post') || $this->request->is('put')) {
            $this->User->id = $user['User']['id'];

            if ($this->User->save($this->request->data)) {

                $users = $this->User->findById($this->request->data['User']['id']);
                $this->Session->delete('user_quesli');
                $this->Session->write('user_quesli', $users);
                $this->Session->setFlash(__('تم التعديل بنجاح'));
                $this->redirect(array('controller' => 'orders', 'action' => 'receipts'));
            } else {
                $this->Session->setFlash(__('لم يتم التعديل'));
            }
        }
        if (!$this->request->data) {
            $this->request->data = $users;
        }
    }

    public function profile() {
	//Configure::write('debug', 2);
        $this->loadModel('User');
        $session = $this->Session->read('user_quesli');
        if ($session) {
            $this->loadModel('Log');
            $this->loadModel('Question');
            $this->paginate = array(
                'fields' => array('Question.title', 'Question.slug,Question.id,Question.status'),
                'conditions' => array('Question.user_id' => $session['User']['id']),
                'limit' => 3, 'order' => 'Question.id DESC'
            );

            $this->set('user_questions', $this->paginate('Question'));
            if (!empty($session['User']['fb'])) {
                $user = $this->User->find('first', array('conditions' => array('User.fb' => trim($session['User']['fb']))));
            } else {
                $user = $this->User->findById($session['User']['id']);
            }
            $count = $this->Question->find('count', array('conditions' => array('Question.user_id' => $session['User']['id'])));

            $count_edit = $this->Log->find('count', array('order' => 'Log.question_id',
                'fields' => array('Log.question_id'),
                
                'conditions' => array('Log.user_id' => $session['User']['id'])));
          //  $count_edit = count($count_edit);
            $cound_edit_only = $count_edit - $count;
            $points = 0;
            $points = $count * 30;
            $points+=$count_edit * 10;

            $this->paginate = array(
                'fields' => array('Question.id,Question.title_update,Question.last,Question.category_id,Question.links,Question.last,Question.edited,Question.quality,Question.expand', 'Question.views,Question.created,Question.image,Question.title_update,Question.title', 'Question.slug,Question.id,Question.status'),
                'limit' => 6,
                'order' => 'Question.id DESC');
            $questions = $this->Paginator->paginate('Question');
            $ranking = $this->ranking($points);
            $points+=50;
            $this->set(compact('user', 'count', 'points', 'count_edit', 'ranking', 'cound_edit_only'));
        } else {
            $this->redirect('/');
        }
    }

   public function view($userid) {
        $this->loadModel('User');
        $this->loadModel('Question');
        $this->loadModel('Answer');
        $this->loadModel('Log');
        $this->paginate = array(
        'contain' => array('Question'),
            'limit' => 15,
            'group' => array('Question.id'),
            'fields' => array('Question.image_user,Answer.image_user,Question.locked,Question.last',
                'Answer.last_title,Answer.last_body,Answer.sort,Question.selected',
                'Question.views,Question.image,Question.status,Question.reference,Question.user_id',
                'Question.title_update,Answer.title_update,Answer.body_update,Question.title_update',
                'Answer.image', 'Answer.title', 'Question.id,Question.title,Question.slug',
                'Answer.id,Answer.body,User.username,User.id,User.image,User.gender'),
            'conditions' => array('Question.user_id' => $userid, 'Question.image <>' => NULL),
            'limit' => 9, 'order' => 'Question.id DESC'
        );
        $this->set('questions', $this->paginate('Answer'));
        $user = $this->User->findById($userid);
        $session = $user;
        $countur = (int) $user['User']['views'] + 1;
        $count = $this->Question->find('count', array('conditions' => array('Question.user_id' => $session['User']['id'])));
        $count_edit = $this->Log->find('all', array('order' => 'Log.question_id',
            'fields' => array('Log.question_id'),
            'conditions' => array('Log.user_id' => $session['User']['id'])));
        $count_edit = count($count_edit);
        $cound_edit_only = $count_edit - $count;
        $points = 0;
        $points = $count * 30;
        $points+=$count_edit * 10;
        $ranking = $this->ranking($points);
        $this->set(compact('user', 'count', 'points', 'count_edit', 'ranking', 'cound_edit_only'));
        $this->set('user', $user);
    }

    function logo() {
        $this->layout = $this->autoRender = false;
        $user = $this->Session->read('user_quesli');
        $this->loadModel('User');
        if (!empty($this->data)) {
            $this->User->create();
            $date = date("Y-m-d") . '_' . rand(0, 999);
            $name = $date . $this->data['Logo']['image']['name'];
            $tmp = $this->data['Logo']['image']['tmp_name'];
            $filePath = WWW_ROOT . DS . 'user/' . DS . $name;
            if (move_uploaded_file($tmp, $filePath)) {
                $logo_update = $this->User->updateAll(array('User.image' => "'" . $name . "'"), array('User.id' => $user['User']['id']));
                return $this->webroot . 'user' . DS . $name;
            }
        }
    }

    function update() {
        $this->layout = $this->autoRender = false;

        $user = $this->Session->read('user_quesli');
        $this->loadmodel('User');

        if (!empty($_POST['quesuserd'])) {
            if ($_POST['quesuserd'] == 'u') {
                $field = 'name';
            } elseif ($_POST['quesuserd'] == 'e') {
                $field = 'email';
            } elseif ($_POST['quesuserd'] == 'b') {
                $field = 'knows';
            }
            $data['User']['user_id'] = $user['User']['id'];
            $data['User']["'" . $field . "'"] = $_POST['quesuserv'];
            $this->User->create();
            $update = $this->User->updateAll(array('User.' . $field => "'" . $_POST['quesuserv'] . "'"), array('User.id' => $user['User']['id']));
        }
    }

    function add_profile() {
        $this->layout = $this->autoRender = false;

        $user = $this->Session->read('user_quesli');
        $this->loadmodel('User');

        if (!empty($_POST['quesuserd'])) {

            if ($_POST['quesuserd'] == 'u') {
                $field = 'name';
            } elseif ($_POST['quesuserd'] == 'e') {
                $field = 'email';
            } elseif ($_POST['quesuserd'] == 'b') {
                $field = 'knows';
            }

            $data['User']['user_id'] = $user['User']['id'];
            $data['User']["'" . $field . "'"] = $_POST['quesuserv'];
            $this->User->create();
            $update = $this->User->updateAll(array('User.' . $field => "'" . $_POST['quesuserv'] . "'"), array('User.id' => $user['User']['id']));
        }
    }
	  public function login() {
        $this->layout = false;
        $this->loadModel('User');
        $this->loadModel('Admin');
        $this->User->recursive = 0;
        if ($this->Session->check('user_quesli')) {
            $this->redirect('/');
        } elseif ($this->Session->check('admin_quesli')) {
            $this->redirect('/');
        } else {
            if ($this->request->query('code')) {
                Configure::write('debug', 2);
                // User login successful
                $fb_user = $this->Facebook->getUser();          # Returns facebook user_id
                if ($fb_user) {
                    $fb_user = $this->Facebook->api('/me');     # Returns user information
                    // We will varify if a local user exists first
                    $local_user = $this->User->find('first', array(
                        'conditions' => array('fb' => $fb_user['id'])
                    ));
                    // If exists, we will log them in
                    if ($local_user) {
                        $this->Cookie->write('user_quesli', $local_user, false, 94608000);
                        $this->Session->write('user_quesli', $local_user);
                        $this->redirect('/dashboard');
                    }

                    // Otherwise we ll add a new user (Registration)
                    else {
                        $username = (empty($fb_user['email'])) ? $fb_user['name'] : $fb_user['email'];

                        $email = (empty($fb_user['email'])) ? str_replace(' ', '_', $fb_user['name']) . '@speeli.com' : $fb_user['email'];
                        $data['User'] = array(
                            'username' => $username,
                            'name' => $fb_user['name'],
                            'fb' => $fb_user['id'],
                            'email' => $email,
                            'gender' => $fb_user['gender'], # Normally Unique
                            'password' => AuthComponent::password(uniqid(md5(mt_rand()))), # Set random password
                        );
                        $this->loadModel('User');
                        $this->User->create();
                        // You should change this part to include data validation
                        if ($this->User->save($data)) {
                            $user_id = $this->User->getLastInsertId();
                            $data['User']['id'] = $user_id;
                            $message = 'Welcome to Speeli  ' . $data['User']['name'] . '   :) <br>
                            Speeli is a website that summarizes anything into fast scan-able lists.<br>
                            Our mission is to help people find direct answers and summaries instead of wasting hours searching the internet or reading long text.<br>
                            <br>The ones who create speeli summaries are me and you. <br>
                            You can use Speeli to summarize anything by just clicking on the add Speeli Summary button.<br><br>
                            To find more about speeli you can check those summarized speeli posts: <a href="/articles/view/What-is-Speeli">What is Speeli?</a>  & 
                            <a href="/articles/view/The-guidelines-for-writing-a-Speeli-article"> Speeli Writing guidelines </a><br><br>
                            Feel free to reply back to me if you need any help<br>
                            looking forward for your contribution so that we can change the world together :)<br><br>
                            Farouk<br>
                            Senior community member <br>';
                            $message_group['MessageGroup']['user_send'] = 3;
                            $message_group['MessageGroup']['user_recieve'] = $user_id;
                            $this->loadModel('Message');
                            $this->loadModel('MessageGroup');
                            $this->MessageGroup->create();
                            if ($this->MessageGroup->save($message_group)) {
                                $group_id = $this->MessageGroup->getLastInsertId();
                                $this->request->data['Message']['user_send'] = 3;
                                $this->request->data['Message']['user_recieve'] = $user_id;
                                $this->request->data['Message']['message'] = $message;
                                $this->request->data['Message']['seen'] = 0;
                                $this->request->data['Message']['group_id'] = $group_id;
                                $this->Message->create();
                                if ($this->Message->save($this->request->data)) {
                                    
                                }
                            }
                            $this->Session->write('user_quesli', $user);
                            $this->Cookie->write('user_quesli', $user, true, time() + (86400 * 30));
                            $this->Session->read('user_quesli');
                            $this->redirect('/dashboard');
                        } else {
                            $this->redirect(array('controller' => 'users', 'action' => 'login#register'));
                        }
                    }
                } else {
                    $this->redirect(array('controller' => 'users', 'action' => 'login#register'));
                }
            } elseif ($this->request->is('post')) {
                if ($this->data['User']['register'] == 0) {
                    if ($this->Session->check('Auth.User')) {
                        $this->redirect('/');
                    }

                    // if we get the post information, try to authenticate
                    if ($this->request->is('post')) {
                        if ($this->Auth->login()) {
                            $this->Cookie->write('user_quesli', $this->Session->read('Auth'), false, 94608000);
                            $this->Session->write('user_quesli', $this->Session->read('Auth'));
                            
                            $this->redirect('/dashboard');
                        } else {
                            $this->Session->setFlash(__('Invalid username or password'));
                        }
                    }
                } else {
                    $password = AuthComponent::password($this->data['User']['password']);
                    $user = $this->User->find('first', array('conditions' => array('User.username' => $this->data['User']['username'], 'User.password' => $password)));

                    if (empty($user['User']['id'])) {
                        $this->User->create();
                        if ($this->User->save($this->request->data)) {
                            $password = AuthComponent::password($this->data['User']['password']);
                            $user = $this->User->find('first', array('conditions' => array('User.username' => $this->data['User']['username'], 'User.password' => $password)));

                            $message = 'Welcome to Speeli  ' . $user['User']['username'] . '   :) <br>
                            Speeli is a website that summarizes anything into fast scan-able lists.<br>
                            Our mission is to help people find direct answers and summaries instead of wasting hours searching the internet or reading long text.<br>
                            <br>The ones who create speeli summaries are me and you. <br>
                            You can use Speeli to summarize anything by just clicking on the add Speeli Summary button.<br><br>
                            To find more about speeli you can check those summarized speeli posts: <a href="/articles/view/What-is-Speeli">What is Speeli?</a>  & 
                            <a href="/articles/view/The-guidelines-for-writing-a-Speeli-article"> Speeli Writing guidelines </a><br><br>
                            Feel free to reply back to me if you need any help<br>
                            looking forward for your contribution so that we can change the world together :)<br><br>
                            Farouk<br>
                            Senior community member <br>';
                            $message_group['MessageGroup']['user_send'] = 3;
                            $message_group['MessageGroup']['user_recieve'] = $user['User']['id'];
                            $this->loadModel('Message');
                            $this->loadModel('MessageGroup');
                            $this->MessageGroup->create();
                            if ($this->MessageGroup->save($message_group)) {
                                $group_id = $this->MessageGroup->getLastInsertId();
                                $this->request->data['Message']['user_send'] = 3;
                                $this->request->data['Message']['user_recieve'] = $user['User']['id'];
                                $this->request->data['Message']['message'] = $message;
                                $this->request->data['Message']['seen'] = 0;
                                $this->request->data['Message']['group_id'] = $group_id;
                                $this->Message->create();
                                if ($this->Message->save($this->request->data)) {
                                    
                                }
                            }
                              $this->Cookie->write('user_quesli', $user, false, 94608000);
                        $this->Session->write('user_quesli', $user);
                            $this->redirect('/dashboard');
                        } else {
                            $this->redirect(array('controller' => 'users', 'action' => 'login#register'));
                        }
                    } else {
                        if (!empty($user['User']['id'])) {
                            if (isset($user)) {
                               $this->Cookie->write('user_quesli', $user, false, 94608000);
                        $this->Session->write('user_quesli', $user);

                                $this->redirect('/dashboard');
                            }
                        }
                    }
                }
            }
        }
    }
/* login before auth 30-10-2016
   public function login() {
        $this->layout = false;
        $this->loadModel('User');
        $this->loadModel('Admin');
        $this->User->recursive = 0;
        if ($this->Session->check('user_quesli')) {
            $this->redirect('/');
        } elseif ($this->Session->check('admin_quesli')) {
            $this->redirect('/');
        } else {
            if ($this->request->query('code')) {
                Configure::write('debug', 2);
                // User login successful
                $fb_user = $this->Facebook->getUser();          # Returns facebook user_id
                if ($fb_user) {
                    $fb_user = $this->Facebook->api('/me');     # Returns user information
                    // We will varify if a local user exists first
                    $local_user = $this->User->find('first', array(
                        'conditions' => array('fb' => $fb_user['id'])
                    ));
                    // If exists, we will log them in
                    if ($local_user) {
                        $this->Session->write('user_quesli', $local_user);
                             $this->Cookie->write('user_quesli',$local_user,true,time() + (86400 * 30));
                            $this->Session->read('user_quesli');
                       
                        $this->redirect('/dashboard');
                    }

                    // Otherwise we ll add a new user (Registration)
                    else {
                        $username = (empty($fb_user['email'])) ? $fb_user['name'] : $fb_user['email'];

                        $email = (empty($fb_user['email'])) ? str_replace(' ', '_', $fb_user['name']) . '@speeli.com' : $fb_user['email'];
                        $data['User'] = array(
                            'username' => $username,
                            'name' => $fb_user['name'],
                            'fb' => $fb_user['id'],
                            'email' => $email,
                            'gender' => $fb_user['gender'], # Normally Unique
                            'password' => AuthComponent::password(uniqid(md5(mt_rand()))), # Set random password
                        );
                        $this->loadModel('User');
                        $this->User->create();
                        // You should change this part to include data validation
                        if ($this->User->save($data)) {
                            $user_id = $this->User->getLastInsertId();
                            $data['User']['id'] = $user_id;
                            $message = 'Welcome to Speeli  ' . $data['User']['name'] . '   :) <br>
                            Speeli is a website that summarizes anything into fast scan-able lists.<br>
                            Our mission is to help people find direct answers and summaries instead of wasting hours searching the internet or reading long text.<br>
                            <br>The ones who create speeli summaries are me and you. <br>
                            You can use Speeli to summarize anything by just clicking on the add Speeli Summary button.<br><br>
                            To find more about speeli you can check those summarized speeli posts: <a href="/articles/view/What-is-Speeli">What is Speeli?</a>  & 
                            <a href="/articles/view/The-guidelines-for-writing-a-Speeli-article"> Speeli Writing guidelines </a><br><br>
                            Feel free to reply back to me if you need any help<br>
                            looking forward for your contribution so that we can change the world together :)<br><br>
                            Farouk<br>
                            Senior community member <br>';
                            $message_group['MessageGroup']['user_send'] = 3;
                            $message_group['MessageGroup']['user_recieve'] = $user_id;
                            $this->loadModel('Message');
                            $this->loadModel('MessageGroup');
                            $this->MessageGroup->create();
                            if ($this->MessageGroup->save($message_group)) {
                                $group_id = $this->MessageGroup->getLastInsertId();
                                $this->request->data['Message']['user_send'] = 3;
                                $this->request->data['Message']['user_recieve'] = $user_id;
                                $this->request->data['Message']['message'] = $message;
                                $this->request->data['Message']['seen'] = 0;
                                $this->request->data['Message']['group_id'] = $group_id;
                                $this->Message->create();
                                if ($this->Message->save($this->request->data)) {
                                    
                                }
                            }
                            $this->Session->write('user_quesli', $user);
                             $this->Cookie->write('user_quesli',$user,true,time() + (86400 * 30));
                            $this->Session->read('user_quesli');
                            $this->redirect('/dashboard');
                        } else {
                            $this->redirect(array('controller' => 'users', 'action' => 'login#register'));
                        }
                    }
                } else {
                    $this->redirect(array('controller' => 'users', 'action' => 'login#register'));
                }
            } elseif ($this->request->is('post')) {
                if ($this->data['User']['register'] == 0) {
                    $user = checka($this->data['User']['username'], $this->data['User']['password']);
                    if ($user) {
                        $this->Session->write('user_quesli', $user);
                             $this->Cookie->write('user_quesli',$user,true,time() + (86400 * 30));
                            $this->Session->read('user_quesli');
                        $this->redirect(array('action' => 'admin_index'));
                    }
                    if (!$user) {

                        $password = AuthComponent::password($this->data['User']['password']);
                        $user = $this->User->find('first', array('conditions' => array('User.username' => $this->data['User']['username'], 'User.password' => $password)));
                    }
                    if (!empty($user['User']['id'])) {
                        if (isset($user)) {
                            $this->Session->write('user_quesli', $user);
                             $this->Cookie->write('user_quesli',$user,true,time() + (86400 * 30));
                            $this->Session->read('user_quesli');
                           
                          
                            $this->redirect('http://www.speeli.com/dashboard');
                        }
                    }
                } else {
                    $password = AuthComponent::password($this->data['User']['password']);
                    $user = $this->User->find('first', array('conditions' => array('User.username' => $this->data['User']['username'], 'User.password' => $password)));

                    if (empty($user['User']['id'])) {
                        $this->User->create();
                        if ($this->User->save($this->request->data)) {
                            $password = AuthComponent::password($this->data['User']['password']);
                            $user = $this->User->find('first', array('conditions' => array('User.username' => $this->data['User']['username'], 'User.password' => $password)));

                            $message = 'Welcome to Speeli  ' . $user['User']['username'] . '   :) <br>
                            Speeli is a website that summarizes anything into fast scan-able lists.<br>
                            Our mission is to help people find direct answers and summaries instead of wasting hours searching the internet or reading long text.<br>
                            <br>The ones who create speeli summaries are me and you. <br>
                            You can use Speeli to summarize anything by just clicking on the add Speeli Summary button.<br><br>
                            To find more about speeli you can check those summarized speeli posts: <a href="/articles/view/What-is-Speeli">What is Speeli?</a>  & 
                            <a href="/articles/view/The-guidelines-for-writing-a-Speeli-article"> Speeli Writing guidelines </a><br><br>
                            Feel free to reply back to me if you need any help<br>
                            looking forward for your contribution so that we can change the world together :)<br><br>
                            Farouk<br>
                            Senior community member <br>';
                            $message_group['MessageGroup']['user_send'] = 3;
                            $message_group['MessageGroup']['user_recieve'] = $user['User']['id'];
                            $this->loadModel('Message');
                            $this->loadModel('MessageGroup');
                            $this->MessageGroup->create();
                            if ($this->MessageGroup->save($message_group)) {
                                $group_id = $this->MessageGroup->getLastInsertId();
                                $this->request->data['Message']['user_send'] = 3;
                                $this->request->data['Message']['user_recieve'] = $user['User']['id'];
                                $this->request->data['Message']['message'] = $message;
                                $this->request->data['Message']['seen'] = 0;
                                $this->request->data['Message']['group_id'] = $group_id;
                                $this->Message->create();
                                if ($this->Message->save($this->request->data)) {
                                    
                                }
                            }
                            $this->Session->write('user_quesli', $user);
                             $this->Cookie->write('user_quesli',$user,true,time() + (86400 * 30));
                            $this->Session->read('user_quesli');
                            $this->redirect('/dashboard');
                        } else {
                            $this->redirect(array('controller' => 'users', 'action' => 'login#register'));
                        }
                    } else {
                        if (!empty($user['User']['id'])) {
                            if (isset($user)) {
                               $this->Session->write('user_quesli', $user);
                             $this->Cookie->write('user_quesli',$user,true,time() + (86400 * 30));
                            $this->Session->read('user_quesli');

                                $this->redirect('/dashboard');
                            }
                        }
                    }
                }
            }
        }
    }*/
    public function admin_index() {
        $this->redirect('/');
    }

    public function register() {
        $this->layout = $this->autoRender = false;
        if ($this->request->is('post')) {
            debug($this->request->data);
            $this->User->create();
            if ($this->User->save($this->request->data)) {

                $this->Session->setFlash(__('Successfully registerd'));
                if (!$user) {
                    $password = AuthComponent::password($this->data['User']['password']);
                    $user = $this->User->find('first', array('conditions' => array('User.username' => $this->data['User']['username'], 'User.password' => $password)));
                }
                if (!empty($user['User']['id'])) {
                    if (isset($user)) {
                        $this->Session->write('user_quesli', $user);
                        $this->Session->read('user_quesli');
                        $this->redirect('/dashboard');
                    }
                }
            } else {
                $this->Session->setFlash(__('failed to register'));
            }
        }
    } 
/* before auth 30-10-2016
    public function logout() {
		 $this->Session->destroy();
       $this->Cookie->destroy();
        $this->redirect('/');
    }*/
	public function logout() {

        $this->Auth->logout();
        $this->Session->destroy();
        $this->Cookie->destroy();
        $this->redirect('/');
    }
    public function login_light(){
        
        $this->layout = false;
    }
public function signup_ajax() {
        $this->autoRender = false;
        $this->request->onlyAllow('ajax');
        $this->layout = $this->autoRender = false;
        if (isset($this->data['User'])) {
            $this->request->data['User']["username"] = $this->request->data['User']["email"] ;
           
            if (!empty($this->request->data['User']['password'])  && !empty($this->data['User']['email'])) {
                $password = AuthComponent::password($this->data['User']['password']);
                $user = $this->User->find(
                        'first', array('conditions' => array(
                        'OR' =>
                        array('User.username' => $this->data['User']['username'], 'User.email' => $this->data['User']['email'])
                    )
                        )
                );
                
                if (empty($user)) {
                    $this->User->create();
                    if ($this->User->save($this->request->data)) {
                        $password = AuthComponent::password($this->data['User']['password']);
                        $user = $this->User->find('first', array('conditions' => array('User.username' => $this->data['User']['username'], 'User.password' => $password)));

                        $message = 'Welcome to Speeli  ' . $user['User']['username'] . '   :) <br>
                            Speeli is a website that summarizes anything into fast scan-able lists.<br>
                            Our mission is to help people find direct answers and summaries instead of wasting hours searching the internet or reading long text.<br>
                            <br>The ones who create speeli summaries are me and you. <br>
                            You can use Speeli to summarize anything by just clicking on the add Speeli Summary button.<br><br>
                            To find more about speeli you can check those summarized speeli posts: <a href="/articles/view/What-is-Speeli">What is Speeli?</a>  & 
                            <a href="/articles/view/The-guidelines-for-writing-a-Speeli-article"> Speeli Writing guidelines </a><br><br>
                            Feel free to reply back to me if you need any help<br>
                            looking forward for your contribution so that we can change the world together :)<br><br>
                            Farouk<br>
                            Senior community member <br>';
                        $message_group['MessageGroup']['user_send'] = 3;
                        $message_group['MessageGroup']['user_recieve'] = $user['User']['id'];
                        $this->loadModel('Message');
                        $this->loadModel('MessageGroup');
                        $this->MessageGroup->create();
                        if ($this->MessageGroup->save($message_group)) {
                            $group_id = $this->MessageGroup->getLastInsertId();
                            $this->request->data['Message']['user_send'] = 3;
                            $this->request->data['Message']['user_recieve'] = $user['User']['id'];
                            $this->request->data['Message']['message'] = $message;
                            $this->request->data['Message']['seen'] = 0;
                            $this->request->data['Message']['group_id'] = $group_id;
                            $this->Message->create();
                            if ($this->Message->save($this->request->data)) {
                                
                            }
                        }
                        $this->Session->write('user_quesli', $user);
                        setcookie('user_quesli', json_encode($user), time() + (86400 * 30), "/");
                        return "1";
                    } else {
                        return '<b style="color:red;">an error occured please try again laters</b>';
                    }
                } else {
                   if ($user['User']['username'] == $this->request->data['User']['username']) {
                        return '<b style="color:red;">The Username is already in use</b>';
                    } elseif ($user['User']['email'] == $this->request->data['User']['email']) {
                        return '<b style="color:red;">The Email is already in use</b>';
                    }else{
						 return '<b style="color:red;">The Email Or Username is already in use</b>';
					}
                }
            }
        }
    }
	 public function continue_speeli() {
        $flag = $this->Cookie->read('quesli_flag');
        $this->loadModel('User');
        if ($this->User->updateAll(array('User.flag_agreement' => 1), array('User.id' => $flag))) {

            $user = $this->User->find('first', array('conditions' => array('User.id' => $flag)));
            $this->Cookie->write('user_quesli', $local_user, false, 94608000);
            $this->Session->write('user_quesli', $user);
            $this->redirect('/dashboard');
        }
    }

    public function delete_account() {
        $this->loadModel('User');
        $flag = $this->Cookie->read('quesli_flag');
        if ($this->User->deleteAll(array('User.id' => $flag))) {
            $this->redirect('/');
        }
    }

    public function login_ajax() {
        $this->autoRender = false;
        $this->request->onlyAllow('ajax');
        $this->layout = $this->autoRender = false;
        if (isset($this->data['User'])) {
            if (!empty($this->data['User']['password']) && !empty($this->data['User']['username'])) {
                $password = AuthComponent::password($this->data['User']['password']);

                $user = $this->User->find('first', array('fields'=>array('User.*'),'conditions' => array('User.username' => $this->data['User']['username'],
                        'User.password' => $password)));
               
                if (!empty($user['User']['id'])) {
                    if ($user['User']['flag_agreement'] == 0) {
                        $this->Cookie->write('quesli_flag', $user['User']['id'], false, 10000);
                        return "2";
                    } elseif ($user['User']['flag_agreement'] == 1) {
                        if ($this->Auth->login()) {
                            $this->Cookie->write('user_quesli', $this->Session->read('Auth'), false, 94608000);
                            $this->Session->write('user_quesli', $this->Session->read('Auth'));

                            return "1";
                        } else {
                            return '<b style="color:red;">Error occured</b>';
                        }
                    }
                } else {

                    return '<b style="color:red;">Username or Passsword is incorrect</b>';
                }
            }
        }
    }

}
?>