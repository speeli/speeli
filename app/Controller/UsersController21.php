<?php 
class UsersController extends AppController {
 	public $name = 'Users';
     public $components = array('Paginator', 'RequestHandler','Email','Session');
    public $helpers = array('Js', 'Paginator','Html','Form','Session');
/**
 * This controller does not use a model
 *
 * @var array
 */
	public $uses = array();
   
     
   public function beforeFilter() {
    parent::beforeFilter();
    // Allow users to register and logout.
   
}

    public function index(){
           $this->loadModel('Category');
           $this->Category->recursive =-1;
           $categories=$this->Category->find('all');
           $this->loadModel('Data');
           $data=$this->Data->find('first');
           $this->set(compact('categories','data'));
    }
     public function edit() {
         $user=$this->Session->read('user_quesli');
         $users = $this->User->findById($user['User']['id']);
            if ($this->request->is('post') || $this->request->is('put')) {
                $this->User->id = $user['User']['id'];
                
                if ($this->User->save($this->request->data)) {
                    
                    $users = $this->User->findById($this->request->data['User']['id']);
                    $this->Session->delete('user_quesli');
                    $this->Session->write('user_quesli',$users);
                    $this->Session->setFlash(__('تم التعديل بنجاح'));
                    $this->redirect(array('controller'=>'orders','action' =>'receipts'));  
                }else{
                    $this->Session->setFlash(__('لم يتم التعديل'));
                }
            }
             if (!$this->request->data) {
                $this->request->data = $users;
            }
    }
    public function profile() {
           $this->loadModel('User');
            $session=$this->Session->read('user_quesli');
            $this->loadModel('Question');
               $user_questions = $this->Question->find('all', array('fields'=>array('Question.title','Question.slug,Question.id,Question.status'),
               'conditions'=>array('Question.user_id'=>$session['User']['id']),     
               'limit' => 40,'order'=>array('Question.id DESC')));
            $user = $this->User->findById($session['User']['id']);
           $this->set('user_questions',$user_questions);
            $this->set('user',$user);
    }
       public function view($userid) {
           $this->loadModel('User');
            $session=$this->Session->read('user_quesli');
            $this->loadModel('Question');
           $user_questions = $this->Question->find('all', array('fields'=>array('Question.title','Question.slug,Question.id,Question.status'),
               'conditions'=>array('Question.user_id'=>$userid),     
               'limit' => 40,'order'=>array('Question.id DESC')));
            $user = $this->User->findById($userid);
           $this->set('user_questions',$user_questions);
            $this->set('user',$user);
    }
      function logo(){
        $this->layout = $this->autoRender = false;
           $user=$this->Session->read('user_quesli');
             $this->loadModel('User');
        if(!empty($this->data)){
            $this->User->create();
            $date=date("Y-m-d").'_'.rand(0,999);
           $name=$date.$this->data['Logo']['image']['name'];
           $tmp=$this->data['Logo']['image']['tmp_name'];
           $filePath = WWW_ROOT . DS . 'user/'.DS.$name  ;  
           if(move_uploaded_file($tmp, $filePath)){
            $logo_update = $this->User->updateAll(array('User.image' =>"'". $name."'"), array('User.id' =>$user['User']['id']));
            return   $this->webroot.DS.'user'.DS.$name;
           }
        }
    }
    function update(){
        $this->layout = $this->autoRender = false;
        
         $user=$this->Session->read('user_quesli');
      $this->loadmodel('User');
         
         if (!empty($_POST['quesuserd'])) {
             if($_POST['quesuserd']=='u'){
                 $field='name';
             }elseif($_POST['quesuserd']=='e'){
                 $field='email';
             }elseif($_POST['quesuserd']=='b'){
                 $field='bio';
             }
             $data['User']['user_id']=$user['User']['id'];
             $data['User']["'".$field."'"]=$_POST['quesuserv'];
            $this->User->create();
            $update = $this->User->updateAll(array('User.'.$field =>"'". $_POST['quesuserv']."'"), array('User.id' =>$user['User']['id']));
        } 
    }
    function add_profile(){
        $this->layout = $this->autoRender = false;
        
         $user=$this->Session->read('user_quesli');
      $this->loadmodel('User');
         
         if (!empty($_POST['quesuserd'])) {
           
             if($_POST['quesuserd']=='u'){
                 $field='name';
             }elseif($_POST['quesuserd']=='e'){
                 $field='email';
             }elseif($_POST['quesuserd']=='b'){
                  $field='bio';
             }
            
             $data['User']['user_id']=$user['User']['id'];
             $data['User']["'".$field."'"]=$_POST['quesuserv'];
            $this->User->create();
            $update = $this->User->updateAll(array('User.'.$field =>"'". $_POST['quesuserv']."'"), array('User.id' =>$user['User']['id']));
        } 
    } public function login() {
         $this->layout=false;
        $this->loadModel('User');
        $this->loadModel('Admin');
         $this->User->recursive =0;
        if ($this->Session->check('user_quesli')) {
            $this->redirect(array('action' => 'index'));  
        } elseif ($this->Session->check('admin_quesli')) {
            $this->redirect(array('action' => 'admin_index'));  
        } else {
            if ($this->request->is('post')) {
              
                $user = checka($this->data['User']['username'], $this->data['User']['password']);
                if ($user) {          
                $this->Session->write('admin_quesli', $user['User']);
                $this->redirect(array('action' => 'admin_index'));  
                }
                if (!$user) {
                    $password=AuthComponent::password($this->data['User']['password']);
                    $user = $this->User->find('first', array('conditions' => array('User.username' => $this->data['User']['username'], 'User.password' => $password)));
                }           
                if (!empty($user['User']['id'])) {
                    if (isset($user)) {
                       $this->Session->write('user_quesli', $user);
                       $this->Session->read('user_quesli');
                            $this->redirect(array('action' => 'profile'));  
                    }
                } else {
                 
                }
            }
        }
    }

    
    public function admin_index(){
        if($this->Session->check('admin_quesli')){
            
        }else{
            $this->redirect(array('controller'=>'users','action' => 'index')); 
        }
    }
    public function register()
        {
        if ($this->request->is('post')) {
                 
            $this->User->create();
            if ($this->User->save($this->request->data)) {
                
                $this->Session->setFlash(__('Successfully registerd'));
                   if (!$user) {
                    $password=AuthComponent::password($this->data['User']['password']);
                    $user = $this->User->find('first', array('conditions' => array('User.username' => $this->data['User']['username'], 'User.password' => $password)));
                }           
                if (!empty($user['User']['id'])) {
                    if (isset($user)) {
                       $this->Session->write('user_quesli', $user);
                       $this->Session->read('user_quesli');
                            $this->redirect(array('action' => 'profile'));  
                    }
                }
            } else {
                $this->Session->setFlash(__('failed to register'));
            }  
        }
        }
  public function logout() {
        $this->Session->destroy();
        $this->redirect('/');
    }
    function do_operation() {
        $ids = $this->params['data']['chk'];
        $operation = $this->params['data']['operation'];
        $user = $this->User->read(null, $ids[0]);
            if ($this->User->deleteAll(array('User.id' => $ids))) 
            {
            $this->redirect(array('action' => 'index'));
                $this->setFlash(__('تم المسح', true), 'alert alert-success');
            } else {
                $this->redirect(array('action' => 'index'));
                $this->setFlash(__('لم تتم عملية المسح الرجاء المحاولة مرة اخرى', true), 'alert alert-error');
            }
   
    }  
   
}
?>