<?php

/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {

     public $components = array(
        'Flash', 'Session', 'Cookie',
        'Auth' => array(
            'loginRedirect' => array('controller' => 'users', 'action' => 'admin_index'),
            'logoutRedirect' => array('controller' => 'users', 'action' => 'login'),
            'authError' => 'You must be logged in to view this page.',
            'loginError' => 'Invalid Username or Password entered, please try again.'
    ));
    
    public $helpers = array('Html' => array('className' => 'MyHtml'));
    public function beforeFilter() {
    ini_set('memory_limit', '-1');
        $this->set('check_cookie', $this->Cookie->check('and_s'));
        $this->Auth->allow();
        if ($this->Auth->user('id')) {
            $this->set('user_quesli', $this->Session->read('Auth'));
        } elseif ($this->Cookie->check('user_quesli')) {
            $this->Session->write('user_quesli', $this->Cookie->read('user_quesli'));
            $this->set('user_quesli', $this->Session->read('Auth'));
        }
        if ($this->Session->read('user_quesli')['User']['admin'] == 1) {
            $this->set('admin', true);
        }
        App::import('Vendor', 'facebook-php-sdk-master/src/facebook');
        if (isset($_SERVER) && isset($_SERVER['SERVER_NAME'])) {
//use live facebook config
            $this->Facebook = new Facebook(array(
                'appId' => '835557989856782',
                'secret' => '105f5d52f045b0e806f1360e14a1141b',
                'fileUpload' => true
            ));
        } else {
//in development mode use local app local app
            $this->Facebook = new Facebook(array(
                'appId' => '220578118121406',
                'secret' => '2188fc10183358a1b6f6ec11d66f9b11',
                'fileUpload' => true
            ));
        }
        $messages = $this->check_message();
        $this->set('message_count', $messages);
    }
	
/* before auth 30-10-2016
    public function beforeFilter() {
        $allowed=array('/users/logout');
	
        if (!in_array($this->here, $allowed)){
        
        
         if ($this->Cookie->check('user_quesli')) {

            $this->Session->write('user_quesli', $this->Cookie->read('user_quesli'));
            $user_speeli = $this->Session->read('user_quesli');
            
            if ($user_speeli['User']['admin'] == 1) {
                $this->set('admin', true);
            }
        }
        }
        App::import('Vendor', 'facebook-php-sdk-master/src/facebook');
        if (isset($_SERVER) && isset($_SERVER['SERVER_NAME'])) {
//use live facebook config
            $this->Facebook = new Facebook(array(
                'appId' => '835557989856782',
                'secret' => '105f5d52f045b0e806f1360e14a1141b',
                'fileUpload' => true
            ));
        } else {
//in development mode use local app local app
            $this->Facebook = new Facebook(array(
                'appId' => '220578118121406',
                'secret' => '2188fc10183358a1b6f6ec11d66f9b11',
                'fileUpload' => true
            ));
        }
        $messages = $this->check_message();
        $this->set('message_count', $messages);
    }
*/
    public function beforeRender() {
        parent::beforeRender();
      //  $this->set('fb_login_url', $this->Facebook->getLoginUrl(array('redirect_uri' => Router::url(array('controller' => 'users', 'action' => 'login'), true))));
    }

    function get_client_ip_server() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if (getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if (getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if (getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if (getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if (getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    /* function get_client_ip_server() {
      $client = @$_SERVER['HTTP_CLIENT_IP'];
      $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
      $remote = $_SERVER['REMOTE_ADDR'];
      $ip = '';
      if (filter_var($client, FILTER_VALIDATE_IP)) {
      $ip = $client;
      } elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
      $ip = $forward;
      } else {
      $ip = $remote;
      }

      return $ip;
      } */

    function check_message() {
        $this->loadModel('Message');
        $user = $this->Session->read('user_quesli');
        if (!empty($user)) {
            $messages = $this->Message->find('count', array(
                'conditions' => array('Message.user_recieve' => $user['User']['id'], 'Message.seen' => 0)
            ));
            return $messages;
        }
    }
	function match_related($title, $string = false) {
        $input = trim($title);
        $input = preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', $input);
        $replace = array(
            'a', 'an', 'of', 'off', 'at','than',
            'how', 'why', 'whose','when','what',
            'you','he','she',
            'is', 'on', 'in','so','popular','Popular',
            'the', 'these', 'those', 'there', 'can', 'could', "couldn't", 'would', 'will', "won't", "wouldn't", 'shall', 'should');
        $explode = explode(' ', $input);
        foreach ($explode as $key => $value) {
            if (in_array(" ", $replace)) {
                unset($explode[$key]);
            }
            if (in_array(strtolower($value), $replace)) {
                unset($explode[$key]);
            }
        }
            return $implode = implode(" ", $explode);
    }
    function spilt_title($title, $string = false) {
        $input = trim($title);

        $input = preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', $input);

        $replace = array(
            'a', 'an', 'of', 'off', 'at',
            'good', 'bad',
            'how', 'why', 'whose', 'when',
            'what', 'is', 'on', 'in', 'the', 'these', 'those', 'there', 'can', 'could', "could't", 'would', 'will', "won't", "wouldn't", 'shall', 'should');
        $explode = explode(' ', $input);
        foreach ($explode as $key => $value) {
            if (in_array(" ", $replace)) {
                unset($explode[$key]);
            }
            if (in_array($value, $replace)) {
                unset($explode[$key]);
            }
        }
        if ($string == 'string') {
            return $implode = implode(" ", $explode);
        } else {
            return $explode;
        }
    }
	public function upload_image($image = null, $tblname = null, $fieldname = null, $filename = null, $place = null) {
        if ($tblname == NULL) {
            $picture = $image;
            $explode = explode('.', $picture['name'][0]);
            $fileext = $explode[1];
            $date = date("Y-m-d") . '_' . rand(0, 99999);
            $name = $date . "." . $explode[1];
            $filePath = WWW_ROOT . $place . DS . $name;
            if (move_uploaded_file($picture['tmp_name'][0], $filePath)) {
                return $name;
            } else {
                return ' ';
            }
        } else {
            if ($image[$tblname][$fieldname]['size'] > 0) {
                str_replace(' ', '_', $image[$tblname][$fieldname]['name']);
                $explode = explode('.', $image[$tblname][$fieldname]['name']);
                $date = date("Y-m-d") . '_' . rand(0, 99999);
                $name = $date . "." . $explode[1];
                $tmp = $image[$tblname][$fieldname]['tmp_name'];
                $filePath = WWW_ROOT . $place . DS . $name;
                if (move_uploaded_file($tmp, $filePath)) {
                    return $name;
                } else {
                    return ' ';
                }
            }
        }
    }

}
