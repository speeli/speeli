<style>
    .toggle-area{
        display: none;
    }
    .gray-tone{
        color: rgba(136, 136, 136, 0.88);
    }
    .hbuilt h2{
        font-weight: 200;
        font-size: 18px;
        color:#22A39F;
    }
    .hbuilt {
        color:black;
    }
    .forum-box{
        color:black;
    }
    h2.page-header{
        color:#22A39F;
    }
    #profile_img{
        min-width: 0;
    }
</style>
<?php
$title = str_replace('&nbsp;', ' ', $answers[0]['Question']['title']);
$title = str_replace('nbsp', ' ', $title);
$title = str_replace('%', ' ', $title);

$this->set("page_title", $title);

if ($answers[0]['Question']['status'] == 0) {
    $this->set("meta_robots", "noindex");
} else {
    $this->set("meta_robots", "index");
}
$this->set("meta_description", $answers[0]['Question']['title']);
?>
<?php
$admin = false;
if ($this->Session->check('user_quesli')) {
    $user = $this->Session->read('user_quesli');
    if ($user['User']['admin'] == 1) {
        $admin = true;
    }
}
?>
<?php if (isset($answers) && !empty($answers)) { ?>
    <div class="row">


        <!-- Add the extra clearfix for only the required viewport -->
        <div  class="clearfix visible-xs-block"></div>
        <br class="clear"><br class="clear">


        <div class="row ">
            <div class="col-md-2 flex-column ">
            </div>
            <div class="col-md-8  ">
                <!-- The time line -->
                <div class="hpanel forum-box">
                    <div class="panel-heading">
                        <?php
                        if (!empty($answers[0]['Question']['title_update'])) {
                            $explode_title = explode(',,,', $answers[0]['Question']['title_update']);
                            $last = end($explode_title);
                            $title = end(unserialize(base64_decode($last)));
                        } else {
                            $title = $answers[0]['Question']['title'];
                        }
                        ?>
                        <center>  
                            <?php if ($admin) { ?>

                                <div class="fa fa-toggle-down "><br class="clear">
                                    <div class="toggle-area">
                                        <div style='float:left; margin:0.5em;'ques-art-a='<?php echo $answers[0]['Question']['id']; ?>' target='a' class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span></div>
                                        <?php
                                        if ($answers[0]['Question']['status'] == 0) {
                                            $toggle = 'glyphicon-eye-close';
                                            $btn = 'btn-block';
                                            $word = 'show';
                                            $status = 1;
                                        } else {
                                            $toggle = 'glyphicon-eye-open';
                                            $btn = 'btn-block';
                                            $word = 'hide';
                                            $status = 0;
                                        }
                                        ?>
                                        <div status='<?php echo $status; ?>' ques-art-a='<?php echo $answers[0]['Question']['id']; ?>' target='a' class="btn toggle <?php echo $btn; ?>"><span class="glyphicon <?php echo $toggle; ?>"><?php echo $word ?></span></div>
                                    </div>
                                </div>


                                <br class="clear">
                                <?php
                                if ($answers[0]['Question']['selected'] == 0) {
                                    $btn = 'btn-success';
                                    $word = 'show in home page';
                                    $status = 1;
                                } else {
                                    $btn = 'btn-warining';
                                    $word = 'remove from home page';
                                    $status = 0;
                                }
                                ?>

                                <div  slug='<?php echo $answers[0]['Question']['slug'] ?>' question_id='<?php echo $answers[0]['Question']['id'] ?>'
                                      title='<?php echo $title; ?>' 
                                      article_image='<?php echo $answers[0]['Question']['image'] ?>'
                                      status='<?php echo $status; ?>' class="btn-select-home btn <?php echo $btn; ?>"><?php echo $word; ?></div>
                                  <?php } ?>

                            <br class="clear">
                            <h2 class="page-header"> 
                                <b><?php echo $title; ?></b> </h2>
                            <a class='right gray-tone' href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'edit', $answers[0]['Question']['slug'])); ?>">
                                <span class="glyphicon glyphicon-edit gray-tone"></span> Edit article</a>
                            <br class="clear">
                            <?php if ($answers[0]['Question']['image'] != NULL) { ?>

                                <img id='profile_img' src="<?php echo $this->webroot . 'question/' . $answers[0]['Question']['id'] . '/' . $answers[0]['Question']['image']; ?>" alt="..." class="img-responsive">
                            <?php } ?>
                        </center>

                        <br class="clear">
                    </div>

                    <?php
                    $i = 1;
                    foreach ($answers as $answer) {
                        if (!empty($answer['Answer']['title_update'])) {
                            $explode_title = explode(',,,', $answer['Answer']['title_update']);
                            $last = end($explode_title);
                            $title = end(unserialize(base64_decode($last)));
                        } else {
                            $title = $answer['Answer']['title'];
                        }
                        if (!empty($answer['Answer']['body_update'])) {
                            $explode_body = explode(',,,', $answer['Answer']['body_update']);
                            $last = end($explode_body);
                            $body = end(unserialize(base64_decode($last)));
                            $body = trim($body);
                            $body = rtrim($body, ' ');
                            $body = rtrim($body, '.');
                            $body.=" .";
                        } else {
                            $body = $answer['Answer']['body'];
                            $body = trim($body);
                            $body = rtrim($body, ' ');
                            $body = rtrim($body, '.');
                            $body.=" .";
                        }
                        ?>

                        <div class="hpanel hblue">
                            <?php if ($admin) { ?>
                                <div class="fa fa-toggle-down"><br class="clear">
                                    <div class="toggle-area">
                                        <div style='float:left; margin:0.5em;' ques-art-a='<?php echo $answer['Answer']['id']; ?>' target='p' class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span></div>
                                        <?php
                                        if ($answer['Answer']['status'] == 0) {
                                            $toggle = 'glyphicon-eye-close';
                                            $btn = 'btn-block';
                                            $word = 'show';
                                            $status = 1;
                                        } else {
                                            $toggle = 'glyphicon-eye-open';
                                            $btn = 'btn-block';
                                            $word = 'hide';
                                            $status = 0;
                                        }
                                        ?>
                                        <div status='<?php echo $status; ?>' ques-art-a='<?php echo $answer['Answer']['id']; ?>' target='p' class="btn toggle <?php echo $btn; ?>"><span class="glyphicon <?php echo $toggle; ?>"><?php echo $word; ?></span></div>
                                    </div>
                                </div>
                            <?php } ?>

                            <div class="panel-heading hbuilt">
                                <h2 ques-ans-id="<?php echo $answer['Answer']['id']; ?>" > <b class="left"><?php echo $i . " )  "; ?> </b><b><?php echo $title; ?></b></h2>

                            </div>
                            <br class="clear">
                            <div class="panel-body" >
                                <?php if ($answer['Answer']['image'] != NULL) { ?>

                                    <img id='profile_img' src="<?php echo $this->webroot . 'question/' . $answers[0]['Question']['id'] . '/' . $answer['Answer']['image']; ?>" alt="..." class="img-thumbnail">
                                <?php } ?>
                                <p ques-ans-id="<?php echo $answer['Answer']['id']; ?>" ><?php echo $body; ?></p>
                            </div>

                        </div>
                        <?php
                        $i++;
                    }
                    ?>
                    <!-- timeline time label -->
                    <!-- END timeline item -->
                    <!-- timeline item -->
                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->


        <br class="clear">
        <div class="container">
            <div class="row">
                <div class="box effect7 col-md-4 col-xs-4 col-lg-4 float-none">
                    <div class="panel with-nav-tabs panel-default">
                        <div class="panel-heading">
                            related Speelis
                        </div>
                        <div class="panel-body-larger">
                            <ul class="list-unstyled" >

                                <?php
                                foreach ($categories as $category) {
                                    if ($category['Question']['slug']) {
                                        ?>
                                        <li>
                                            <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', trim($category['Question']['slug']))); ?>"><i class="fa fa-tag"></i> <?php echo $category['Question']['title']; ?></a></li>
                                        <?php
                                    }
                                }
                                ?> 
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="box effect7 col-md-4 col-xs-4 col-lg-4 float-none">
                    <div class="panel with-nav-tabs panel-default">
                        <div class="panel-heading">
                            <ul class="nav nav-tabs">

                                <li class="active"><a href="#tab1default" data-toggle="tab">Contributors</a></li>
                                <li><a href="#tab2default" data-toggle="tab">References</a></li>
                                <li><a href="#tab3default" data-toggle="tab">Topics</a></li>
                            </ul>
                        </div>
                        <div class="panel-body-larger">
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="tab1default">

                                    <ul class="list-unstyled" >


                                        <?php
                                        $j = 1;
                                        $u = 0;
                                        $n = 1;
                                        $answer_writers = array();
                                        $user_writers = array();
                                        $annonymus = 'Anonymous wrote point(s) No ( ';

                                        foreach ($answers as $answer) {
                                            if ($answer['User']['username']) {
                                                if (array_key_exists($answer['User']['id'], $user_writers)) {
                                                    $user_writers[$answer['User']['id']].= $j . " ,";
                                                } else {
                                                    $user_writers[$answer['User']['id']] = $answer['User']['username'] . " wrote point(s) No : ( " . $j . " ,";
                                                }
                                            } else {
                                                $annonymus.= $j . " ,";
                                            }
                                            $j++;
                                        }
                                        if ($annonymus === 'Anonymous wrote point(s) No ( ') {
                                            $annonymus = NULL;
                                        } else {
                                            $annonymus = rtrim($annonymus, ",");
                                            $annonymus.=" )";
                                        }


                                        foreach ($user_writers as $key => $user_writer) {
                                            $user_writer = rtrim($user_writer, ',');
                                            ?>
                                            <li>

                                                <h5>
                                                    <a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'view', $key)); ?>"><i class="fa fa-user"></i> <?php echo "<b>" . $user_writer . " ) </b>"; ?></a>

                                                </h5>   
                                            </li>
                                        <?php } if ($annonymus) { ?>

                                            <li><h5><a href="#"><i class="fa fa-user"></i> <?php echo $annonymus; ?></a>
                                                </h5> </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                                <div class="tab-pane fade" id="tab2default">
                                    <ul class="list-unstyled" >

                                        <?php

                                        function validate_host($url) {
                                            $pattern = "/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?/";
                                            if (!preg_match($pattern, $url)) {
                                                return false;
                                            } else {
                                                if (preg_match("/http:/", $url)) {
                                                    
                                                } else {
                                                    $url = 'http://' . $url;
                                                }
                                                return $url;
                                            }
                                        }

                                        $refs = explode(',,,,', $answers[0]['Question']['reference']);
                                        foreach ($refs as $ref) {
                                            $ref_url = explode(',,', $ref);
                                            if (!empty($ref_url[1]) && $ref_url[1] != ' ') {
                                                $ref_url[1] = validate_host($ref_url[1]);
                                                if (validate_host($ref_url[1])) {
                                                    ?>
                                                    <li>
                                                        <a target="_blank" href="<?php echo $ref_url[1]; ?>"><i class="fa fa-certificate"></i> <?php echo $ref_url[0]; ?></a>
                                                    </li>
                                                    <?php
                                                }
                                            }
                                        }
                                        ?> 
                                    </ul>
                                </div>
                                <div class="tab-pane fade" id="tab3default">
                                    <ul class="list-unstyled" >
                                        <?php foreach ($tags as $tag) { ?>
                                            <li><a href="<?php echo Router::url(array('controller' => 'categories', 'action' => 'view', $tag['Category']['slug'])); ?>"><i class="fa fa-tag"></i><?php echo $tag['Category']['name']; ?></a></li>
                                                <?php } ?>
                                    </ul>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <br/>
    </div>
<?php } ?>  
<?php
echo $this->Html->script(array('jquery.nicefileinput.min'));
?>
<?php
echo $this->Html->css(array('sweet-alert'));
echo $this->Html->script(array('sweet-alert', 'jquery.animate-colors'));
?>
<script>
    $('document').ready(function () {
        $('.fa-toggle-down').click(function () {
            $(this).children('.toggle-area').slideToggle('slow', 'swing');
        });
        $('.btn-danger').click(function () {
            target = $(this).attr('target');
            ques_art = $(this).attr('ques-art-a');
            box1 = $(this).parent();
            box2 = $(box1).parent();
            console.log(ques_art);

            swal({
                title: "Confirm Delete?",
                type: "warning", showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Delete",
                cancelButtonText: "Cancel",
                closeOnConfirm: false, closeOnCancel: false
            },
            function (isConfirm) {
                if (isConfirm) {
                    $.post("<?php echo Router::url(array('controller' => 'articles', 'action' => 'delete_article')); ?>",
                            {
                                target: target,
                                ques_art: ques_art
                            },
                    function (data) {
                        swal("Delete");
                        if (target == 'a') {
                            window.location = "<?php echo $this->Html->url(array('controller' => 'homes', 'action' => 'index')); ?>";
                            return false;
                        } else {
                            box3 = $(box2).parent().slideToggle();
                        }

                    });
                } else {
                    swal("Canceling", ")", "error");

                }
            });
        });
        $('.toggle').click(function () {
            target = $(this).attr('target');
            status = $(this).attr('status');
            ques_art = $(this).attr('ques-art-a');
            console.log(ques_art);
            console.log(status);
            box1 = $(this).parent();
            box2 = $(box1).parent();


            $.post("<?php echo Router::url(array('controller' => 'articles', 'action' => 'update_status')); ?>",
                    {
                        target: target,
                        ques_art: ques_art,
                        status: status
                    },
            function (data) {

                if (target == 'a') {

                    return false;
                } else {

                }

            });
        })
        $('.btn-select-home').click(function () {
            status = $(this).attr('status');
            if (status == 1) {
                  $(this).removeClass('btn-success');
            $(this).addClass('btn-warning');
            $(this).html('remove home page');
            $(this).attr('status',0);
            }else{
              $(this).attr('status',1);
                       $(this).removeClass('btn-warning');
            $(this).addClass('btn-success');
            $(this).html('show in home page');
          
            }
            slug = $(this).attr('slug');
            question_id = $(this).attr('question_id');
            title = $(this).attr('title');
            article_image = $(this).attr('article_image');
           
            $.post("<?php echo Router::url(array('controller' => 'articles', 'action' => 'select_home')); ?>",
                    {
                        question_id: question_id, title: title, article_image: article_image, slug: slug,
                        status: status
                    },
            function (data) {

               

            });
        })
    });

</script>