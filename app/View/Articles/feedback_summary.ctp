
<style>
    .textarea{
        resize: none;
    }
    .hpanel p, .hpanel h4,.hpanel h2{

    }
    .close{
        float: none;
        font-size: 0.7em;
        font-weight: 700;
        line-height: 1;
        margin-left: 1em;
        color: #428BCD;
        text-shadow: 0 1px 0 #fff;
        opacity: .2;
    }
    .add-link{
        margin-top: 1.5em;
        margin-left: -1em;
    }
    .no_padding{
        padding: 0
    }
    click{
        cursor: pointer;
    }
</style>


<br class="clear"><br class="clear">

<div class="row ">
    <div class="col-md-2 flex-column ">

    </div>
    <div class="col-md-8  ">
        <!-- The time line -->
        <div class=" hpanel forum-box" >
            <?php
            echo $this->Html->css(array('daterangepicker'));
            echo $this->Html->script(array('moment.min', 'daterangepicker'));
            ?>
            <div class="x_title">
                <?php
                echo $this->Form->create('Feedback', array('class' => 'form-horizontal form-label-left'));
                ?>

                <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="group">Filter By date
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <?php
                        echo $this->Form->input('date', array('type' => 'text', 'class' => 'form-control daterange col-md-7 col-xs-12',
                            'div' => false, 'label' => false));
                        ?>
                    </div>
                </div>
                <script type="text/javascript">
                    $('.daterange').daterangepicker({
                        "showDropdowns": true,
                        timePicker: false,
                        "timePicker24Hour": false,
                        "autoApply": true,
                        ranges: {
                            'Today': [moment(), moment()],
                            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                            'This Month': [moment().startOf('month'), moment().endOf('month')],
                            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                        },
                        "locale": {
                            "format": "YYYY-MM-DD",
                            "separator": "_",
                            "applyLabel": "Apply",
                            "cancelLabel": "Cancel",
                            "fromLabel": "From",
                            "toLabel": "To",
                            "customRangeLabel": "Custom",
                            "daysOfWeek": [
                                "Su",
                                "Mo",
                                "Tu",
                                "We",
                                "Th",
                                "Fr",
                                "Sa"
                            ],
                            "monthNames": [
                                "January",
                                "February",
                                "March",
                                "April",
                                "May",
                                "June",
                                "July",
                                "August",
                                "September",
                                "October",
                                "November",
                                "December"
                            ],
                            "firstDay": 1
                        },
                        "alwaysShowCalendars": true,
                    }, function (start, end, label) {
                        console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
                    });</script>

                <div class="form-group">
                    <div class="col-md-3 col-md-offset-4">
                        <button id="send" type="submit" style="background: #63b5b2;color:white;" class="btn btn-info col-md-12">Select</button>
                    </div>
                </div>

                <?php echo $this->Form->end(); ?>


                <div class="clearfix"></div>
            </div>
            <div class='list' >
                <div class="" >
                    <div class="panel-heading hbuilt text-center ">
                        <h2 class="text-center col-md-12  float-none"> 
                            Total Record <?php
                            $up = $firstsup + $secondsup + $thirdsup + $fourthsup + $fifthsup;

                            $down = $firstsdown + $secondsdown + $thirdsdown + $fourthsdown + $fifthsdown;
                            echo $total = $up + $down;
                            $total = ($total == 0) ? 1 : $total;
                            $review_array = array(0 => 'Was this summary helpful ?', 1 => 'Was this summary intersting ? ',
                                2 => "Do you like Speeli's design?", 3 => 'How do you like your experience on Speeli?',
                                4 => 'How can we improve Speeli?');
                            ?>
                        </h2><br class="clear"/>
                        <div class="clear clearfix"></div>

                        <div class="col-md-4 col-xs-12 float-none">
                            <div class="col-md-6">
                                <img class="img-responsive" src="<?php echo $this->webroot . 'imgs/like.png' ?>">

                                <h3 style="color:#2ced1c;"class="text-center col-md-12 col-xs-12"><b><?php echo ceil((($up / $total) * 100)) . "%"; ?></b></h3>
                                <h4 style="color:#2ced1c;"class="text-center col-md-12 col-xs-12"><b><?php echo $up . "/" . $total; ?></b></h4>
                            </div>
                            <div class="col-md-6">
                                <img class="img-responsive"
                                     src="<?php echo $this->webroot . 'imgs/dislike.png' ?>">
                                <h3 style="color:red;"class="text-center col-md-12 col-xs-12"><b><?php echo ceil((($down / $total) * 100)) . "%"; ?></b></h3>
                                <h4 style="color:red;" class="text-center col-md-12 col-xs-12"><b><?php echo $down . "/" . $total; ?></b></h4>
                            </div>

                        </div>
                        <div class="clear clearfix"></div>


                    </div>
                    <br class="clear">
                    <br class="clear">

                    <br class="clear">

                    <!-- first question start -->
                    <li class="hpanel hblue">
                        <div class="col-md-4 col-xs-12 float-none">
                            <div class="col-md-6">
                                <img class="img-responsive" src="<?php echo $this->webroot . 'imgs/like.png' ?>">
                                <?php
                                $totalfirst = $firstsup + $firstsdown;
                                $totalfirst = ($totalfirst > 0) ? $totalfirst : 1;
                                $totalfirst
                                ?>
                                <h3 style="color:#2ced1c;"class="text-center col-md-12 col-xs-12"><b><?php echo ceil((($firstsup / $totalfirst) * 100)) . "%"; ?></b></h3>
                                <h4 style="color:#2ced1c;"class="text-center col-md-12 col-xs-12"><b><?php echo $firstsup . "/" . $totalfirst; ?></b></h4>
                            </div>
                            <div class="col-md-6">
                                <img class="img-responsive"
                                     src="<?php echo $this->webroot . 'imgs/dislike.png' ?>">
                                <h3 style="color:red;"class="text-center col-md-12 col-xs-12"><b><?php echo ceil((($firstsdown / $totalfirst) * 100)) . "%"; ?></b></h3>
                                <h4 style="color:red;" class="text-center col-md-12 col-xs-12"><b><?php echo $firstsdown . "/" . $totalfirst; ?></b></h4>
                            </div>

                        </div>
                        <div class="panel-heading hbuilt">
                             <h3 class="col-md-9 no-padding no-margin"> <b><?php echo $review_array[0]; ?></b></h3>
                            <div class="col-md-3 show_first btn btn-info "style="color:white;">Show Feedback</div>
                            <div class="clearfix clear"></div><br class="clear clear-fix">
                        </div>
                        <div class="first_wrapper" style="display: none;">
                        
                    <?php
                    foreach ($firsts as $feed) {
                        ?>

                        
                            <div class="group-feedback" target="<?php echo $review_array[$feed[$key[1]]['target_id']]; ?>">
                                <?php foreach ($feed as $fed) { ?>
                                     <?php if ($fed['type'] == 1) { 
                                        if (!empty($fed['Article']['title_update'])) {
                                $explode_title = explode(',,,', $fed['Article']['title_update']);
                                $last = end($explode_title);
                                $title = end(unserialize(base64_decode($last)));
                            } else {
                                $title =$fed['Article']['title'];
                            }
                            
                                        ?>
                                        <div  class="panel-body" >

                                            <p style="float: left;"> Answer  : <?php echo $fed['body'] . "</p> <span style='float:right;'>' " . $fed['created'] . " ' </span>"; ?>
                                                <br class="clear"/><a target="_blank" href="<?php echo Router::url(array('controller'=>'articles','view'=>$fed['Article']['slug']))?>"><?php echo $title;?></a>
                                        </div>
                                    <?php } ?>
                                <?php }
                                ?>
                            </div>
                        
                        <?php
                    }
                    ?>
                        
                    </div>
                    </li>
                   
                    <!-- first question end -->
                    <!-- second question start -->
                    <li class="hpanel hblue">
                        <div class="col-md-4 col-xs-12 float-none">
                            <div class="col-md-6">
                                <img class="img-responsive" src="<?php echo $this->webroot . 'imgs/like.png' ?>">
                                <?php
                                $totalseconds = $secondsup + $secondsdown;
                                $totalseconds = ($totalseconds > 0) ? $totalseconds : 1;
                                $totalseconds
                                ?>
                                <h3 style="color:#2ced1c;"class="text-center col-md-12 col-xs-12"><b><?php echo ceil((($secondsup / $totalseconds) * 100)) . "%"; ?></b></h3>
                                <h4 style="color:#2ced1c;"class="text-center col-md-12 col-xs-12"><b><?php echo $secondsup . "/" . $totalseconds; ?></b></h4>
                            </div>
                            <div class="col-md-6">
                                <img class="img-responsive"
                                     src="<?php echo $this->webroot . 'imgs/dislike.png' ?>">
                                <h3 style="color:red;"class="text-center col-md-12 col-xs-12"><b><?php echo ceil((($secondsdown / $totalseconds) * 100)) . "%"; ?></b></h3>
                                <h4 style="color:red;" class="text-center col-md-12 col-xs-12"><b><?php echo $secondsdown . "/" . $totalseconds; ?></b></h4>
                            </div>

                        </div>
                        <div class="panel-heading hbuilt">
                             <h3 class="col-md-9 no-padding no-margin"> <b><?php echo $review_array[1]; ?></b></h3>
                            <div class="col-md-3 show_second btn btn-info "style="color:white;">Show Feedback</div>
                            <div class="clearfix clear"></div><br class="clear clear-fix">
                        </div>
                        <div class="second_wrapper" style="display: none;">
                        
                    <?php
                    foreach ($seconds as $feed) {
                        ?>

                        
                            <div class="group-feedback" target="<?php echo $review_array[$feed[$key[1]]['target_id']]; ?>">
                                <?php foreach ($feed as $fed) { ?>
                                    <?php if ($fed['type'] == 1) { 
                                        if (!empty($fed['Article']['title_update'])) {
                                $explode_title = explode(',,,', $fed['Article']['title_update']);
                                $last = end($explode_title);
                                $title = end(unserialize(base64_decode($last)));
                            } else {
                                $title =$fed['Article']['title'];
                            }
                                        ?>
                                        <div  class="panel-body" >

                                            <p style="float: left;"> Answer  : <?php echo $fed['body'] . "</p> <span style='float:right;'>' " . $fed['created'] . " ' </span>"; ?>
                                               <br class="clear"/><a target="_blank" href="<?php echo Router::url(array('controller'=>'articles','view'=>$fed['Article']['slug']))?>"><?php echo $title;?></a>
                                        </div>
                                    <?php } ?>
                                <?php }
                                ?>
                            </div>
                        
                        <?php
                    }
                    ?>
                        
                    </div>
                    </li>
                 
                    <!-- second question end -->

                    <!-- third question start -->
                    <li class="hpanel hblue">
                        <div class="col-md-4 col-xs-12 float-none">
                            <div class="col-md-6">
                                <img class="img-responsive" src="<?php echo $this->webroot . 'imgs/like.png' ?>">
                                <?php
                                $totalthird = $thirdsup + $thirdsdown;
                                $totalthird = ($totalthird > 0) ? $totalthird : 1;
                                $totalthird
                                ?>
                                <h3 style="color:#2ced1c;"class="text-center col-md-12 col-xs-12"><b><?php echo ceil((($thirdsup / $totalthird) * 100)) . "%"; ?></b></h3>
                                <h4 style="color:#2ced1c;"class="text-center col-md-12 col-xs-12"><b><?php echo $thirdsup . "/" . $totalthird; ?></b></h4>
                            </div>
                            <div class="col-md-6">
                                <img class="img-responsive"
                                     src="<?php echo $this->webroot . 'imgs/dislike.png' ?>">
                                <h3 style="color:red;"class="text-center col-md-12 col-xs-12"><b><?php echo ceil((($thirdsdown / $totalthird) * 100)) . "%"; ?></b></h3>
                                <h4 style="color:red;" class="text-center col-md-12 col-xs-12"><b><?php echo $thirdsdown . "/" . $totalthird; ?></b></h4>
                            </div>

                        </div>
                        <div class="panel-heading hbuilt">
                             <h3 class="col-md-9 no-padding no-margin"> <b><?php echo $review_array[2]; ?></b></h3>
                            <div class="col-md-3 show_third btn btn-info "style="color:white;">Show Feedback</div>
                            <div class="clearfix clear"></div><br class="clear clear-fix">
                        </div>
                         <div class="third_wrapper" style="display: none;">
                        
                    <?php
                    foreach ($thirds as $feed) {
                        ?>

                        
                            <div class="group-feedback" target="<?php echo $review_array[$feed[$key[1]]['target_id']]; ?>">
                                <?php foreach ($feed as $fed) { ?>
                                     <?php if ($fed['type'] == 1) { 
                                        if (!empty($fed['Article']['title_update'])) {
                                $explode_title = explode(',,,', $fed['Article']['title_update']);
                                $last = end($explode_title);
                                $title = end(unserialize(base64_decode($last)));
                            } else {
                                $title =$fed['Article']['title'];
                            }
                                        ?>
                                        <div  class="panel-body" >

                                            <p style="float: left;"> Answer  : <?php echo $fed['body'] . "</p> <span style='float:right;'>' " . $fed['created'] . " ' </span>"; ?>
                                               <br class="clear"/><a target="_blank" href="<?php echo Router::url(array('controller'=>'articles','view'=>$fed['Article']['slug']))?>"><?php echo $title;?></a>
                                        </div>
                                    <?php } ?>
                                <?php }
                                ?>
                            </div>
                        
                        <?php
                    }
                    ?>
                        
                    </div>
                    </li>
                   
                    <!-- third question end -->  

                    <!-- fourth question start -->
                    <li class="hpanel hblue">
                        <div class="col-md-4 col-xs-12 float-none">
                            <div class="col-md-6">
                                <img class="img-responsive" src="<?php echo $this->webroot . 'imgs/like.png' ?>">
<?php
$totalfourths = $fourthsup + $fourthsdown;
$totalfourths = ($totalfourths > 0) ? $totalfourths : 1;
$totalfourths
?>
                                <h3 style="color:#2ced1c;"class="text-center col-md-12 col-xs-12"><b><?php echo ceil((($fourthsup / $totalfourths) * 100)) . "%"; ?></b></h3>
                                <h4 style="color:#2ced1c;"class="text-center col-md-12 col-xs-12"><b><?php echo $fourthsup . "/" . $totalfourths; ?></b></h4>
                            </div>
                            <div class="col-md-6">
                                <img class="img-responsive"
                                     src="<?php echo $this->webroot . 'imgs/dislike.png' ?>">
                                <h3 style="color:red;"class="text-center col-md-12 col-xs-12"><b><?php echo ceil((($fourthsdown / $totalfourths) * 100)) . "%"; ?></b></h3>
                                <h4 style="color:red;" class="text-center col-md-12 col-xs-12"><b><?php echo $fourthsdown . "/" . $totalfourths; ?></b></h4>
                            </div>

                        </div>
                        <div class="panel-heading hbuilt">
                            <h3 class="col-md-9 no-padding no-margin"> <b><?php echo $review_array[3]; ?></b></h3>
                            <div class="col-md-3 show_fourth btn btn-info "style="color:white;">Show Feedback</div>
                        </div><div class="clearfix clear"></div><br class="clear clear-fix">
                        <div class="fourth_wrapper"style="display: none;">
<?php
foreach ($fourths as $feed) {
    ?>



                                <div class="group-feedback" target="<?php echo $review_array[$feed[$key[1]]['target_id']]; ?>">
    <?php foreach ($feed as $fed) { ?>
                   <?php if ($fed['type'] == 1) { 
                                        if (!empty($fed['Article']['title_update'])) {
                                $explode_title = explode(',,,', $fed['Article']['title_update']);
                                $last = end($explode_title);
                                $title = end(unserialize(base64_decode($last)));
                            } else {
                                $title =$fed['Article']['title'];
                            }
                                        ?>
                                        <div  class="panel-body" >

                                            <p style="float: left;"> Answer  : <?php echo $fed['body'] . "</p> <span style='float:right;'>' " . $fed['created'] . " ' </span>"; ?>
                                               <br class="clear"/><a target="_blank" href="<?php echo Router::url(array('controller'=>'articles','view'=>$fed['Article']['slug']))?>"><?php echo $title;?></a>
                                        </div>
                                    <?php } ?>
                                    <?php }
                                    ?>
                                </div>

    <?php
}
?>
                        </div>
                    </li>

                    <!-- fourth question end -->


                    <!-- fifth question start -->
                    <li class="hpanel hblue">
                        <div class="col-md-4 col-xs-12 float-none">


                        </div>
                        <div class="panel-heading hbuilt">
                            <h3> <b><?php echo $review_array[4]; ?></b></h3>
                        </div>
<?php
foreach ($fifths as $feed) {
    ?>
                            <div class="group-feedback" target="<?php echo $review_array[$feed[$key[1]]['target_id']]; ?>">
                            <?php foreach ($feed as $fed) { ?>
                   <?php if ($fed['type'] == 1) { 
                                        if (!empty($fed['Article']['title_update'])) {
                                $explode_title = explode(',,,', $fed['Article']['title_update']);
                                $last = end($explode_title);
                                $title = end(unserialize(base64_decode($last)));
                            } else {
                                $title =$fed['Article']['title'];
                            }
                                        ?>
                                        <div  class="panel-body" >

                                            <p style="float: left;"> Answer  : <?php echo $fed['body'] . "</p> <span style='float:right;'>' " . $fed['created'] . " ' </span>"; ?>
                                               <br class="clear"/><a target="_blank" href="<?php echo Router::url(array('controller'=>'articles','view'=>$fed['Article']['slug']))?>"><?php echo $title;?></a>
                                        </div>
                                    <?php } ?>
                                <?php }
                                ?>
                            </div>

    <?php
}
?>
                    </li>

                    <!-- first question end -->

                </div>
            </div>


        </div><!-- /.col -->


    </div><!-- /.row -->


</div>


<script>
    $(document).ready(function () {
        $('.show_first').click(function(){
           $('.first_wrapper').slideToggle(1000); 
        });
         $('.show_second').click(function(){
           $('.second_wrapper').slideToggle(1000); 
        });
         $('.show_third').click(function(){
           $('.third_wrapper').slideToggle(1000); 
        });
         $('.show_fourth').click(function(){
           $('.fourth_wrapper').slideToggle(1000); 
        });
    });
</script>