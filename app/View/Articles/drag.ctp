<?php $slug = "http://www.speeli.com/articles/view/" . trim($answers[0]['Question']['slug']); ?>  
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<style>
    h5{
          color: rgb(34, 163, 159);
    }
    .handle:hover{
        cursor: pointer;
    }
    .hblue:hover{
        cursor: pointer;
        -moz-box-shadow:rgba(16, 102, 99, 0.65) 1px 1px 13px 2px;
        -webkit-box-shadow:rgba(16, 102, 99, 0.65) 1px 1px 13px 2px;
          box-shadow: rgba(16, 102, 99, 0.65) 1px 1px 13px 2px;
    }
</style>

<?php
$this->set('og:title', $answers[0]['Question']['title']);

$this->set("meta_description", $answers[0]['Question']['title']);
?>
<?php
$admin = false;
if ($this->Session->check('user_quesli')) {
    $user = $this->Session->read('user_quesli');
    if ($user['User']['admin'] == 1) {
        $admin = true;
    }
}
?>
<div id="fb-root"></div>
<?php if (isset($answers) && !empty($answers)) { ?>
    <div class="row">

    <?php echo $this->Form->create('Question', array("type" => 'file')); ?>
        <!-- Add the extra clearfix for only the required viewport -->
        <div  class="clearfix visible-xs-block"></div>
        <br class="clear"><br class="clear">
        <div class="row ">
            <div class="col-md-2 flex-column ">
            </div>
            <div class="col-md-8  ">
                <!-- The time line -->
                <div class=" hpanel forum-box" id="">
                    <div class="panel-heading">
                        <?php
                        if (!empty($answers[0]['Question']['last'])) {
                            $title = $answers[0]['Question']['last'];
                        } elseif (!empty($answers[0]['Question']['title_update'])) {
                            $explode_title = explode(',,,', $answers[0]['Question']['title_update']);
                            $last = end($explode_title);
                            $title = end(unserialize(base64_decode($last)));
                        } else {
                            $title = $answers[0]['Question']['title'];
                        }
                        $this->set("page_title", $title);
                        ?>
                        <center>  


                            <br class="clear">
                            <h2 class="page-header"> 
                            <b><?php echo strip_tags($title); ?></b> </h2>
                            <br class="clear">
                            <?php if ($answers[0]['Question']['image'] != NULL) { ?>

                                <img id='profile_img' src="<?php echo $this->webroot . 'question/' . $answers[0]['Question']['id'] . '/' . $answers[0]['Question']['image']; ?>" alt="..." class="img-responsive">
                            <?php } ?>
                        </center>

                        <br class="clear">
                    </div>
                    <div class=' sortable list' id="sortable-with-handles">
                        <?php
                        $i = 1;

                        foreach ($answers as $answer) {
                            if (!empty($answer['Answer']['last_title'])) {
                                $title = $answer['Answer']['last_title'];
                            } else {
                                if (!empty($answer['Answer']['title_update'])) {
                                    $explode_title = explode(',,,', $answer['Answer']['title_update']);
                                    $last = end($explode_title);
                                    $title = end(unserialize(base64_decode($last)));
                                } else {
                                    $title = $answer['Answer']['title'];
                                }
                            }
                            ?>
                        <div sort_num='<?php echo $i; ?>' id="<?php echo $answer['Answer']['id']; ?>"class="hpanel hblue" draggable="true"  ><span class="handle">::::</span>

                                <h5 ques-ans-id="<?php echo $answer['Answer']['id']; ?>" > <b class="left"><?php echo $i . " )  "; ?> </b>
                                    <?php
                                    echo $this->Form->input('title', array('value' => $i, 'type' => 'hidden', 'name' => 'sort_value[]', 'div' => false, 'label' => false));
                                    echo $this->Form->input('id', array('value' => $answer['Answer']['id'], 'type' => 'hidden', 'name' => 'id[]', 'div' => false, 'label' => false));
                                    ?>
                                    <b><?php echo $title; ?></b></h5>
                            </div>
                            <br class="clear">
                            <?php
                            $i++;
                        }
                        ?>
                    </div>
                </div>
                  <?php
        echo $this->Form->submit('Save changes', array('style'=>array('float:right'),'value' => __('Create Speeli article'), 'id' => 'login', 'class' => array('btn', ' btn-always'), 'div' => false));
        echo $this->Form->end();
        ?>
            </div><!-- /.col -->
        </div><!-- /.row -->
      

        <br class="clear"/>
    </div>
<?php } ?>  


<script>
    $(function() {
        $("#sortable-with-handles").sortable();
        $("#sortable-with-handles").disableSelection();
        $('#sortable-with-handles').sortable({
            axis: 'y',
            change: function(event, ui) {
                var data = $(this).sortable('toArray', {attribute: 'id'});
                var num = $(this).sortable('toArray', {attribute: 'sort_num'});
                console.log(data);
                console.log(num);

                // POST to server using $.post or $.ajax

            }
        });
    });
</script>
<script type="text/javascript" async src="//platform.twitter.com/widgets.js"></script>
