<style>
    .expand{
        cursor: pointer;
    }
    .click{
        cursor: pointer;
    }
    .progress{
        display: none;
    }
    hr{
        border: 0;
        height: 1px;
        background-image: -webkit-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -moz-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -ms-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -o-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
    }
    .clear{
        clear: both;
    }
    .question_wrapper{
        margin: 0 auto;
    }
    .float-none{
        float:none;
        margin: 0 auto;
    }
    .hblue{
        background-color: #fff;
        padding: 1em;
        border-radius: 4px;
        -moz-border-radius: 4px;
        -webkit-border-radius: 4px;
        margin-bottom: 25px;
        position: relative;
        line-height: 25px;
        border: 1px solid #e5e5e5;
    }
    .hblue .panel-heading{
        padding: 10px 15px;
        background-color: rgba(245, 245, 245, 0.24);
        border-top: 1px solid #ddd;

        border-bottom-right-radius: 3px;
        border-bottom-left-radius: 3px;
    }
    .list-unstyled li{
        margin-left: 1em;
    }
</style>
<?php
$this->set("meta_description", "Latest Speeli articles");
$this->set("meta_robots", "noindex");
?>
<?php
$this->Paginator->options(array(
    'update' => '.question_update',
    'evalScripts' => true
));
?>
<div class="row">
    <!-- Add the extra clearfix for only the required viewport -->
    <div  class="clearfix visible-xs-block"></div>
    <br class="clear">
    <hr>
    <div class="row ">

        <div class="col-md-8 float-none update_art_speeli  " style="border-left: 1px solid #ddd;">
            <!-- The time line -->
            <div class="hpanel forum-box question_update">
                <?php
                $i = 1;

                $user = $this->Session->read('user_quesli');
                if ($user['User']['admin'] == 1) {
                    foreach ($questions as $question) {
                        if (!empty($question['Question']['title_update'])) {
                            $explode_title = explode(',,,', $question['Question']['title_update']);
                            $last = end($explode_title);
                            $title = end(unserialize(base64_decode($last)));
                        } else {
                            $title = $question['Question']['title'];
                        }
                        
                        ?>
                        <div class="hpanel hblue">
                            <div class="panel-heading hbuilt">
                                <h3> <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $question['Question']['slug'])); ?>"> <?php echo html_entity_decode($title); ?></a>  </h3>
                                       <?php
                            $review_array = array(0 => 'Was this summary helpful ?', 1 => 'Was this summary intersting ? ',
                                2 => "Do you like Speeli's design?", 3 => 'How do you like your experience on Speeli?', 4 => 'How can we improve Speeli?');

                           
                            ?>
                                <h4>
                                     <?php echo $review_array[$question['Feedback']['target_id']]; ?>  </h4>

 <?php if ($question['Feedback']['type'] == 1) { ?>
                                        <div  class="panel-body" >
                                            
                                             <img class="img-responsive col-md-2"
                                                  src="<?php echo $this->webroot . 'imgs/dislike.png' ?>"><br class="clear">
                                            <p style="float: left;"> Answer  : <?php echo $question['Feedback']['body'] . "</p> <span style='float:right;'>' " . $question['Feedback']['created'] . " ' </span>"; ?> 
                                        </div>
                                    <?php }else{ ?>
                                <div  class="panel-body" >
                                            
                                              <img class="img-responsive col-md-2"
                                     src="<?php echo $this->webroot . 'imgs/like.png' ?>">
                                   <?php echo "</p> <span style='float:right;'>' " . $question['Feedback']['created'] . " ' </span>"; ?> 
                                </div>
                                
                                 
                                    <?php } ?>
                            </div>
                            
                             <br class="clear">

                        </div>
                        <?php
                        $i++;
                    }
                } else {
                    foreach ($questions as $question) {
                        if ($question['Question']['status'] == 1) {
                            if (!empty($question['Question']['last'])) {
                                $title = $question['Question']['last'];
                            } else {
                                if (!empty($question['Question']['title_update'])) {
                                    $explode_title = explode(',,,', $question['Question']['title_update']);
                                    $last = end($explode_title);
                                    $title = end(unserialize(base64_decode($last)));
                                } else {
                                    $title = $question['Question']['title'];
                                }
                            }
                            ?>
                            <div class="hpanel hblue">
                                <div class="panel-heading hbuilt">
                                    <h3> <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $question['Question']['slug'])); ?>"> <?php
                                            $title = str_replace('&nbsp;', ' ', $title);
                                            $title = str_replace('nbsp', ' ', $title);
                                            $title = str_replace('%', ' ', $title);

                                            echo htmlspecialchars($title);
                                            ?></a>  </h3>
            <?php echo $status; ?>
                                </div>


                            </div>
                            <?php
                            $i++;
                        }
                    }
                }
                ?>
                <!-- timeline time label -->
                <!-- END timeline item -->
                <!-- timeline item -->
            </div>
        </div><!-- /.col -->
    </div><!-- /.row -->
    <ul class="pagination">
        <?php
        echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
        echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'currentClass' => 'active', 'tag' => 'li', 'first' => 1));
        echo $this->Paginator->next(__('next'), array('tag' => 'li', 'currentClass' => 'disabled'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
        ?>
    </ul>
<?php echo $this->Js->writeBuffer(); ?>
</div>
<?php if ($user['User']['admin'] == 1) { ?>
    <script type="text/javascript">

        $('body').undelegate('.update_art', 'click').delegate('.update_art', 'click', function (e) {
            event.preventDefault();
            target = $(this).attr('target');
            $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'articles', 'action' => 'interest')); ?>",
                data: {target: target}, // outer quotes removed
                cache: false,
                success: function (response, status) {
                    $('.update_art_speeli').html(response);
                },
            });
        });

        $('body').undelegate('.expand', 'click').delegate('.expand', 'click', function (e) {
            var art_id = $(this).attr('art_id');
            var art_status = $(this).attr('art_status');
            if (art_status == 0) {
                $(this).html('<span style="color:grey" class="fa fa-lightbulb-o">Mark as :needs expanding</span>');
            } else {
                $(this).html('<span style="color:green" class="fa fa-lightbulb-o">Needs expanding</span>');
            }
            $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'articles', 'action' => 'expand')); ?>",
                data: {art_id: art_id, art_status: art_status}, // outer quotes removed
                cache: false,
                success: function (response, status) {
                },
            });
        });
        $('body').undelegate('.quality', 'click').delegate('.quality', 'click', function (e) {
            console.log(art_status);
            var art_id = $(this).attr('art_id');
            var art_status = $(this).attr('art_quality');
            console.log(art_status);
            if (art_status == 0) {
                $(this).html('<span style="color:grey" class="fa fa-lightbulb-o">No quality check</span>');
            } else {
                $(this).html('<span style="color:green" class="fa fa-lightbulb-o">Passed quality quidelines</span>');
            }
            $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'articles', 'action' => 'quality')); ?>",
                data: {art_id: art_id, art_status: art_status}, // outer quotes removed
                cache: false,
                success: function (response, status) {
                },
            });
        });
        $('body').undelegate('.edited', 'click').delegate('.edited', 'click', function (e) {
            var art_id = $(this).attr('art_id');
            var art_status = $(this).attr('art_edit');
            console.log(art_status);
            if (art_status == 0) {
                $(this).html('<span style="color:grey" class="fa fa-lightbulb-o">Not edited</span>');
            } else {
                $(this).html('<span style="color:green" class="fa fa-lightbulb-o">Edited</span>');
            }
            $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'articles', 'action' => 'art_edit')); ?>",
                data: {art_id: art_id, art_status: art_status}, // outer quotes removed
                cache: false,
                success: function (response, status) {
                },
            });
        });
         $('body').undelegate('.locked', 'click').delegate('.locked', 'click', function (e) {
            var art_id = $(this).attr('art_id');
            var art_status = $(this).attr('locked');
            console.log(art_status);
            if (art_status == 0) {
                $(this).html('<span style="color:grey" >Open</span>');
            } else {
                $(this).html('<span style="color:green" >Locked</span>');
            }
            $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'articles', 'action' => 'art_lock')); ?>",
                data: {art_id: art_id, art_status: art_status}, // outer quotes removed
                cache: false,
                success: function (response, status) {
                },
            });
        });
        $('body').undelegate('.linked', 'click').delegate('.linked', 'click', function (e) {
            var art_id = $(this).attr('art_id');
            var art_status = $(this).attr('link_status');
            console.log(art_id);
            if (art_status == 0) {
                $(this).html('<span style="color:grey" class="fa fa-lightbulb-o">No Links</span>');
            } else {
                $(this).html('<span style="color:green" class="fa fa-lightbulb-o">Has links</span>');
            }
            $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'articles', 'action' => 'linked')); ?>",
                data: {art_id: art_id, art_status: art_status}, // outer quotes removed
                cache: false,
                success: function (response, status) {
                },
            });
        });
        $('body').undelegate('.disable_ads', 'click').delegate('.disable_ads', 'click', function (e) {
            var art_id = $(this).attr('art_id');
            var ads_status = $(this).attr('ads_status');
            console.log(art_id);
            if (ads_status == 0) {
                $(this).html('<span style="color:grey" class="fa fa-unlock">Enable Ads</span>');
                $(this).attr('ads_status',1);
            } else {
                $(this).html('<span style="color:green" class="fa fa-ban">Disable Ads</span>');
                $(this).attr('ads_status',0);
            }
            $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'articles', 'action' => 'disable_ads')); ?>",
                data: {art_id: art_id, ads_status: ads_status}, // outer quotes removed
                cache: false,
                success: function (response, status) {
                },
            });
        });
        $('.new-cat-value').keyup(function () {
            var data_send = $(this).val();
            var cat_id = $(this).attr('target');
            console.log($('.results_' + cat_id));

            $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'categories', 'action' => 'search_category')); ?>",
                data: {data_send: data_send, cat_id: cat_id}, // outer quotes removed
                cache: false,
                success: function (response, status) {

                    $('.results_' + cat_id).html(response);
                },
            });
        });
        $('.new-sum-value').keyup(function () {
            var data_send = $(this).val();
            var cat_id = $(this).attr('target');
            console.log($('.results_' + cat_id));

            $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'categories', 'action' => 'search_summary')); ?>",
                data: {data_send: data_send, cat_id: cat_id}, // outer quotes removed
                cache: false,
                success: function (response, status) {

                    $('.results_' + cat_id).html(response);
                },
            });
        });
    </script>
<?php } ?>