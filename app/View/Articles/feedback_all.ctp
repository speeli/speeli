<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="/css/nicefileinput.css">

<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<style>
    .textarea{
        resize: none;
    }
    .hpanel p, .hpanel h4,.hpanel h2{
        border: 1px solid rgb(121, 199, 197);
    }
    .close{
        float: none;
        font-size: 0.7em;
        font-weight: 700;
        line-height: 1;
        margin-left: 1em;
        color: #428BCD;
        text-shadow: 0 1px 0 #fff;
        opacity: .2;
    }
.add-link{
margin-top: 1.5em;
    margin-left: -1em;
    }
    .no_padding{
        padding: 0
    }
    click{
        cursor: pointer;
    }
</style>
<?php
$admin = false;
if ($this->Session->check('user_quesli')) {
    $user = $this->Session->read('user_quesli');
    if ($user['User']['admin'] == 1) {
        $admin = true;
    }
}
?>
<?php if (isset($answers) && !empty($answers)) { ?>
    <?php
    $title = str_replace('&nbsp;', ' ', $answers[0]['Question']['title']);
    $title = str_replace('nbsp', ' ', $title);
    $title = str_replace('%', ' ', $title);




    $this->set("description", $answers[0]['Question']['title']);
    ?>
    <div class="row">

        <?php echo $this->Form->create('Question', array("type" => 'file')); ?>
        <!-- Add the extra clearfix for only the required viewport -->
        <div  class="clearfix visible-xs-block"></div>
        <br class="clear"><br class="clear">

        <div class="row ">
            <div class="col-md-2 flex-column ">

            </div>
            <div class="col-md-8  ">
                <!-- The time line -->
                <div class=" hpanel forum-box" >
                    <div class="panel-heading">
                        <div class="quesuserarticle-i"><?php echo $answers[0]['Question']['id']; ?></div>
                        <?php
                        echo $this->Form->create('Question', array('onclick'=>'unhook()',"type" => 'file'));
                        if (!empty($answers[0]['Question']['title_update'])) {
                            $explode_title = explode(',,,', $answers[0]['Question']['title_update']);
                            $last = end($explode_title);
                            $titles = end(unserialize(base64_decode($last)));
                        } else {
                            $titles = $answers[0]['Question']['title'];
                        }
                        $titles = str_replace("&nbsp;", "", $titles);
                        $this->set("page_title", $title);
                        ?>
                        <script type="text/javascript"><!-- // --><![CDATA[
                document.write('<input type="hidden" id="as_clear" name="as_clear" value="1" />');
                // ]]></script>
                        <center>  
                            <?php
                            echo $this->Form->input('art_id', array('value' => $answers[0]['Question']['id'], 'id' => 'art_id', 'type' => 'hidden', 'name' => 'art_id[]', 'div' => false, 'label' => false));
                            echo $this->Form->input('art_title', array('value' => strip_tags($titles), 'class' => 'calc', 'name' => 'art_title[]', 'div' => false, 'label' => false));
                            ?>

                            <br class="clear">
                            <br class="clear">
                            <?php echo $this->Form->input('image_title', array('id' => 'FileImage1', 'type' => 'file', 'name' => 'image_title[]', 'label' => false, 'div' => false)); ?>                    
                        </center>
                    </div>
                    <div class='  list' >
                        <?php
                        $i = 1;
                        foreach ($answers as $answer) {
                            if(!empty($answer['Answer']['last_title'])){
                           $title=$answer['Answer']['last_title'];
                       }else{
                            if (!empty($answer['Answer']['title_update'])) {
                            $explode_title = explode(',,,', $answer['Answer']['title_update']);
                            $last = end($explode_title);
                            $title = end(unserialize(base64_decode($last)));
                        } else {
                            $title = $answer['Answer']['title'];
                        }
                       }
                       if(!empty($answer['Answer']['last_body'])){
                            $body = $answer['Answer']['last_body'];
                            $body = trim($body);
                            $body = rtrim($body, ' ');
                            $body = rtrim($body, '.');
                            $body.=" .";
                       }else{
                            if (!empty($answer['Answer']['body_update'])) {
                            $explode_body = explode(',,,', $answer['Answer']['body_update']);
                            $last = end($explode_body);
                            $body = end(unserialize(base64_decode($last)));
                            $body = trim($body);
                            $body = rtrim($body, ' ');
                            $body = rtrim($body, '.');
                            $body.=" .";
                        } else {
                            $body = $answer['Answer']['body'];
                            $body = trim($body);
                            $body = rtrim($body, ' ');
                            $body = rtrim($body, '.');
                            $body.=" .";
                        }
                       }
                    
                            ?>
                            <div sort_num='<?php echo $i; ?>' id="<?php echo $answer['Answer']['id']; ?>"class="hpanel hblue" draggable="true"  >
                                <div class="panel-heading hbuilt">
                                    <b class="left"><?php echo $i . " )  "; ?> </b>
                                    <?php
                                    echo $this->Form->input('title', array('value' => $i, 'type' => 'hidden', 'name' => 'sort_value[]', 'div' => false, 'label' => false));
                                    echo $this->Form->input('title', array('value' => $answer['Answer']['id'], 'type' => 'hidden', 'name' => 'id[]', 'div' => false, 'label' => false));
                                    ?>
                                    <?php
                                    echo $this->Form->input('title', array('value' => $title, 'class' => 'calc', 'name' => 'title[]', 'div' => false, 'label' => false));
                                    ?>
                                </div>
                                <br class="clear">
                                <div class="panel-body" >
                                    <?php
                                    if (($answer['Answer']['image'] != NULL)) {
                                        if ($answer['Answer']['image_user'] == 0) {
                                            ?>
                                            <div style='float:right;top:1.5em; position: relative'  ques-art-a='<?php echo $answer['Answer']['id']; ?>' target='p' class="btn btn-danger">
                                                <span style="color:white;" class="fa fa-remove"></span>
                                            </div>           
                                            <img id='profile_img' src="<?php echo $this->webroot . 'question/' . $answers[0]['Question']['id'] . '/' . $answer['Answer']['image']; ?>" alt="..." class="img-thumbnail">
                                        <?php
                                        } else {
                                            if ($admin) {
                                                ?> 
                                                <div style='float:right;top:1.5em; position: relative'  ques-art-a='<?php echo $answer['Answer']['id']; ?>' target='p' class="btn btn-danger">
                                                    <span style="color:white;" class="fa fa-remove"></span>
                                                </div>           
                                                <img id='profile_img' src="<?php echo $this->webroot . 'question/' . $answers[0]['Question']['id'] . '/' . $answer['Answer']['image']; ?>" alt="..." class="img-thumbnail">

                                            <?php } ?>
                                            <?php
                                        }
                                    }?>
<div style='float: right; margin-bottom: -1em;' class=" add-url"> <span class=" fa fa-link"> add link</span></div><br class="clear">
                                    <?php
                                    echo $this->Form->textarea('body', array('maxlength' => '300', 'value' => $body, 'class' => 'QuestionBody', 'name' => 'body[]', 'div' => false, 'label' => false));
                                    ?>                                
                                    <div class="counter"></div><br class="clear">

                                </div>
                                <div class="panel-footer panel-footer-status">
                                    <a  href="<?php
                                    echo Router::url(array('controller' => 'articles',
                                        'action' => 'history', $answer['Answer']['id']))
                                    ?>" class="btn iframe-edit btn-main right">Show Edit History
                                    </a>
                                    <br class="clear"></div>


                            </div>
                            <?php
                            $i++;
                        }
                        ?>
                    </div>
                    <div class="hpanel hblue" >
                        <div class="panel-heading hbuilt">
                            <b class="left">Point <?php echo (count($answers) + 1) . " )  "; ?> </b>
                            <?php
                            echo $this->Form->input('title', array('placeholder' => 'Enter point header ...', 'class' => 'calc', 'name' => 'title_new[]', 'div' => false, 'label' => false));
                            ?>
                        </div>
                        <br class="clear">
                        <div class="panel-body" >
                            <div style='float: right; margin-bottom: -1em;' class=" add-url"> <span class=" fa fa-link"> add link</span></div>  <br class="clear">                    
                            <?php
                            echo $this->Form->textarea('body', array('maxlength' => '300', 'class' => 'QuestionBody', 'name' => 'body_new[]', 'div' => false, 'label' => false));

                            echo "<br class='clear'><br class='clear'>" . $this->Form->input('images', array('id' => 'FileImage1', 'type' => 'file', 'name' => 'image_new[]', 'label' => false, 'div' => false));
                            ?><br class="clear"> 
                            <div class="counter">300 characters left</div><br class="clear">
                        </div>

                        <div class="panel-footer panel-footer-status">


                            <br class="clear"></div>

                    </div>
                    <br class="clear">

                </div>
                
            </div><!-- /.col -->


        </div><!-- /.row -->


    </div>
    <?php
} else {
    
}
?>
