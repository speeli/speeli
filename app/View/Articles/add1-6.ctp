 <style>
#loader{
        display: none;
    }
    .second-step{
        display: none;
        float: left;
    }
    .step_number{
        float: left;
        margin-right: 1vw;
    }
    ul{
        list-style: none;
    }
    li{
        list-style: none;
    }
    .clear{
        clear: both;
    }
    .text{
     margin: 1em auto;
     width: 70%;
    }
    .ask-new{
        cursor: pointer;
    }
    .click{
        cursor: pointer;
    }
    .close{
        float: right;
  font-size: 1em;
    margin: 0.2em auto;
    }
    .new_cat_result .btn-main{
        border: 1px solid rgb(0, 151, 255);
  font-size: 1em;
  border-radius: 0.5em;
  margin: 1em;
}
.new_cat_result{
    margin: 1em;
}
.panel-footer-status{
      width: 100%;
  margin: 0px;
  margin-top: -21px;
} 
.counter{
    float: right;
}
</style>
<style>
     .suggest {
         margin-top: -1em;
  border: 1px solid rgba(128, 128, 128, 0.7);
  /* margin: 0 auto; */
  margin-left: 14px;
  box-shadow: 1px 1px 7px;
}
.suggest-items {
      float: left;
  margin-top: 0.25em;
  margin-bottom: 0.25em;
}
#title{min-height: 3em; }
.btn-always-blue{
      color: white;
  padding: 0.2em 0.2em;
}
</style>
    <?php
    $this->set("page_title", 'Add');

    echo $this->Form->create('Question',array("type"=>'file')); ?>

<div class="row question">
    
    <div class="panel panel-default">
        <div class="panel-heading flex-container">
            <h5 class="panel-title text-info">Article title : </h5>
            <div id="Title" class="login-form-text text " contenteditable="true"></div>
     <div id="loader" class="col-md-3 col-xs-3 float-none" >
                <cetner><img src="<?php echo $this->webroot . 'files/8.gif'; ?>">
                    Related Speeli articles
                </cetner>
            </div> 
    <br class="clear">
        <br class="clear">
    
    <div class="input-cat">

    </div>
        </div>

    </div>


    
    
    <br class="clear">
    <div class="row">
    <div class="second-step  col-md-10 col-xs-12 float-none " >
        <center><p class="gray-tone">Article title </p></center><center><h2 class="page-header panel-article"></h2></center>
        <div class="flex-edit btn btn-main-always right">Edit your title</div>
        <?php echo $this->Form->text('title', array('type' => 'hidden', 'label' => false, 'div' => false)); ?>
        
        <br class="clear">
        <?php echo $this->Form->input('image_title', array('id'=>'FileImage1','type' => 'file','name' => 'image_title[]', 'label' => false, 'div' => false)); ?>                    
        <br class="clear">
        <ul class=" st-steps  ">
            <li>
            <div class="panel panel-default ">
                <div class="panel-heading">
                    <h1 class="panel-title">Point number 1</h1>
                </div>
                <br class="clear">
                <div class="panel-body">
  <?php echo $this->Form->input('title', array('placeholder'=>'enter point header .. . ','name' => 'title[]', 'label' => false, 'div' => false)); ?>                  
  <?php echo $this->Form->textarea('body', array('placeholder'=>'Descripte your point .. . ','class'=>'QuestionBody','name' => 'body[]', 'label' => false, 'div' => false))."<div class='panel-footer panel-footer-status'><div class='counter'>300 characters left</div><br class='clear'></div>"; ?>
  <?php echo $this->Form->input('image', array('id'=>'FileImage1','type' => 'file','name' => 'image[]', 'label' => false, 'div' => false)); ?>                    
                </div>
            </div>
            </li>
        </ul>
        <br class="clear">
        <div step="1" class="add_new btn btn-main right">Add another point</div>
        <br class="clear">
        <div class="get_cat">
            
        </div>
        <br class="clear">
       
         <?php echo $this->Form->input('new', array('class'=>'new-cat-value','placeholder'=>'write your new tags', 'label' => false, 'div' => false)); ?>
         <div class="results">
             
         </div>
         <br class="clear">
         <div class="hpanel hblue">
                    <div class="panel-heading hbuilt">
                        <b class="left">Add Reference</b>
                    </div>
                    <br class="clear">
                    <div class="panel-body panel-ref" >
                        <b class="left"> 1)</b> <br>
   <div class='col-md-3 col-xs-3'><?php echo $this->Form->input('text', array('placeholder' => 'Reference name.. . ','class' => 'Questionref', 'name' => 'ref_name[]', 'div' => false, 'label' => false)); ?></div>
   <div class='col-md-8 col-xs-8'><?php echo $this->Form->input('text', array('placeholder' => 'Reference url.. . ','class' => 'Questionref', 'name' => 'ref[]', 'div' => false, 'label' => false)); ?></div><br class="clear">
                        
                    </div>
                    <br class="clear"><div stepref='1' class='btn btn_ref'><span class="fa fa-plus"></span></div>
                </div>
         <br class="clear">
             <?php
        echo $this->Form->submit('Puplish Speeli article', array('value' => __('Create Speeli article'), 'id' => 'login', 'class' => 'button', 'div' => false));
        echo $this->Form->end();
        ?>
        <br class="clear">
        
    </div>
    </div>
</div>

<?php
echo $this->Html->script(array('jquery.nicefileinput.min'));
?>

<script type="text/javascript">
    $(document).ready(function () {
        
          $('.btn_ref').click(function () {
            
            var step_number = parseInt($(this).attr('stepref'));
            step_number++;
            $(this).attr('stepref', step_number);
            $('.panel-ref').append(
                    '\n\
                              <b class="left">' + step_number + ' )</b><br class="clear"><div class="col-md-3 col-xs-3"> \n\
<?php
echo $this->Form->input('title', array('placeholder' => 'Reference name.. . ', 'class' => 'calc', 'name' => 'ref_name[]', 'div' => false, 'label' => false));?></div><div class="col-md-8 col-xs-8"><?php
echo  $this->Form->input('title', array('placeholder' => 'Reference url .. . ', 'class' => 'calc', 'name' => 'ref[]', 'div' => false, 'label' => false));?></div><br class="clear">');

        });
        $('.action').click(function(){
          var action_tag= $(this).text();
          var highlight = window.getSelection();  
        var span = '<span class="bold">' + highlight + '</span>';
        var text = $('.textEditor').html();
        $('.textEditor').html(text.replace(highlight, span));
        });
        
        $('body').undelegate('.suggest-items', 'click').delegate('.suggest-items', 'click', function (e) {
            console.log('ataket');
            var cat_id=$(this).attr('ques-cat-id');
            var cat_name=$(this).html();
            $('.new_cat_result').append("<div class='col-md-2 btn-main click'>\n\
          <input name='new[]' value="+cat_id+" type='hidden'>"+cat_name+"<label class='close remove-cat'>X</label>")
        });
              
        $('body').undelegate('.remove-cat', 'click').delegate('.remove-cat', 'click', function (e) {
            $(this).parent().remove();
        })
    
          $('.new-cat-value').keyup(function(){
    var data_send=$(this).val();
    $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'articles', 'action' => 'search_cat')); ?>",
                data: {data_send: data_send}, // outer quotes removed
                cache: false,
                success: function (response, status) {
                    $('.results').html(response);
                },
            });
    });
    $('#Title').keydown(function(){
        var questitle = $(this).html();
       if(questitle.length>0){
        questitle.replace(/ {2,}/g, '');
        questitle.replace('&nbsp;', '');
        $('#QuestionTitle').val(questitle); 
    }
    });
    });
    $('.ask-new').click(function(){
    var questitle = $('#Title').html();          
            if(questitle.length>0){
                
    $('.flex-container').slideToggle();
    
    
                alert('dasd');
                $('.second-step').show();
        event.preventDefault();
        questitle.replace(/ {2,}/g, '');
        questitle.replace('&nbsp;', '');
        $('#QuestionTitle').val(questitle);    
        console.log(questitle);
            $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'articles', 'action' => 'get_cat')); ?>",
                data: {questitle: questitle}, // outer quotes removed
                cache: false,
                success: function (response, status) {
                    $('.get_cat').html(response);
                },
            });
    } else{
    alert('Write Your article title');
    }
    
    })
   $(document).keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            event.preventDefault();
        }
    });
    
 
   
   $('body').undelegate('.QuestionBody', 'keyup').delegate('.QuestionBody', 'keyup', function (e) {
  var max = 300;
  var len = $(this).val().length;
  if (len >= max) {
    $('.counter').text(' you have reached the limit');
    $('.counter').css({color:'red'});
  } else {
    var char = max - len;
    $('.counter').text(char + ' characters left');
    $('.counter').css({color:'gray'});
  }
});
    $('.add_new').click(function(){
        
        var step_number = parseInt($(this).attr('step'));
        step_number++;
        $(this).attr('step', step_number);
console.log(step_number);
        $('ul.st-steps li:last').after(
              '  <li>\n\
                    <div class="panel panel-default">\n\
                        <div class="panel-heading">\n\
                            <h1 class="panel-title">Point number ' +step_number+' </h1>\n\
                        </div> \n\
                        <div class="panel-body"><?php
                echo $this->Form->input('title', array('placeholder'=>'Enter point header .. . ','class' => 'calc', 'name' => 'title[]','div'=>false, 'label' => false));
                 echo $this->Form->textarea('body', array('placeholder'=>'Descripte your point .. . ','class'=>'QuestionBody', 'name' => 'body[]', 'label' => false)).'<div class="panel-footer panel-footer-status"><div class="counter">300 characters left</div><br class="clear"></div></div>';
                  echo $this->Form->input('image', array('type'=>'file','class' => 'calc', 'name' => 'image[]', 'label' => false));?><br class="clear"> \n\
</div>\n\
                  \n\
                  </div></li>');

    });
    
    $('.check_status').on('click', function () {
        //$('.second-step').show();
        event.preventDefault();
        var questitle = $('#Title').html();
        if(questitle.length>0){
        $('#QuestionTitle').val(questitle);
        $.ajax({
            type: "post", // Request method: post, get
            url: "<?php echo Router::url(array('controller' => 'articles', 'action' => 'check_question')); ?>",
            data: {questitle: questitle}, // outer quotes removed
            cache: false,
            success: function (response, status) {
                $('.input-cat').html(response);
            },
           
        });
}else{
alert('Write Your article !');
}

    });
    $('body').undelegate('#Title', 'keyup').delegate('#Title', 'keyup', function (e) {
        //$('.second-step').show();
        event.preventDefault();
        var questitle = $('#Title').html();
        if(questitle.length>0){
        $('#QuestionTitle').val(questitle);
        $.ajax({
            type: "post", // Request method: post, get
            url: "<?php echo Router::url(array('controller' => 'articles', 'action' => 'check_question')); ?>",
            data: {questitle: questitle}, // outer quotes removed
            cache: false,
                   beforeSend: function () {
            $("#loader").fadeIn();
                },    
                success: function (response, status) {
                   $("#loader").fadeOut();
                    $('.input-cat').html(response);
                },
           
        });
}else{

}

    });
$('.flex-edit').click(function(){
$('.flex-container').slideToggle();
 var questitle = $('#Title').html();
$('.panel-article').html(questitle);
});


</script>
