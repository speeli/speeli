<style>
    .clear{
        clear:both;
    }
    *{
        font-family: Georgia,Cambria,'Times New Roman',Times,serif; 
    }
    .btn{
        text-transform: none;
    }
</style>
<div class="ask-new btn  btn-always-blue right" style="margin-right: 1em;"><h6>Create Speeli Article</h6></div>
<ol class="rounded-list" style="width: 100%; float:left;">

            <br class="clear">
    <?php
    if (!empty($questions)) {
        echo '<div class="info-box">
         <center><div class="info-box-text">We found articles with similar titles ,do you want to edit them instead ?</div></center>
    </div>';
        foreach ($questions as $question) {
            ?>
		<br class="clear">

            <?php
            //Create article anyway
            if (!empty($question['Question']['title_update'])) {
                $explode_title = explode(',,,', $question['Question']['title_update']);
                $last = end($explode_title);
                $title = end(unserialize(base64_decode($last)));
            } else {
                $title = $question['Question']['title'];
            }
            $title = preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', $title);
            echo"<li>" . $this->Html->link($title, array('action' => 'edit', $question['Question']['slug']), array('class' => 'navbar-brand', 'escape' => false,)
            ) . "</li>";
            ?>
        <?php
        }
      
    } else {
        echo '<br class="clear"><div class="ask-new-trig check"></div>';
      
    }
    echo $this->Html->css(array('lists'));
    ?>
</ol>
<br class="clear">
<script>
    $(document).ready(function () {
        var t = $('.check').hasClass("ask-new-trig");
        if (t) {
            setTimeout(function () {
                $('.ask-new-trig').trigger('click');
            }, 300);
        }
        function ask_new(questitle){
          var questitle =questitle;
          
            $('.panel-article').html(questitle);
            $('.flex-container').slideToggle();
            $('.second-step').show();
            $('.page_title_hidden').slideUp();
            $('#QuestionTitle').val(questitle);
            $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'articles', 'action' => 'get_cat')); ?>",
                data: {questitle: questitle}, // outer quotes removed
                cache: false,
                success: function (response, status) {
                    $('.get_cat').html(response);
                },
            });      
        }
        
        $('.ask-new').click(function () {
         var questitle = $('#Title').val();
            $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'articles', 'action' => 'filter')); ?>",
                data: {questitle: questitle}, // outer quotes removed
                cache: false,
                success: function (response, status) {
                    console.log(response);
                    ask_new(response);   
                },
            });  
        });
      
    });

</script>