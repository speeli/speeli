<div class="hpanel forum-box">
    <?php
    $i = 1;
    foreach ($questions as $question) {
        if ($question['Question']['status'] == 1) {
            ?>
            <div class="hpanel hblue">
                <div class="panel-heading hbuilt">
                    <h3> <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $question['Question']['slug'])); ?>"> <?php echo htmlspecialchars($question['Question']['title']); ?></a>  </h3>
                </div>
            </div>
            <?php
            $i++;
        }
    }
    ?>
</div>