<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="/css/nicefileinput.css">

<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

<style>
    .textarea{
        resize: none;
    }
    .hpanel p, .hpanel h4,.hpanel h2{
        border: 1px solid rgb(121, 199, 197);
    }
    .close{
        float: none;
        font-size: 0.7em;
        font-weight: 700;
        line-height: 1;
        margin-left: 1em;
        color: #428BCD;
        text-shadow: 0 1px 0 #fff;
        opacity: .2;
    }
.add-link{
margin-top: 0;
cursor:pointer;
    margin-left: -1em;
    }
    .no_padding{
        padding: 0
    }
    click{
        cursor: pointer;
    }
</style>
<?php
$admin = false;
if ($this->Session->check('user_quesli')) {
    $user = $this->Session->read('user_quesli');
    if ($user['User']['admin'] == 1) {
        $admin = true;
    }
}
?>
<?php if (isset($answers) && !empty($answers)) { ?>
    <?php
    $title = str_replace('&nbsp;', ' ', $answers[0]['Question']['title']);
    $title = str_replace('nbsp', ' ', $title);
    $title = str_replace('%', ' ', $title);




    $this->set("description", $answers[0]['Question']['title']);
    ?>
    <div class="row">

        <?php echo $this->Form->create('Question', array("type" => 'file')); ?>
        <!-- Add the extra clearfix for only the required viewport -->
        <div  class="clearfix visible-xs-block"></div>
        <br class="clear"><br class="clear">

        <div class="row ">
            <div class="col-md-2 flex-column ">

            </div>
            <div class="col-md-8  ">
                <!-- The time line -->
                <div class=" hpanel forum-box" >
                    <div class="panel-heading">
                        <div class="quesuserarticle-i"><?php echo $answers[0]['Question']['id']; ?></div>
                        <?php
                        echo $this->Form->create('Question', array('onclick'=>'unhook()',"type" => 'file'));
                        if (!empty($answers[0]['Question']['title_update'])) {
                            $explode_title = explode(',,,', $answers[0]['Question']['title_update']);
                            $last = end($explode_title);
                            $titles = end(unserialize(base64_decode($last)));
                        } else {
                            $titles = $answers[0]['Question']['title'];
                        }
                        $titles = str_replace("&nbsp;", "", $titles);
                        $this->set("page_title", $title);
                        ?>
                        <script type="text/javascript"><!-- // --><![CDATA[
                document.write('<input type="hidden" id="as_clear" name="as_clear" value="1" />');
                // ]]></script>
                        <center>  
                            <?php
                            echo $this->Form->input('art_id', array('value' => $answers[0]['Question']['id'], 'id' => 'art_id', 'type' => 'hidden', 'name' => 'art_id[]', 'div' => false, 'label' => false));
                            echo $this->Form->input('art_title', array('value' => strip_tags($titles), 'class' => 'calc', 'name' => 'art_title[]', 'div' => false, 'label' => false));
                            ?>

                            <br class="clear">
                            <br class="clear">
                            <?php echo $this->Form->input('image_title', array('id' => 'FileImage1', 'type' => 'file', 'name' => 'image_title[]', 'label' => false, 'div' => false)); ?>                    
                        </center>
                    </div>
                    <div class='  list' >
                        <?php
                        $i = 1;
                        foreach ($answers as $answer) {
                            if(!empty($answer['Answer']['last_title'])){
                           $title=$answer['Answer']['last_title'];
                       }else{
                            if (!empty($answer['Answer']['title_update'])) {
                            $explode_title = explode(',,,', $answer['Answer']['title_update']);
                            $last = end($explode_title);
                            $title = end(unserialize(base64_decode($last)));
                        } else {
                            $title = $answer['Answer']['title'];
                        }
                       }
                       if(!empty($answer['Answer']['last_body'])){
                            $body = $answer['Answer']['last_body'];
                            $body = trim($body);
                            $body = rtrim($body, ' ');
                            $body = rtrim($body, '.');
                            $body.=" .";
                       }else{
                            if (!empty($answer['Answer']['body_update'])) {
                            $explode_body = explode(',,,', $answer['Answer']['body_update']);
                            $last = end($explode_body);
                            $body = end(unserialize(base64_decode($last)));
                            $body = trim($body);
                            $body = rtrim($body, ' ');
                            $body = rtrim($body, '.');
                            $body.=" .";
                        } else {
                            $body = $answer['Answer']['body'];
                            $body = trim($body);
                            $body = rtrim($body, ' ');
                            $body = rtrim($body, '.');
                            $body.=" .";
                        }
                       }
                    
                            ?>
                            <div sort_num='<?php echo $i; ?>' id="<?php echo $answer['Answer']['id']; ?>"class="hpanel hblue" draggable="true"  >
                                <div class="panel-heading hbuilt">
                                    <b class="left"><?php echo $i . " )  "; ?> </b>
                                    <?php
                                    echo $this->Form->input('title', array('value' => $i, 'type' => 'hidden', 'name' => 'sort_value[]', 'div' => false, 'label' => false));
                                    echo $this->Form->input('title', array('value' => $answer['Answer']['id'], 'type' => 'hidden', 'name' => 'id[]', 'div' => false, 'label' => false));
                                    ?>
                                    <?php
                                    echo $this->Form->input('title', array('value' => $title, 'class' => 'calc', 'name' => 'title[]', 'div' => false, 'label' => false));
                                    ?>
                                </div>
                                <br class="clear">
                                <div class="panel-body" >
                                    <?php
                                    if (($answer['Answer']['image'] != NULL)) {
                                        if ($answer['Answer']['image_user'] == 0) {
                                            ?>
                                            <div style='float:right;top:1.5em; position: relative'  ques-art-a='<?php echo $answer['Answer']['id']; ?>' target='p' class="btn btn-danger">
                                                <span style="color:white;" class="fa fa-remove"></span>
                                            </div>           
                                            <img id='profile_img' src="<?php echo $this->webroot . 'question/' . $answers[0]['Question']['id'] . '/' . $answer['Answer']['image']; ?>" alt="..." class="img-thumbnail">
                                        <?php
                                        } else {
                                            if ($admin) {
                                                ?> 
                                                <div style='float:right;top:1.5em; position: relative'  ques-art-a='<?php echo $answer['Answer']['id']; ?>' target='p' class="btn btn-danger">
                                                    <span style="color:white;" class="fa fa-remove"></span>
                                                </div>           
                                                <img id='profile_img' src="<?php echo $this->webroot . 'question/' . $answers[0]['Question']['id'] . '/' . $answer['Answer']['image']; ?>" alt="..." class="img-thumbnail">

                                            <?php } ?>
                                            <?php
                                        }
                                    }?>
<div style='float: right; margin-bottom: -1em;' class=" add-url"> <span class=" fa fa-link"> add link</span></div><br class="clear">
                                    <?php
                                    echo $this->Form->textarea('body', array('maxlength' => '300', 'value' => $body, 'class' => 'QuestionBody', 'name' => 'body[]', 'div' => false, 'label' => false));
                                    ?>                                
                                    <div class="counter"></div><br class="clear">

                                </div>
                                <div class="panel-footer panel-footer-status">
                                    <a  href="<?php
                                    echo Router::url(array('controller' => 'articles',
                                        'action' => 'history', $answer['Answer']['id']))
                                    ?>" class="btn iframe-edit btn-main right">Show Edit History
                                    </a>
                                    <br class="clear"></div>


                            </div>
                            <?php
                            $i++;
                        }
                        ?>
                    </div>
                    <div class="hpanel hblue" >
                        <div class="panel-heading hbuilt">
                            <b class="left">Point <?php echo (count($answers) + 1) . " )  "; ?> </b>
                            <?php
                            echo $this->Form->input('title', array('placeholder' => 'Enter point header ...', 'class' => 'calc', 'name' => 'title_new[]', 'div' => false, 'label' => false));
                            ?>
                        </div>
                        <br class="clear">
                        <div class="panel-body" >
                            <div style='float: right; margin-bottom: -1em;' class=" add-url"> <span class=" fa fa-link"> add link</span></div>  <br class="clear">                    
                            <?php
                            echo $this->Form->textarea('body', array('maxlength' => '300', 'class' => 'QuestionBody', 'name' => 'body_new[]', 'div' => false, 'label' => false));

                            echo "<br class='clear'><br class='clear'>" . $this->Form->input('images', array('id' => 'FileImage1', 'type' => 'file', 'name' => 'image_new[]', 'label' => false, 'div' => false));
                            ?><br class="clear"> 
                            <div class="counter">300 characters left</div><br class="clear">
                        </div>

                        <div class="panel-footer panel-footer-status">


                            <br class="clear"></div>

                    </div>
                    <br class="clear">


                </div>
                <div step="<?php echo (count($answers) + 1); ?>" class="add_new btn btn-main right">Add another point</div><br class="clear">

                <br class="clear">

                <div class="panel with-nav-tabs hpanel hblue">
                    <div class="">
                        <ul class="nav nav-tabs panel-heading hbuilt">
                            <li class="active"><a href="#tab1default" data-toggle="tab">Add Reference</a></li>
                            <li><a href="#tab3default" data-toggle="tab">Topics</a></li>
                        </ul>
                    </div>
                    <div class="panel-body-larger">
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="tab1default">

                                <br class="clear">
                                <div class="panel-body panel-ref" >
                                    <ul class="list-unstyled" >

                                        <?php
                                        $f = 1;

                                        function validate_host($url) {
                                            $pattern = "/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?/";
                                            if (!preg_match($pattern, $url)) {
                                                return false;
                                            } else {
                                                if (preg_match("/http:/", $url)) {
                                                    
                                                } elseif (preg_match("/https:/", $url)) {
                                                    
                                                } else {
                                                    $url = 'http://' . $url;
                                                }
                                                return $url;
                                            }
                                        }

                                        $refs = explode(',,,,', $answers[0]['Question']['reference']);

                                        foreach ($refs as $ref) {
                                            $ref_url = explode(',,', $ref);
                                            if (!empty($ref_url[1]) && $ref_url[1] != ' ') {
                                                $ref_url[1] = validate_host($ref_url[1]);
                                                if (validate_host($ref_url[1])) {
                                                    $f++;
                                                    ?>
                                                    <div class='col-md-3 col-xs-12'><?php echo $this->Form->input('text', array('value' => $ref_url[0], 'placeholder' => 'Reference name.. . ', 'class' => 'Questionref', 'name' => 'ref_name[]', 'div' => false, 'label' => false)); ?></div>
                                                    <div class='col-md-8 col-xs-12'><?php echo $this->Form->input('text', array('value' => $ref_url[1], 'placeholder' => 'Reference url.. . ', 'class' => 'Questionref', 'name' => 'ref[]', 'div' => false, 'label' => false)); ?></div><br class="clear">
                                                    <?php
                                                }
                                            }
                                        }
                                        ?> 
                                    </ul>
                                    <b class="left"> <?php echo $f; ?>)</b> <br>

                                    <div class='col-md-3 col-xs-12'><?php echo $this->Form->input('text', array('placeholder' => 'Reference name.. . ', 'class' => 'Questionref', 'name' => 'ref_name[]', 'div' => false, 'label' => false)); ?></div>
                                    <div class='col-md-8 col-xs-12'><?php echo $this->Form->input('text', array('placeholder' => 'Reference url.. . ', 'class' => 'Questionref', 'name' => 'ref[]', 'div' => false, 'label' => false)); ?></div><br class="clear">

                                </div>
                                <div stepref='1' class='btn btn_ref'><span class="fa fa-plus"></span></div>

                            </div>

                            <div class="tab-pane fade" id="tab3default">
                                <ul class="list-unstyled new_cat_result" >
                                    <?php foreach ($tags as $tag) { ?>
                                        <li><a  art_id='<?php echo $answers[0]['Question']['id']; ?> ' href="<?php echo Router::url(array('controller' => 'categories', 'action' => 'view', $tag['Category']['slug'])); ?>"><i class="fa fa-tag"></i><?php echo $tag['Category']['name']; ?></a><label cat_id='<?php echo $tag['Category']['id']; ?>' class='close remove-cat'>X</label></li>
                                <?php } ?>
                                </ul>
    <?php echo $this->Form->input('new', array('class' => 'new-cat-value', 'placeholder' => 'write your new tags', 'label' => false, 'div' => false)); ?>

                                <div class="results">

                                </div>
                            </div>


                        </div>
                    </div>
                </div>



                <div class="right">
                    <a class='btn btn-alert' href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $answers[0]['Question']['slug'])); ?>">
                        <span class="fa fa-refresh">Cancel</span></a>
                        <?php
                        echo $this->Form->submit('Save changes', array('onclick'=>'unhook()','value' => __('Create Speeli article'), 'id' => 'login', 'class' => array('btn', ' btn-save'), 'div' => false));
                        echo $this->Form->end();
                        ?>
                </div>
            </div><!-- /.col -->


        </div><!-- /.row -->


    </div>
    <?php
} else {
    
}
?>
<?php
echo $this->Html->script(array('jquery.nicefileinput.min', 'jquery.colorbox-min'));
?>
<script>
    $('document').ready(function () {
jQuery.fn.extend({
insertAtCaret: function(myValue){
  return this.each(function(i) {
    if (document.selection) {
      //For browsers like Internet Explorer
      this.focus();
      sel = document.selection.createRange();
      sel.text = myValue;
      this.focus();
    }
    else if (this.selectionStart || this.selectionStart == '0') {
      //For browsers like Firefox and Webkit based
      var startPos = this.selectionStart;
      var endPos = this.selectionEnd;
      var scrollTop = this.scrollTop;
      this.value = this.value.substring(0, startPos)+myValue+this.value.substring(endPos,this.value.length);
      this.focus();
      this.selectionStart = startPos + myValue.length;
      this.selectionEnd = startPos + myValue.length;
      this.scrollTop = scrollTop;
    } else {
      this.value += myValue;
      this.focus();
    }
  })
}
});

        $('body').undelegate('.suggest-items', 'click').delegate('.suggest-items', 'click', function (e) {
            console.log('ataket');
            var art_id = $('#art_id').val();
            var cat_id = $(this).attr('ques-cat-id');
            var cat_name = $(this).html();
            $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'articles', 'action' => 'edit_art_add_cat')); ?>",
                data: {cat_id: cat_id, cat_name: cat_name, art_id: art_id}, // outer quotes removed
                cache: false,
                success: function (response, status) {
                    $('.new_cat_result').append(response);
                }
            });
        });
        $('.new-cat-value').keyup(function () {
            var data_send = $(this).val();
            $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'articles', 'action' => 'search_cat_edit')); ?>",
                data: {data_send: data_send}, // outer quotes removed
                cache: false,
                success: function (response, status) {
                    $('.results').html(response);
                },
            });
        });
        $(".iframe-edit").colorbox({opacity: '0.5', width: '50%', minHeight: '50%'});

        $(document).ready(function () {
            // your code...
            $("input[type=file]").nicefileinput();
        });
        $('.add_new').click(function () {
            console.log('dass');
            var step_number = parseInt($(this).attr('step'));
            step_number++;
            $(this).attr('step', step_number);
            $('.forum-box').append(
                    ' \n\
                    <div class="hpanel hblue">\n\
                        <div class="panel-heading hbuilt">\n\
                              <b class="left">' + step_number + ' )</b> \n\
<?php
echo $this->Form->input('title', array('placeholder' => 'enter point header .. . ', 'class' => 'calc', 'name' => 'title_new[]', 'div' => false, 'label' => false));
?></div><br class="clear"> \n\
                        <div class="panel-body"><?php
echo '<br class="clear"><div style="float: right; margin-bottom: -1em;" class=" add-url"> <span class=" fa fa-link"> add link</span></div><br class"clear">' . $this->Form->textarea('body', array('maxlength' => '300', 'placeholder' => 'Describe  your point .. . ', 'class' => 'QuestionBody', 'name' => 'body_new[]', 'label' => false)) . '<div class="panel-footer panel-footer-status"><br class="clear"><div class="counter">300 characters left</div><br class="clear"></div></div>';
echo $this->Form->input('image', array('type' => 'file', 'class' => 'right', 'name' => 'image_new[]', 'label' => false));
?><br class="clear"> \n\
</div>\n\
                  \n\
                  </div>');

        });
        $('.btn_ref').click(function () {

            var step_number = parseInt($(this).attr('stepref'));
            step_number++;
            $(this).attr('stepref', step_number);
            $('.panel-ref').append(
                    '\n\
                              <b class="left">' + step_number + ' )</b><br class="clear"><div class="col-md-3 col-xs-12"> \n\
<?php echo $this->Form->input('title', array('placeholder' => 'Reference name.. . ', 'class' => 'calc', 'name' => 'ref_name[]', 'div' => false, 'label' => false)); ?></div><div class="col-md-8 col-xs-12"><?php echo $this->Form->input('title', array('placeholder' => 'Reference url .. . ', 'class' => 'calc', 'name' => 'ref[]', 'div' => false, 'label' => false)); ?></div><br class="clear">');

        });
        $('body').undelegate('.remove-cat', 'click').delegate('.remove-cat', 'click', function (e) {
            $(this).parent().remove();
            var art_id = $('#art_id').val();
            var cat_id = $(this).attr('cat_id');

            $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'articles', 'action' => 'edit_art_delete_cat')); ?>",
                data: {cat_id: cat_id, art_id: art_id}, // outer quotes removed
                cache: false,
                success: function (response, status) {

                }
            })
        });

        $('body').undelegate('.QuestionBody', 'keyup').delegate('.QuestionBody', 'keyup', function (e) {
            var max = 300;
            var len = $(this).val().length;
            if (len >= max) {
                $(this).parent().find('.counter').text(' you have reached the limit');
                $(this).parent().find('.counter').css({color: 'red'});
            } else {
                var char = max - len;
                $(this).parent().find('.counter').text(char + ' characters left');
                $(this).parent().find('.counter').css({color: 'gray'});
            }
        });
                  $('body').undelegate('.add-url', 'click').delegate('.add-url', 'click', function (e) {
    $(this).parent('.panel-body').find('.add-url').after(
                '  <div class="panel-body panel-url panel-ref" >\n\
                       <div class="col-md-3 col-xs-12 no_padding">\n\
                           <?php echo $this->Form->input('urls', array('placeholder' => 'url title.. . ', 'class' => 'LinkName no_padding', 'div' => false, 'label' => false)); ?></div>\n\
                             <div class="col-md-7 col-xs-12 no_padding">\n\
                           <?php echo $this->Form->input('urls', array('placeholder' => 'url link like http://.. . ', 'class' => 'LinkLink no_padding', 'div' => false, 'label' => false)); ?></div>\n\
                    <span class="fa fa-plus no_padding add-link click col-md-2 col-xs-12"></span>\n\
                  \n\
                  </div>');
                               
    });
       $('body').undelegate('.add-link', 'click').delegate('.add-link', 'click', function (e) {
                            var current=$(this).parent().parent().find('.QuestionBody');
                            console.log($(this).parent().parent().parent().find('.QuestionBody').val());
                            var url_title=$(this).parent().find('.LinkName').val();
                            var url_link=$(this).parent().parent().find('.LinkLink').val();
                            var all_wor=current+ ' '+' <a href="'+url_link+'">'+url_title+'</a> '+' ';
                            var all_link=' <a href="'+url_link+'">'+url_title+'</a> '+' ';
                          // $(this).parent().parent().find('.QuestionBody').val(all_wor);
                              current.insertAtCaret(all_link);                       
                          // insertAtCursor(current,all_link);
                            $(this).parent().remove();
                           return false;

    });
        $('.btn-danger').click(function () {
            target = $(this).attr('target');
            ques_art = $(this).attr('ques-art-a');
            box1 = $(this).parent();
            box2 = $(box1).parent();
            console.log(ques_art);

            swal({
                title: "Confirm Delete?",
                type: "warning", showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Delete",
                cancelButtonText: "Cancel",
                closeOnConfirm: false, closeOnCancel: false
            },
            function (isConfirm) {
                if (isConfirm) {
                    $.post("<?php echo Router::url(array('controller' => 'articles', 'action' => 'delete_image')); ?>",
                            {
                                target: target,
                                ques_art: ques_art
                            },
                    function (data) {
                        swal("Delete");
                        if (target == 'a') {
                            window.location = "<?php echo $this->Html->url('/'); ?>";
                            return false;
                        } else {
                            box3 = $(box2).parent().slideToggle();
                        }

                    });
                } else {
                    swal("Canceling", ")", "error");

                }
            });
        });
        $('body').undelegate('.add-new-cat', 'click').delegate('.add-new-cat', 'click', function (e) {
            var art_id = $('#art_id').val();
            var add_new_cat = $('.new-cat-value').val();
            add_new_cat.replace(/ {2,}/g, '');
            $('.new-cat-value').val(' ');
            console.log(add_new_cat);
            $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'articles', 'action' => 'add_new_cat_edit')); ?>",
                data: {add_new_cat: add_new_cat, art_id: art_id}, // outer quotes removed
                cache: false,
                success: function (response, status) {
                    $('.new_cat_result').append(response);
                }
            });
        });
    });
</script>
<script>
        var warnMessage = "Save your unsaved changes before leaving this page!";
      
        $(function () {
            $('input[type=submit]').click(function (e) {
                window.onbeforeunload = null;
            });
        });
    </script>
  <script>
 window.onbeforeunload = function (evt) {
 var message = '';
if (typeof evt == 'undefined') {
 evt = window.event;

}
 if (evt ) {
   evt.returnValue = message;
 }

    return message;

}
</script>

<?php
echo $this->Html->css(array('sweet-alert'));
echo $this->Html->script(array('sweet-alert'));
?>
<script type="text/javascript" async src="//platform.twitter.com/widgets.js"></script>