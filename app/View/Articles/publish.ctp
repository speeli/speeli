<style>

    .hpanel p, .hpanel h4,.hpanel h2{
          border: 1px solid rgb(121, 199, 197);
    }
</style>
<div class="row">


    <!-- Add the extra clearfix for only the required viewport -->
    <div  class="clearfix visible-xs-block"></div>
    <br class="clear"><br class="clear">

<hr>
    <div class="row ">
         <div class="col-md-2 flex-column ">
             
    </div>
        <div class="col-md-8  ">
            <!-- The time line -->
            <div class="hpanel forum-box">
                <div class="panel-heading">
                    <div class="quesuserarticle-i"><?php echo $answers[0]['Question']['id']; ?></div>
                    <?php
                if(!empty($answers[0]['Question']['title_update'])){
                      $explode_title=  explode(',,,', $answers[0]['Question']['title_update']);
                $last=  end($explode_title);
                    $titles=end(unserialize(base64_decode($last)));
                }else{
                    $titles=$answers[0]['Question']['title'];
                }
                
                    ?>
                    <center>  
                        <h3 >  <?php echo 'Review Your Article' ?></h3>
                        <h2 >  <?php echo $titles; ?></h2>
                            
                            <div  class="edit-art-t btn btn-main" edit-article="f"  ><span class="glyphicon glyphicon-edit"></span></div>
                            <br class="clear">
                            <a class='btn btn-always-blue right' href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view',$answers[0]['Question']['slug'])); ?>">
                            <span class="glyphicon glyphicon-check">Publish SpeeLi</span></a>
                    <br class="clear">
                               
                    </center>
                </div>
                
                <?php
                $i = 1;
                foreach ($answers as $answer) {
                if(!empty($answer['Answer']['title_update'])){
                      $explode_title=  explode(',,,', $answer['Answer']['title_update']);
                $last=  end($explode_title);
                
                    $title=end(unserialize(base64_decode($last)));
                }else{
                    $title=$answer['Answer']['title'];
                }
                if(!empty($answer['Answer']['body_update'])){
                 $explode_body=  explode(',,,', $answer['Answer']['body_update']);
                $last=  end($explode_body);
                 $body=end(unserialize(base64_decode($last)));
                }else{
                    $body=$answer['Answer']['body'];
                }
                    ?>
                    <div class="hpanel hblue">
                        <div class="panel-heading hbuilt">
                           <b class="left"><?php echo  $i . " )  "; ?> </b><h4 ques-ans-id="<?php echo $answer['Answer']['id']; ?>" ><?php echo $title; ?></h4> 
                            <div  class="edit-ques-t btn btn-main" edit-article="f"  ><span class="glyphicon glyphicon-edit"></span></div>
                        </div>
                        <br class="clear">
                        <div class="panel-body" >
                            <?php if ($answer['Answer']['image'] != NULL) { ?>

                                <img id='profile_img' src="<?php echo $this->webroot . 'question/' . $answers[0]['Question']['id'] . '/' . $answer['Answer']['image']; ?>" alt="..." class="img-thumbnail">
                            <?php } ?>
                                <p ques-ans-id="<?php echo $answer['Answer']['id']; ?>" contenteditable="true"><?php echo $body; ?></p>
                                <div  class="edit-ques-b btn btn-main" edit-article="f" ><span class="glyphicon glyphicon-edit"></span></div>
                        </div>
                      
  

                    </div>
                    <?php
                    $i++;
                }
                ?>
                <!-- timeline time label -->
                <!-- END timeline item -->
                <!-- timeline item -->
            </div>
        </div><!-- /.col -->
    </div><!-- /.row -->
    <hr>
      <br class="clear">
  

</div><?php
echo $this->Html->script(array('jquery.nicefileinput.min','jquery.colorbox-min'));
?>
<script>
    $('document').ready(function () {
        $(".iframe-edit").colorbox({opacity:'0.5',width:'50%',minHeight:'50%'});
        $('.hpanel h4').attr('contenteditable', 'true');
        $('.hpanel p').attr('contenteditable', 'true');
        $('.hpanel h2').attr('contenteditable', 'true');
        $(document).ready(function () {
            // your code...
            $("input[type=file]").nicefileinput();
        });
           $('.edit-ques-t').on('click', function () {
            event.preventDefault();
            var quesuserarticle_i=$('.quesuserarticle-i').html();
            
     var text_change=$(this).attr('edit-article');
     console.log(text_change);
           
            $(this).attr('edit-article','f');   
                
                $(this).parent().find('h4').removeAttr('contenteditable');
                var after_edit=$(this).parent().find('h4');
        var quesuseri = $(this).parent().find('h4').attr('ques-ans-id');        
        var quesuserv = $(this).parent().find('h4').html();
                var quesuserd ='t';
                console.log(quesuseri+quesuserv+quesuserarticle_i);
                $.post("<?php echo Router::url(array('controller' => 'articles', 'action' => 'update')); ?>",
                        {   
                            quesuserarticle_i:quesuserarticle_i,
                            quesuserv: quesuserv,
                            quesuseri:quesuseri,
                            quesuserd: quesuserd
                        },
                function (data) {
                        after_edit.html(quesuserv);
                });
            
        });
    $('.edit-art-t').on('click', function () {
            event.preventDefault();
            var quesuserarticle_i=$('.quesuserarticle-i').html();
            
     var text_change=$(this).attr('edit-article');
     console.log(text_change);
           
            $(this).attr('edit-article','f');   
                
                $(this).parent().find('h2').removeAttr('contenteditable');
                var after_edit=$(this).parent().find('h2');
                
        var quesuserv = $(this).parent().find('h2').html();
                var quesuserd ='t';
                console.log(quesuserv+quesuserarticle_i);
                $.post("<?php echo Router::url(array('controller' => 'articles', 'action' => 'update_article')); ?>",
                        {   
                            quesuserarticle_i:quesuserarticle_i,
                            quesuserv: quesuserv,
                            quesuserd: quesuserd
                        },
                function (data) {
                        after_edit.html(quesuserv);
                });
            
        });
$('.edit-ques-b').on('click', function () {
            event.preventDefault();
           var text_change=$(this).attr('edit-article');
            var quesuserarticle_i=$('.quesuserarticle-i').html();
            var text_change = $(this).html();
            $(this).attr('edit-article','f');
        $(this).parent().find('p').removeAttr('contenteditable');
                var after_edit=$(this).parent().find('p');
        var quesuseri = $(this).parent().find('p').attr('ques-ans-id');        
        var quesuserv = $(this).parent().find('p').html();
                var quesuserd ='b';
                $.post("<?php echo Router::url(array('controller' => 'articles', 'action' => 'update')); ?>",
                        {   
                            quesuserarticle_i:quesuserarticle_i,
                            quesuserv: quesuserv,
                            quesuseri:quesuseri,
                            quesuserd: quesuserd
                        },
                function (data) {
                        after_edit.html(quesuserv);
                });
            
        });
        $("#FileImage1").nicefileinput({
            label: 'Upload your new photo...' // Spanish label
        });
        /* update image start*/
        $('body').undelegate('.update-image', 'click').delegate('.update-image', 'click', function (e) {
            var form = $(this).parents('form');
            e.preventDefault();
            if (document.getElementById('FileImage1').value != '') {
                start_upload(form);
            } else {
                alert('choose file');
            }
        });
        function generate(quesuserd, quesuserv) {
            return generatble = ' <h3 quesuserd=' + quesuserd + '>' + quesuserv + '</h3> <div class="btn btn-success edit edit-bio"></div>';
        }
        function start_upload(form) {
            var data = new FormData();

            var UploadedFile = document.getElementById("FileImage1");
            for (var i = 0; i < UploadedFile.files.length; i++) {
                data.append('data[Logo][image]', UploadedFile.files[i]);
            }
            //console.dir(UploadedFile.files);
            var request = new XMLHttpRequest();
            $('.progress').show();
            $('#upload_progress').addClass('active');
            $('#upload_progress').show(1);
            request.upload.addEventListener('load', function () {
                $('#upload_progress').removeClass('active');
                $('#upload_progress').hide(200);
            });
            request.upload.addEventListener('progress', function (event) {
                if (event.lengthComputable) {
                    var progress = Math.round(100 * event.loaded / event.total);
                    $('#upload_progress .bar').css({'width': progress + '%'});
                }
            });

            request.open("POST", '<?php echo Router::url(array('controller' => 'users', 'action' => 'logo')); ?>');
            request.onreadystatechange = function ()
            {
                if (request.readyState == 4 && request.status == 200)
                {
                    var url = request.responseText;

                    $('#profile_img').attr("src", url);

                }
            }
            request.setRequestHeader('Cache-Control', 'no-cache');
            request.send(data);
        }
        ;
        /* update image end*/

        $('.edit').on('click', function () {
            event.preventDefault();

            var target = $(this).attr('target');
            var text_change = $(this).html();
            console.log(text_change);
            if (text_change == 'edit') {
                $(this).html('update');
                $(this).parent().find('h3').attr('contenteditable', 'true');
                $(this).attr('update', target);
                $(this).parent().find('h3').focus();
            } else {
                $(this).html('edit');
                $(this).parent().find('h3').removeAttr('contenteditable');
                var quesuserv = $(this).parent().find('h3').html();
                var quesuserd = $(this).parent().find('h3').attr('quesuserd');

                $(this).removeAttr('update');
                $.post("<?php echo Router::url(array('controller' => 'users', 'action' => 'update')); ?>",
                        {
                            quesuserv: quesuserv,
                            quesuserd: quesuserd
                        },
                function (data) {

                });
            }
        });

        $('.add').on('click', function () {
            event.preventDefault();
            var text_change = $(this).html();

            if (text_change == 'add') {

                $(this).html('edit');
                var quesuserd = $(this).attr('quesuserd');

                var quesuserv = $(this).parent().find('#quesuserput').val();
                console.log(quesuserv);
                $.post("<?php echo Router::url(array('controller' => 'users', 'action' => 'add_profile')); ?>",
                        {
                            quesuserv: quesuserv,
                            quesuserd: quesuserd
                        },
                function (data) {
                    console.log('wla 7mo ltmsa7 2l');

                    $(this).parent().find('#quesuserput').remove();
                });
            }
        });


    });
</script>