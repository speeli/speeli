<style>
    .QuesTitle{
        display: none;
    }
    #QuestionTitle,.QuestionTitle{
        width: 100%;
	margin-bottom: 2em;
    }
    .add-link{
        margin-top: 1.5em;
        margin-left: -1em;
    }
    .no_padding{
        padding: 0
    }
    .flex-done{
        display: none;
    }
    #loader{
        display: none;
    }
    .second-step{
        display: none;
        float: left;
    }
    .step_number{
        float: left;
        margin-right: 1vw;
    }
    ul{
        list-style: none;
    }
    li{`
       list-style: none;
       }
       .clear{
       clear: both;
       }
       .text{
       margin: 1em auto;
       width: 70%;
       }
       .ask-new{
       cursor: pointer;
       }
       .click{
       cursor: pointer;
       }
       .close{
       float: right;
       font-size: 1em;
       margin: 0.2em auto;
       }
       .new_cat_result .btn-main{
       border: 1px solid rgb(0, 151, 255);
       font-size: 1em;
       border-radius: 0.5em;
       margin: 1em;
       }
       .new_cat_result{
       margin: 1em;
       }
       .panel-footer-status{
       width: 100%;
       margin: 0px;
       margin-top: -21px;
       } 
       .counter{
       float: right;
       }
    </style>
    <style>
        .suggest {
        margin-top: -1em;
        border: 1px solid rgba(128, 128, 128, 0.7);
        /* margin: 0 auto; */
        margin-left: 14px;
        box-shadow: 1px 1px 7px;
        }
        .suggest-items {
        float: left;
        margin-top: 0.25em;
        margin-bottom: 0.25em;
        }
        #title{min-height: 3em; }
        .btn-always-blue{
        color: white;
        padding: 0.2em 0.2em;
        }
    </style>
    <?php
    $this->set("page_title", 'Add');
    echo $this->Form->create('Question', array('onclick' => 'unhook()', "type" => 'file'));
    ?>
    <div class="row question">
        <center><h4>Summarise something in a <a target="_blank" style="color:#63B5B2" href="<?php echo Router::url(array('controller'=>'articles','action'=>'view','What-is-Speeli'));?>">Speeli article</a></h4></center>
        <br class="clear"><br class="clear">
        
        <div class="panel panel-default add-article">
            <div class="panel-heading flex-container">
                <h5 class="panel-title text-info article-title">Article title : </h5>
                <?php
                echo $this->Form->input('title', array('value'=>$request['Request']['title'],'id' => 'Title', 'placeholder' => "Write your Article's Title here", 'class' => 'login-form-text flex-column',
                    'div' => false, 'label' => false));
                ?>
                 <?php echo $this->Form->text('request_check', array('type' => 'hidden', 'value' => $request_check)); ?>
                    <?php echo $this->Form->text('request_id', array('type' => 'hidden', 'value' => $request['Request']['id'])); ?>
              
                <div id="loader" class="col-md-12 float-none" >
                    <cetner><img src="<?php echo $this->webroot . 'files/8.gif'; ?>">
                        Related Speeli articles
                    </cetner>
                </div> 
                <div class="input-cat clear">
                </div>
            </div>
        </div>
        <br class="clear">
        <div class="row">
            <div class="second-step  col-md-10 col-xs-12 float-none " >
                <p class="gray-tone">Article title </p><center><h2 class="page-header panel-article"></h2>
                    <?php echo $this->Form->text('title', array('required'=>true,'class' => 'QuesTitle', 'label' => false, 'div' => false)); ?>
                </center>
                <script type="text/javascript"><!-- // -->< ![CDATA[
                            document.write('<input type="hidden" id="as_clear" name="as_clear" value="1" />');
                    // ]]></script>
                <div class="flex-edit btn btn-main-always right">Edit your title</div>
                <div class="flex-done btn btn-main-always right">Edit your title</div>


                <br class="clear">
                <?php echo $this->Form->input('image_title', array('id' => 'FileImage1', 'type' => 'file', 'name' => 'image_title[]', 'label' => false, 'div' => false)); ?>                    
                <br class="clear">
                <br class="clear">
                <ul class=" st-steps  ">
                    <li>
                        <div class="panel panel-default ">
                            <div class="panel-heading">
                                <h1 class="panel-title">Point number 1</h1>
                            </div>
                            <br class="clear">
                            <div class="panel-body">
                                <?php echo $this->Form->input('title', array('maxlength'=>70,'required'=>true,'placeholder' => 'enter point header .. . ', 'name' => 'title[]', 'label' => false, 'div' => false)); ?>                  
                                <br class="clear">
                                <?php echo $this->Form->textarea('body', array('maxlength' => '300', 'placeholder' => 'Describe  your point .. . ', 'class' => 'QuestionBody', 'name' => 'body[]', 'label' => false, 'div' => false)) . "<div class='panel-footer panel-footer-status'>            <br class='clear'></div>"; ?>
                                <?php echo $this->Form->input('image', array('id' => 'FileImage1', 'type' => 'file', 'name' => 'image[]', 'label' => false, 'div' => false)); ?>                    

                            </div>
                        </div>
                    </li>
                </ul>
                
                <br class="clear">
                <div step="1" class="add_new btn btn-main right">Add another point</div>
                <br class="clear">
                <div class="get_cat">

                </div>
                <br class="clear">

                <?php echo $this->Form->input('new', array('class' => 'new-cat-value', 'placeholder' => 'write your new tags', 'label' => false, 'div' => false)); ?>

                <div class="results">


                </div>
                <br class="clear">
                <div class="hpanel hblue">
                    <div class="panel-heading hbuilt">
                        <b class="left">Add Reference</b>
                    </div>
                    <br class="clear">
                    <div class="panel-body panel-ref" >
                        <b class="left"> 1)</b> <br>
                        <div class='col-md-3 col-xs-12'><?php echo $this->Form->input('text', array('placeholder' => 'Reference name.. . ', 'class' => 'Questionref', 'name' => 'ref_name[]', 'div' => false, 'label' => false)); ?></div>
                        <div class='col-md-8 col-xs-12'><?php echo $this->Form->input('text', array('placeholder' => 'Reference url.. . ', 'class' => 'Questionref', 'name' => 'ref[]', 'div' => false, 'label' => false)); ?></div><br class="clear">

                    </div>
                    <br class="clear"><div stepref='1' class='btn btn_ref'><span class="fa fa-plus"></span></div>
                </div>
                <br class="clear">
                <?php
                echo $this->Form->submit('Publish Speeli Article', array('value' => __('Create Speeli article'), 'id' => 'login', 'class' => 'btn  btn-save pull-right', 'div' => false));
                echo $this->Form->end();
                ?>
                <br class="clear">

            </div>
        </div>
    </div>

    <?php
    echo $this->Html->css(array('sweet-alert'));
    echo $this->Html->script(array('sweet-alert'));
    ?>
    <?php
    echo $this->Html->script(array('tinymce/tinymce.min'));
    ?>

    <script>
    
    
</script>
    <script type="text/javascript">
        
        var max_chars = 300; //max characters
        var max_for_html = 300; //max characters for html tags
        var allowed_keys = [8, 13, 16, 17, 18, 20, 33, 34, 35, 36, 37, 38, 39, 40, 46];
        var chars_without_html = 0;
        function alarmChars() {
            if (chars_without_html > (max_chars - 25)) {
                $('#chars_left').css('color', 'red');
            } else {
                $('#chars_left').css('color', 'gray');
            }
        }
        function myCustomOnChangeHandler(inst) {

            var test = inst.getBody().innerHTML;
            var temp = document.createElement("div");
            temp.innerHTML = test;
            var test = temp.textContent || temp.innerText;
            console.log(test);
            if (test.length > 300) {
                var test = test.substring(0, 300);
                return  tinyMCE.activeEditor.setContent(test);
            }
        }
        $(function () {
           

            chars_without_html = $.trim($("#description_edit").text().replace(/(<([^>]+)>)/ig, "")).length;
            $('#chars_left').html(max_chars - chars_without_html);
            alarmChars();
        });
    

         tinymce.init({
                selector: "textarea",
                plugins: [
                    " lists link customcharactercount",
                ],
                menubar: false,
                remove_linebreaks : true,
                browser_spellcheck: true,
                link_title: false,
                force_br_newlines : true,
                toolbar1: "bold italic  | link ",
                 target_list: false,
                        setup: function (ed) {
                    
     ed.on('init', function() 
    {
        this.getDoc().body.style.fontSize = '0.8em';
    });
    ed.on("keyup", function () {
                    myCustomOnChangeHandler(ed);
                });
                    
                },
                 init_instance_callback: function (editor) {
    $('.mce-tinymce').show('fast');
    $(editor.getContainer()).find(".mce-path").css("display", "none");
  },
            });


        $(document).ready(function () {
           
            $('.btn_ref').click(function () {

                var step_number = parseInt($(this).attr('stepref'));
                step_number++;
                $(this).attr('stepref', step_number);
                $('.panel-ref').append(
                        '\n\
                                  <b class="left">' + step_number + ' )</b><br class="clear"><div class="col-md-3 col-xs-12"> \n\
<?php echo $this->Form->input('title', array('placeholder' => 'Reference name.. . ', 'class' => 'calc', 'name' => 'ref_name[]', 'div' => false, 'label' => false)); ?></div><div class="col-md-8 col-xs-12"><?php echo $this->Form->input('title', array('placeholder' => 'Reference url .. . ', 'class' => 'calc', 'name' => 'ref[]', 'div' => false, 'label' => false)); ?></div><br class="clear">');

            });
            $('.action').click(function () {
                var action_tag = $(this).text();
                var highlight = window.getSelection();
                var span = '<span class="bold">' + highlight + '</span>';
                var text = $('.textEditor').html();
                $('.textEditor').html(text.replace(highlight, span));
            });

            $('body').undelegate('.suggest-items', 'click').delegate('.suggest-items', 'click', function (e) {
                
                $('.suggest').hide();
                $('.new-cat-value').val('');
                var cat_id = $(this).attr('ques-cat-id');
                var cat_name = $(this).html();
                $('.new_cat_result').append("<div class='col-md-2 btn-main click'>\n\
              <input name='new[]' value=" + cat_id + " type='hidden'>" + cat_name + "<label class='close remove-cat'>X</label>")
            });

            $('body').undelegate('.remove-cat', 'click').delegate('.remove-cat', 'click', function (e) {
                $(this).parent().remove();
            })

            $('.new-cat-value').keyup(function () {
                var data_send = $(this).val();

                $.ajax({
                    type: "post", // Request method: post, get
                    url: "<?php echo Router::url(array('controller' => 'articles', 'action' => 'search_cat')); ?>",
                    data: {data_send: data_send}, // outer quotes removed
                    cache: false,
                    success: function (response, status) {

                        $('.results').html(response);
                    },
                });
            });
            $('#Title').keydown(function () {
                var questitle = $(this).html();
                if (questitle.length > 0) {
                    questitle.replace(/ {2,}/g, '');
                    questitle.replace('&nbsp;', '');
                    $('#QuestionTitle').val(questitle);
                }
            });
        });

        $(document).keypress(function (event) {
            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {
                event.preventDefault();
            }
        });



        $('body').undelegate('.QuestionBody', 'keyup').delegate('.QuestionBody', 'keyup', function (e) {
            var max = 300;
            var len = $(this).val().length;
            if (len >= max) {
                //$(this).('.counter').text(' you have reached the limit');
                $(this).parent().find('.counter').css({color: 'red'});
            } else {
                var char = max - len;
                $(this).parent().find('.counter').text(char + ' characters left');
                $(this).parent().find('.counter').css({color: 'gray'});
            }
        });
        $('.add_new').click(function () {
            tinymce.remove();
            var step_number = parseInt($(this).attr('step'));
            step_number++;
            $(this).attr('step', step_number);
            
            
            $('ul.st-steps li:last').after(
                    '  <li>\n\
                        <div class="panel panel-default">\n\
                            <div class="panel-heading">\n\
                                <h1 class="panel-title">Point number ' + step_number + ' </h1>\n\
                            </div> \n\
                            <div class="panel-body"><?php
echo $this->Form->input('title', array('maxlength'=>70,'required'=>true,'id' => 'id', 'placeholder' => 'Enter point header .. . ', 'class' => 'QuestionTitle calc', 'name' => 'title[]', 'div' => false, 'label' => false));
echo '<br class="clear"><div style="float: right; margin-bottom: -1em;" class=" add-url"> <span class=" fa fa-link"> add link</span></div>' .
 $this->Form->textarea('body', array('id' => false, 'maxlength' => '300', 'placeholder' => 'Describe  your point .. . ', 'class' => 'QuestionBody', 'name' => 'body[]', 'label' => false))
 . '<div class="panel-footer panel-footer-status"><br class="clear"></div></div>';
echo $this->Form->input('image', array('type' => 'file', 'class' => 'calc', 'name' => 'image[]', 'label' => false));
?><br class="clear"> \n\
    </div>\n\
                      \n\
                      </div></li>');
            tinymce.init({
                selector: "textarea",
                plugins: [
                    " lists link customcharactercount",
                ],
                
                menubar: false,
                target_list: false,
                browser_spellcheck: true,
                link_title: false,
                remove_linebreaks : true,
                toolbar1: "bold italic  | link ",
              
                        init_instance_callback: function (editor) {
    $('.mce-tinymce').show('fast');
    $(editor.getContainer()).find(".mce-path").css("display", "none");
  },
  
  setup :
  function(ed) {
  
    ed.on('init', function() 
    {
        this.getDoc().body.style.fontSize = '0.8em';
    });
    ed.on("keyup", function () {
                    myCustomOnChangeHandler(ed);
                });
  },
            });

        });

        $('.check_status').on('click', function (event) {
            //$('.second-step').show();
            event.preventDefault();
            var questitle = $('#Title').val();
            if (questitle.length > 0) {
                $('#QuestionTitle').val(questitle);
                $.ajax({
                    type: "post", // Request method: post, get
                    url: "<?php echo Router::url(array('controller' => 'articles', 'action' => 'check_question')); ?>",
                    data: {questitle: questitle}, // outer quotes removed
                    cache: false,
                    success: function (response, status) {
                        $('.input-cat').html(response);
                    },
                });
            } else {
                alert('Write Your article !');
            }

        });

        $('body').undelegate('#Title', 'keyup').delegate('#Title', 'keyup', function (event) {
            //$('.second-step').show();
            event.preventDefault();
            var questitle = $('#Title').val();
            if (questitle.length > 0) {
                $('#QuestionTitle').val(questitle);
                $.ajax({
                    type: "post", // Request method: post, get
                    url: "<?php echo Router::url(array('controller' => 'articles', 'action' => 'check_question')); ?>",
                    data: {questitle: questitle}, // outer quotes removed
                    cache: false,
                    beforeSend: function () {
                        $("#loader").fadeIn();
                    },
                    success: function (response, status) {
                        $("#loader").fadeOut();
                        $('.input-cat').html(response);
                    },
                });
            } else {

            }

        });



        $('body').undelegate('.add-url', 'click').delegate('.add-url', 'click', function (e) {
            $(this).parent('.panel-body').find('.add-url').after(
                    '  <div class="panel-body panel-url panel-ref" >\n\
                           <div class="col-md-3 col-xs-12 no_padding">\n\
<?php echo $this->Form->input('urls', array('placeholder' => 'url title.. . ', 'class' => 'LinkName no_padding', 'div' => false, 'label' => false)); ?></div>\n\
                                 <div class="col-md-7 col-xs-12 no_padding">\n\
<?php echo $this->Form->input('urls', array('placeholder' => 'url link like http://.. . ', 'class' => 'LinkLink no_padding', 'div' => false, 'label' => false)); ?></div>\n\
                        <span class="fa fa-plus no_padding add-link click col-md-2 col-xs-12"></span>\n\
                      \n\
                      </div>');

        });
       
        $('body').undelegate('.flex-done', 'click').delegate('.flex-done', 'click', function (e) {
            $(this).hide();
            $('.flex-edit').show();
            $('.QuesTitle').hide();
            $('.panel-article').show();
            var questitle = $('.QuesTitle').val();
            
            $('.panel-article').html(questitle);
            event.stopPropagation();
        });
        $('.flex-edit').click(function () {
            $(this).hide();
            $('.flex-done').show();
            $('.QuesTitle').show();
            $('.panel-article').hide();
            var questitle = $('.QuesTitle').val();
            $('.panel-article').html(questitle);
            event.stopPropagation();
        });

        $('#login').click(function () {
            var t = 0;
            $(".QuestionTitle").each(function () {
                t++;
                if ($(this).val().length < 2) {
                    swal('Speeli alert', "You have to write title for point Number : " + t);

                    event.stopPropagation();
                    event.preventDefault();
                }
            });
            var b = 0;

        });

    </script>
    <script>
        var warnMessage = "Save your unsaved changes before leaving this page!";

        $(function () {
            $('input[type=submit]').click(function (e) {
                window.onbeforeunload = null;
            });
        });
    </script>
    <script>
        window.onbeforeunload = function (evt) {
            var message = '';
            if (typeof evt == 'undefined') {
                evt = window.event;

            }
            if (evt) {
                evt.returnValue = message;
            }

            return message;

        }
    </script>
    