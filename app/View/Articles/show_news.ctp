<style>
    .expand{
        cursor: pointer;
    }
    .click{
        cursor: pointer;
    }
    .progress{
        display: none;
    }
    hr{
        border: 0;
        height: 1px;
        background-image: -webkit-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -moz-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -ms-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -o-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
    }
    .clear{
        clear: both;
    }
    .question_wrapper{
        margin: 0 auto;
    }
    .float-none{
        float:none;
        margin: 0 auto;
    }
    .hblue{
        background-color: #fff;
        padding: 1em;
        border-radius: 4px;
        -moz-border-radius: 4px;
        -webkit-border-radius: 4px;
        margin-bottom: 25px;
        position: relative;
        line-height: 25px;
        border: 1px solid #e5e5e5;
    }
    .hblue .panel-heading{
        padding: 10px 15px;
        background-color: rgba(245, 245, 245, 0.24);
        border-top: 1px solid #ddd;

        border-bottom-right-radius: 3px;
        border-bottom-left-radius: 3px;
    }
    .list-unstyled li{
        margin-left: 1em;
    }
</style>
<?php
$this->set("meta_description", "Latest Speeli articles");
$this->set("meta_robots", "noindex");
?>
<?php
$this->Paginator->options(array(
    'update' => '.question_update',
    'evalScripts' => true
));
?>
<div class="row">
    <!-- Add the extra clearfix for only the required viewport -->
    <div  class="clearfix visible-xs-block"></div>
    <br class="clear">
    <hr>
    <div class="row ">

        <div class="col-md-8 float-none update_art_speeli  " style="border-left: 1px solid #ddd;">
            <!-- The time line -->
            <div class="hpanel forum-box question_update">
                <?php
                $i = 1;

                $user = $this->Session->read('user_quesli');
                if ($user['User']['admin'] == 1) {
                    foreach ($questions as $question) {
                        if (!empty($question['Question']['title_update'])) {
                            $explode_title = explode(',,,', $question['Question']['title_update']);
                            $last = end($explode_title);
                            $title = end(unserialize(base64_decode($last)));
                        } else {
                            $title = $question['Question']['title'];
                        }
                        if ($question['Question']['status'] == 1) {
                            $status = '<span style="color:green" class="fa fa-check-circle">verified</span>';
                        } else {
                            $status = '';
                        }
                        if ($question['Question']['expand'] == 1) {
                            $expand = '<span style="color:green" class="fa fa-lightbulb-o">Needs expanding</span>';
                            $art_status = 0;
                        } else {
                            $art_status = 1;
                            $expand = '<span style="color:grey" class="fa fa-lightbulb-o">Mark as :needs expanding</span>';
                        }
                        if ($question['Question']['quality'] == 1) {
                            $quality = '<span style="color:green" class="fa fa-lightbulb-o">passed quality guidelines</span>';
                            $art_quality = 0;
                        } else {
                            $art_quality = 1;
                            $quality = '<span style="color:grey" class="fa fa-lightbulb-o">no quality check</span>';
                        }
                        if ($question['Question']['edited'] == 1) {
                            $edited = '<span style="color:green" class="fa fa-lightbulb-o">Edited </span>';

                            $art_edit = 0;
                        } else {
                            $art_edit = 1;
                            $edited = '<span style="color:grey" class="fa fa-lightbulb-o">not edited</span>';
                        }
                        if ($question['Question']['locked'] == 1) {
                            $locked = '<span style="color:green" >Locked </span>';

                            $art_lock = 0;
                        } else {
                            $art_lock = 1;
                            $locked = '<span style="color:grey" >Open</span>';
                        }
                        if ($question['Question']['image'] != NULL) {
                            $image_Status = '<span style="color:green" class="fa fa-lightbulb-o">Has image</span>';
                        } else {
                            $image_Status = '<span style="color:grey" class="fa fa-lightbulb-o">No image</span>';
                        }
                        if ($question['Question']['links'] == 1) {
                            $link_title = '<span style="color:green" class="fa fa-lightbulb-o">Has link</span>';
                            $link_status = 0;
                        } else {
                            $link_status = 1;
                            $link_title = '<span style="color:grey" class="fa fa-lightbulb-o">No links</span>';
                        }
                        if ($question['Question']['disable_ads'] == 0) {
                            $ads_title = '<span style="color:green" class="fa fa-ban"> Disable Ads</span>';
                            $ads_status = 0;
                        } else {
                            $ads_status = 1;
                            $ads_title = '<span style="color:grey" class="fa fa-unlock"> Enable Ads</span>';
                        }
                        ?>
                        <div class="hpanel hblue">
                            <div class="panel-heading hbuilt">
                                <h3> <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $question['Question']['slug'])); ?>"> <?php echo html_entity_decode($title); ?></a>  </h3>
                                <?php echo $status; ?>
                                <div class="expand click"  art_status="<?php echo $art_status; ?>" art_id="<?php echo $question['Question']['id'] ?>"><?php echo $expand ?></div>
                                <div class="quality click"art_quality="<?php echo $art_quality; ?>" art_id="<?php echo $question['Question']['id'] ?>"><?php echo $quality ?></div>
                                <div class="edited click"  art_edit="<?php echo $art_edit; ?>" art_id="<?php echo $question['Question']['id'] ?>"><?php echo $edited ?></div>
                                <div class="locked click"  locked="<?php echo $art_lock; ?>" art_id="<?php echo $question['Question']['id'] ?>"><?php echo $locked ?></div>
                                <div class="linked click"  link_status="<?php echo $link_status; ?>" art_id="<?php echo $question['Question']['id'] ?>"><?php echo $link_title ?></div>
                                <div class="disable_ads click"  ads_status="<?php echo $ads_status; ?>" art_id="<?php echo $question['Question']['id'] ?>"><?php echo $ads_title ?></div>
                                <div class=""><?php echo $image_Status ?></div>
                                <?php if ($question['Question']['summary_id'] == 0) { ?>
                                    <?php echo $this->Form->input('news', array('class' => 'new-sum-value', 'target' => $question['Question']['id'], 'placeholder' => 'write main category', 'label' => false, 'div' => false)); ?>
                                    <div  class="results_<?php echo $question['Question']['id'] ?>">
                                    </div>
                                <?php } ?>
                            </div>
                            <div style="float:right;" class="panel-footer-status"><span class="fa fa-calendar"><?php echo $question['Question']['created'] ?></span></div>
                            <div style="float:left;" class="panel-footer-status"><span class="fa fa-camera"></span><b> <?php echo $question['Question']['views'] ?></b></div>
                           
                            <div style="float:left;" class="panel-footer-status"><span class="fa fa-facebook"></span><b> | Shares :  <?php echo $question['Question']['share_count'] ?></b></div>
                            <br class="clear">

                        </div>
                        <?php
                        $i++;
                    }
                } else {
                    foreach ($questions as $question) {
                        if ($question['Question']['status'] == 1) {
                            if (!empty($question['Question']['last'])) {
                                $title = $question['Question']['last'];
                            } else {
                                if (!empty($question['Question']['title_update'])) {
                                    $explode_title = explode(',,,', $question['Question']['title_update']);
                                    $last = end($explode_title);
                                    $title = end(unserialize(base64_decode($last)));
                                } else {
                                    $title = $question['Question']['title'];
                                }
                            }
                            ?>
                            <div class="hpanel hblue">
                                <div class="panel-heading hbuilt">
                                    <h3> <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $question['Question']['slug'])); ?>"> <?php
                                            $title = str_replace('&nbsp;', ' ', $title);
                                            $title = str_replace('nbsp', ' ', $title);
                                            $title = str_replace('%', ' ', $title);

                                            echo htmlspecialchars($title);
                                            ?></a>  </h3>
            <?php echo $status; ?>
                                </div>


                            </div>
                            <?php
                            $i++;
                        }
                    }
                }
                ?>
                <!-- timeline time label -->
                <!-- END timeline item -->
                <!-- timeline item -->
            </div>
        </div><!-- /.col -->
    </div><!-- /.row -->
    <ul class="pagination">
        <?php
        echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
        echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'currentClass' => 'active', 'tag' => 'li', 'first' => 1));
        echo $this->Paginator->next(__('next'), array('tag' => 'li', 'currentClass' => 'disabled'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
        ?>
    </ul>
<?php echo $this->Js->writeBuffer(); ?>
</div>
<?php if ($user['User']['admin'] == 1) { ?>
    <script type="text/javascript">

        $('body').undelegate('.update_art', 'click').delegate('.update_art', 'click', function (e) {
            event.preventDefault();
            target = $(this).attr('target');
            $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'articles', 'action' => 'interest')); ?>",
                data: {target: target}, // outer quotes removed
                cache: false,
                success: function (response, status) {
                    $('.update_art_speeli').html(response);
                },
            });
        });

        $('body').undelegate('.expand', 'click').delegate('.expand', 'click', function (e) {
            var art_id = $(this).attr('art_id');
            var art_status = $(this).attr('art_status');
            if (art_status == 0) {
                $(this).html('<span style="color:grey" class="fa fa-lightbulb-o">Mark as :needs expanding</span>');
            } else {
                $(this).html('<span style="color:green" class="fa fa-lightbulb-o">Needs expanding</span>');
            }
            $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'articles', 'action' => 'expand')); ?>",
                data: {art_id: art_id, art_status: art_status}, // outer quotes removed
                cache: false,
                success: function (response, status) {
                },
            });
        });
        $('body').undelegate('.quality', 'click').delegate('.quality', 'click', function (e) {
            console.log(art_status);
            var art_id = $(this).attr('art_id');
            var art_status = $(this).attr('art_quality');
            console.log(art_status);
            if (art_status == 0) {
                $(this).html('<span style="color:grey" class="fa fa-lightbulb-o">No quality check</span>');
            } else {
                $(this).html('<span style="color:green" class="fa fa-lightbulb-o">Passed quality quidelines</span>');
            }
            $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'articles', 'action' => 'quality')); ?>",
                data: {art_id: art_id, art_status: art_status}, // outer quotes removed
                cache: false,
                success: function (response, status) {
                },
            });
        });
        $('body').undelegate('.edited', 'click').delegate('.edited', 'click', function (e) {
            var art_id = $(this).attr('art_id');
            var art_status = $(this).attr('art_edit');
            console.log(art_status);
            if (art_status == 0) {
                $(this).html('<span style="color:grey" class="fa fa-lightbulb-o">Not edited</span>');
            } else {
                $(this).html('<span style="color:green" class="fa fa-lightbulb-o">Edited</span>');
            }
            $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'articles', 'action' => 'art_edit')); ?>",
                data: {art_id: art_id, art_status: art_status}, // outer quotes removed
                cache: false,
                success: function (response, status) {
                },
            });
        });
         $('body').undelegate('.locked', 'click').delegate('.locked', 'click', function (e) {
            var art_id = $(this).attr('art_id');
            var art_status = $(this).attr('locked');
            console.log(art_status);
            if (art_status == 0) {
                $(this).html('<span style="color:grey" >Open</span>');
            } else {
                $(this).html('<span style="color:green" >Locked</span>');
            }
            $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'articles', 'action' => 'art_lock')); ?>",
                data: {art_id: art_id, art_status: art_status}, // outer quotes removed
                cache: false,
                success: function (response, status) {
                },
            });
        });
        $('body').undelegate('.linked', 'click').delegate('.linked', 'click', function (e) {
            var art_id = $(this).attr('art_id');
            var art_status = $(this).attr('link_status');
            console.log(art_id);
            if (art_status == 0) {
                $(this).html('<span style="color:grey" class="fa fa-lightbulb-o">No Links</span>');
            } else {
                $(this).html('<span style="color:green" class="fa fa-lightbulb-o">Has links</span>');
            }
            $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'articles', 'action' => 'linked')); ?>",
                data: {art_id: art_id, art_status: art_status}, // outer quotes removed
                cache: false,
                success: function (response, status) {
                },
            });
        });
        $('body').undelegate('.disable_ads', 'click').delegate('.disable_ads', 'click', function (e) {
            var art_id = $(this).attr('art_id');
            var ads_status = $(this).attr('ads_status');
            console.log(art_id);
            if (ads_status == 0) {
                $(this).html('<span style="color:grey" class="fa fa-unlock">Enable Ads</span>');
                $(this).attr('ads_status',1);
            } else {
                $(this).html('<span style="color:green" class="fa fa-ban">Disable Ads</span>');
                $(this).attr('ads_status',0);
            }
            $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'articles', 'action' => 'disable_ads')); ?>",
                data: {art_id: art_id, ads_status: ads_status}, // outer quotes removed
                cache: false,
                success: function (response, status) {
                },
            });
        });
        $('.new-cat-value').keyup(function () {
            var data_send = $(this).val();
            var cat_id = $(this).attr('target');
            console.log($('.results_' + cat_id));

            $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'categories', 'action' => 'search_category')); ?>",
                data: {data_send: data_send, cat_id: cat_id}, // outer quotes removed
                cache: false,
                success: function (response, status) {

                    $('.results_' + cat_id).html(response);
                },
            });
        });
        $('.new-sum-value').keyup(function () {
            var data_send = $(this).val();
            var cat_id = $(this).attr('target');
            console.log($('.results_' + cat_id));

            $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'categories', 'action' => 'search_summary')); ?>",
                data: {data_send: data_send, cat_id: cat_id}, // outer quotes removed
                cache: false,
                success: function (response, status) {

                    $('.results_' + cat_id).html(response);
                },
            });
        });
    </script>
<?php } ?>