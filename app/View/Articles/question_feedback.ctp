
<style>
    .textarea{
        resize: none;
    }
    .hpanel p, .hpanel h4,.hpanel h2{

    }
    .close{
        float: none;
        font-size: 0.7em;
        font-weight: 700;
        line-height: 1;
        margin-left: 1em;
        color: #428BCD;
        text-shadow: 0 1px 0 #fff;
        opacity: .2;
    }
    .add-link{
        margin-top: 1.5em;
        margin-left: -1em;
    }
    .no_padding{
        padding: 0
    }
    click{
        cursor: pointer;
    }
</style>


<br class="clear"><br class="clear">

<div class="row ">
    <div class="col-md-2 flex-column ">

    </div>
    <div class="col-md-8  ">
        <!-- The time line -->
        <div class=" hpanel forum-box" >
             <?php
            echo $this->Html->css(array('daterangepicker'));
            echo $this->Html->script(array('moment.min', 'daterangepicker'));
            ?>
            <div class="x_title">
                <?php
                echo $this->Form->create('Feedback', array('class' => 'form-horizontal form-label-left'));
                ?>

                <div class="item form-group">
                    <label class="control-label col-md-3 col-sm-3 col-xs-12" for="group">Filter By date
                    </label>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <?php
                        echo $this->Form->input('date', array('type' => 'text', 'class' => 'form-control daterange col-md-7 col-xs-12',
                            'div' => false, 'label' => false));
                        ?>
                    </div>
                </div>
                <script type="text/javascript">
                    $('.daterange').daterangepicker({
                        "showDropdowns": true,
                        timePicker: false,
                        "timePicker24Hour": false,
                        "autoApply": true,
                        ranges: {
                            'Today': [moment(), moment()],
                            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                            'This Month': [moment().startOf('month'), moment().endOf('month')],
                            'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                        },
                        "locale": {
                            "format": "YYYY-MM-DD",
                            "separator": "_",
                            "applyLabel": "Apply",
                            "cancelLabel": "Cancel",
                            "fromLabel": "From",
                            "toLabel": "To",
                            "customRangeLabel": "Custom",
                            "daysOfWeek": [
                                "Su",
                                "Mo",
                                "Tu",
                                "We",
                                "Th",
                                "Fr",
                                "Sa"
                            ],
                            "monthNames": [
                                "January",
                                "February",
                                "March",
                                "April",
                                "May",
                                "June",
                                "July",
                                "August",
                                "September",
                                "October",
                                "November",
                                "December"
                            ],
                            "firstDay": 1
                        },
                        "alwaysShowCalendars": true,
                    }, function (start, end, label) {
                        console.log("New date range selected: ' + start.format('YYYY-MM-DD') + ' to ' + end.format('YYYY-MM-DD') + ' (predefined range: ' + label + ')");
                    });</script>

                <div class="form-group">
                    <div class="col-md-3 col-md-offset-4">
                        <button id="send" type="submit" style="background: #63b5b2;color:white;" class="btn btn-info col-md-12">Select</button>
                    </div>
                </div>
                
                <?php echo $this->Form->end(); ?>


                <div class="clearfix"></div>
            </div>
            <div class='list' >
                <div class="" >
                    <div class="panel-heading hbuilt text-center ">
                        <h2 class="text-center col-md-12  float-none"> 
                            Total Record <?php
                            
                            echo $total = $up + $down;
                            $total =($total ==0)?1:$total;
                            $review_array = array(0 => 'Was this summary helpful ?', 1 => 'Was this summary intersting ? ',
                                2 => "Do you like Speeli's design?", 3 => 'How do you like your experience on Speeli?');
                            ?>
                        </h2><br class="clear"/>
                        <div class="clear clearfix"></div>

                        <div class="col-md-4 col-xs-12 float-none">
                            <div class="col-md-6">
                                <img class="img-responsive" src="<?php echo $this->webroot . 'imgs/like.png' ?>">

                                <h3 style="color:#2ced1c;"class="text-center col-md-12 col-xs-12"><b><?php echo ceil((($up / $total) * 100)) ."%"; ?></b></h3>
                                <h4 style="color:#2ced1c;"class="text-center col-md-12 col-xs-12"><b><?php echo $up . "/" . $total; ?></b></h4>
                            </div>
                            <div class="col-md-6">
                                <img class="img-responsive"
                                     src="<?php echo $this->webroot . 'imgs/dislike.png' ?>">
                                <h3 style="color:red;"class="text-center col-md-12 col-xs-12"><b><?php echo ceil((($down / $total) * 100))."%"; ?></b></h3>
                                <h4 style="color:red;" class="text-center col-md-12 col-xs-12"><b><?php echo $down . "/" . $total; ?></b></h4>
                            </div>

                        </div>
                        <div class="clear clearfix"></div>
                        <div class="col-md-6 col-xs-12 float-none">
                            <div class="col-md-6">

                                <div style="color:white;" class="btn btn-warning col-md-12 reset-btn" target="<?php
                                echo $feedbacks[0]['Feedback']['question_id'];
                                ?>">Reset <span class="fa fa-refresh"></span></div>
                            </div>

                            <div class="col-md-6">
                                <div style="color:white;" class="btn btn-info col-md-12 history-btn" target="<?php
                                echo $feedbacks[0]['Feedback']['question_id'];
                                ?>">History <span class="fa fa-bars"></span></div>

                            </div>

                        </div>

                    </div>
                    <br class="clear">
                    <br class="clear">

                    <br class="clear">
                    <?php
                    foreach ($feeds as $feed) {
                        $key = array_keys($feed);
                        $total = ceil($feed['up'] +$feed['down']);
                       
                        ?>
                        
                    <li class="hpanel hblue">
                        <div class="col-md-4 col-xs-12 float-none">
                            <div class="col-md-6">
                                <img class="img-responsive" src="<?php echo $this->webroot . 'imgs/like.png' ?>">

                                <h3 style="color:#2ced1c;"class="text-center col-md-12 col-xs-12"><b><?php echo ceil((($feed['up'] / $total) * 100)) ."%"; ?></b></h3>
                                <h4 style="color:#2ced1c;"class="text-center col-md-12 col-xs-12"><b><?php echo $feed['up'] . "/" . $total; ?></b></h4>
                            </div>
                            <div class="col-md-6">
                                <img class="img-responsive"
                                     src="<?php echo $this->webroot . 'imgs/dislike.png' ?>">
                                <h3 style="color:red;"class="text-center col-md-12 col-xs-12"><b><?php echo ceil((($feed['down'] / $total) * 100))."%"; ?></b></h3>
                                <h4 style="color:red;" class="text-center col-md-12 col-xs-12"><b><?php echo $feed['down'] . "/" . $total; ?></b></h4>
                            </div>

                        </div>
                            <div class="panel-heading hbuilt">
                                <h3> <b><?php echo $review_array[$feed[$key[1]]['target_id']]; ?></b></h3>
                            </div>
                            <div class="group-feedback" target="<?php echo $review_array[$feed[$key[1]]['target_id']]; ?>">
                                <?php foreach ($feed as $fed) { ?>
                                    <?php if ($fed['type'] == 1) { ?>
                                        <div  class="panel-body" >

                                            <p style="float: left;"> Answer  : <?php echo $fed['body'] . "</p> <span style='float:right;'>' " . $fed['created'] . " ' </span>"; ?> 
                                        </div>
                                    <?php } ?>
                                <?php }
                                ?>
                            </div>
                        </li>
                        <?php
                    }
                    ?>
                    <div class=" history_feedback" style="display: none;">
                        <hr>
                        <div class="thumbnail">
                            
                        
                        <?php
                        foreach ($history as $feed) {
                            $key = array_keys($feed);
                            
                            ?>
                            <li class="hpanel hblue">
                                <div class="panel-heading hbuilt">
                                    <h3> <b><?php echo $review_array[$feed[$key[1]]['target_id']]; ?></b></h3>
                                </div>
                                <div class="group-feedback" target="<?php echo $review_array[$feed[$key[1]]['target_id']]; ?>">
                                    <?php foreach ($feed as $fed) { ?>
                                        <?php if ($fed['type'] == 1) { ?>
                                            <div  class="panel-body" >

                                                <p style="float: left;"> Answer  : <?php echo $fed['body'] . "</p> <span style='float:right;'>' " . $fed['created'] . " ' </span>"; ?> 
                                            </div>
                                        <?php } ?>
                                    <?php }
                                    ?>
                                </div>
                            </li>
                            <?php
                        }
                        ?>
                            </div>
                    </div>

                </div>
            </div>


        </div><!-- /.col -->


    </div><!-- /.row -->


</div>


<script>
    $(document).ready(function () {
        $('.history-btn').click(function () {
            $('.history_feedback').toggle();
        });
        $('.reset-btn').click(function () {
            var target = $(this).attr('target');
            $.post("<?php echo Router::url(array('controller' => 'articles', 'action' => 'feedback_reset')); ?>",
                    {
                        question_id: target
                    });
        });
    });
</script>