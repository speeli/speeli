<style>
    .expand{
        cursor: pointer;
    }
    .click{
        cursor: pointer;
    }
    .progress{
        display: none;
    }
    hr{
        border: 0;
        height: 1px;
        background-image: -webkit-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -moz-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -ms-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -o-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
    }
    .clear{
        clear: both;
    }
    .question_wrapper{
        margin: 0 auto;
    }
    .float-none{
        float:none;
        margin: 0 auto;
    }
    .hblue{
        background-color: #fff;
        padding: 1em;
        border-radius: 4px;
        -moz-border-radius: 4px;
        -webkit-border-radius: 4px;
        margin-bottom: 25px;
        position: relative;
        line-height: 25px;
        border: 1px solid #e5e5e5;
    }
    .hblue .panel-heading{
        padding: 10px 15px;
        background-color: rgba(245, 245, 245, 0.24);
        border-top: 1px solid #ddd;

        border-bottom-right-radius: 3px;
        border-bottom-left-radius: 3px;
    }
    .list-unstyled li{
        margin-left: 1em;
    }
</style>
<?php
$this->set("meta_description", "Latest Speeli articles");
$this->set("meta_robots", "noindex");
?>
<?php
$this->Paginator->options(array(
    'update' => '.question_update',
    'evalScripts' => true
));
?>
<div class="row">
    <!-- Add the extra clearfix for only the required viewport -->
    <div  class="clearfix visible-xs-block"></div>
    <br class="clear">
    <hr>
    <div class="row ">

        <div class="col-md-8 float-none update_art_speeli  " style="border-left: 1px solid #ddd;">
            <!-- The time line -->
            <div class="hpanel forum-box question_update">
                <?php
                $i = 1;

                $user = $this->Session->read('user_quesli');
                if ($user['User']['admin'] == 1) {
                    foreach ($questions as $question) {
                        if (!empty($question['Question']['title_update'])) {
                            $explode_title = explode(',,,', $question['Question']['title_update']);
                            $last = end($explode_title);
                            $title = end(unserialize(base64_decode($last)));
                        } else {
                            $title = $question['Question']['title'];
                        }
                        ?>
                        <div id="main_<?php echo $question['Question']['id']; ?>" class="hpanel hblue">
                            <div class="panel-heading hbuilt">
                                <h3> <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $question['Question']['slug'])); ?>"> <?php echo html_entity_decode($title); ?></a>  </h3>

                                <?php
                                if ($category == 1) {
                                    if ($question['Question']['category_id'] == 0) {
                                        ?>
                                        <div class="item form-groups">
                                            <?php
                                            echo $this->Form->input('field', array(
                                                'legend' => false,
                                                'class'=>$question['Question']['id'],
                                                'div' => false,
                                                'separator' => '  ||  ',
                                                'options' => $categories,
                                                'type' => 'radio',
                                                'name' => $question['Question']['id'] 
                                            ));
                                            ?>
                                        </div>
                                <div target="<?php echo $question['Question']['id'];?>"  class="add-new-cat btn cdivck btn-main right">Add topic</div>    
                                        <?php
                                    }
                                } else {
                                    if ($question['Question']['summary_id'] == 0) {
                                        ?>
                                        <?php echo $this->Form->input('news', array('class' => 'new-sum-value', 'target' => $question['Question']['id'], 'placeholder' => 'write main summary', 'label' => false, 'div' => false)); ?>
                                        <div  class="results_<?php echo $question['Question']['id'] ?>">
                                        </div>
                                        <?php
                                    }
                                }
                                ?>

                            </div>
                            <div style="float:right;" class="panel-footer-status"><span class="fa fa-calendar"><?php echo $question['Question']['created'] ?></span></div>
                            <br class="clear">

                        </div>
                        <?php
                        $i++;
                    }
                }
                ?>
                <!-- timeline time label -->
                <!-- END timeline item -->
                <!-- timeline item -->
            </div>
        </div><!-- /.col -->
    </div><!-- /.row -->
    <ul class="pagination">
        <?php
        echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
        echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'currentClass' => 'active', 'tag' => 'li', 'first' => 1));
        echo $this->Paginator->next(__('next'), array('tag' => 'li', 'currentClass' => 'disabled'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
        ?>
    </ul>
    <?php echo $this->Js->writeBuffer(); ?>
</div>
<?php if ($user['User']['admin'] == 1) { ?>
    <script type="text/javascript">

        $('.new-cat-value').keyup(function () {
            var data_send = $(this).val();
            var cat_id = $(this).attr('target');
            console.log($('.results_' + cat_id));

            $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'categories', 'action' => 'search_category')); ?>",
                data: {data_send: data_send, cat_id: cat_id}, // outer quotes removed
                cache: false,
                success: function (response, status) {

                    $('.results_' + cat_id).html(response);
                },
            });
        });
        $('.new-sum-value').keyup(function () {
            var data_send = $(this).val();
            var cat_id = $(this).attr('target');
            console.log($('.results_' + cat_id));

            $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'categories', 'action' => 'search_summary')); ?>",
                data: {data_send: data_send, cat_id: cat_id}, // outer quotes removed
                cache: false,
                success: function (response, status) {

                    $('.results_' + cat_id).html(response);
                },
            });
        });
    </script>
<?php } ?>
<script>
    $(document).ready(function () {
        $('.add-new-cat').click(function () {
            cat_id=$(this).attr('target');
           var add_new_cat=$('.'+cat_id+':checked').val();
           $('#main_'+cat_id).slideUp();
           $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'categories', 'action' => 'update_cat')); ?>",
                data: {add_new_cat: add_new_cat, cat_id: cat_id}, // outer quotes removed
                cache: false,
                success: function (response, status) {
                },
            });
        });
        
    });
</script>