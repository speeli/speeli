<?php $slug = "http://www.speeli.com/articles/view/" . trim($answers[0]['Question']['slug']); ?>  


<?php
$notification = $this->Session->flash('speeli_notification');
$user = $this->Session->read('user_quesli');
if (isset($user) && !empty($user)) {
    ?>

    <?php
    $link = '/dashboard';
} else {
    $link = '/users/login#register';
}
?>

<style>
    body{
        overflow-x:hidden;
    }
    .image_array{
        display: none;
    }
    .list-unstyled a{
        text-transform: capitalize;
        text-align: left;
        font-size: 1.2em;
        top: 0.3em;
    }  
    .site_color{
        color:#63B5B2;
    }

    .circle_wrapper{
        display: none;
    }
    .saved_minutes{
        display: none;
        background: rgb(120, 204, 196);
        color: white;
        padding: 0.5em;
        margin: 14px auto;
        text-align: center;
        border-radius: 1em;
    }
    .saved_minutes a{
        color:white;text-align: center;font-size:1.1em;
    }
    .fa-close{
        cursor: pointer;
        float: right;
    }
    .progress {
        -moz-border-radius: 3px;
        -webkit-border-radius: 3px;
        border-radius: 3px;
        box-shadow: inset 0 -1px 1px rgba(255,255,255,0.3);

        display: block!important;
        text-align: left;
        width: 100%;
        position: absolute;
        left: 0;
        opacity: 0;
    }

    .progress .wrapper_progress {
        position: absolute;
        width: inherit;
        margin-top: 0;
        -moz-border-radius: 3px;
        -webkit-border-radius: 3px;
        border-radius: 3px;
        box-shadow: inset 0 -1px 1px rgba(255,255,255,0.3);

    }
    .progress .wrapper_progress.affix {
        position: fixed;
        top: 5.5em;
        z-index: 9999;
        -moz-border-radius: 3px;
        -webkit-border-radius: 3px;
        border-radius: 3px;
        box-shadow: inset 0 -1px 1px rgba(255,255,255,0.3);

    }
    @media only screen and (max-width: 768px){
        .row , .effect7{
            overflow-y:hidden;
        }
        .effect7{
            overflow-x:hidden;
        }
        .progress .wrapper_progress.affix {
            top :4.8em;
        }
        .list-unstyled a {
            text-align: center;
            padding-bottom: 1em;
        }
    }
    .progress .bar {
        position: relative;
        display: block;
        width: 100%;
        text-align: center;
        height: 10px;
        overflow: hidden;
        margin-bottom: 10px;
        -moz-border-radius: 3px;
        -webkit-border-radius: 3px;
        border-radius: 3px;
        box-shadow: inset 0 -1px 1px rgba(255,255,255,0.3);
        background-color: #E5E5E5;
    }
    .progress .bar span {
        position: absolute;
        display: block;
        top: 0;
        width: 0;
        height: 10px;
        z-index: 0;
        background-color: #9BCDCD;
        -moz-border-radius: 3px;
        -webkit-border-radius: 3px;
        border-radius: 3px;

        box-shadow: inset 0 -1px 1px rgba(255,255,255,0.3);
    }
    .progress .bar a {
        line-height: 7px;
        position: relative;
        z-index: 1;
    }
    .progress .bar i {
        position: absolute;
        right: -30px;
        height: 7px;
        line-height: 7px;
        color: #36a7f3;

        -webkit-transition: color .3s;
        transition: color .3s;
    }
    @media only screen and (max-width: 768px){
        .progress .bar {
            height: 7px;
        }
        .progress .bar span {
            height: 7px;
        }
        .circle_wrapper {
            position: fixed;
            height: 2em;
            z-index: 99999;
            display: block;
            right: 0;
            bottom: 3em;

        }

        .circle_add{
            position: fixed;

            right: 3.2em;
            text-align: right;

        }
        .circle-content{
            background: #63B5B2;
            color: white;
            border-radius: 0.5em;
            text-align: center;
            display:none;
            height: 3.8em;
            line-height: 1em;
        }
        .circle-content a{
            font-size:1.3em;
            color:white;
        }

    }
</style>  

<?php
$hashtags = '';
$t_i = 0;
foreach ($tags as $tag) {
    if ($t_i == 0) {
        $hashtags.=$tag['Category']['name'];
    } else {
        $hashtags.=" , " . $tag['Category']['name'];
    }
    $t_i++;
    ?>
    <?php
}

$text_twitter = str_replace(' ', '+', $answers[0]['Question']['title']);
?>

<style>
    .panel-body{
        font-size: 1.3em;
        line-height: 1.635em;

    }
    .toggle-area{
        display: none;
    }

    .hbuilt h2{
        font-weight: 200;
        font-size: 18px;
        color:#22A39F;

    }
    .hbuilt {
        color:black;
    }
    .forum-box{

        color:black;
    }
    h2.page-header,h1.page-header{
        color:#22A39F;
        text-align: center;
        display: block;
        font-size: 2.2em;
        border-bottom: 0;

    }
    #profile_img{
        width:42em !important;
        height:auto;
    }
    .forum-box a{
        color:#22A39F;
    }
    .gray-tone{
        color: rgba(136, 136, 136, 0.88)!important;
    }
</style>
<style>
    .FBConnectButton {
        height: 15px;
        float: left;
        background-position-y: -251px;
        background-position-x: -1px;
        border-bottom: 1px solid #1a356e;
    }
    .FBConnectButton_Text {
        font-size: 8px ;
        height: 9px;
    }
    .share{
        margin: 0 2em;  
    }
    .fb-share-button,.twitter-share-button {
        background: rgb(59, 89, 152);
        text-align: center;
        float: left;
        margin: 0 2em;
        padding: 0.8em 5.2em;
        border-radius: 0.4em;
        transform: scale(1);
        -ms-transform: scale(1);
        -webkit-transform: scale(1);
        -o-transform: scale(1);
        -moz-transform: scale(1);
        width: 12em !important;
        height: 3em !important;
    }
    #twitter-widget-0{
        margin: 0 2em !important;
        width: 12em !important;
        height: 3em !important;
        float: left;
        text-align: center;
        background: rgb(0, 172, 237);
        padding: 0.8em 4em;
    }
    .fb-share-button iframe ,.twitter-share-button iframe{
        width: 12em !important;
        margin-left: -1.4em!important;

    }
    .fb-share-button iframe .pluginSkinLight div.pluginButtonContainer  div.pluginButton .pluginButtonLabel{

        font-size: 1.5em !important;

        text-transform: uppercase !important; 
    }
    .fb-share-button .pluginButtonContainer .pluginButton .pluginButtonLabel{

        font-size: 1.5em !important;

        text-transform: uppercase !important;
    }
    .fb-share-button_inner {
        height: 9px;
        position: relative;
    }
    .fb-share-button .fb-share-button_right {
        float: left;
    }
    /***** New share style *****/

    ul.share-buttons{
        list-style: none;
        padding:0px 15px;
        margin: 0;
        padding: 0px;
        text-align: center;
    }

    ul.share-buttons li{
        display: inline;
    }


    li.speeli-share a{

        margin: 0px ;
        color: #FFF;
        text-align: center;
        font-size: 11px;
        border-radius: 2px;
        padding: 13px 0px;
        width: 40%;
        display: inline-block;

    }


    li.speeli-share.tw-share a{
        background: #00ACED none repeat scroll 0% 0%;
        float: right;
    }

    li.speeli-share.tw-share a:hover, .li.speeli-share.tw-share a:focus {
        background: #21C2FF none repeat scroll 0% 0%;
        text-decoration: none;
    }

    li.speeli-share.fb-share a {
        background: #3B5998 none repeat scroll 0% 0%;
        float: left;
    }

    li.speeli-share.fb-share a:hover, li.speeli-share.fb-share a:focus {
        background: #4C70BA none repeat scroll 0% 0%;
        text-decoration: none;
    }
    ul.share-buttons li span{
        font-family: arial;
        padding: 0 0 0 10px;
        text-transform: uppercase;
        font-size: 12px;
    }




</style>

<?php
$title = str_replace('&nbsp;', ' ', $answers[0]['Question']['title']);
$title = str_replace('nbsp', ' ', $title);
$title = str_replace('%', ' ', $title);
$this->set("meta_fb", true);
$this->set('og:title', $answers[0]['Question']['title']);
if ($answers[0]['Question']['status'] == 0) {
    
} else {
    $this->set("meta_robots", "index");
}
if (count($answers) > 12) {
    $this->set('google_ads', 0);
} else {
    $this->set('google_ads', 0);
}
$this->set("meta_description", $answers[0]['Question']['title']);
?>
<?php
$admin = false;
if ($this->Session->check('user_quesli')) {
    $user = $this->Session->read('user_quesli');
    if ($user['User']['admin'] == 1) {
        $admin = true;
    }
}
?>
<meta property="og:url" content="<?php echo $slug; ?>" />






<style>
    .hbuilt h2{
        font-size: 20.5px;
        padding-top: 0.4em;
        color:black;
    }
    h1.page-header{
        color:black;
    }
    @media only screen and (max-width: 768px){
        .hbuilt h2{
            font-size: 21px;
            padding-top: 10px;
            vertical-align: middle;
        }
    }
</style>
<div class="progress ">
    <div class="  wrapper_progress">
        <div class="bar meter">
            <a href="#intro"></a>
            <i class="icon-ok"></i>
            <span></span>
        </div>
    </div>
</div>
<?php
if ($this->Session->check('user_quesli')) {
    $user = $this->Session->read('user_quesli');
    if ($user['User']['admin'] == 1) {
        ?>
        <div class="circle_wrapper col-xs-12 info-box">
            <div class="circle_add col-xs-1 circle_add_btn">
                <div href="#" class="site_color">
                    <i class="fa fa-4x fa-plus-circle "></i>

                </div>
            </div>
            <div class=" col-xs-10 circle-content">
                <a style="text-align:center;" href="<?php echo Router::url('/articles/add'); ?>">
                    <br>
                    Add Speeli Summary
                </a>
            </div>
        </div>
        <?php
    }
}
?>

<div id="fb-root"></div>
<?php if (isset($answers) && !empty($answers)) { ?>
    <div class="row">




        <!-- Add the extra clearfix for only the required viewport -->
        <div  class="clearfix visible-xs-block"></div>



        <div class="row ">
            <div class="col-md-2 flex-column ">
            </div>
            <div class="col-md-8  ">
                <!-- The time line -->
                <div class=" hpanel forum-box" id="">
                    <ul class="article_wrapper" id='intro' style="padding: 0">
                        <div class="panel-heading" style='padding:0;'>
                            <?php
                            if (!empty($answers[0]['Question']['title_update'])) {
                                $explode_title = explode(',,,', $answers[0]['Question']['title_update']);
                                $last = end($explode_title);
                                $title = end(unserialize(base64_decode($last)));
                            } else {
                                $title = $answers[0]['Question']['title'];
                            }
                            if ($answers[0]['Question']['id'] == 1796) {



                                $this->set("page_title", $title . " - Speeli Summary");
                            } else {
                                $this->set("page_title", $title . " - Speeli Summary");
                            }
                            ?>
                            <center>  
                                <?php if ($admin) { ?>
                                    <div class="btn btn-primary fa fa-edit image_array_btn">edit article images</div>
                                    <div  class="fa fa-toggle-down "><br class="clear">
                                        <div class="toggle-area">
                                            <div style='float:left; margin:0.5em;'ques-art-a='<?php echo $answers[0]['Question']['id']; ?>' target='a' class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span></div>
                                            <?php
                                            if ($answers[0]['Question']['status'] == 0) {
                                                $toggle = 'glyphicon-eye-close';
                                                $btn = 'btn-block';
                                                $word = 'show';
                                                $status = 1;
                                            } else {
                                                $toggle = 'glyphicon-eye-open';
                                                $btn = 'btn-block';
                                                $word = 'hide';
                                                $status = 0;
                                            }
                                            ?>
                                            <div status='<?php echo $status; ?>' ques-art-a='<?php echo $answers[0]['Question']['id']; ?>' target='a' class="btn toggle <?php echo $btn; ?>"><span class="glyphicon <?php echo $toggle; ?>"><?php echo $word ?></span></div>
                                        </div>
                                    </div>


                                    <br class="clear">
                                    <?php
                                    if ($answers[0]['Question']['selected'] == 0) {
                                        $btn = 'btn-success';
                                        $word = 'show in home page';
                                        $status = 1;
                                    } else {
                                        $btn = 'btn-warining';
                                        $word = 'remove from home page';
                                        $status = 0;
                                    }
                                    ?>

                                    <div  slug='<?php echo $answers[0]['Question']['slug'] ?>' question_id='<?php echo $answers[0]['Question']['id'] ?>'
                                          title='<?php echo $title; ?>' 
                                          article_image='<?php echo $answers[0]['Question']['image'] ?>'
                                          status='<?php echo $status; ?>' class="btn-select-home btn <?php echo $btn; ?>"><?php echo $word; ?></div>
                                      <?php } ?>

                                <h1 class="page-header" style="margin-bottom: 1em;margin-top: 2.3em;"> 
                                    <b><?php echo $title; ?></b> 
                                </h1>
                                <?php if ($answers[0]['Question']['locked'] == 1 && $this->Session->check('user_quesli') == false) { ?>
                                    <a style="margin:0px !important;"  class='right gray-tone' href="<?php echo Router::url(array('controller' => 'users', 'action' => 'login#register')) ?>">
                                        <span  class="glyphicon glyphicon-edit gray-tone"></span> Edit article</a>
                                <?php } else { ?>
                                    <a style="margin:0px !important;"  class='right gray-tone' href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'edit', $answers[0]['Question']['slug'])); ?>">
                                        <span  class="glyphicon glyphicon-edit gray-tone"></span> Edit article</a>
                                <?php } ?>

                                <br class="clear">
                                <?php if ($answers[0]['Question']['image'] != NULL) { ?>

                                    <img width="800" heught="400" id='profile_img' src="<?php echo $this->webroot . 'question/' . $answers[0]['Question']['id'] . '/' . $answers[0]['Question']['image']; ?>" alt="<?php echo $title ?>" class="img-responsive" rel="Facebook image">
                                <?php } ?>
                                <?php if ($admin) { ?>

                                    <div class="thumbnail image_array" >
                                        <?php
                                        $i = 0;
                                        $explodes = explode('____', $answers[0]['Question']['image_user']);
                                        if (is_array($explodes)) {
                                            foreach ($explodes as $photo):
                                                if (!empty($photo)) {
                                                    ?>
                                                    <div class="panel col-md-4 ">
                                                        <div class="img-thumbnail panel-body">
                                                            <img width="775" heught="400" id='profile_img' src="<?php echo $this->webroot . 'question/' . $answers[0]['Question']['id'] . '/' . $photo; ?>" alt="..." class="img-thumbnail">
                                                        </div>
                                                        <div class="panel-footer">
                                                            <?php
                                                            if ($photo == $answers[0]['Question']['image']) {
                                                                echo "<b>Main image</b>";
                                                            } else {
                                                                ?>
                                                                <div article_id="<?php echo $answers[0]['Question']['id']; ?>" photo_id="<?php echo $photo; ?>" class="btn main_photo btn-primary">Make it main image</div>
                                                            <?php } ?>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                            endforeach;
                                        }
                                        ?>
                                        <div class="panel col-md-4 ">
                                            <div class="img-thumbnail panel-body">

                                            </div>
                                            <div class="panel-footer">
                                                <div article_id="<?php echo $answers[0]['Question']['id']; ?>"
                                                     class="btn blank main_photo btn-primary">Make it blank</div>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>

                            </center>

                            <br class="clear">
                        </div>

                        <?php
                        $i = 1;
                        foreach ($answers as $answer) {
                            if (!empty($answer['Answer']['last_title'])) {
                                $title = $answer['Answer']['last_title'];
                            } else {
                                if (!empty($answer['Answer']['title_update'])) {
                                    $explode_title = explode(',,,', $answer['Answer']['title_update']);
                                    $last = end($explode_title);
                                    $title = end(unserialize(base64_decode($last)));
                                } else {
                                    $title = $answer['Answer']['title'];
                                }
                            }
                            if (!empty($answer['Answer']['last_body'])) {
                                $body = $answer['Answer']['last_body'];
                                $body = trim($body);
                            } else {
                                if (!empty($answer['Answer']['body_update'])) {
                                    $explode_body = explode(',,,', $answer['Answer']['body_update']);
                                    $last = end($explode_body);
                                    $body = end(unserialize(base64_decode($last)));
                                    $body = trim($body);
                                } else {
                                    $body = $answer['Answer']['body'];
                                    $body = trim($body);
                                }
                            }
                            if (!empty($title)) {
                                ?>
                                <li class="hpanel hblue">
                                    <?php if ($admin || ($answer['User']['id'] == $user['User']['id'] && isset($user['User']['id']))) { ?>
                                        <div style='float: right' class="fa fa-toggle-down"><br class="clear">
                                            <div class="toggle-area">
                                                <div style='float:left; margin:0.5em;' ques-art-a='<?php echo $answer['Answer']['id']; ?>' target='p' class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span></div>
                                                <?php
                                                if ($answer['Answer']['status'] == 0) {
                                                    $toggle = 'glyphicon-eye-close';
                                                    $btn = 'btn-block';
                                                    $word = 'show';
                                                    $status = 1;
                                                } else {
                                                    $toggle = 'glyphicon-eye-open';
                                                    $btn = 'btn-block';
                                                    $word = 'hide';
                                                    $status = 0;
                                                }
                                                ?>
                                                <div status='<?php echo $status; ?>' ques-art-a='<?php echo $answer['Answer']['id']; ?>' target='p' class="btn toggle <?php echo $btn; ?>"><span class="glyphicon <?php echo $toggle; ?>"><?php echo $word; ?></span></div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                    <div class="panel-heading hbuilt">
                                        <h2 style="padding-left: 2em;" ques-ans-id="<?php echo $answer['Answer']['id']; ?>" > <b class="left rounded-lists"><?php echo $i; ?> </b><b ><?php echo $title; ?></b></h2>

                                    </div>

                                    <div class="panel-body" >

                                        <?php if (($answer['Answer']['image'] != NULL) && $answer['Answer']['image_user'] == 0) { ?>

                                            <img width="775" heught="400" id='profile_img' src="<?php echo $this->webroot . 'question/' . $answers[0]['Question']['id'] . '/' . $answer['Answer']['image']; ?>" alt="..." class="img-thumbnail">
                                        <?php } ?>
                                        <p ques-ans-id="<?php echo $answer['Answer']['id']; ?>" ><?php
                            echo $body;
                                        ?>
                                        </p>
                                    </div>

                                </li>
                                <?php
                            }
                            $i++;
                        }
                        ?>
                    </ul>
                    <!-- timeline time label -->
                    <!-- END timeline item -->
                    <!-- timeline item -->
                </div>

                <!--
                <ul class="share-buttons">
                    <li class="speeli-share fb-share">
                        <a class="speeli-share-btn popup" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $slug; ?>"
                           title="Share on Facebook" ><i class="fa fa-facebook fa-2x"></i><span>share</span></a></li>
                    <li class="speeli-share tw-share"><a class=" speeli-share-btn popup" href="https://twitter.com/intent/tweet?url=<?php echo $slug; ?>&text=<?php echo $text_twitter; ?>&hashtags=<?php echo $hashtags; ?>" title="Tweet"><i class="fa fa-twitter fa-2x"></i><span>Tweet</span></a></li>


                </ul>
                -->
                <div class="col-xs-12 col-md-12  saved_minutes ">
                    <a href="<?php echo Router::url('/articles/view/What-is-Speeli'); ?>">
                        Congrats! You have just saved <b><?php echo ($i - 1) * 2 ?> minutes </b>by reading this article. learn more
                    </a>
                    <span class="fa fa-close" title="hide"></span>
                </div>


            </div>















            <!-- /.col -->
        </div><!-- /.row -->


        <br class="clear">

        <div class="container">
            <div class="row">
                <div class="row float-none">



                </div>
                <br class="clear">
                <?php if (count($answers) >= 7) { ?>
                    <div class="speeli_question_id" question_id='<?php echo $answers[0]['Question']['id'] ?>'></div>
                    <div class="speeli_recommend">
                        <div class="box effect7 col-md-4 col-xs-4 col-lg-4 float-none ">

                        </div>
                    </div>
                    <script>
                        question_id = $('.speeli_question_id').attr('question_id');
                        $.ajax({
                            type: "POST", // Request method: post, get
                            url: "<?php echo Router::url(array('controller' => 'homes', 'action' => 'recommend')); ?>",
                            data: {question_id: question_id},
                            success: function (data) {
                                $('.speeli_recommend').html(data);
                            }
                        });
                        $('body').undelegate('#buyrecommend', 'click').delegate('#buyrecommend', 'click', function () {
                            $.ajax({
                                type: "POST", // Request method: post, get
                                url: "<?php echo Router::url(array('controller' => 'homes', 'action' => 'recommend_count')); ?>",
                                data: {},
                                success: function (data) {

                                }
                            });
                        });
                    </script>
                <?php } ?>
                <div class="box col-md-4 col-xs-4 col-lg-4 float-none">
                    <div class="panel with-nav-tabs panel-default related-Speelis">
                        <div class="panel-heading" style='color:#6b6a6a;padding-left:2em;font-weight:bold;'>
                            Related Speeli Summaries
                        </div>
                        <div class="panel-body-larger">
                            <ul class="list-unstyled" >
                                <?php
                                foreach ($categories as $category) {
                                    if ($category['Question']['slug']) {
                                        if (!empty($category['Question']['title_update'])) {
                                            $explode_title = explode(',,,', $category['Question']['title_update']);
                                            $last = end($explode_title);
                                            $title = end(unserialize(base64_decode($last)));
                                        } else {
                                            $title = $category['Question']['title'];
                                        }
                                        ?>
                                        <li class="clear clearfix">
                                            <a  href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', trim($category['Question']['slug']))); ?>">
                                                <img width="115" height="55" src="<?php echo $this->webroot . 'img/white.png' ?>"
                                                     alt="<?php echo $title ?>" 
                                                     data-toload="<?php echo $this->webroot . 'question/' . $category['Question']['id'] . '/' . $category['Question']['image']; ?>"  
                                                     class="img-responsive data-toload  col-md-2 col-xs-12">
                                            </a>     
                                            <a  class='col-md-9 col-xs-12 text-center' style="color:#37a09d;"
                                                href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', trim($category['Question']['slug']))); ?>">
                                                    <?php if ($admin) { ?>
                                                    <?php } ?>
                                                    <?php echo $title; ?>
                                            </a>
                                        </li>
                                        <br class="clear">
                                        <?php
                                    }
                                }
                                ?> 
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="box effect7 col-md-4 col-xs-4 col-lg-4 float-none">
                    <div class="panel with-nav-tabs panel-default bottom-tab">
                        <div class="panel-heading">
                            <ul class="nav nav-tabs">

                                <li class="active"><a href="#tab3default" data-toggle="tab">Topics</a></li>
                                <li><a href="#tab2default" data-toggle="tab">References</a></li>
                                <li><a href="#tab1default" data-toggle="tab">Contributors</a></li>
                            </ul>
                        </div>
                        <div class="panel-body-larger">
                            <div class="tab-content">
                                <div class="tab-pane fade in " id="tab1default">

                                    <ul class="list-unstyled" >


                                        <?php
                                        $j = 1;
                                        $u = 0;
                                        $n = 1;
                                        $answer_writers = array();
                                        $user_writers = array();
                                        $annonymus = 'Anonymous wrote point(s) No ( ';

                                        foreach ($answers as $answer) {
                                            if ($answer['User']['username']) {
                                                if (array_key_exists($answer['User']['id'], $user_writers)) {
                                                    $user_writers[$answer['User']['id']].= $j . " ,";
                                                } else {
                                                    $user_writers[$answer['User']['id']] = $answer['User']['username'] . " wrote point(s) No : ( " . $j . " ,";
                                                }
                                            } else {
                                                $annonymus.= $j . " ,";
                                            }
                                            $j++;
                                        }
                                        if ($annonymus === 'Anonymous wrote point(s) No ( ') {
                                            $annonymus = NULL;
                                        } else {
                                            $annonymus = rtrim($annonymus, ",");
                                            $annonymus.=" )";
                                        }


                                        foreach ($user_writers as $key => $user_writer) {
                                            $user_writer = rtrim($user_writer, ',');
                                            ?>
                                            <li>

                                                <h5>
                                                    <a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'view', $key)); ?>"><i class="fa fa-user"></i> <?php echo "<b>" . $user_writer . " ) </b>"; ?></a>

                                                </h5>   
                                            </li>
                                        <?php } if ($annonymus) { ?>

                                            <li><h5><a href="#"><i class="fa fa-user"></i> <?php echo $annonymus; ?></a>
                                                </h5> </li>
                                        <?php } ?>
                                    </ul>
                                </div>
                                <div class="tab-pane fade" id="tab2default">
                                    <ul class="list-unstyled" >
                                        <?php

                                        function validate_host($url) {
                                            $pattern = "/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?/";
                                            if (!preg_match($pattern, $url)) {
                                                return false;
                                            } else {
                                                if (preg_match("/http:/", $url)) {
                                                    
                                                } elseif (preg_match("/https:/", $url)) {
                                                    
                                                } else {
                                                    $url = 'http://' . $url;
                                                }
                                                return $url;
                                            }
                                        }

                                        $refs = explode(',,,,', $answers[0]['Question']['reference']);
                                        foreach ($refs as $ref) {
                                            $ref_url = explode(',,', $ref);
                                            if (!empty($ref_url[1]) && $ref_url[1] != ' ') {
                                                $ref_url[1] = validate_host($ref_url[1]);
                                                if (validate_host($ref_url[1])) {
                                                    ?>
                                                    <li>
                                                        <a target="_blank" href="<?php echo $ref_url[1]; ?>"><i class="fa fa-certificate"></i> <?php echo $ref_url[0]; ?></a>
                                                    </li>
                                                    <?php
                                                }
                                            }
                                        }
                                        ?> 
                                    </ul>
                                </div>
                                <div class="tab-pane fade in active" id="tab3default">
                                    <ul class="list-unstyled" >
                                        <?php foreach ($tags as $tag) { ?>
                                            <li><a href="<?php echo Router::url(array('controller' => 'categories', 'action' => 'view', $tag['Category']['slug'])); ?>"><i class="fa fa-tag"></i><?php echo $tag['Category']['name']; ?></a></li>
                                                <?php } ?>
                                    </ul>
                                </div>


                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <br/>
    </div>
<?php } ?>  
<?php
?>
<?php
if ($admin) {
    echo $this->Html->css(array('sweet-alert'));
    echo $this->Html->script(array('sweet-alert'), array('async' => 'async'));
    ?>
    <script>
        $(document).mouseup(function (e)
        {
            var container = $(".searchboxform_header");

            if (!container.is(e.target) // if the target of the click isn't the container...
                    && container.has(e.target).length === 0) // ... nor a descendant of the container
            {
                container.fadeOut();
            }
        });
        var lastscrolltop = 0;
        $(window).scroll(function (event) {
            var st = $(this).scrollTop();
            if (st > lastscrolltop) {
                $('#menuboxform_header').fadeOut();
            } else {
                $('#menuboxform_header').fadeIn();
            }
            lastscrolltop = st;
        });
        $('document').ready(function () {
            $('.btn-danger').click(function () {
                target = $(this).attr('target');
                ques_art = $(this).attr('ques-art-a');
                box1 = $(this).parent();
                box2 = $(box1).parent();
                console.log(ques_art);

                swal({
                    title: "Confirm Delete?",
                    type: "warning", showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Delete",
                    cancelButtonText: "Cancel",
                    closeOnConfirm: false, closeOnCancel: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $.post("<?php echo Router::url(array('controller' => 'articles', 'action' => 'delete_article')); ?>",
                                {
                                    target: target,
                                    ques_art: ques_art
                                },
                        function (data) {
                            swal("Delete");
                            if (target == 'a') {
                                window.location = "<?php echo $this->Html->url(array('controller' => 'homes', 'action' => 'index')); ?>";
                                return false;
                            } else {
                                box3 = $(box2).parent().slideToggle();
                            }

                        });
                    } else {
                        swal("Canceling", ")", "error");

                    }
                });
            });
            $('.toggle').click(function () {
                target = $(this).attr('target');
                status = $(this).attr('status');
                ques_art = $(this).attr('ques-art-a');
                box1 = $(this).parent();
                box2 = $(box1).parent();


                $.post("<?php echo Router::url(array('controller' => 'articles', 'action' => 'update_status')); ?>",
                        {
                            target: target,
                            ques_art: ques_art,
                            status: status
                        },
                function (data) {

                    if (target == 'a') {

                        return false;
                    } else {

                    }

                });
            })
            $(document).on('click', '.main_photo', function () {
                var article_id = $(this).attr('article_id');
                var photo_id = $(this).attr('photo_id');
                swal({
                    title: "Are you sure ?",
                    type: "info", showCancelButton: true,
                    confirmButtonText: " main image ",
                    cancelButtonText: "Cancel",
                    closeOnConfirm: false, closeOnCancel: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $.post("<?php echo Router::url(array('controller' => 'articles', 'action' => 'main_photo')); ?>",
                                {
                                    article_id: article_id,
                                    photo_id: photo_id
                                },
                        function (data) {

                            swal("Done");
                        });
                    } else {
                        swal("Cancel", "", "error");
                    }
                });


            });
            $('.image_array_btn').click(function () {
                $('.image_array').toggle();
            });
            $(document).on('click', '.blank', function () {
                var article_id = $(this).attr('article_id');
                swal({
                    title: "Are you sure ?",
                    type: "info", showCancelButton: true,
                    confirmButtonText: " bank ",
                    cancelButtonText: "Cancel",
                    closeOnConfirm: false, closeOnCancel: false
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $.post("<?php echo Router::url(array('controller' => 'articles', 'action' => 'blank')); ?>",
                                {
                                    article_id: article_id
                                },
                        function (data) {

                            swal("Done");
                        });
                    } else {
                        swal("Cancel", "", "error");
                    }
                });
            });
            $('.btn-select-home').click(function () {
                status = $(this).attr('status');
                if (status == 1) {
                    $(this).removeClass('btn-success');
                    $(this).addClass('btn-warning');
                    $(this).html('remove home page');
                    $(this).attr('status', 0);
                } else {
                    $(this).attr('status', 1);
                    $(this).removeClass('btn-warning');
                    $(this).addClass('btn-success');
                    $(this).html('show in home page');

                }
                slug = $(this).attr('slug');
                question_id = $(this).attr('question_id');
                title = $(this).attr('title');
                article_image = $(this).attr('article_image');

                $.post("<?php echo Router::url(array('controller' => 'articles', 'action' => 'select_home')); ?>",
                        {
                            question_id: question_id, title: title, article_image: article_image, slug: slug,
                            status: status
                        },
                function (data) {



                });
            })
        });

    </script>
    <?php
}
?>

<script>
    $(document).ready(function () {
        $(function () {
            $('.popup').click(function (event) {
                var width = 575, height = 400, left = ($(window).width() - width) / 2, top = ($(window).height() - height) / 2, url = $(this).attr('href'), opts = 'status=1' + ',width=' + width + ',height=' + height + ',top=' + top + ',left=' + left;
                window.open(url, 'twitter', opts);
                return false;
            });
        });
        $('.data-toload').each(function (index, value) {
            var link = $(this).attr('data-toload');
            $(this).attr('src', link);
        });
        $(".progress").css("opacity", "0");
        $('.panel-body').find(':not(a,div)').contents().unwrap();
        $("hpanel a").attr("target", "_blank");
        $('.fa-toggle-down').click(function () {
            $(this).children('.toggle-area').slideToggle('slow', 'swing');
        });
        var wrapper_top = $(".progress .wrapper_progress").offset().top;
        $(window).scroll(function () {
            var wrapper_height = $(".progress .wrapper_progress").height();

            // Affixes Progress Bars
            var top = $(this).scrollTop();
            if (top > wrapper_top - 10) {
                $(".progress .wrapper_progress").addClass("affix");
            }
            else {
                $(".progress .wrapper_progress").removeClass("affix");
            }

            var flag = 0;
            // Calculate each progress section
            $(".article_wrapper").each(function (i) {

                var this_top = $(this).offset().top;
                var height = $(this).height();
                var this_bottom = this_top + height;
                var percent = 0;

                // Scrolled within current section

                if (top >= this_top && top <= this_bottom) {
                    percent = ((top - this_top) / (height - wrapper_height)) * 100;
                    if (percent >= 100) {
                        percent = 100;
                        $(".progress .wrapper_progress .bar:eq(" + i + ") i").css("color", "#fff");
                    }
                    else {

                        $(".progress .wrapper_progress .bar:eq(" + i + ") i").css("color", "#36a7f3");

                    }
                }
                else if (top > this_bottom) {
                    percent = 100;


                    $(".progress .wrapper_progress .bar:eq(" + i + ") i").css("color", "#fff");
                }

                if (percent === 100) {
                    $(".progress").css("opacity", "1");

                }
                else if (percent <= 0) {
                    $(".progress").css("opacity", "0");
                }
                else {
                    $(".progress").css("opacity", "1");
                }
                if (percent >= 95) {
                    $('.saved_minutes').fadeIn('slow');
                } else {
                    $('.saved_minutes').fadeOut('slow');
                }
                $(".progress .wrapper_progress .bar:eq(" + i + ") span").css("width", percent + "%");
            });
            $('.fa-close').click(function () {
                $('.saved_minutes').css("opacity", "0");
            });

        });
        $('.circle_add_btn').click(function () {
            $('.circle-content').fadeToggle('slow');

        });
        $('.remove_class_btn').click(function () {
            console.log('dasdsa');
            $('.circle-content').animate({width: '0', opacity: '0'}, 1000, function () {
                $('.circle_add').removeClass('remove_class_btn');
                $('.circle_add').addClass('circle_add_btn');
            });


        });

        $('.speeli-share-btn').click(function () {
            question_id = $('.btn-select-home').attr('question_id');
            $.post("<?php echo Router::url(array('controller' => 'articles', 'action' => 'share_count')); ?>",
                    {
                        question_id: question_id,
                    },
                    function () {
                    });
        });

    });

</script>   
<?php
if (!empty($notification)) {
    ?>
    <script>

        var jNotify = $.JNotify({
            'className': 'JNotify-danger',
            // warning, info, success or error

            'theme': 'info',
            'backgroundColor': 'rgba(217, 237, 247, 0.1)',
            'borderColor': '#BCE8F1',
            'borderRadius': '3px',
            'position': 'center',
            'maxWidth': '500px',
            'top': 30,
            'zIndex': 9999,
            'height': null,
            'padding': '15px',
            'message': '<a href="<?php echo Router::url($link); ?>" ><?php echo $notification; ?></a>',
            'fontSize': '14px',
            'fontColor': '#31708F',
            'autoClose': false,
            'showCloseButton': true,
            'showDuration': 5000,
            'closeDuration': 1000
        });
    </script>

<?php }
?>