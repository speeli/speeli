<!-- SlIDER -->










<!-- THE SIDEBAR HELPER ROW CONTAINER -->
<div class="row">

    <!-- THE CONTENT PART WITH SIDEBAR -->
    <section class="span9 leftfloat withsidebar">




        <!-- 7 to 5 ROW -->
        <section class="row">
            <div class="span9">

                <article>
                    <!-- HOW TO BUY -->

                    <h3 class="article-title"><?php echo $details->title ?></h3>

                </article>
            </div><!-- END OF SPAN 7 -->









        </section>



        <!-- SOME CALL OUT -->



        <div class="divide15"></div>

        <!-- HORIZONTAL DIVIDER WITH ICON AND TEXT -->
        <div class="table bottomhr smartdivider">
            <div class="table-cell rp10">
                <p class="verysmall  lh30 coloredbottomhr nobreak"><strong><?php echo $details->title ?></strong></p>
            </div>

            <div class="table-cell fullwidth">
            </div>

            <div class="table-cell">
                <i class="icon-user small coloredbottomhr"></i>
            </div>
        </div>

        <!-- plugin newwwww part -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->

        <div id="main_area">
            <!-- Slider -->
            <div class="row">
                <div class="span9" id="slider">
                    <!-- Top part of the slider -->
                    <div class="row">
                        <div class="span8" id="carousel-bounding-box">
                            <div class="carousel slide" id="myCarousel">
                                <!-- Carousel items -->
                                <div class="carousel-inner">


                                    <?php
                                    if (!empty($allmedia)) {
                                        foreach ($allmedia as $media) {
                                            ?>




                                            
                                            <div class="item" data-slide-number="<?php echo $media->id ?>">
                                                <?php echo $media->title ?>
                                                <img src="<?php echo Yii::app()->getBaseUrl(true) ?>/uploads/<?php echo $media->image ?>"></div>


                                        <?php }
                                    }
                                    ?>    






                                </div><!-- Carousel nav -->
                                <a class="carousel-control left" data-slide="prev" href="#myCarousel">‹</a> <a class="carousel-control right" data-slide="next" href="#myCarousel">›</a>
                            </div>
                        </div>


                    </div>
                </div>
            </div><!--/Slider-->

            <div class="row hidden-phone" id="slider-thumbs">
                <div class="span9">
                    <!-- Bottom switcher of slider -->

                    <ul class="thumbnails">



                        <?php
                        if (!empty($allmedia)) {
                            foreach ($allmedia as $media) {
                                ?>		                     
                                <li class="span2">
                                    <a class="thumbnail" id="carousel-selector-<?php echo $media->id ?>"><img src="<?php echo Yii::app()->getBaseUrl(true) ?>/uploads/<?php echo $media->image ?>"></a>
                                </li>

                            <?php }
                        } ?>    





                    </ul>
                </div>
            </div>
        </div>


        <!-- plugin newwwww part -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------->
        <div class="clear"></div>
        </article> <!-- END OF OUR TEAM ARTICLE -->



        <div class="divide30"></div>



    </section> <!-- END OF THE CONTENT PART -->

    <!-- THE SIDEBAR -->
    <aside class="span3 graybg sidebar rightfloat">
        <div class="innerwrapper heightadjustter">


            <article class="widget simpletabs">

                <!-- THE TABS / SELECTORS -->


                <div class="divide15"></div>


                <div class="table bottomhr smartdivider">
                    <div class="table-cell rp10">
                        <p class="verysmall  lh30 coloredbottomhr nobreak"><strong>Members Area</strong></p>
                    </div>

                    <div class="table-cell fullwidth">
                    </div>

                    <div class="table-cell">
                        <i class="icon-user small coloredbottomhr"></i>
                    </div>
                </div><!-- END OF SMART DIVIDER -->



                <div class="tab-pane active"  style="display:<?php
                $employeeid = Yii::app()->user->id;

                if (!empty($employeeid)) {
                    echo 'none';
                } else {
                    echo
                    'block';
                }
                ?>;" id="recent1">




                    <?php
                    $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
                        'id' => 'login-form',
                        'enableClientValidation' => true,
                        'clientOptions' => array(
                            'validateOnSubmit' => true,
                        ),
                    ));
                    ?>



                    <div class="form-group">
                        <input type="text" class="form-control" id="login-username" name="LoginForm[username]"  placeholder="Username">
                    </div>

                    <div class="divide15"></div>
                    <div class="form-group">
                        <input id="login-password" type="password" class="form-control" name="LoginForm[password]" id="exampleInputPassword1" placeholder="Password">
                    </div>
                    <div class="divide15"></div>





                    <button type="submit" class="btn btn-primary">Login</button>


                    <div class="form-group">
                    </div> 
<?php $this->endWidget(); ?>
                </div>	




                <div class="tab-pane tab-pane-login active" style="display:<?php
                $employeeid = Yii::app()->user->id;

                if (!empty($employeeid)) {
                    echo 'block';
                } else {
                    echo
                    'none';
                }
                ?>;" id="">

                    <div>
                        <?php
                        $employeeid = Yii::app()->user->id;
                        $username = User::model()->findByPk($employeeid);
                        echo $username->fname;
                        echo $username->lname;
                        ?>
                    </div>
                    </br>
                    <div class="" >
                        <a href="<?php echo Yii::app()->getBaseUrl(true) ?>/home/member" class="tab-profile">Folders<span class="glyphicon glyphicon-log-out pull-right"></a>
                        <a href="<?php echo Yii::app()->getBaseUrl(true) ?>/home/logout" class="tab-profile">Logout<span class="glyphicon glyphicon-log-out pull-right">	</a>
                    </div>						  

                </div>

        </div>
        </article>


        <div class="divide15"></div>
        <style>
            .aside-style-image{

                height: 100px !important;
                margin: auto;
                display: block;

            }
        </style>
        <!-- THE SIMPLE FEATURED WORK MEdIA HOLDER -->
        <div class="table bottomhr smartdivider">
            <div class="table-cell rp10">
                <p class="verysmall  lh30 coloredbottomhr nobreak"><strong>Brochure</strong></p>
            </div>

            <div class="table-cell fullwidth">
            </div>

            <div class="table-cell">
                <i class="icon-picture small coloredbottomhr"></i>
            </div>										</div><!-- END OF SMART DIVIDER -->
        <a href="<?php echo Yii::app()->getBaseUrl(true) ?>/uploads/<?php echo $allmain->url ?>" download>
            <img src="<?php echo Yii::app()->getBaseUrl(true) ?>/uploads/<?php echo $allmain->image ?>" class="aside-style-image" />
        </a>


        <div class="divide15"></div>

        <!-- THE SIMPLE FEATURED WORK MEdIA HOLDER -->

        <div class="table bottomhr smartdivider">
            <div class="table-cell rp10">
                <p class="verysmall  lh30 coloredbottomhr nobreak"><strong>Newsletter</strong></p>
            </div>

            <div class="table-cell fullwidth">
            </div>

            <div class="table-cell">
                <i class="icon-mail small coloredbottomhr"></i>
            </div>
        </div><!-- END OF SMART DIVIDER -->
        <div class="pull-right" style="width:100%">
            <a href="<?php echo Yii::app()->getBaseUrl(true) ?>/uploads/<?php echo $allmain->google ?>" download>

                <img src="<?php echo Yii::app()->getBaseUrl(true) ?>/uploads/<?php echo $allmain->image2 ?>"  class="aside-style-image" />
            </a>
            <div class="divide15"></div>
            <form role="search" method="get" id="searchform">
                <input type="text" id="Form_Search" class="searchinput" name="Subscribe" value="Subscribe">
                <input type="submit" id="Form_Go" class="icon-mail" name="go" value="">
                <i class="icon-mail medium"></i>
                <div class="clear"></div>
            </form>

            <div class="divide15"></div>
        </div>

        <div class="divide15"></div>


</div><!-- END OF INNER WRAPER AND HIGHT JUSTIFIER -->
</aside><!-- END OF THE SIDEBAR -->

</div> <!-- END OF THE ROW CONTAINER -->

</section><!-- END OF CONTAINER -->
</section><!-- END OF CONTENT -->

</section> <!-- END OF MAIN CONTENT HERE -->

<footer>
    <section class="footer">
        <!-- A SPECIAL FOOTER FOR 4 - 8 SPAN LEFT SIDE !! -->
        <div class="container">
            <div class="row">

                <!-- SIMPLE ABOUT TEXT BLOCK -->
                <article class="span12">
                    <p>

                    <div class="center">


                    </div>

                    </p>
            </div>



        </div>
    </section>






