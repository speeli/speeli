<style>
    .expand{
        cursor: pointer;
    }
    .click{
        cursor: pointer;
    }
    .progress{
        display: none;
    }
    hr{
        border: 0;
        height: 1px;
        background-image: -webkit-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -moz-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -ms-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -o-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
    }
    .clear{
        clear: both;
    }
    .question_wrapper{
        margin: 0 auto;
    }
    .float-none{
        float:none;
        margin: 0 auto;
    }
    .hblue{
        background-color: #fff;
        padding: 1em;
        border-radius: 4px;
        -moz-border-radius: 4px;
        -webkit-border-radius: 4px;
        margin-bottom: 25px;
        position: relative;
        line-height: 25px;
        border: 1px solid #e5e5e5;
    }
    .hblue .panel-heading{
        padding: 10px 15px;
        background-color: rgba(245, 245, 245, 0.24);
        border-top: 1px solid #ddd;

        border-bottom-right-radius: 3px;
        border-bottom-left-radius: 3px;
    }
    .list-unstyled li{
        margin-left: 1em;
    }
</style>
<?php
$this->set("meta_description", "Latest Speeli articles");
$this->set("meta_robots", "noindex");
?>
<div class="row">
    <!-- Add the extra clearfix for only the required viewport -->
    <div  class="clearfix visible-xs-block"></div>
    <br class="clear">
    <hr>
    <div class="row ">
       
        <div class="col-md-8 float-none update_art_speeli  " style="border-left: 1px solid #ddd;">
            <!-- The time line -->
            <div class="hpanel forum-box">
                <?php
                $i = 1;

                $user = $this->Session->read('user_quesli');
                if ($user['User']['admin'] == 1) {
                    foreach ($questions as $question) {
                        if ($question['Question']['status'] == 1) {
                            $status = '<span style="color:green" class="fa fa-check-circle">verified</span>';
                        } else {
                            $status = '';
                        }
                         if ($question['Question']['expand'] == 1) {
                            $expand = '<span style="color:green" class="fa fa-lightbulb-o">Needs expanding</span>';
                            $art_status=0;
                        } else {
                            $art_status=1;
                            $expand = '<span style="color:grey" class="fa fa-lightbulb-o">Mark as :needs expanding</span>';
                        }
                         if ($question['Question']['quality'] == 1) {
                            $quality = '<span style="color:green" class="fa fa-lightbulb-o">passed quality guidelines</span>';
                            $art_quality=0;
                        } else {
                            $art_quality=1;
                            $quality = '<span style="color:grey" class="fa fa-lightbulb-o">no quality check</span>';
                        }
                         if ($question['Question']['edited'] == 1) {
                            $edited = '<span style="color:green" class="fa fa-lightbulb-o">Edited</span>';
                            $art_edit=0;
                        } else {
                            $art_edit=1;
                            $edited = '<span style="color:grey" class="fa fa-lightbulb-o">not edited</span>';
                        }
                        ?>
                        <div class="hpanel hblue">
                            <div class="panel-heading hbuilt">
                                <h3> <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $question['Question']['slug'])); ?>"> <?php echo $question['Question']['title']; ?></a>  </h3>
                                <?php echo $status; ?>
                                <div class="expand click"  art_status="<?php echo $art_status;?>" art_id="<?php echo $question['Question']['id']?>"><?php echo $expand?></div>
                                <div class="quality click"art_quality="<?php echo  $art_quality ;?>" art_id="<?php echo $question['Question']['id']?>"><?php echo $quality?></div>
                                <div class="edited click"  art_edit="<?php echo$art_edit ;?>" art_id="<?php echo $question['Question']['id']?>"><?php echo $edited?></div>
                            </div>
                            

                        </div>
                        <?php
                        $i++;
                    }
                } else {
                    foreach ($questions as $question) {
                        if ($question['Question']['status'] == 1) {
                            ?>
                            <div class="hpanel hblue">
                                <div class="panel-heading hbuilt">
                                    <h3> <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $question['Question']['slug'])); ?>"> <?php echo htmlspecialchars($question['Question']['title']); ?></a>  </h3>
                                    <?php echo $status; ?>
                                </div>


                            </div>
                            <?php
                            $i++;
                        }
                    }
                }
                ?>
                <!-- timeline time label -->
                <!-- END timeline item -->
                <!-- timeline item -->
            </div>
        </div><!-- /.col -->
    </div><!-- /.row -->
</div>
<?php if ($user['User']['admin'] == 1) { ?>
<script type="text/javascript">
      
            $('body').undelegate('.update_art', 'click').delegate('.update_art', 'click', function (e) {
        event.preventDefault();       
        target=$(this).attr('target');
                $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'articles', 'action' => 'interest')); ?>",
                data: {target: target}, // outer quotes removed
                cache: false,
                success: function (response, status) {
                   $('.update_art_speeli').html(response);
                },
            });
            });
            
        $('body').undelegate('.expand', 'click').delegate('.expand', 'click', function (e) {
            var art_id = $(this).attr('art_id');
             var art_status = $(this).attr('art_status');
         if(art_status==0){
        $(this).html('<span style="color:grey" class="fa fa-lightbulb-o">Mark as :needs expanding</span>');
        }else{
        $(this).html('<span style="color:green" class="fa fa-lightbulb-o">Needs expanding</span>'); 
        }
         $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'articles', 'action' => 'expand')); ?>",
                data: {art_id: art_id, art_status: art_status}, // outer quotes removed
                cache: false,
                success: function (response, status) {
                },
            });
        });
        $('body').undelegate('.quality', 'click').delegate('.quality', 'click', function (e) {
        console.log(art_status);
            var art_id = $(this).attr('art_id');
             var art_status = $(this).attr('art_quality');
             console.log(art_status);
         if(art_status==0){
        $(this).html('<span style="color:grey" class="fa fa-lightbulb-o">No quality check</span>');
        }else{
        $(this).html('<span style="color:green" class="fa fa-lightbulb-o">Passed quality quidelines</span>'); 
        }
         $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'articles', 'action' => 'quality')); ?>",
                data: {art_id: art_id, art_status: art_status}, // outer quotes removed
                cache: false,
                success: function (response, status) {
                },
            });
        });
        $('body').undelegate('.edited', 'click').delegate('.edited', 'click', function (e) {
            var art_id = $(this).attr('art_id');
             var art_status = $(this).attr('art_edit');
             console.log(art_status);
         if(art_status==0){
        $(this).html('<span style="color:grey" class="fa fa-lightbulb-o">Not edited</span>');
        }else{
        $(this).html('<span style="color:green" class="fa fa-lightbulb-o">Edited</span>'); 
        }
         $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'articles', 'action' => 'art_edit')); ?>",
                data: {art_id: art_id, art_status: art_status}, // outer quotes removed
                cache: false,
                success: function (response, status) {
                },
            });
        });
        </script>
        <?php } ?>