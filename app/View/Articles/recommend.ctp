<div class="panel with-nav-tabs panel-default bottom-tab">
    <div class="panel-heading">
        <h3 class="text-center ">Speeli Recommends </h3>
    </div>
    <div class="panel-body-larger">
        <div class="tab-content">
            <img src="<?php echo $this->webroot . 'ads/' . $ads['Ads']['image']; ?>" class="img-responsive img-thumbnail" style="">
            <h3 class="text-center "><a href="<?php echo $ads['Ads']['url'];?>" ><?php echo $ads['Ads']['title'];?></a></h3>
        </div>
    </div>
</div>