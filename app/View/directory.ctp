<style>

    body{overflow-x:hidden;}
    .card {

        height:13em;
        padding: 0;
    }
    .wrapper{
        margin: 0;
        padding: 0;
        width: 100%;
    }
    .card:nth-child(1){background-color:#667779;}
    .card:nth-child(2) {background-color:#579999;}
    .card:nth-child(3) {background-color:#DD5422;}
    .card:nth-child(4) {background-color:#88BBBB;}
    .card:nth-child(5) {background-color:#e22885;}
    .card:nth-child(6) {background-color:#ffd800;}
    .card:nth-child(7) {background-color:#667779;}
    .card:nth-child(8) {background-color:#3D9EAA;} 
    .card:nth-child(9) {background-color:#ddff00;}
    .card:nth-child(10){background-color:#667779;}
    .card:nth-child(5) {background-color:#579999;}
    .front, .back {
        text-align: center;
        font-size:1.5em;
        cursor:pointer;
    }
    .card a{
        text-align: center;
        font-size:1.2em;
        color:white;
    }
    .back{
        background: #22A39F;
        padding-top: 2em;
    }
    .front{
        padding-top: 3em;
    }
    .front h4{
        margin-top:42px;
        font-size:32px;font-family:'PT Sans Narrow', sans-serif;
        color:#fff;

    }

    .front span {display:block;color:#fff;font-size:60px;margin-top:-20px;}
    .back {
        font-family:'PT Sans Narrow', sans-serif;
        color:#fff;
    }

    .back strong
    {    display: block;
         margin: 17px 0 0px;
    }
    section{
        position: relative;
        background-position: center center;
        background-size: cover;
    }
    .fade{
        width: 100%;
        height: 100%;
        position: absolute;
        background: rgba(221, 221, 221, 0.37)
    }
</style>
<?php
echo $this->Html->script(array('jquery.flip.min','typed'));
?>
<section class="half_height" style="background-image: url(<?php echo $this->webroot . 'img/directory6.jpg' ?>)">
    <div class="fade"></div>
    <center><h3  style="font-size:2.2em;font-weight: 700;color:white;" class="mobile page-header hometitle">
            What do you want to explore today ?
        </h3></center>
    <br class="clear">

</section>
<script>
  $(function(){
      $(".page-header").typed({
        strings: ["What do you want to explore today ?", "Speeli summarize cluttered internet into list"],
        typeSpeed: 0
      });
  });
</script>
<div class="row">
    <div class="col-md-10 col-xs-12 float-none card-grid"  id="grid-containter">
        <center><h3  style="font-size:2.2em;font-weight: 700;" class="mobile page-header hometitle">
                What do you want to explore today ?
            </h3></center>
        <br class="clear">
        <?php foreach ($grids as $grid) { ?>
            <div  class="col-md-4 col-xs-12  card green "> 

                <div class="front"> 
                    <a href="<?php echo Router::url('/categories/category/' . $grid['MainCategory']['slug']) ?>" >
                        <?php echo $grid['MainCategory']['name'] ?>
                        <br>

                    </a>
                </div> 
                <div class="back">
                    <a href="<?php echo Router::url('/categories/category/' . $grid['MainCategory']['slug']) ?>" >
                        <?php echo $grid['MainCategory']['name'] ?> <br>
                        Summaries :  <?php echo $grid[0]['co'] ?><br>
                        Views : <?php echo $grid[0]['v'] ?>
                    </a>
                </div> 
            </div>

        <?php } ?>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function () {


        $('body').undelegate('.front', 'click').delegate('.front', 'click', function (e) {
            $(this).closest('a').click();
        });
        $('body').undelegate('.back', 'click').delegate('.back', 'click', function (e) {
            $(this).closest('a').click();
        });
    });
</script>
<script type="text/javascript">
    $('.showmore').click(function () {
        event.preventDefault();

    })
    $(function () {
        $(".card").flip({
            trigger: "hover",
            speed: 400
        });
    });
    $(".front").click(function () {
        if ($(this).find("a").length) {
            window.location.href = $(this).find("a:first").attr("href");
        }
    });
    $(".back").click(function () {
        if ($(this).find("a").length) {
            window.location.href = $(this).find("a:first").attr("href");
        }
    });
</script>
<script>
    $('.half_height').height((document.body.clientHeight / 4)*2);
    $('section').width(document.body.clientWidth);
    $('.half_height').css({'top': ($('.navbar-fixed-top').height()) + 10});
    if (typeof window.orientation !== 'undefined') {
        $('.mobile').hide();
    } else {

    }


</script>