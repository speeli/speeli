
<?php
if (!empty($questions)) {
    foreach ($questions as $question) {
        ?>
        <?php
        if (!empty($question['Question']['title_update'])) {
            $explode_title = explode(',,,', $question['Question']['title_update']);
            $last = end($explode_title);
            $title = end(unserialize(base64_decode($last)));
        } else {
            $title = $question['Question']['title'];
        }
        $title = preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', $title);
        ?>
<li class="clear clearfix">
            <a  href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', trim($category['Question']['slug']))); ?>">
                <img width="100" height="70" src="<?php echo $this->webroot . 'img/directory.jpg'  ?>"
                     title="<?php echo $title ?>" alt="<?php echo $title ?>"  class="img-responsive no-padding col-md-3 col-xs-4">
            </a>
            <a class="col-md-9 col-xs-8 text-white" href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $question['Question']['slug'])); ?>">
                <?php echo $title ?></a></li>

        <?php
    }
}
?>
