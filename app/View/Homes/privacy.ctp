<?php 
//$this->set("meta_robots", "index");
$this->set("meta_description", 'Privacy Policy for Speeli.com');
$this->set("page_title",  "Privacy Policy for Speeli.com");
?>
<style>
    h2{color:#888; width: 100%;text-align: center;}
    h3,h4{ color: #63b5b2;
           font-weight: bold;
          
           /* width: 100%; */
          }
    .panel-body ul{font-size: 1.1em;}
    p{
        font-size: 1.1em;
    }
    li{list-style: inherit}
     @media only screen and (max-width: 480px){
         h2{margin-top: 1.7em}
        h3,h4 {
          text-align: center;
           
        }
    }
</style>
<div class="row">
    <div class="col-md-10 float-none">
        <div class="panel">
            <div class="panel-body">

                <h2>Privacy Policy for Speeli.com
</h2>
                <div id="1">
                    <h3>Information Collection and Use</h3>
                    <p>
                    <b style='color:#3BC8C2'> Speeli </b> uses the information we collect to analyze how the Service is used,
                    diagnose service or technical problems, maintain security, personalize content,
                    remember information to help you efficiently access your account,
                    monitor aggregate metrics such as total number of visitors, traffic,
                    and demographic patterns, and track User Content and users as necessary to 
                    comply with the Digital Millennium Copyright Act and other applicable laws.
                    </p>

                </div>
                <div id="2">
                    <h3>User-Provided Information:</h3>
                    <p>
                      
You provide us information about yourself, such as your name, e-mail address, gender, preferences, interests, behaviour while using <b style='color:#3BC8C2'> Speeli </b> or any other information you submit on <b style='color:#3BC8C2'> Speeli </b>, if you register for a member account with the Service. Your name and other information you choose to add to your profile will be available for public viewing on the Service. We may use your email address to send you Service-related notices (including any notices required by law, in lieu of communication by postal mail). 
<br/>
We may also use your contact information to send you marketing or personalized content on email messages. If you do not want to receive such messages, you may opt out by following the instructions in the message. If you correspond with us by email, we may retain the content of your email messages, your email address and our responses.
<br/>
If you use your Facebook account information to sign in to <b style='color:#3BC8C2'> Speeli </b>, we will collect and store your Facebook user ID. If you connect your <b style='color:#3BC8C2'> Speeli </b> account with your Facebook, we ask for your permission to collect certain information from your Facebook account (such as your email address or name)
<br/>
You also provide us information in User Content you post to the Service. Your contributions on the Service, and metadata about them (such as when you posted them), are publicly viewable on the Service, along with your name (unless the Service permits you to post anonymously). This information may be searched by search engines and be republished elsewhere on the Web.
<br/>
                    </p>
                </div>
                <div id="3">
                    <h3> Information Collected Automatically </h3>
                    <p>
                       
When you use the Service, we might use and store persistent and session cookies and other tracking technologies such as log files. We also :
                    </p>
                    <ul>
                        <li>
                           (a) store your username and password; 
                        </li>
                        <li>
                            (b) analyze the usage of the Service;
                        </li>
                         <li>
                            (c) customize the Service to your preferences after saving and tracking the user’s behaviour on <b style='color:#3BC8C2'> Speeli </b>; 
                        </li>
                         <li>
                          (d) control the advertising displayed by the Service  
                        </li>
                        
                    </ul>
                    <p>
   . As we adopt additional technology, we may also gather additional information through other methods.
<br/>
We use these automated technologies to collect and analyze certain types of information, including:
                    </p>
                    <ul>
                         <li>
                            (a) information related to the devices or browsers you use to access or interact with the Service, such as: IP addresses,
geolocation information, unique device identifiers and other information about your mobile phone or other mobile device(s),
browser types, browser language, and unique numbers or codes in cookies;
                        </li>
                        <li>
(b) information related to the ways in which you interact with the Service,
such as: referring and exit pages and URLs, platform type, the number of clicks, 
domain names, landing pages, pages viewed and the order of those pages, 
the amount of time spent on particular pages, the date and time you used the Service, and other similar information.
                </li>
                </ul>
                    <p>
We may also capture other data, such as search criteria and results.
<br/>
We may collect different types of information about your location, including general information (e.g., IP address, zip code) and more specific information (e.g., GPS-based functionality on mobile devices used to access the Service), and may use that information to customize the Service with location-based information and features. If you access the Service through a mobile device and you do not want your device to provide us with location-tracking information, you can disable the GPS or other location-tracking functions on your device, provided your device allows you to do this.
<br/>

                    </p>
                    
                    
                </div>
                <div id="4">
                    <h3>Cookies</h3>
                    
                    <p>
                        
                        
We do use cookies to store information, such as your personal preferences when you visit our site. This could include showing you a popup once in your visit, or the ability to login to some of our features, such as community dashboard.
We might also use third party advertisements on www.speeli.com to support our site. Some of these advertisers may use technology such as cookies and web beacons when they advertise on our site, which will also send these advertisers (such as Google through the Google AdSense program) information including your IP address, your ISP , the browser you used to visit our site, and in some cases, whether you have Flash installed. 
<br/>
This is generally used for geotargeting purposes (showing New York real estate ads to someone in New York, for example) or showing certain ads based on specific sites visited (such as showing cooking ads to someone who frequents cooking sites).
<br/>
We sometimes use cookies to personalize content and ads, to provide social media features and to analyze our traffic. We also share information about your use of our site with our advertising and analytics partners (Google Adsense and Google Analytics) who may combine it with other information you’ve provided to them or they’ve collected from your use of their services.
<br/>
Cookies cannot be used to find your name, email address, or anything that is uniquely identifying about you, unless you provide that information. We use cookies for those applications that require us to keep track of information to customize results.
<br/>
You can choose to disable or selectively turn off our cookies or third-party cookies in your browser settings, or by managing preferences in programs such as Norton Internet Security. However, this can affect how you are able to interact with our site as well as other websites. This could include the inability to login to services or programs, such as logging into forums or accounts.
 <br/>

                    </p> 
                </div>
                <div id="4">
                    <h3>Server Log Files</h3>
                    <p>
                        
As with most other websites, we collect and use the data contained in log files. The information in the log files include your IP (internet protocol) address, your ISP (internet service provider, such as AOL or Shaw Cable), the browser you used to visit our site (such as Internet Explorer or Firefox), the time you visited our site and which pages you visited throughout our site We may use the ip address to block it in case it doesn’t comply with our terms of usage. 
This information is never sold to any party and is only used to troubleshoot and maintain the website.
<br/>

                    </p>
                      

                </div>
                <div id="5">
                    <h3> Third Party Analytics and Advertising</h3>
                    <p>
                     
<b style='color:#3BC8C2'> Speeli </b> may allow third parties to help us collect and analyze information about your use of the service, generate aggregate site usage statistics and provide content sharing services to support the Service. These third parties may also use cookies or similar technologies to collect information about your use of the Service. <b style='color:#3BC8C2'> Speeli </b> does not control the third parties’ use of such technologies and their use is governed by those parties’ privacy policies.
<br/>
<b style='color:#3BC8C2'> Speeli </b> may also allow third-party ad servers or ad networks to serve advertisements on the Service and on third-party services. These third-party ad servers or ad networks use technology to send, directly to your browser, the advertisements and links that appear on <b style='color:#3BC8C2'> Speeli </b>. They automatically receive your IP address when this happens. They may also use other technologies (such as cookies, JavaScript, or web beacons) to measure the effectiveness of their advertisements and to personalize the advertising content. <b style='color:#3BC8C2'> Speeli </b> does not provide any information that can identify you personally to these third-party ad servers or ad networks without your consent. However, please note that if an advertiser asks <b style='color:#3BC8C2'> Speeli </b> to show an advertisement to a certain audience and you respond to that advertisement, the advertiser or ad server may conclude that you fit the description of the audience they are trying to reach.
<br/>
<b style='color:#3BC8C2'> Speeli </b> itself does not respond to “do not track” signals, and we do not control whether third parties do. 
<br/>
   </p>
                </div>
                <div id="6">
                    <h3>How We Share Your Information </h3>
                    <p>

<b style='color:#3BC8C2'> Speeli </b> may share your information with third party service providers for the purpose of providing the Service to you, such as payment processors, email service providers, analytics platforms and providers of technical infrastructure (such as servers or databases co-located with hosting providers), engineering, or other support.
<br/>
As we develop our business, we may buy or sell assets or business offerings. Customer, email, and visitor information are generally among the transferred business assets in these types of transactions. We may also transfer or assign such information in the course of corporate divestitures, mergers, or dissolution.
<br/>
We may share or disclose your information with your consent, such as if you choose to sign on to the Service through a third-party service. We cannot control third parties’ use of your information.
<br/>
We may disclose your information if we are required to do so by law, or if we believe in good faith that it is reasonably necessary to
                    </p>
                    <ul>
                        <li>
                            (i) respond to claims asserted against us or to comply with legal process (for example, subpoenas or warrants),
                        </li>
                         <li>
                            (ii) enforce or administer our agreements with users, such as the Terms of Service; 
                        </li>
                         <li>
                            (iii) for fraud prevention, risk assessment, investigation, customer support, product development and debugging purposes.
                        </li>
                         <li>
                            (iv) protect the rights, property, user-experience or safety of <b style='color:#3BC8C2'> Speeli </b>, its users, or members of the public.
                        </li>
                        
                    </ul>
                    <p>
<br/>
By using the Service you consent to the transfer of your information to the United States and/or to other countries for storage, processing and use by <b style='color:#3BC8C2'> Speeli </b> in accordance with our Privacy Policy.
<br/>
We may aggregate and/or anonymize information collected through the Service so that the information does not identify you. We may use aggregated, anonymized, and other de-identified information for any purpose, including for research and marketing purposes, and our use and disclosure of such information is not subject to any restrictions under this Privacy Policy.
<br/>
</p>
                </div>
                <div id="6">
                    <h3>How We Protect Your Information</h3>
                    <p>
<b style='color:#3BC8C2'> Speeli </b> uses a variety of physical, managerial, and technical safeguards to preserve the integrity and security of your information, 
based on the sensitivity of the information. 
We cannot, however, ensure or warrant the security of any information you transmit to <b style='color:#3BC8C2'> Speeli </b> or guarantee that your 
information on the Service may not be accessed, disclosed, altered,
or destroyed by breach of any of our physical, technical, or managerial safeguards.
<br/>
</p>
                </div>
                <div id="7">
                    <h3>Children’s Privacy</h3>
                    <p>
                    
Protecting the privacy of young children is especially important.
<b style='color:#3BC8C2'> Speeli </b> does not knowingly collect or solicit personal information from anyone under the age of 13 
(or under the age of 14 for anyone living in Spain or South Korea) or knowingly allow such persons to register with the Service. 
If we become aware that we have collected personal information from a child under the relevant age without parental consent, 
we take steps to remove that information.
<br/>
</p>
                </div>
                <div id="8">
                    <h3> Links to Other Web Sites</h3>
                    <p>

                       
We are not responsible for the practices employed by websites linked to or from the Service,
nor the information or content contained therein nor third parties platforms <b style='color:#3BC8C2'> Speeli </b> use. Please remember that when you use a link to go from the Service to another website, our Privacy Policy is no longer in effect. Your browsing and interaction on any other website, including those that have a link on our website, is subject to that website’s own rules and policies.
<br/> </p>
                </div>
                <div id="9">
                    <h3> Changes to Our Privacy Policy</h3>
                    <p>

If we change our privacy policies and procedures, we will post those changes on this page to keep you aware of what information we collect, how we use it and under what circumstances we may disclose it. Changes to this Privacy Policy are effective when they are posted on this page. 
<b style='color:#3BC8C2'> Speeli </b> may modify or update this Privacy Policy from time to time, so please review it periodically. We may provide you additional forms of notice of modifications or updates as appropriate under the circumstances. Your continued use of <b style='color:#3BC8C2'> Speeli </b> or the Service after any modification to this Privacy Policy will constitute your acceptance of such modification.
If you have any questions about our practices or this Privacy Policy, please contact us at <b style='color:#3BC8C2;text-decoration: underline;'>support@speeli.com</b>
                    </p>


                </div>
                
            </div>   
        </div>
    </div>
</div>