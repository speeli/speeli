<style>
    .expand{
        cursor: pointer;
    }
    .click{
        cursor: pointer;
    }
    .progress{
        display: none;
    }
    hr{
        border: 0;
        height: 1px;
        background-image: -webkit-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -moz-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -ms-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -o-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
    }
    .clear{
        clear: both;
    }
    .question_wrapper{
        margin: 0 auto;
    }
    .float-none{
        float:none;
        margin: 0 auto;
    }
    .hblue{
        background-color: #fff;
        padding: 1em;
        border-radius: 4px;
        -moz-border-radius: 4px;
        -webkit-border-radius: 4px;
        margin-bottom: 25px;
        position: relative;
        line-height: 25px;
        border: 1px solid #e5e5e5;
    }
    .hblue .panel-heading{
        padding: 10px 15px;
        background-color: rgba(245, 245, 245, 0.24);
        border-top: 1px solid #ddd;

        border-bottom-right-radius: 3px;
        border-bottom-left-radius: 3px;
    }
    .list-unstyled li{
        margin-left: 1em;
    }
</style>
<?php
$this->set("meta_description", "Latest Speeli articles");
$this->set("meta_robots", "noindex");
?>

<div class="row">
    <!-- Add the extra clearfix for only the required viewport -->
    <div  class="clearfix visible-xs-block"></div>
    <br class="clear">
    <hr>
    <div class="row ">
        <div class="ads_type_id" target="<?php echo $id; ?>">
            <div class="col-md-8 float-none update_art_speeli  " style="border-left: 1px solid #ddd;">
                <!-- The time line -->
                <div class="hpanel forum-box question_update">
                    <div class="item form-group">
                        <label  class="control-label col-md-3 col-sm-3 col-xs-12">Category<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <?php echo $this->Form->input('new', array('class' => 'new-cat-value', 'placeholder' => 'Write Tags', 'label' => false, 'div' => false)); ?>
                            <div class="results">

                            </div>
                        </div>
                    </div>
                    <!-- timeline time label -->
                    <!-- END timeline item -->
                    <!-- timeline item -->
                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->

        <?php echo $this->Js->writeBuffer(); ?>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.new-cat-value').keyup(function () {
                var data_send = $(this).val();
                $.ajax({
                    type: "post", // Request method: post, get
                    url: "<?php echo Router::url(array('controller' => 'homes', 'action' => 'ads_cat')); ?>",
                    data: {data_send: data_send}, // outer quotes removed
                    cache: false,
                    success: function (response, status) {
                        $('.results').html(response);
                    },
                });
            });
        });
        $('body').undelegate('.suggest-items', 'click').delegate('.suggest-items', 'click', function (e) {
            var ads_id=$('.ads_type_id').attr('target');
            cat_id = $(this).attr('ques-cat-id');
            $(this).remove();
            $('.new-cat-value').val("");
            console.log(ads_id);
            console.log(cat_id);
            $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'homes', 'action' => 'attach_article')); ?>",
                data: {ads_id: ads_id,cat_id:cat_id}, // outer quotes removed
                cache: false,
                success: function (response, status) {
                    $('.results').html(response);
                },
            });
        });
    </script>