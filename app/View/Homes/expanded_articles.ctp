<div class="row">


    <!-- Add the extra clearfix for only the required viewport -->
    <div class="clearfix visible-xs-block"></div>
    <br class="clear"><br class="clear">

    <br class="clear">

            <center> 
                <b><a style='background:#63B5B2;color :white;' class="btn site_color" href="<?php
                      echo Router::url('/dashboard');
                      ?>"><span class="fa fa-history"> </span> Back to dashboard</a></b> </center>
    
    <center><h2 class="page-header"> 
            <b>Speeli Articles that need to expand</b> </h2></center>
    <div class="row ">

        <div class="col-md-8 float-none ">
            <!-- The time line -->
            <div class="hpanel forum-box">
                <?php
                for ($q = 0; $q < count($questions); $q++) {
                    if (!empty($questions[$q]['Question']['title_update'])) {
                        $explode_title = explode(',,,', $questions[$q]['Question']['title_update']);
                        $last = end($explode_title);
                        $title = end(unserialize(base64_decode($last)));
                    } else {
                        $title = $questions[$q]['Question']['title'];
                    }
                    ?> 
                    <div class="hpanel hblue">
                        <div class="panel-heading hbuilt">
                            <h3> 
                                <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $questions[$q]['Question']['slug'])); ?>"> <?php
                                    $title = str_replace('&nbsp;', ' ', $title);
                                    $title = str_replace('nbsp', ' ', $title);
                                    $title = str_replace('%', ' ', $title);
                                    echo htmlspecialchars($title);
                                    ?>
                                </a> 
                            </h3>
                        </div>
                    </div>
                <?php } ?>


                <!-- timeline time label -->
                <!-- END timeline item -->
                <!-- timeline item -->
            </div>
        </div><!-- /.col -->
    </div><!-- /.row -->
</div>