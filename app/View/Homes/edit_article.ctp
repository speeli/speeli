<?php
    $this->Paginator->options(array(
        'update' => '.edit_article',
        'evalScripts' => true,
        'before' => $this->Js->get('.panel-edit_article #loader')->effect(
                'fadeIn', array('buffer' => false)
        ),
        'complete' => $this->Js->get('.panel-edit_article #loader')->effect(
                'fadeOut', array('buffer' => false)
        ),
    ));
    ?>
    <?php
    for ($q = 0; $q < count($questions); $q++) {
        if (!empty($questions[$q]['Question']['title_update'])) {
            $explode_title = explode(',,,', $questions[$q]['Question']['title_update']);
            $last = end($explode_title);
            $title = end(unserialize(base64_decode($last)));
        } else {
            $title = $questions[$q]['Question']['title'];
        }
        ?> 
        <h5>
            <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'edit', $questions[$q]['Question']['slug'])); ?>"> <?php
                $title = str_replace('&nbsp;', ' ', $title);
                $title = str_replace('nbsp', ' ', $title);
                $title = str_replace('%', ' ', $title);
                echo htmlspecialchars($title);
                ?>
            </a> 
        </h5>
    <?php } ?>

<div class="col-md-7 col-xs-12 nopadding">
    <?php
    if ($this->Paginator->params()['prevPage'] == true) {
        echo $this->Paginator->prev('<i class="fa fa-chevron-left"></i>', array('limit' => '6','escape' => false,));
    }
    echo $this->Js->writeBuffer();
    ?>
    <?php
    if ($this->Paginator->params()['nextPage'] == true) {
        echo $this->Paginator->next('Next articles <i class="fa fa-chevron-right"></i>', array('limit' => '6','escape' => false,));

    }
    echo $this->Js->writeBuffer();
    ?>

</div>
<div class="col-md-5 col-xs-12 nopadding loadall">
    <a href="<?php echo Router::url(array('controller'=>'homes','action'=>'edit_articles'));?>">Load All articles</a>  
</div>