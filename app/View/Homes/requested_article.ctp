<?php
$this->Paginator->options(array(
    'update' => '.request_article',
    'evalScripts' => true,
    'before' => $this->Js->get('.panel-request_article #loader')->effect(
            'fadeIn', array('buffer' => false)
    ),
    'complete' => $this->Js->get('.panel-request_article #loader')->effect(
            'fadeOut', array('buffer' => false)
    ),
));
?>
<?php
for ($q = 0; $q < count($questions); $q++) {
    $title = $questions[$q]['Request']['title'];
    ?> 
    <h5>
        <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'add', $questions[$q]['Request']['id'])); ?>"> <?php
            $title = str_replace('&nbsp;', ' ', $title);
            $title = str_replace('nbsp', ' ', $title);
            $title = str_replace('%', ' ', $title);
            echo htmlspecialchars($title);
            ?>
        </a> 
    </h5>
<?php } ?>
<div class="col-md-7 col-xs-12 nopadding">
    <?php
    if ($this->Paginator->params()['prevPage'] == true) {
        echo $this->Paginator->prev('<i class="fa fa-chevron-left"></i>', array('limit' => '6','escape' => false,));
    }
    echo $this->Js->writeBuffer();
    ?>
    <?php
    if ($this->Paginator->params()['nextPage'] == true) {
        echo $this->Paginator->next('Next articles <i class="fa fa-chevron-right"></i>', array('limit' => '6','escape' => false,));

    }
    echo $this->Js->writeBuffer();
    ?>

</div>
<div class="col-md-5 col-xs-12 nopadding loadall">
    <a href="<?php echo Router::url(array('controller'=>'homes','action'=>'requested_articles'));?>">Load All articles</a>  
</div>