<style>
    .clear{
        clear:both;
    }
    input[type='radio']{
        padding: 1px;
        margin-left: 1em;
        margin-right: 1em;
    }
</style>
<?php
$this->set("meta_robots", "noindex");
?>
<br class="clear">
<div class="col-md-6 col-xs-12 float-none">
    <?php
    echo $this->Form->create('Question');
    echo $this->Form->input('category', array('style' => 'position:absolute;width:90%;',
        'id' => 'Title', 'placeholder' => 'Search in logs: by articles name,username,ip address ', 'class' => 'login-form-text flex-column',
        'div' => false, 'label' => false));
    ?>
    <div style="right:0px;position: absolute;top:3px;" class="check_status  btn btn-main"><i class="fa fa-search"></i></div>
    <br class="clear">
    <br class="clear">
    <div class="col-md-12 col-xs-12 ">
        <?php
        echo $this->Form->input('type', array('legend' => false, 'type' => 'radio', 'options' => array(1 => 'By article name', 2 => 'By user', 3 => 'By ip address'),
            'div' => false, 'label' => false));
        ?></div>
</div> 

<br class="clear">
<div class="col-md-6 col-xs-12 float-none">
    <?php
    echo $this->Form->create('Block');
    echo $this->Form->input('user_ip', array(
        'placeholder' => 'Add the ip address here', 'div' => false, 'label' => false));
    ?>
    <div style="right:0px;position: absolute;top:3px;" class="block_ip  btn btn-main"><i class="fa fa-plus"></i></div>
    <div style='text-align: center; display: none;'class="success">ip address blocked</div>
    <br class="clear">
    <br class="clear">
</div> 
<div class="col-md-12 col-xs-12 " >
    <div class="panel-heading">
        <ul class="nav nav-tabs">
            <li speel_ques='0'  class="active update_art"><a href="#ta12default" data-toggle="tab">Question title updated</a></li>
            <li speel_ques='1'  class="update_art"><a href="#tab2default" data-toggle="tab">point title updated</a></li>
            <li speel_ques='2' class='update_art' ><a href="#tab3default" data-toggle="tab">point body updated</a></li>
            <li speel_ques='3' class='update_art' ><a href="#tab4default" data-toggle="tab">point added</a></li>
            <li speel_ques='4' class='update_art' ><a href="#tab4default" data-toggle="tab">new articles added</a></li>
        </ul>
    </div>
</div>
<br class="clear">
<div class="status_log" user_status="6" speeli_title="0" target="0"></div>
<div class="col-md-12 col-xs-12 " >
    <div class="panel-heading">
        <ul class="nav nav-tabs">
            <li speel_ques='5'   class="active update_art"><a href="#ta12default" data-toggle="tab">anonymous</a></li>
            <li speel_ques='6'  class="update_art"><a href="#tab2default" data-toggle="tab">registered</a></li>
        </ul>
    </div>
</div>
<br class="clear">
<div class="update_art_speeli">         
    <table class="table table-hover table-responsive col-md-12 col-xs-12">
        <tr>
            <th class="col-xs-3">Text</th>
            <th>User</th>
            <th>article</th>
            <th>Time</th>
        </tr>
        <?php foreach ($logs as $log) { ?>
            <?php
            if (!empty($log)) {
                if (empty($log['Log']['question'])) {
                    $log['Log']['question'] = $log['Log']['question_slug'];
                }
                ?>

                <tr>
                    <td  ><?php echo $log['Log']['title']; ?></td>
                    <td  ><?php if (!empty($log['User']['username'])) { ?>
                            <a  href="<?php
                            echo Router::url(array('controller' => 'users',
                                'action' => 'view', $log['User']['id']))
                            ?>" class="btn  btn-main "><?php echo  $log['User']['username']; ?>
                            </a> <?php
                        } else {
                            echo $log['Log']['user_ip'];
                        }
                        ?></td>
                    <td class="col-md-4"> 
                        <?php if ($log['Log']['answer_id'] != 0) { ?>
                            <a  href="<?php
                            echo Router::url(array('controller' => 'articles',
                                'action' => 'view', $log['Log']['question_slug']))
                            ?>" class="btn  btn-main "><?php echo strip_tags($log['Log']['question']).' point number : '.$log['Log']['answer_id']; ?>
                            </a>     
                        <?php } else { ?>
                            <a  href="<?php
                            echo Router::url(array('controller' => 'articles',
                                'action' => 'view', $log['Log']['question_slug']))
                            ?>" class="btn  btn-main "><?php echo strip_tags($log['Log']['question']).' point number : '.$log['Log']['answer_id']; ?>
                            </a>
                        <?php } ?> 
                    </td>

                    <td  ><?php echo $log['Log']['created']; ?></td>
                </tr>
                <?php
                
                ?>



            <?php
            }
        }
        ?>
    </table>

    <ul class="pagination">
        <?php
        echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
        echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'currentClass' => 'active', 'tag' => 'li', 'first' => 1));
        echo $this->Paginator->next(__('next'), array('tag' => 'li', 'currentClass' => 'disabled'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
        ?>
    </ul>
</div>
<script>
    $(document).ready(function () {

        $('body').undelegate('.update_art', 'click').delegate('.update_art', 'click', function (e) {
            event.preventDefault();
            target = $(this).attr('speel_ques');
            var status_log;
            if (target == 0) {
                status_log = 'Question title updated';
            } else if (target == 1) {
                status_log = 'point title updated';
            }
            else if (target == 2) {
                status_log = 'point body updated';
            }
            else if (target == 3) {
                status_log = 'new point added';
            }

            else if (target == 4) {
                status_log = 'New Question Added';
            }

            $('.status_log').attr('target', status_log);
            status_log_send = $('.status_log').attr('target');
            if (status_log_send == 0) {

                status_log_send = status_log;
            } else {

            }
            if (status_log < 5) {
                target = $('.status_log').attr('user_status');
            } else {
                $('.status_log').attr('user_status', target);
            }
            if (status_log_send) {

            } else {
                status_log_send = 0;
            }
            console.log(status_log_send);
            console.log(target);

            $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'homes', 'action' => 'logs_updates')); ?>",
                data: {target: target, status_log_send: status_log_send}, // outer quotes removed
                cache: false,
                success: function (response, status) {
                    $('.update_art_speeli').html(response);
                },
            });
        });

        $('.check_status').on('click', function () {

            event.preventDefault();
            var questitle = $('#Title').val();
            var log_type = $("#QuestionLogsForm input[type='radio']:checked").val();
            console.log(questitle);
            $('#QuestionTitle').val(questitle);
            $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'homes', 'action' => 'logs_search')); ?>",
                data: {questitle: questitle, log_type: log_type}, // outer quotes removed
                cache: false,
                beforeSend: function () {
                    $("#loader").fadeIn();
                },
                success: function (response, status) {
                    $("#loader").fadeOut();
                    $('.update_art_speeli').html(response);
                },
            });

        });
        $('.block_ip').on('click', function () {

            event.preventDefault();
            var user_ip = $('#BlockUserIp').val();
            console.log(user_ip);
            $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'homes', 'action' => 'blocks')); ?>",
                data: {user_ip: user_ip}, // outer quotes removed
                cache: false,
                success: function (response, status) {
                    $(".success").fadeIn();

                },
            });

        });
        $(document).keypress(function (event) {

            var keycode = (event.keyCode ? event.keyCode : event.which);
            if (keycode == '13') {

                event.preventDefault();
                var questitle = $('#Title').val();
                console.log(questitle);
                $('#QuestionTitle').val(questitle);
                $.ajax({
                    type: "post", // Request method: post, get
                    url: "<?php echo Router::url(array('controller' => 'homes', 'action' => 'logs_search')); ?>",
                    data: {questitle: questitle}, // outer quotes removed
                    cache: false,
                    beforeSend: function () {
                        $("#loader").fadeIn();
                    },
                    success: function (response, status) {
                        $("#loader").fadeOut();
                        $('.update_art_speeli').html(response);
                    },
                });
            }
        });
    });
</script>