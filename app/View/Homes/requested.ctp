<style>
    .expand{
        cursor: pointer;
    }
    .click{
        cursor: pointer;
    }
    .progress{
        display: none;
    }
    hr{
        border: 0;
        height: 1px;
        background-image: -webkit-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -moz-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -ms-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -o-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
    }
    .clear{
        clear: both;
    }
    .question_wrapper{
        margin: 0 auto;
    }
    .float-none{
        float:none;
        margin: 0 auto;
    }
    .hblue{
        background-color: #fff;
        padding: 1em;
        border-radius: 4px;
        -moz-border-radius: 4px;
        -webkit-border-radius: 4px;
        margin-bottom: 25px;
        position: relative;
        line-height: 25px;
        border: 1px solid #e5e5e5;
    }
    .hblue .panel-heading{
        padding: 10px 15px;
        background-color: rgba(245, 245, 245, 0.24);
        border-top: 1px solid #ddd;

        border-bottom-right-radius: 3px;
        border-bottom-left-radius: 3px;
    }
    .list-unstyled li{
        margin-left: 1em;
    }
</style>
<?php
$this->set("meta_description", "Latest Speeli articles");
$this->set("meta_robots", "noindex");
?>
<?php
$this->Paginator->options(array(
    'update' => '.question_update',
    'evalScripts' => true
));
?>
<div class="row">
    <!-- Add the extra clearfix for only the required viewport -->
    <div  class="clearfix visible-xs-block"></div>
    <br class="clear">
    <hr>
    <div class="row ">

        <div class="col-md-8 float-none update_art_speeli  " style="border-left: 1px solid #ddd;">
            <!-- The time line -->
            <div class="hpanel forum-box question_update ">
                <?php
                $i = 1;

                $user = $this->Session->read('user_quesli');
                if ($user['User']['admin'] == 1) {
                    foreach ($questions as $question) {
                        echo $question['Request']['checked'];
                        if ($question['Request']['checked'] == 1) {
                            $art_status = 0;
                            $status = '<span style="color:green" class="fa fa-check-circle">Checked</span>';
                        } else {
                            $art_status = 1;
                            $status = '<span style="color:gray" class="fa fa-remove">Un Checked</span>';
                        }
                        ?>
                        <div class="hpanel hblue">
                            <div class="panel-heading hbuilt">
                                <?php
                                echo $this->Form->input('new', array(
                                    'value' => $question['Request']['title'],
                                    'class' => 'new-cat-value', 'target' => $question['Request']['id'], 'label' => false, 'div' => false));
                                ?>
                                <div class="btn btn-primary update_request "   
                                    target="<?php echo $question['Request']['id'] ?>">
                                    Update
                                </div>
                                <div class="expand click"   art_status="<?php echo $art_status; ?>"
                                     art_id="<?php echo $question['Request']['id'] ?>">
                                    <?php echo $status ?></div>
                            </div>
                            <div style="float:right;" class="panel-footer-status"><span class="fa fa-calendar"><?php echo $question['Request']['created'] ?></span></div>
                            <br class="clear">

                        </div>
                        <?php
                        $i++;
                    }
                }
                ?>
            </div>
        </div><!-- /.col -->
    </div><!-- /.row -->
    <ul class="pagination">
        <?php
        echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
        echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'currentClass' => 'active', 'tag' => 'li', 'first' => 1));
        echo $this->Paginator->next(__('next'), array('tag' => 'li', 'currentClass' => 'disabled'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
        ?>
    </ul>
    <?php echo $this->Js->writeBuffer(); ?>
</div>
<?php if ($user['User']['admin'] == 1) { ?>
    <script type="text/javascript">

        $('body').undelegate('.expand', 'click').delegate('.expand', 'click', function (e) {
            var art_id = $(this).attr('art_id');
            var art_status = $(this).attr('art_status');
            if (art_status == 0) {
                $(this).attr('art_status', 1);
                $(this).html('<span style="color:green" class="fa fa-remove">Un Checked</span>');
            } else {
                $(this).attr('art_status', 0);
                $(this).html('<span style="color:grey" class="fa fa-check-circle">Checked</span>');
                
            }
            $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'homes', 'action' => 'request_check')); ?>",
                data: {art_id: art_id, art_status: art_status}, // outer quotes removed
                cache: false,
                success: function (response, status) {
                },
            });
        });
        $('.update_request').click(function () {
            var data_send = $(this).parent().find('.new-cat-value').val();
            var cat_id = $(this).attr('target');
            console.log(cat_id);
            $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'homes', 'action' => 'request_edit')); ?>",
                data: {data_send: data_send, cat_id: cat_id}, // outer quotes removed
                cache: false,
                success: function (response, status) {

                },
            });
        });
    </script>
<?php } ?>