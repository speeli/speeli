<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
            <div class="x_title">
                
                <br>
                <h4>  <a href="<?php echo Router::url(array('action' => 'ads_add')); ?>">
                        Add new Ads type
                        <span class="fa fa-plus"></span>
                        <small></small>
                    </a> </h4>
            </div>
            <div class="x_content">
                <div class="col-md-12">
                    <div class="settings form">
                        <h3><?php echo $cast[$id]; ?></h3>
                        <table class="table table-striped table-responsive table-hover table-bordered" >
                            <thead>
                                <tr>
                                    <th><?php echo $this->Paginator->sort('title', 'title'); ?>  </th>
                                    <th><?php echo $this->Paginator->sort('image', 'Image'); ?></th>
                                    <th><?php echo $this->Paginator->sort('created', 'Created'); ?></th>
                                    <th><?php echo $this->Paginator->sort('status', 'Status'); ?></th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>                       
                                <?php $count = 0; ?>
                                <?php foreach ($adses as $ads): ?>                
                                    <?php $count ++; ?>
                                    <?php
                                    if ($count % 2): echo '<tr>';
                                    else: echo '<tr class="zebra">'
                                        ?>
                                    <?php endif; ?>
                                <td><?php
                                
                                echo $this->Html->link($ads['Ads']['title'],  url_encode($ads['Ads']['url']), array('escape' => false)); ?></td>
                                
                                <td>
                                    <?php
                                    if (@getimagesize(WWW_ROOT . 'boxes' . DS . $ads['Ads']['image'])) {
                                        $imgurl = $this->webroot . 'boxes' . DS . $ads['Ads']['image'];
                                    } else {
                                        $imgurl = $this->webroot . 'files' . DS . 'logo.png';
                                    }
                                    ?>
                                    <img class="img-thumbnail img-rounded img-responsive" style="max-height: 10em;width: auto;max-width: 20em;" src="<?php echo $imgurl ?>"
                                </td>
                                <td style="text-align: center;"><?php echo $this->Time->niceShort($ads['Ads']['created']); ?></td>
                                <td style="text-align: center;"><?php echo $ads['Ads']['status']; ?></td>
                                <td>
                                    <a href="<?php echo Router::url(array('action' => 'ads_edit', $ads['Ads']['id'])); ?>">
                                        <span class="fa fa-edit"></span>
                                    </a><br/>
                                     <a href="<?php echo Router::url(array('action' => 'ads_list', $ads['Ads']['id'])); ?>">
                                        <span class="fa fa-plus">Add Articles</span>
                                    </a>
                                    <br/>
                                    <?php if ($ads['Ads']['status'] != 0) { ?>
                                        <a href="<?php echo Router::url(array('action' => 'ads_deactivate', $ads['Ads']['id'])); ?>">
                                            <span class="fa fa-ban"></span>
                                        </a>
                                    <?php } else { ?>
                                        <a href="<?php echo Router::url(array('action' => 'ads_activate', $ads['Ads']['id'])); ?>">
                                            <span class="fa fa-check"></span>
                                        </a>
                                    <?php } ?>
                                </td>
                                </tr>
                            <?php endforeach; ?>
                            <?php unset($ads); ?>
                            </tbody>
                        </table>
                        <div class="pagination pagination-large">
                            <ul class="pagination">
                                <?php
                                echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
                                echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'currentClass' => 'active', 'tag' => 'li', 'first' => 1));
                                echo $this->Paginator->next(__('next'), array('tag' => 'li', 'currentClass' => 'disabled'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
                                ?>
                            </ul>
                        </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
