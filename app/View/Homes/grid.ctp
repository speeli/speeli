
<center><h3  style="font-size:2.2em;font-weight: 700;" class="mobile page-header hometitle">
        What do you want to explore today ?
    </h3></center>
<br class="clear">
<?php foreach ($grids as $grid) { ?>
    <div  class="col-md-4 col-xs-12  card green "> 

        <div class="front"> 
            <a href="<?php echo Router::url('/categories/category/' . $grid['MainCategory']['slug']) ?>" >
                <?php echo $grid['MainCategory']['name'] ?>
                <br>
                
            </a>
        </div> 
        <div class="back">
            <a href="<?php echo Router::url('/categories/category/' . $grid['MainCategory']['slug']) ?>" >
                 <?php echo $grid['MainCategory']['name'] ?> <br>
                 Summaries :  <?php echo $grid[0]['co'] ?><br>
                Views : <?php echo $grid[0]['v'] ?>
            </a>
        </div> 
    </div>

<?php } ?>
<div  class="col-md-4 col-xs-12  card green "> 

        <div class="front" class="showmore" > 
            <div style="color:white;" href="#" >
                
                Show More >>
                <br>
                
            </div>
        </div> 
        <div class="back">
            <div href="#" class="showmore" >
                 <br>
                Show More >>
                <br>
                
            </div>
        </div> 
    </div>
<script type="text/javascript">
    $('.showmore').click(function(){
        event.preventDefault();
        $('.card-grid').load('<?php echo $this->Html->url('/Homes/grid/9'); ?>');
    })
    $(function () {
        $(".card").flip({
            trigger: "hover",
            speed: 400
        });
    });
    $(".front").click(function () {
        if ($(this).find("a").length) {
            window.location.href = $(this).find("a:first").attr("href");
        }
    });
    $(".back").click(function () {
        if ($(this).find("a").length) {
            window.location.href = $(this).find("a:first").attr("href");
        }
    });
</script>
<script>
    if (typeof window.orientation !== 'undefined') {
        $('.mobile').hide();
    } else {

    }


</script>