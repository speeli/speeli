<style>
    .share{
        margin: 0 2em;  
    }
    .fb-share-button,.twitter-share-button {
        background: rgb(59, 89, 152);
        text-align: center;
        float: left;
        margin: 0 2em;
        padding: 0.8em 5.2em;
        border-radius: 0.4em;
        transform: scale(1);
        -ms-transform: scale(1);
        -webkit-transform: scale(1);
        -o-transform: scale(1);
        -moz-transform: scale(1);
        width: 12em !important;
        height: 3em !important;
    }
    #twitter-widget-0{
        margin: 0 2em !important;
        width: 12em !important;
        height: 3em !important;
        float: left;
        text-align: center;
        background: rgb(0, 172, 237);
        padding: 0.8em 4em;
    }
    .fb-share-button iframe ,.twitter-share-button iframe{
        width: 12em !important;
        margin-left: -1.4em!important;

    }
    .fb-share-button iframe .pluginSkinLight div.pluginButtonContainer  div.pluginButton .pluginButtonLabel{

        font-size: 1.5em !important;

        text-transform: uppercase !important; 
    }
    .fb-share-button .pluginButtonContainer .pluginButton .pluginButtonLabel{

        font-size: 1.5em !important;

        text-transform: uppercase !important;
    }
    .fb-share-button_inner {
        height: 9px;
        position: relative;
    }
    .fb-share-button .fb-share-button_right {
        float: left;
    }
    /***** New share style *****/

    ul.share-buttons{
        list-style: none;
        padding:0px 15px;
        margin: 0;
        padding: 0px;
        text-align: center;
    }

    ul.share-buttons li{
        display: inline;
    }


    li.speeli-share a{

        margin: 0px ;
        color: #FFF;
        text-align: center;
        font-size: 11px;
        border-radius: 2px;
        padding: 13px 0px;
        width: 40%;
        display: inline-block;

    }


    li.speeli-share.tw-share a{
        background: #00ACED none repeat scroll 0% 0%;
        float: right;
    }

    li.speeli-share.tw-share a:hover, .li.speeli-share.tw-share a:focus {
        background: #21C2FF none repeat scroll 0% 0%;
        text-decoration: none;
    }

    li.speeli-share.fb-share a {
        background: #3B5998 none repeat scroll 0% 0%;
        float: left;
    }

    li.speeli-share.fb-share a:hover, li.speeli-share.fb-share a:focus {
        background: #4C70BA none repeat scroll 0% 0%;
        text-decoration: none;
    }
    ul.share-buttons li span{
        font-family: arial;
        padding: 0 0 0 10px;
        text-transform: uppercase;
        font-size: 12px;
    }

</style>
<style>
    input[type="text"], .login-form-text{
        width:80%;
    }
    .float-none{
        float:none;
        margin: 0 auto;
    }
    .next a{
        float:none !important;
        color:#777;

    }
    #loader{
        display: none;
		text-align: center;
		font-family: arial;
		font-size: 13px;
		color: rgb(164, 164, 164);
		}
    .wrapper{
        margin: 0 auto ;
        padding: 0;
    }
    .panel{
       /* background:url(<?php echo $this->webroot . 'img/box.png' ?>);background-size: 100% 100%;*/
    }

    .panel-body h5{
        margin:1.2em auto;
        color:#63B5B2;
    }
    h5 a,.main_color{
        color:#63B5B2;
    }
    .border-bottom{
        border-bottom: 1px solid rgba(99, 181, 178, 0.81);
        padding-bottom: 0.5em;
    }
	
	#home-boxs {
	padding-top: 50px;
	clear: both;
	}
	
	.box-header{
	color: #5A5A5A;
	font-family: Helvetica Neue;
	}
	
	.panel-heading h5{
	margin: 15px 0px 0px;
    color:#536D7A;
    font-size: 20px;
	}
	
	h5 span::before {
    font-size: 35px !important;
    margin-right: 15px;
	}

	ul.boxs-list {
		margin: 20px 0px 30px;
		padding: 0 20px;
		min-height: 160px;
	}
	ul.boxs-list li {
	color:#6CC !important
	}
	ul.boxs-list li:hover {
		color: #139292 !important;
	}
	ul.boxs-list li a {
	color: #454545;
	font-family: arial;
	}
	ul.boxs-list li a:hover {
	color: #000;
	}

	.next {
    margin-left: 10px;
}

	#home-boxs .next a,
	#home-boxs .prev a,
	a[rel="next"],
	a[rel="prev"]{
	font-size: 13px;
	font-family: arial;
	color: #A4A4A4;
	}	
	
	#home-boxs .next a:hover,
	#home-boxs .prev a:hover,
	a[rel="next"]:hover,
	a[rel="prev"]:hover{
	color: #000;
	}	
	
	#home-boxs .next i,
	#home-boxs .prev i,
	a[rel="next"] i,
	a[rel="prev"] i{
	font-size: 20px;
	position: relative;
	bottom: -4px;
	}
	
	.nopadding{padding:0}
	
	.loadall{
	text-align: right;
	}
	
	.loadall a{
	font-size: 14px;
	text-align: right;
	color: rgb(99, 181, 178);
	font-weight: bold;
	font-family: arial;
	}	
	.loadall a:hover{
	color:#000;
	}	
	
	.ribbon-top {
		left: 0px;
		top: -12px;
		border-radius: 20px;
		position: absolute;
		background-color: rgb(102, 204, 204);
		height: 100px;
		width: 100px;
	}
	.ribbon-bottom {
		bottom: 10px;
		border-radius: 30px;
		background-color: #88e6e6;
		height: 100px;
		width: 100px;
		right: 7px;
		position: absolute;
	}

	.box-container {
		border: 10px solid rgb(102, 204, 204);
		border-radius: 30px;
		background-color: rgb(255, 255, 255);
		position: relative;
	}
	.dash-usr-rank{
		position: relative;
		background-image: url("<?php echo $this->webroot . 'img/speeli-rank.jpg' ?>");
		background-position: center center;
		height: 158px;
		background-repeat: no-repeat;
		width: 379px;
		float: right;
		
	}
	.dash-usr-rank .nxt-rank {
		margin: 0px;
		color: #656160;
		top: 14px;
		font-size: 13px;
		font-weight: bold;
		font-family: arial;
		display: block;
		position: absolute;
		left: 30px;
	}
	
	.dash-usr-rank .wri-wizard {
		margin: 0px;
		color: #655F5E;
		top: 31px;
		font-size: 18px;
		font-weight: bold;
		font-family: arial;
		position: absolute;
		left: 30px;
	}
	
	.dash-usr-rank .usr-points{
	position: absolute;
	top: 55px;
	right: 160px;
	font-weight: bold;
	font-family: arial;
	color: #536D7A;
	}
	
	@media only screen and (max-width:480px){
	
	.panel-heading {
    padding: 10px 0px 10px 10px;
	}
	.back-image{
	margin-top: 46.8px !important
	}

	.dash-usr-rank {
		background-size: 319px;
		width: 319px;
		float: none;
		margin: auto;
	}
	.dash-usr-rank .percircle {
	width: 100px;
	height: 100px;
	font-size: 100px;
	margin: 25px 13px 0px 0px;
	}
	
.dash-usr-rank .nxt-rank {
    top: 20px;
    left: 30px;
}
	
	.dash-usr-rank .wri-wizard {
    top: 35px;
    left: 30px;
	}


	.dash-usr-rank .usr-points {
		top: 57px;
		left: 143px;
		display: block;
		width: auto;
		right: auto;
		}
		
	.dash-usr-rank .percircle > span {
    top: 56%;
    margin-top: -23px;
	font-size: 21px;
	}
	#home-boxs .col-md-4{
	
	}
	.ninja-img {
		width: auto !important;
		max-width: 70%;
	}
	
}
	
</style>
<?php
$this->set("meta_description", "Speeli Dashboard");
?>
<div class="row float-none" style="margin-top: 20px;">
    <div  class="clearfix visible-xs-block col-md-12 col-xs-12 float-none"></div>
    <div  class="col-md-12 col-lg-12 col-xs-12 float-none" >
        <div class="col-md-5">
            <h4 class="box-header">
                We need your help creating and editing Speeli articles to help
                all the people in the world find quick summarized information
            </h4>
        </div>
        <div class="col-md-2">
            <center>
                <img class="ninja-img" style="width: 100%" src="<?php echo$this->webroot . 'img/'.$ranking['image']; ?>">
                <?php echo $ranking['rank'];?>
            </center>
        </div>
        <div class="col-md-5">
            <div class="dash-usr-rank">
			<b class="nxt-rank">Next Rank:</b>
			<b class="wri-wizard"><?php echo $ranking['next'];?></b>
                <?php 
				
                echo "<b class='usr-points'>".$ranking['remain_dash']."</b>";

                ?>
                <?php 
				
                echo "<div id='orangecircle' data-percent='".$ranking['percent']."' class='green' ></div>";

                ?> 
			
			
            </div>

            </div>
			

			
        </div>
    </div>
	
		<!--

    <div id="searchbox" class="row question">
        <div class="col-md-12 col-xs-12">    
            <?php
            echo $this->Form->create('Question');
            ?>
            <?php
            echo $this->Form->input('title', array('id' => 'Title', 'placeholder' => 'Search Speeli summaries .. . ', 'class' => 'flex-column',
                'div' => false, 'label' => false));
            ?>
            <div class="check_status  btn btn-main"><i class="fa fa-search"></i></div></h1>
        </div>
        <br class="clear">
        <div class="input-cat">

        </div>
        <br class="clear">

    </div>
-->
    <div id="home-boxs" class="col-md-12 col-lg-12 float-none col-xs-12">
        <div class="col-md-4 col-lg-4  col-xs-12">
            <div class="panel panel-expand_article">
			<span class="ribbon-top"></span>
			<span class="ribbon-bottom"></span>
            <div class="box-container">
			
                <div class="panel-heading">
                    <h5><span class="fa fa-arrows"></span><b class="border-bottom"> Expand Articles </b></h5>
                </div>
                <div class="panel-body expand_article">
		<ul class="boxs-list">
                    <?php
                    for ($q = 0; $q < count($questions); $q++) {
                        if (!empty($questions[$q]['Question']['title_update'])) {
                            $explode_title = explode(',,,', $questions[$q]['Question']['title_update']);
                            $last = end($explode_title);
                            $title = end(unserialize(base64_decode($last)));
                        } else {
                            $title = $questions[$q]['Question']['title'];
                        }
                        ?> 
                        <li>
                            <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $questions[$q]['Question']['slug'])); ?>"> <?php
                                $title = str_replace('&nbsp;', ' ', $title);
                                $title = str_replace('nbsp', ' ', $title);
                                $title = str_replace('%', ' ', $title);
                                echo htmlspecialchars($title);
                                ?>
                            </a> 
                        </li>
                    <?php } ?>
					</ul>
                </div>
                <div id="loader" class="col-md-3 col-xs-3 float-none" >
                    <cetner><img src="<?php echo $this->webroot . 'files/8.gif'; ?>">
                        Loading
                    </cetner>
                </div>
                <div class="panel-footer">
                    <center>
                        <h5>Read Expand articles <a href="">Guidlines</a></h5>
                    </center>
                </div>
                <br class="clear">
            </div>
            </div>
			
        </div>
        <div class="col-md-4 col-lg-4  col-xs-12">
            <div class="panel panel-request_article" >
			<span class="ribbon-top"></span>
			<span class="ribbon-bottom"></span>

            <div class="box-container">
                <div class="panel-heading">
                    <h5><span class="fa fa-users"></span><b class="border-bottom"> Requested articles </b></h5>

                </div>
                <div class="panel-body request_article">
				
						<ul class="boxs-list">
                    <?php
                    for ($q = 0; $q < count($questions); $q++) {
                        if (!empty($questions[$q]['Question']['title_update'])) {
                            $explode_title = explode(',,,', $questions[$q]['Question']['title_update']);
                            $last = end($explode_title);
                            $title = end(unserialize(base64_decode($last)));
                        } else {
                            $title = $questions[$q]['Question']['title'];
                        }
                        ?> 
                        <li> <a target="blank" href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'add', $questions[$q]['Question']['slug'])); ?>"> <?php
                                $title = str_replace('&nbsp;', ' ', $title);
                                $title = str_replace('nbsp', ' ', $title);
                                $title = str_replace('%', ' ', $title);
                                echo htmlspecialchars($title);
                                ?>
                            </a> 
                        </li>
                    <?php } ?>
						</ul>

                </div>
                <div id="loader" class="col-md-3 col-xs-3 float-none" >
                    <cetner><img src="<?php echo $this->webroot . 'files/8.gif'; ?>">
                        Loading
                    </cetner>
                </div> 
                <div class="panel-footer">
                    <center>
                        <h5>Read Request articles <a href="">Guidlines</a></h5>
                    </center>
                </div>
                <br class="clear">
            </div>
            </div>
			
        </div>
        <div class="col-md-4 col-lg-4  col-xs-12">
            <div class="panel panel-edit_article">
			<span class="ribbon-top"></span>
			<span class="ribbon-bottom"></span>

            <div class="box-container">
                <div class="panel-heading">
                    <h5><span class="fa fa-edit"></span><b class="border-bottom"> Edit articles </b></h5>
                </div>
                <div class="panel-body edit_article">
						<ul class="boxs-list">

                    <?php
                    for ($q = 0; $q < count($questions); $q++) {
                        if (!empty($questions[$q]['Question']['title_update'])) {
                            $explode_title = explode(',,,', $questions[$q]['Question']['title_update']);
                            $last = end($explode_title);
                            $title = end(unserialize(base64_decode($last)));
                        } else {
                            $title = $questions[$q]['Question']['title'];
                        }
                        ?> 
                        <li> <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'edit', $questions[$q]['Question']['slug'])); ?>"> <?php
                                $title = str_replace('&nbsp;', ' ', $title);
                                $title = str_replace('nbsp', ' ', $title);
                                $title = str_replace('%', ' ', $title);
                                echo htmlspecialchars($title);
                                ?></a> 
                        </li>
                    <?php } ?>
                        </ul>
					
                </div>
                <div id="loader" class="col-md-3 col-xs-3 float-none" >
                    <cetner><img src="<?php echo $this->webroot . 'files/8.gif'; ?>">
                        Loading
                    </cetner>
                </div> 
                <div class="panel-footer">
                    <center>
                        <h5>Read Edit articles <a href="">Guidlines</a></h5>
                    </center>
                </div>
                <br class="clear">
            </div>
            </div>

        </div>
    </div><div class="clearfix"></div>
</div>

    <script type="text/javascript" src="<?php echo $this->webroot . 'js/percircle.js' ?>"></script>
	    <link rel="stylesheet" href="<?php echo $this->webroot . 'css/percircle.css' ?>">

    <script type="text/javascript">
        $(function(){
            $("[id$='circle']").percircle();
        });
    </script>

<script>

    $(document).keypress(function (event) {

        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {

            event.preventDefault();
            var questitle = $('#Title').val();
            $('#QuestionTitle').val(questitle);
            console.log(questitle);
            $.ajax({
                type: "get", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'homes', 'action' => 'search')); ?>",
                data: {questitle: questitle}, // outer quotes removed
                cache: false,
                beforeSend: function () {
                    $("#loader").fadeIn();
                },
                success: function (response, status) {
                    $("#loader").fadeOut();
                    $('.input-cat').html(response);
                },
            });
        }
    });


    $('.check_status').on('click', function () {

        event.preventDefault();
        var questitle = $('#Title').val();
        $('#QuestionTitle').val(questitle);
        $.ajax({
            type: "get", // Request method: post, get
            url: "<?php echo Router::url(array('controller' => 'homes', 'action' => 'search')); ?>",
            data: {questitle: questitle}, // outer quotes removed
            cache: false,
            beforeSend: function () {
                $("#loader").fadeIn();
            },
            success: function (response, status) {
                $("#loader").fadeOut();
                $('.input-cat').html(response);
            },
        });

    });
</script>
<script>
    $(document).ready(function () {
        $('.request_article').load('<?php echo $this->Html->url('/Homes/requested_article'); ?>');
        $('.expand_article').load('<?php echo $this->Html->url('/Homes/expanded_article'); ?>');
        $('.edit_article').load('<?php echo $this->Html->url('/Homes/edit_article'); ?>');
    });
    $(document).ready(function () {
        resizeContent();
        $(window).resize(function () {
            resizeContent();
        });
    });
    function resizeContent() {
        $height = $(window).height();
        $width = $(window).width();
        $height_tlt = $(window).height() / 3;
        $height_nav = $('.navbar').height()/* + 15*/;
       /* $('.back-image').height($height - $height_nav);
        $('.back-image').width($width);*/
        $('.back-image').css({'margin-top': $height_nav});

    }
</script>

