
<center><h3  style="font-size:2.2em;font-weight: 700;" class="mobile page-header hometitle">
        What do you want to explore today ?
    </h3></center>
<br class="clear">
<?php foreach ($grids as $grid) { ?>
    <div  class="col-md-4 col-xs-12  card green "> 

        <div class="front"> 
            <a href="<?php echo Router::url('/summary/' . $grid['Summary']['slug']) ?>" >
                <?php echo $grid['Summary']['name'] ?>
                <br>
                <i style='font-size: 1.2em; 'class="fa <?php echo $grid['Summary']['icons'] ?>"></i>
            </a>
        </div> 
        <div class="back">
            <a href="<?php echo Router::url('/summary/' . $grid['Summary']['slug']) ?>" >
                Total <?php echo $grid['Summary']['name'] ?>  <?php echo $grid[0]['co'] ?><br>
                Total Number of Views <?php echo $grid[0]['v'] ?>
            </a>
        </div> 
    </div>

<?php } ?>
<script type="text/javascript">
    $(function () {
        $(".card").flip({
            trigger: "hover",
            speed: 400
        });
    });
    $(".front").click(function () {
        if ($(this).find("a").length) {
            window.location.href = $(this).find("a:first").attr("href");
        }
    });
    $(".back").click(function () {
        if ($(this).find("a").length) {
            window.location.href = $(this).find("a:first").attr("href");
        }
    });
</script>
<script>
    if (typeof window.orientation !== 'undefined') {
        $('.mobile').hide();
    } else {

    }


</script>