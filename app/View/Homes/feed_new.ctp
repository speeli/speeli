<?php if ($admin == true) {
    ?>
    <style>
        .category_top{
            background: #63b5b2;
            position: absolute;
            padding: 0.5em;
            color: white;
            border-bottom-right-radius: 1em;
            border: 1px solid;
            font-weight: bold;
        }
        .category_top a {
            color: white;
        }
        .thumbnail{
            padding: 0;
        }
        .panel-footer h3{

            color:black !important;
        }
        .panel-footer{
            background: none !important;
        }

    </style>
<?php } ?>
<style>

    #loader{
        display:none;
    }

    .home-thumb{
        padding: 0;
    }
    .panel-footer{
        padding: 0;
        background-color: rgba(141, 205, 203, 0.82);
    }
    .page-header{
        margin-top: 0;
        float: none;
    }
    .middle-line{
        width: 100%;
        background-color: rgba(99, 181, 178, 0.78);
        left: 0;
        top: 17em;
        margin-left: -7em;
        -webkit-box-shadow: 0 2px 7px rgba(0, 0, 0, 0.25);
        box-shadow: 0 2px 7px rgba(0, 0, 0, 0.25);
        -ms-filter: "progid:DXImageTransform.Microsoft.Shadow(Strength=3,Direction=180,Color='#333333')";
        filter: progid:DXImageTransform.Microsoft.Shadow(Strength=3, Direction=180, Color='#333333');
        margin: 0 auto;
    }
    .forum-box{
        margin: 1em;
    }
    .progress{
        display: none;
    }
    .panel-footer,.panel-heading{
        text-align: center;
    }
    .panel-footer h3{
        margin-top: 0;
        padding-top: 0;

    }
    hr{
        border: 0;
        height: 1px;
        background-image: -webkit-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -moz-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -ms-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -o-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
    }
    .clear{
        clear: both;
    }
    .question_wrapper{
        margin: 0 auto;
    }
    .float-none{
        float:none;
        margin: 0 auto;
        clear:both;
    }
    .hblue{
        background-color: #fff;
        padding: 1em;
        border-radius: 4px;
        -moz-border-radius: 4px;
        -webkit-border-radius: 4px;
        margin-bottom: 25px;
        position: relative;
        line-height: 25px;
        border: 1px solid #e5e5e5;
    }
    .hblue .panel-heading{
        padding: 10px 15px;
        background-color: rgba(245, 245, 245, 0.24);

        border-bottom-right-radius: 3px;
        border-bottom-left-radius: 3px;
    }
    .list-unstyled li{
        margin-left: 1em;
    }
    .panel-footer h3 {
        padding-top: 0.3em;
    }

    #Title {
        margin: auto;
    }
    #searchbox .check_status.btn.btn-main{
        margin-left: 5px;
    }

    input[type="text"], .login-form-text {
        width: 90%;
    }
    @media only screen and (max-width: 768px){
        #searchbox{
            margin-top: 3em;
        }
        .signup{
            font-size: 1.1em;
        }
        .fa-toggle-menu {
            display: none !important;
        }
        .panel-footer h3{
            font-size: 1.1em;
            text-align: center;
        }
        .panel-heading h3{
            font-size: 0.9em;
            text-align: left;
            padding: 0;
            margin: 0;
        }
        .middle-line{
            top:9em;
        }


    }


    @media only screen and (max-width: 560px){

        #searchbox input[type="text"], .login-form-text
        {
            width: 78%;

        }
        #searchbox .check_status.btn.btn-main{
            width:14%
        }


    }
 body{overflow-x:hidden;}
    .card {

        height:13em;
        padding: 0;
    }
    .wrapper{
        margin: 0;
        padding: 0;
        width: 100%;
    }
section{
        position: relative;
        background-position: center top;
        background-size: cover;
    }
    .fade{
        width: 100%;
        height: 100%;
        opacity:0.15;
        position: absolute;
        background: rgb(99, 181, 178);
    }



</style>

<?php
$this->set("meta_description", "Latest Speeli articles");
$this->set("meta_robots", "noindex");
?>
<?php
echo $this->Html->css(array('blog'));
?>
<?php
echo $this->Html->script(array('typed'));
?>
<section class="half_height" style="background-image: url(<?php echo $this->webroot . 'img/directory7.png' ?>)">
    <div class="fade"></div>
    <center><h3  style="font-size:2.2em;font-weight: 700;color:white;margin-top: 8em;" class="mobile page-header hometitle">
            What do you want to explore today ?
        </h3></center>  
</section>
<div class="clear clearfix"></div>
<script>
  $(function(){
      $(".page-header").typed({
        strings: ["What do you want to explore today ?"],
       
      });
  });
</script>
<script>
    $('.half_height').height(($(window).height())/2);
    
    $('section').width(document.body.clientWidth);
    $('.half_height').css({'margin-top': ($('.navbar-fixed-top').height()) + 10});
    if (typeof window.orientation !== 'undefined') {
        $('.mobile').hide();
    } else {

    }
</script>
<div class="row">
    <!-- Add the extra clearfix for only the required viewport -->
    <div  class="clearfix visible-xs-block"></div>

    <div  class="clearfix visible-xs-block col-md-12 col-xs-12 float-none"></div>


    <div class="middle-line">
        <nav class="pi-navigation">
            <div class="container">
                <ul style="height:auto" class="navlist">
                    <?php foreach ($main_categories as $key => $main_category) { ?>
                        <li>
                            <a  cat="<?php echo trim($key); ?>" href="<?php echo Router::url(array('controller' => 'categories', 'action' => 'category', trim($key))); ?>">
                                <?php echo $main_category ?>
                            </a>
                        </li>
                    <?php } ?>

                </ul>
            </div>
        </nav> 
        
    </div>



    <article class="blog-content blog-grid">
        <div class="container">
            <div class="content">
                <div class="main section" id="main">

                    <div class="widget Blog" data-version="1" id="Blog1">

                        <div class="post-wrapper" style="position: relative; height: 1965px;">

                            <?php
                            $i = 0;

                            foreach ($questions as $question) {
                                if (!empty($question['Question']['image'])) {


                                    if (!empty($question['Question']['last'])) {
                                        $title = $question['Question']['last'];
                                    } else {
                                        if (!empty($question['Question']['title_update'])) {
                                            $explode_title = explode(',,,', $question['Question']['title_update']);
                                            $last = end($explode_title);
                                            $title = end(unserialize(base64_decode($last)));
                                        } else {
                                            $title = $question['Question']['title'];
                                        }
                                    }
                                    ?>
                                    <div class="grid-item col-md-4 col-xs-12" >

                                        <div class="post hentry" itemprop="blogPost" itemscope="itemscope" itemtype="http://schema.org/BlogPosting">
                                            <div class="entry-content" id="post-body-202452633544924310" itemprop="description articleBody">
                                                <div class="post-media">
                                                    <div class="image-wrap">
                                                        <div class="mask"></div>
                                                        <img width="360" height="180" alt="<?php echo $title; ?>" src="<?php echo $this->webroot . "question/" . $question['Question']['id'] . "/" . $question['Question']['image']; ?>">
                                                    </div>

                                                </div>
                                                <div class="post-meta">
                                                    <div class="post-date">
                                                        <span class="year">2015</span>
                                                        <span class="month">Mar</span>
                                                        <span class="day">04</span></div>
                                                    <div class="post-comment">
                                                        <i class="fa fa-eye"></i>
                                                        <a href="http://gridz-themexpose.blogspot.com.eg/2015/03/papilion-minter-savior.html#comments">
                                                            <?php echo $question['Question']['views'] ?> views
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="post-body">
                                                    <div class="post-author">
                                                        <div class="image-thumb">
                                                            <?php
                                                            if ($question['User']['image'] == "") {
                                                                $gender = ($question['User']['gender'] == 'f' || $question['User']['gender'] == 'female') ? 'female' : 'male';
                                                                ?>
                                                                <img   src="<?php echo $this->webroot . 'user' . '/' . $gender . '.png'; ?>" alt="..." class="img-responsive">
                                                            <?php } else { ?>
                                                                <img  src="<?php echo $this->webroot . 'user' . '/' . $question['User']['image']; ?>" alt="<?php echo $question['User']['username'] ?>" class="img-responsive">
                                                            <?php } ?>

                                                        </div>
                                                        <div class="name-author">
                                                            <cite>
                                                                <?php if (!empty($question['User']['username'])) { ?>
                                                                    <a alt="<?php echo $question['User']['username'] ?>" href="<?php echo Router::url(array('controller' => 'users', 'action' => 'view', $question['User']['id'])); ?>">
                                                                        <?php echo $question['User']['username'] ?>
                                                                    </a>
                                                                <?php
                                                                } else {
                                                                    echo "Annonymus";
                                                                }
                                                                ?>
                                                            </cite>
                                                        </div>
                                                    </div>
                                                    <div class="post-title">
                                                        <h2><span>
                                                                <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $question['Question']['slug'])); ?>">
        <?php echo $title; ?>
                                                                </a></span></h2>
                                                    </div>
                                                    <div class="post-entry">
                                                        <p><?php
                                                            if (!empty($question['Answer']['last_body'])) {
                                                                $body = $question['Answer']['last_body'];
                                                                $body = trim($body);
                                                            } else {
                                                                if (!empty($question['Answer']['body_update'])) {
                                                                    $explode_body = explode(',,,', $question['Answer']['body_update']);
                                                                    $last = end($explode_body);
                                                                    $body = end(unserialize(base64_decode($last)));
                                                                    $body = trim($body);
                                                                } else {
                                                                    $body = $question['Answer']['body'];
                                                                    $body = trim($body);
                                                                }
                                                            }
                                                            $pos = strpos($body, ' ', 100);
                                                            if ($pos) {
                                                                echo substr($body, 0, $pos);
                                                            } else {
                                                                echo $body;
                                                            }
                                                            ?></p>
                                                    </div>
                                                    <div class="traingle"></div>
                                                    <div class="postfooter clearfix">
                                                        <div class="socialpost">
                                                            <div class="icons clearfix">
                                                                <a href="http://twitter.com/share?url=http://gridz-themexpose.blogspot.com.eg/2015/03/papilion-minter-savior.html" target="_blank"><i class="fa fa-twitter"></i>
                                                                    <div class="texts">Twitter</div>
                                                                </a>
                                                                <a href="http://www.facebook.com/sharer.php?u=http://gridz-themexpose.blogspot.com.eg/2015/03/papilion-minter-savior.html" target="_blank"><i class="fa fa-facebook"></i>
                                                                    <div class="texts">Facebook</div>
                                                                </a>
                                                                <a href="https://plus.google.com/share?url=http://gridz-themexpose.blogspot.com.eg/2015/03/papilion-minter-savior.html" target="_blank"><i class="fa fa-google-plus"></i>
                                                                    <div class="texts">Google+</div>
                                                                </a>
                                                                <a href="http://pinterest.com/pin/create/button/?source_url=http://gridz-themexpose.blogspot.com.eg/2015/03/papilion-minter-savior.html" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');
                                                                                    return false
                                                                                ;" target="_blank"><i class="fa fa-pinterest"></i><div class="texts">Pinterest</div></a>
                                                            </div>
                                                        </div>
                                                        <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $question['Question']['slug'])); ?>">
                                                            <div class="read">
                                                                Continue... </div>
                                                        </a>
                                                    </div>

                                                </div>
                                                <div style="clear: both;"></div>
                                            </div>
                                        </div>

                                    </div>

                                    <?php
                                    $i++;
                                }
                            }
                            ?>

                        </div>

                        <div class="blog-pager" id="blog-pager"><div class="showpageArea"><span class="showpagePoint">1</span><span class="showpageNum"><a href="/search?updated-max=2014-02-08T11%3A36%3A00-08%3A00&amp;max-results=9">2</a></span><span class="showpage"> <a href="/search?updated-max=2014-02-08T11%3A36%3A00-08%3A00&amp;max-results=9"><i class="fa fa-angle-right"></i></a></span></div></div>
                        <div class="clear"></div>

                    </div>

                </div>
            </div>    
        </div>
    </article>




   
</div>

<?php
echo $this->Html->script(array('jquery.jnotify.min'));
?>
<?php
$message = $this->Session->read('user_quesli_message');
if ($message) {
    ?>
    <script>
            
            var jNotify = $.JNotify({
            'className': 'JNotify-danger',
            // warning, info, success or error
                
            'theme': 'info',
            'backgroundColor': 'rgba(217, 237, 247, 0.1)',
            'borderColor': '#BCE8F1',
            'borderRadius': '3px',
            'position': 'center',
            'maxWidth': '250px',
            'top': 30,
            'zIndex': 9999,
            'height': null,
            'padding': '15px',
                    'message': '<?php echo $message; ?>',
                    'fontSize': '14px',
                    'fontColor': '#31708F',
                    'autoClose': false,
                    'showCloseButton': true,
                    'showDuration': 5000,
                'closeDuration': 1000
        });    </script>
<?php } ?>
<script>
    
    $(document).keypress(function (event) {
        
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            
            event.preventDefault();
            var questitle = $('#Title').val();
            $('#QuestionTitle').val(questitle);
            console.log(questitle);
                $.ajax({
                type: "get", // Request method: post, get
                                url: "<?php echo Router::url(array('controller' => 'homes', 'action' => 'search')); ?>",
                                data: {questitle: questitle}, // outer quotes removed
                                cache: false,
                                    beforeSend: function () {
                                $("#loader").fadeIn();
                                },
                                    success: function (response, status) {
                                    $("#loader").fadeOut();
                                $('.input-cat').html(response);
                            },
                        });
                    }
    });

                    
    $('.check_status').on('click', function () {
                        
                        event.preventDefault();
                        var questitle = $('#Title').val();
                        $('#QuestionTitle').val(questitle);
                            $.ajax({
                            type: "get", // Request method: post, get
                        url: "<?php echo Router::url(array('controller' => 'homes', 'action' => 'search')); ?>",
                        data: {questitle: questitle}, // outer quotes removed
                        cache: false,
                            beforeSend: function () {
                        $("#loader").fadeIn();
                        },
                            success: function (response, status) {
                            $("#loader").fadeOut();
                        $('.input-cat').html(response);
                    },
        });
                
    });


</script>