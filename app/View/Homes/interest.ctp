<style>
    .home-thumb{
        padding: 0;
    }
    .page-header{
        margin-top: 0;
        float: none;
    }
    .image-title{
        position: absolute;
        bottom: 0.5em;
          background-color: rgba(252, 245, 245, 0.59);
  color: rgb(74, 72, 72);
  display: none;
    }
    .thumbnail:hover .image-title{
        display: block
    }
    .forum-box{
        margin: 1em;
    }
    .progress{
        display: none;
    }
    hr{
        border: 0;
        height: 1px;
        background-image: -webkit-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -moz-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -ms-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -o-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
    }
    .clear{
        clear: both;
    }
    .question_wrapper{
        margin: 0 auto;
    }
    .float-none{
        float:none;
        margin: 0 auto;
    }
    .hblue{
        background-color: #fff;
        padding: 1em;
        border-radius: 4px;
        -moz-border-radius: 4px;
        -webkit-border-radius: 4px;
        margin-bottom: 25px;
        position: relative;
        line-height: 25px;
        border: 1px solid #e5e5e5;
    }
    .hblue .panel-heading{
        padding: 10px 15px;
        background-color: rgba(245, 245, 245, 0.24);
        border-top: 1px solid #ddd;

        border-bottom-right-radius: 3px;
        border-bottom-left-radius: 3px;
    }
    .list-unstyled li{
        margin-left: 1em;
    }
</style>
<?php
$this->set("meta_description", "Latest Speeli articles");
$this->set("meta_robots", "noindex");
?>
<div class="row">


    <!-- Add the extra clearfix for only the required viewport -->
    <div  class="clearfix visible-xs-block"></div>
    <br class="clear"><br class="clear">

    <?php if (isset($questions) && !empty($questions)) { ?>
        <div class="selected-speeli col-md-9 float-none">
           
            <center><h3 class="page-header">
                    Selected speeli articles
                </h3></center>
            <br class="clear">
            <?php 
            $i=0;
            foreach ($questions as $question) {
                
                if($question['Question']['image']!=NULL){
                   if($i<2){ ?>
                         <div class="col-md-6 home-thumb">
                    <div class="thumbnail">
                        <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $question['Question']['slug'])) ?>">
                            <img style="width:100%; height: 200px;" class="img-responsive" src="<?php echo $this->webroot . "question/" . $question['Question']['id'] . "/" . $question['Question']['image']; ?>">
                            <h3 class="image-title"><?php echo $question['Question']['title']?></h3>
                        </a>
                    </div>    
                </div>
                  <?php  }else{ ?>
                         <div class="col-md-4 home-thumb">
                    <div class="thumbnail">
                        <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $question['Question']['slug'])) ?>">
                            <img style="width:100%; height: 200px;" class="img-responsive" src="<?php echo $this->webroot . "question/" . $question['Question']['id'] . "/" . $question['Question']['image']; ?>">
                            <h3 class="image-title"><?php echo $question['Question']['title']?></h3>
                        </a>
                    </div>   
                </div>
                   <?php } ?>
            <?php 
            $i++;
                } } ?>
        </div>
    <?php } ?>
    <div class="row ">

        <div class="col-md-5 col-xs-6" >
            <div class="thumbnail">
                <!-- The time line -->
                <div class="hpanel forum-box">
                    <center>  <h4 class="page-header">New Speeli articles</h4></center>
                    <?php
                    $i = 1;
                    $user = $this->Session->read('user_quesli');
                    foreach ($questions as $question) {
                        if ($question['Question']['status'] == 1) {
                            ?>
                            <div class="hpanel hblue">
                                <div class="panel-heading hbuilt">
                                    <h3> <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $question['Question']['slug'])); ?>"> <?php echo htmlspecialchars($question['Question']['title']); ?></a>  </h3>

                                </div>


                            </div>
                            <?php
                            $i++;
                        }
                    }
                    ?>
                    <!-- timeline time label -->
                    <!-- END timeline item -->
                    <!-- timeline item -->
                </div>
              
            </div>
        </div><!-- /.col -->
        <div class="col-md-5 col-xs- right" >
            <div class="thumbnail">
                <!-- The time line -->
                <div class="hpanel forum-box" >
                       <center>  <h4 class="page-header ">Expand this article</h4>    </center> 
                    <?php
                    $i = 1;
                    $user = $this->Session->read('user_quesli');
                    foreach ($expands as $question) {
                        if ($question['Question']['status'] == 1) {
                            ?>
                            <div class="hpanel hblue">
                                <div class="panel-heading hbuilt">
                                    <h3> <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $question['Question']['slug'])); ?>"> <?php echo htmlspecialchars($question['Question']['title']); ?></a>  </h3>

                                </div>


                            </div>
                            <?php
                            $i++;
                        }
                    }
                    ?>
                    <!-- timeline time label -->
                    <!-- END timeline item -->
                    <!-- timeline item -->
                </div>
              
            </div>
        </div><!-- /.col -->
    </div><!-- /.row -->
</div>
<script>
$(document).ready(function(){
    console.log('dsa');
    $('.thumbnail').focusin(function(){
       
          
    });
   
    
})
</script>