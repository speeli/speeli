<?php 
//$this->set("meta_robots", "index");
$this->set("meta_description", 'Terms Of Services for Speeli.com');
$this->set("page_title",  "Terms Of Services for Speeli.com");
?>
<style>
    h2{color:#888; width: 100%;text-align: center;}
    h3,h4{ color: #63b5b2;
           font-weight: bold;
           
           display: block;
          }
    .panel-body ul{font-size: 1.1em;}
    p,.panel-body{
        font-size: 1.1em;
        line-height: 1.67em;
    }
     @media only screen and (max-width: 480px){
         h2{margin-top: 1.7em}
        h3,h4 {
          text-align: center;
          
        }
    }
    li{list-style: inherit}
</style>
<div class="row">
    <div class="col-md-10 float-none">
        <div class="panel">
            <div class="panel-body">
                
                <h2>    Terms of Service</h2>
These Terms of Service ("Terms") govern your access to and use of the services, websites, and applications offered by <b style='color:#3BC8C2;'> Speeli </b>, Inc.  And define your responsibilities and liabilities.
 Your access to and use of the Service <b style='color:#3BC8C2;'> Speeli </b> is conditioned on your acceptance of and compliance with these Terms. By accessing or using the Service you agree to be bound by these Terms.
<h3> Use of the Service </h3> 
You may use the Service only if you can form a binding contract with <b style='color:#3BC8C2;'> Speeli </b>, and only in compliance with these Terms and all applicable local, state, national, and international laws, rules and regulations. 
<br/>
We may, without prior notice, change the Service; stop providing the Service or features of the Service, to you or to users generally; or create usage limits for the Service.
<br/>
You are responsible for safeguarding the password that you use to access the Service and for any activities or actions under your password. We encourage you to use "strong" passwords (that use a combination of upper and lower case letters, numbers and symbols) with your account. <b style='color:#3BC8C2;'> Speeli </b> will not be liable for any loss or damage arising from your failure to comply with this instruction.
<br/>
You represent that you are over the age of thirteen (13) years old (or over the age of fourteen (14) years old if you live in Spain or South Korea)(the “Minimum Age”). If you are over the Minimum Age, but under the legal age of majority, your parent or legal guardian must consent to this Terms of Service and Privacy Policy, and affirm that they accept this Agreement on behalf of, and bear all legal and financial responsibility and liability for the actions of, any child between the Minimum Age and majority, and expressly ratify and confirm any acts of any such child and all users of the account.
<br/>
<h3> User Content</h3> 
"Content" means any information, text, graphics, or other materials uploaded, downloaded or appearing on the Service.
<br/>
To grow the commons of free knowledge and free culture, all users contributing to the Projects are required to grant broad permissions to the general public to re-distribute and re-use their contributions freely, so long as that use is properly attributed and the same freedom to re-use and re-distribute is granted to any derivative works. In keeping with our goal of providing free information to the widest possible audience, we require that when necessary all submitted content be licensed so that it is freely reusable by anyone who cares to access it. 
<br/>
You <b> retain ownership of all Content you submit, post, display, or otherwise make available on the Service.</b>
<br/>
<h3> Your License to <b style='color:#3BC8C2;'> Speeli </b></h3> 
By submitting, posting or displaying Content on or through the Service, you grant us a worldwide, non-exclusive, royalty-free license (with the right to sublicense) to use, copy, reproduce, process, adapt, modify, create derivative works from, publish, transmit, display and distribute such Content in any and all media or distribution methods (now known or later developed).
<br/>
You agree that this license includes the right for other users of the Service to modify your Content, and for <b style='color:#3BC8C2;'> Speeli </b> to make your Content available to others for the publication, distribution, syndication, or broadcast of such Content on other media and services, subject to our terms and conditions for such Content use. Such additional uses by <b style='color:#3BC8C2;'> Speeli </b> or others may be made with no compensation paid to you with respect to the Content that you submit, post, transmit or otherwise make available through the Service.
<br/>
We may modify, adapt, or create derivative works from your Content in order to transmit, display or distribute it over computer networks and in various media and/or make changes to your Content as are necessary to conform and adapt that Content to any requirements or limitations of any networks, devices, services or media.
<br/>
Additionally, by submitting or uploading content to the site, you warrant, represent and agree that you have the right to grant <b style='color:#3BC8C2;'> Speeli </b> the license described above. You also represent, warrant and agree that you have not and will not contribute any Content that <ul><li>(a) infringes, violates or otherwise interferes with any copyright or trademark of another party.</li><li> (b) reveals any trade secret, unless the trade secret belongs to you or you have the owner's permission to disclose it</li><li> (c) infringes any intellectual property right of another or the privacy or publicity rights of another</li><li> (d) is libelous, defamatory, abusive, threatening, harassing, hateful, offensive or otherwise violates any law or right of any third party</li><li> (e) creates an impression that you know is incorrect, misleading, or deceptive, including by impersonating others or otherwise misrepresenting your affiliation with a person or entity; </li><li>(f) contains other people's private or personal information without their express authorization and permission, and/or</li><li> (g) contains or links to a virus, trojan horse, worm, time bomb or other computer programming routine or engine that is intended to damage, detrimentally interfere with, surreptitiously intercept or expropriate any system, data or information. <b style='color:#3BC8C2;'> Speeli </b> reserves the right in its discretion to remove any Content from the Site, suspend or terminate your account at any time, or pursue any other remedy or relief available under equity or law
</ul>.
<br/>
You must not defame, stalk, bully, abuse, harass, threaten, impersonate or intimidate people or entities and you must not post private or confidential information via the Service, including, without limitation, your or any other person's credit card information, social security or alternate national identity numbers, non-public phone numbers or non-public email addresses.
<br/>
You are solely responsible for your conduct and any data, text, files, information, usernames, images, graphics, photos, profiles, audio and video clips, sounds, musical works, works of authorship, applications, links and other content or materials (collectively, "Content") that you submit, post or display on or via the Service.
<br/>
You must not interfere or disrupt the Service or servers or networks connected to the Service, including by transmitting any worms, viruses, spyware, malware or any other code of a destructive or disruptive nature. You may not inject content or code or otherwise alter or interfere with the way any <b style='color:#3BC8C2;'> Speeli </b> page is rendered or displayed in a user's browser or device.
<br/>
You may not upload or submit violent, nude, partially nude, discriminatory, unlawful, infringing, pornographic or sexually suggestive photos or other content via the Service.
<br/>

 
<h3> <b style='color:#3BC8C2;'> Speeli </b>'s Licenses to You</h3> 
Your Content will be viewable by other users of the Service and through third party services and websites. You should only provide Content that you are comfortable sharing with others under these Terms.
<br/>
All Content, whether publicly posted or privately transmitted, is the sole responsibility of the person who originated such Content. We may not monitor or control the Content posted via the Service. Any use of or reliance on any Content or materials posted via the Service or obtained by you through the Service is at your own risk. 
<br/>
We do not endorse, support, represent or guarantee the completeness, truthfulness, accuracy, or reliability of any Content or communications posted via the Service or endorse any opinions expressed via the Service. You understand that by using the Service, you may be exposed to Content that might be offensive, harmful, inaccurate or otherwise inappropriate. Under no circumstances will <b style='color:#3BC8C2;'> Speeli </b> be liable in any way for any Content, including, but not limited to, any errors or omissions in any Content, or any loss or damage of any kind incurred as a result of the use of any Content made available via the Service or broadcast elsewhere.
<br/>
You are responsible for your use of the Service, for any Content you provide, and for any consequences thereof, including the use of your Content by other users and third party partners. You understand that your Content may be republished and if you do not have the right to submit Content for such use, it may subject you to liability. <b style='color:#3BC8C2;'> Speeli </b> will not be responsible or liable for any use of your Content by <b style='color:#3BC8C2;'> Speeli </b> in accordance with these Terms.
<br/>
We reserve the right at all times (but will not have an obligation) to remove or refuse to distribute any Content on the Service and to terminate users or reclaim usernames. We also reserve the right to access, read, preserve, and disclose any information if we believed in good faith that it is reasonably necessary to<ul> <li>(i) satisfy any applicable law, regulation, legal process or governmental request (for example, subpoenas or warrants),
    </li><li>(ii) enforce or administer our agreements with users (such as these Terms), including investigation of potential violations hereof</li><li> (iii) detect, prevent, or otherwise address fraud, security or technical issues, (iv) respond to user support requests, or </li><li>(v) protect the rights, property or safety of <b style='color:#3BC8C2;'> Speeli </b>, its users and the public. </li></ul>
<br/>
<h3> Rules</h3> 
You must not do any of the following while accessing or using the Service:<ul> <li> (i) use the Service for any unlawful purposes or for promotion of illegal activities;</li> <li> (ii) upload or post any Content (as defined above) in violation of the provisions contained in the "Your License to <b style='color:#3BC8C2;'> Speeli </b>" section of these terms;</li> <li> (iii) use the Service for the purpose of spamming anyone; </li> <li>(iv) access or tamper with non-public areas of the Service, <b style='color:#3BC8C2;'> Speeli </b>'s computer systems, or the technical delivery systems of <b style='color:#3BC8C2;'> Speeli </b>'s providers; </li> <li>(v) probe, scan, or test the vulnerability of any system or network or breach or circumvent any security or authentication measures;</li> <li> (vi) access or search or attempt to access or search the Service by any means (automated or otherwise) other than through the currently available, published interfaces that are provided by <b style='color:#3BC8C2;'> Speeli </b> (and only pursuant to those terms and conditions), unless you have been specifically allowed to do so in a separate agreement with <b style='color:#3BC8C2;'> Speeli </b> (crawling the Service is permissible in accordance with these Terms, but scraping the Service without the prior consent of <b style='color:#3BC8C2;'> Speeli </b> except as permitted by these Terms is expressly prohibited); </li> <li>(vii) forge any TCP/IP packet header or any part of the header information in any email or posting, or in any way use the Service to send altered, deceptive or false source-identifying information; or</li> <li> (viii) interfere with or disrupt (or attempt to do so) the access of any user, host or network, including, without limitation, sending a virus, overloading, flooding, spamming, mail-bombing the Service, or by scripting the creation of Content in such a manner as to interfere with or create an undue burden on the Service.</li> </ul> 

<br/>
You are solely responsible for your interaction with other users of the Service, whether online or offline. You agree that <b style='color:#3BC8C2;'> Speeli </b> is not responsible or liable for the conduct of any user. <b style='color:#3BC8C2;'> Speeli </b> reserves the right, but has no obligation, to monitor or become involved in disputes between you and other users. Exercise common sense and your best judgment when interacting with others, including when you submit or post Content or any personal or other information.
<br/>
We may make available one or more APIs for interacting with the Service. Your use of any <b style='color:#3BC8C2;'> Speeli </b> API is subject to these terms and the <b style='color:#3BC8C2;'> Speeli </b>  API Rules, which will be posted before we make these APIs available (as part of these Terms). 
<br/>
<h3> Proprietary Rights</h3> 
All right, title, and interest in and to the Service (excluding Content provided by users) are and will remain the exclusive property of <b style='color:#3BC8C2;'> Speeli </b> and its licensors. The Service is protected by copyright, trademark, and other laws of both the United States and foreign countries. Except as expressly provided herein, nothing in the Terms gives you a right to use the <b style='color:#3BC8C2;'> Speeli </b> name or any of the <b style='color:#3BC8C2;'> Speeli </b> trademarks, logos, domain names, and other distinctive brand features. Any feedback, comments, or suggestions you may provide regarding the Service is entirely voluntary and we will be free to use such feedback, comments or suggestions as we see fit and without any obligation to you.
<br/>
The Service may include advertisements, which may be targeted to the Content or information on the Service, queries made through the Service, or other information. The types and extent of advertising by <b style='color:#3BC8C2;'> Speeli </b> on the Service are subject to change. In consideration for <b style='color:#3BC8C2;'> Speeli </b> granting you access to and use of the Service, you agree that <b style='color:#3BC8C2;'> Speeli </b> and its third party providers and partners may place such advertising on the Service or in connection with the display of Content or information from the Service whether submitted by you or others.
<br/>
<h3> Copyright Policy</h3> 
<b style='color:#3BC8C2;'> Speeli </b> respects the intellectual property rights of others and expects users of the Service to do the same. We will respond to notices of alleged copyright infringement that comply with applicable law and are properly provided to us. The time of the response may vary. If you believe that your Content has been copied in a way that constitutes copyright infringement, please provide our copyright agent with the following information in accordance with the Digital Millennium Copyright Act:<ul> <li>
        (i) a physical or electronic signature of the copyright owner or a person authorized to act on their behalf; </li> <li>
        (ii) identification of the copyrighted work claimed to have been infringed; </li> <li>
        (iii) identification of the material that is claimed to be infringing or to be the subject 
        of infringing activity and that is to be removed or access to which is to be disabled, 
        and information reasonably sufficient to permit us to locate the material; </li> <li>
        (iv) your contact information, including your address, telephone number, and an email address;
        (v) a statement by you that you have a good faith belief that use of the material 
        in the manner complained of is not authorized by the copyright owner, its agent, or the law; and
        </li> <li>(vi) a statement that the information in the notification is accurate, and, under penalty of perjury,
            that you are authorized to act on behalf of the copyright owner.</li> </ul>
<br/>
Our designated copyright agent for notice of alleged copyright infringement or other legal notices regarding Content appearing on the Service is:

Email: Support@speeli.com
We reserve the right to remove Content alleged to be infringing or otherwise illegal without prior notice and at our sole discretion. In appropriate circumstances, <b style='color:#3BC8C2;'> Speeli </b> will also terminate a user's account if the user is determined to be a repeat infringer.
<br/>
<h3> Links</h3> 
The Service may contain links to third-party websites or resources. You acknowledge and agree that we are not responsible or liable for: (i) the availability or accuracy of such websites or resources; or (ii) the content, products, or services on or available from such websites or resources. Links to such websites or resources do not imply any endorsement by <b style='color:#3BC8C2;'> Speeli </b> of such websites or resources or the content, products, or services available from such websites or resources. You acknowledge sole responsibility for and assume all risk arising from your use of any such links or websites or resources.
<br/>
<h3> Third party products</h3> 
We may provide links to third party products as a part of our monetization process. While we will choose products that we have good faith in still we do not guarantee the quality of the products nor do we guarantee the promises given by each seller.
<br/>
Once you leave the <b style='color:#3BC8C2;'> Speeli </b>.com domain <b style='color:#3BC8C2;'> Speeli </b> does not become responsible of the site you are visiting or the product you are purchasing. 
<br/>
We will however do our best to make sure that the third party vendors we are dealing with are honest and authentic. In case we made sure that any of those vendors are violating the rights of our users, we won’t hesitate in taking the proper actions to protect our users.
<br/>
<h3> Medical content</h3> 
Content posted by Medical Contributors is not intended to be medical advice or instructions for medical diagnosis or treatment, and no physician-patient relationship is, or is intended to be, created by Content provided by Medical Contributors.
If you think you may have a medical emergency, call your doctor or your local emergency number (911 in the United States) immediately.
<ul>
<li>
Content is not a substitute for professional medical advice, examination, diagnosis or treatment.</li><li>
You should not delay or forego seeking treatment for a medical condition or disregard professional medical advice based on Content.</li><li>
You should always seek the advice of your physician or other qualified healthcare professional before starting or changing treatment.</li><li>
Content should not be used to diagnose, treat, cure, or prevent disease without supervision of a doctor or qualified healthcare provider.</li><li>
Content does not recommend or endorse any tests, physicians, products, procedures, opinions or other information.</li><li>
Content is not regulated by the Food and Drug Administration or any state or national medical board.</li><li>
Information posted to <b style='color:#3BC8C2;'> Speeli </b> publicly or 
sent in an unsolicited message to a Medical Contributor is not confidential 
and does not establish a physician-patient relationship without the express consent of the Medical Contributor.
</li></ul>
<br/>
<h3> Disclaimers; Indemnity</h3> 
Your access to and use of the Service or any Content is at your own risk. You understand and agree that the Service is provided to you on an "AS IS" and "AS AVAILABLE" basis. Without limiting the foregoing, <b style='color:#3BC8C2;'> Speeli </b> AND ITS PARTNERS DISCLAIM ANY WARRANTIES, EXPRESS OR IMPLIED, OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE, OR NON-INFRINGEMENT. We make no warranty and disclaim all responsibility and liability for the completeness, accuracy, availability, timeliness, security or reliability of the Service or any content thereon. <b style='color:#3BC8C2;'> Speeli </b> will not be responsible or liable for any harm to your computer system, loss of data, or other harm that results from your access to or use of the Service, or any Content. You also agree that <b style='color:#3BC8C2;'> Speeli </b> has no responsibility or liability for the deletion of, or the failure to store or to transmit, any Content and other communications maintained by the Service. We make no warranty that the Service will meet your requirements or be available on an uninterrupted, secure, or error-free basis. No advice or information, whether oral or written, obtained from <b style='color:#3BC8C2;'> Speeli </b> or through the Service, will create any warranty not expressly made herein.
<br/>
TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, <b style='color:#3BC8C2;'> Speeli </b> AND ITS AFFILIATES, OFFICERS, EMPLOYEES, AGENTS, PARTNERS AND LICENSORS WILL NOT BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, CONSEQUENTIAL OR PUNITIVE DAMAGES, INCLUDING WITHOUT LIMITATION, LOSS OF PROFITS, DATA, USE, GOOD-WILL, OR OTHER INTANGIBLE LOSSES, RESULTING FROM 
<ul>
    <li>
        (i) YOUR ACCESS TO OR USE OF OR INABILITY TO ACCESS OR USE THE SERVICE;</li><li>
            
        
        (ii) ANY CONDUCT OR CONTENT OF ANY THIRD PARTY ON THE SERVICE, 
        INCLUDING WITHOUT LIMITATION, ANY DEFAMATORY, OFFENSIVE OR ILLEGAL CONDUCT OF OTHER
        USERS OR THIRD PARTIES;</li><li> 
        (iii) ANY CONTENT OBTAINED FROM THE SERVICE; AND </li><li> 
        (iv) UNAUTHORIZED ACCESS, USE OR ALTERATION OF YOUR TRANSMISSIONS OR CONTENT,
        WHETHER BASED ON WARRANTY, CONTRACT, TORT (INCLUDING NEGLIGENCE) OR ANY OTHER LEGAL THEORY,
        WHETHER OR NOT QUORA HAS BEEN INFORMED OF THE POSSIBILITY OF SUCH DAMAGE,
        AND EVEN IF A REMEDY SET FORTH HEREIN IS FOUND TO HAVE FAILED OF ITS ESSENTIAL PURPOSE.
        </li></ul>
<br/>
Some jurisdictions do not allow the exclusion of certain warranties or the exclusion or limitation of liability for consequential or incidental damages, so the limitations above may not apply to you.
<br/>
If anyone brings a claim against us related to your actions or Content on the Service, or actions or Content by or from someone using your account, you will indemnify and hold us harmless from and against all damages, losses, and expenses of any kind (including reasonable legal fees and costs) related to such claim.
<br/>
<h3> General Terms</h3> 
These Terms are the entire and exclusive agreement between <b style='color:#3BC8C2;'> Speeli </b> and you regarding the Service (excluding any services for which you have a separate agreement with <b style='color:#3BC8C2;'> Speeli </b> that is explicitly in addition or in place of these Terms), and these Terms supersede and replace any prior agreements between <b style='color:#3BC8C2;'> Speeli </b> and you regarding the Service.
<br/>
The failure of <b style='color:#3BC8C2;'> Speeli </b> to enforce any right or provision of these Terms will not be deemed a waiver of such right or provision. In the event that any provision of these Terms is held to be invalid or unenforceable, the remaining provisions of these Terms will remain in full force and effect. 
<br/>
We reserve the right, in our sole discretion, to change these Terms of Use ("Updated Terms") from time to time. Unless we make a change for legal or administrative reasons, we will provide reasonable advance notice before the Updated Terms become effective. You agree that we may notify you of the Updated Terms by posting them on the Service, and that your use of the Service after the effective date of the Updated Terms (or engaging in such other conduct as we may reasonably specify) constitutes your agreement to the Updated Terms. Therefore, you should review these Terms of Use and any Updated Terms before using the Service. The Updated Terms will be effective as of the time of posting, or such later date as may be specified in the Updated Terms, and will apply to your use of the Service from that point forward. These Terms of Use will govern any disputes arising before the effective date of the Updated Terms.
<br/>
If you have any questions or concerns please feel free to contact us on
support@speeli.com







