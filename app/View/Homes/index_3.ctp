<style>
    #loader{
        display:none;
    }
    .speeli-boxes{
        margin-top:0em;
        text-align: center;
		font-family:'PT Sans Narrow', sans-serif;
		font-size:18px;
    }
    .speeli-boxes .box-title{
        color: #66bcb6;
    }
	    .speeli-boxes .box-title span{
			display:block;
			margin:15px 0 30px;
			font-size:70px;
		}
     .speeli-boxes h4{
        font-size: 1.5em;
     }
    .speeli-boxes .box-body{
        font-size: 1.1em;
        color:rgb(51, 50, 50);
    }
    #Title{
        margin: auto;
		text-align:center;
		
    }
    .second-step{
        display: none;
        float: left;
    }
    .step_number{
        float: left;
        margin-right: 1vw;
    }
    ul{
        list-style: none;
    }
    li{
        list-style: none;
    }

    .text{
    }
    .flex-container{
        display: -webkit-flex;
        display: flex;
    }
    .flex-column{
        -webkit-flex: 1;
        flex: 1;
    }
    .thumbnail a img{
        width: 100%;
        height: 17em;
    }
    .question{
        height: 30em;
    }
	
	
	
	
	.card {
        width: 180px;
        height: 190px;
		text-align:center;
        /*margin: 20px;
        display: inline-block;*/
      }
	  
      .card .green {background-color:#569173;}
	  .card .blue {background-color:#3D9EAA;}
	  .card .orange{background-color:#D9562C;}
	  .card .lblue {background-color:#8CB5BD;}

	  .card .prbole {background-color:#706E81;}
	  .card .lgreen {background-color:#D4BD49;}
	  .card .dblue {background-color:#2D6B8C;}
	  .card .red {background-color:#C25656;}
      .front, .back {
       
        padding: 10px;
		
      }
      .front h4{
		  margin-top:42px;
       font-size:32px;font-family:'PT Sans Narrow', sans-serif;
color:#fff;
		
      }
	  
	  .front span {display:block;color:#fff;font-size:60px;margin-top:-20px;}
      .back {
      font-size:22px;font-family:'PT Sans Narrow', sans-serif;
color:#fff;
      }
	  
	  .back strong
	  {    display: block;
           margin: 17px 0 0px;
	  }
	  .back h4 
	  {opacity:.5;font-size:22px;font-family:'PT Sans Narrow', sans-serif;
color:#fff;margin:-2px;}
	  
	  .back span{color:#fff;font-size:28px;display:block;margin-top:8px;}
    @media only screen and (max-width: 768px){
        .selected-speeli{
display: none;
        }
        .speeli-boxes .box-body{
                padding-left: 5em;
    padding-right: 5em;
        }
        .speeli-boxes h4{
            font-size: 1.7em;
        }
        .speeli-boxes{
                border-bottom: 1px solid rgba(128, 128, 128, 0.11);
    padding-bottom: 1em;
    margin-top: 2.5em;
        }
        
    }
    
</style>
            <?php
                    echo $this->Html->css(array('style.min_2', 'responsive','main_2','main.min_2'));
                    ?>
<?php
echo $this->Html->script(array('jquery-1.11.3.min', 'jquery.colorbox-min', 'bootstrap.min', 'flip.js', 'wow.min.js', 'jquery.easing.1.3.js'));
?>
    <?php
$this->set("page_title", "Speeli know it in seconds");
$this->set("meta_robots", "index");
$this->set("meta_description", "Speeli know it in seconds");

echo $this->Form->create('Question');
?>

<div id="searchbox" class="row question">

    <div class="panel panel-default">
        <div class="panel-heading flex-container">
            
            <?php
            

            echo $this->Form->input('title', array('id' => 'Title', 'placeholder' => 'Search Speeli summaries .. . ', 'class' => 'login-form-text flex-column',
                'div' => false, 'label' => false));
            ?>

            <div class="check_status  btn btn-main"><i class="fa fa-search"></i></div>
        </h1>
        </div>

    </div>


    <div id="loader" class="col-md-3 col-xs-3 float-none" >
        <cetner><img src="<?php echo $this->webroot . 'files/8.gif'; ?>">
            Related Speeli articles
        </cetner>
    </div> 
    <div class="input-cat">
        <?php echo $this->fetch('content'); ?>

    </div>
    <br class="clear">

</div>

<div class="row">
    <br class="clear">
    <div class="col-md-4 col-xs-12">
        <div class=" speeli-boxes">
            <div class="box-title">
            <span><div class="icon1"></div></span>
                <h4><b> Speeli summarizes the internet </b></h4>
            </div>
            
        </div>
    </div>
    <div class="col-md-4 col-xs-12">
        <div class=" speeli-boxes">
            <div class="box-title">
            <span><div class="icon2"></div></span>
                <h4><b>Know anything in seconds</b></h4>
            </div>
            
        </div>
    </div>
    <div class="col-md-4 col-xs-12">
        <div class=" speeli-boxes">
            <div class="box-title">
            <span><div class="icon3"></div></span>

                <h4><b>Save hours of searching </b></h4>
            </div>
        </div>
    </div>

</div>

<center>
 <h3 class="page-header hometitle">
                What Do You Want To Explore Today ?
 </h3>
</center>


 <div class="row" id="more_info">
    
    <div class="col-md-3 col-xs-5">
       <div class="card">
           <div class="front prbole">
           <h4>Technology</h4>
           <span ><i class="fa fa-tablet"></i></span>
              
           </div>
          <div class="back prbole">
            <strong>5554</strong>
            <h4>Article</h4>
            <span><i class="fa fa-eye"></i></span>
            <h4>548</h4>
          </div>
       </div> 
    </div>
    
    <div class="col-md-3 col-xs-5">
       <div class="card">
           <div class="front blue">
           <h4>Business</h4>
           <span ><i class="fa fa-pie-chart"></i></span>
              
           </div>
          <div class="back blue">
            <strong>5554</strong>
            <h4>Article</h4>
            <span><i class="fa fa-eye"></i></span>
            <h4>548</h4>
          </div>
       </div> 
    </div>
    
  <div class="col-md-3 col-xs-5">
       <div class="card">
           <div class="front orange">
           <h4>Arts</h4>
           <span ><i class="fa fa-paint-brush"></i></span>
              
           </div>
          <div class="back orange">
            <strong>5554</strong>
            <h4>Article</h4>
            <span><i class="fa fa-eye"></i></span>
            <h4>548</h4>
          </div>
       </div> 
    </div>
    
    
   <div class="col-md-3 col-xs-5">
       <div class="card">
           <div class="front green">
           <h4>Health</h4>
           <span ><i class="fa fa-user-md"></i></span>
              
           </div>
          <div class="back green">
            <strong>5554</strong>
            <h4>Article</h4>
            <span><i class="fa fa-eye"></i></span>
            <h4>548</h4>
          </div>
       </div> 
    </div>
    
    
    <div class="col-md-3 col-xs-5">
       <div class="card">
           <div class="front lblue">
           <h4>Entertainment</h4>
           <span ><i class="fa fa-gamepad"></i></span>
              
           </div>
          <div class="back lblue">
            <strong>5554</strong>
            <h4>Article</h4>
            <span><i class="fa fa-eye"></i></span>
            <h4>548</h4>
          </div>
       </div> 
    </div>
    
    <div class="col-md-3 col-xs-5">
       <div class="card">
           <div class="front lgreen">
           <h4>Sports</h4>
           <span ><i class="fa fa-bicycle"></i></span>
              
           </div>
          <div class="back lgreen">
            <strong>5554</strong>
            <h4>Article</h4>
            <span><i class="fa fa-eye"></i></span>
            <h4>548</h4>
          </div>
       </div> 
    </div>
    
    <div class="col-md-3 col-xs-5">
       <div class="card">
           <div class="front dblue">
           <h4>Education</h4>
           <span ><i class="fa fa-graduation-cap"></i></span>
              
           </div>
          <div class="back dblue">
            <strong>5554</strong>
            <h4>Article</h4>
            <span><i class="fa fa-eye"></i></span>
            <h4>548</h4>
          </div>
       </div> 
    </div>
    
    
     <div class="col-md-3 col-xs-5">
       <div class="card">
           <div class="front red">
           <h4>Relationship</h4>
           <span ><i class="fa fa-heart"></i></span>
              
           </div>
          <div class="back red">
            <strong>5554</strong>
            <h4>Article</h4>
            <span><i class="fa fa-eye"></i></span>
            <h4>548</h4>
          </div>
       </div> 
    </div>
  
  
  
     <script type="text/javascript">
    $(function(){
      $(".card").flip({
        trigger: "hover"
      });
    });
    </script>

</div>



<?php /*?><?php if (isset($selecteds) && !empty($selecteds)) { ?>
    <div class="selected-speeli row">
        <center>
            <h3 class="page-header hometitle">
                What Do You Want To Explore Today ?
            </h3>
        </center>
        <br class="clear">
        <?php foreach ($selecteds as $article) { ?>
            <div class="col-md-4">
                <div class="thumbnail">
                    <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $article['Selected']['slug'])) ?>">
                        <img  class="img-responsive" src="<?php echo $this->webroot . "question/" . $article['Selected']['question_id'] . "/" . $article['Selected']['image']; ?>">
                    </a>
                    <h3>   <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $article['Selected']['slug'])) ?>">
                            <?php echo $article['Selected']['title']; ?>
                        </a></h3>


                </div>    



            </div>
        <?php } ?>
    </div>
<?php } ?><?php */?>

<script type="text/javascript">
    $(document).ready(function () {
    });
    $(document).keypress(function (event) {

        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {

            event.preventDefault();
            var questitle = $('#Title').val();
            $('#QuestionTitle').val(questitle);
            console.log(questitle);
            $.ajax({
                type: "get", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'homes', 'action' => 'search')); ?>",
                data: {questitle: questitle}, // outer quotes removed
                cache: false,
                beforeSend: function () {
                    $("#loader").fadeIn();
                },
                success: function (response, status) {
                    $("#loader").fadeOut();
                    $('.input-cat').html(response);
                },
            });
        }
    });


    $('.check_status').on('click', function () {

        event.preventDefault();
        var questitle = $('#Title').val();
        $('#QuestionTitle').val(questitle);
        $.ajax({
            type: "get", // Request method: post, get
            url: "<?php echo Router::url(array('controller' => 'homes', 'action' => 'search')); ?>",
            data: {questitle: questitle}, // outer quotes removed
            cache: false,
            beforeSend: function () {
                $("#loader").fadeIn();
            },
            success: function (response, status) {
                $("#loader").fadeOut();
                $('.input-cat').html(response);
            },
        });

    });

    //get courses


</script>
