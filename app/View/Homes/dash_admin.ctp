<style>
    .expand{
        cursor: pointer;
    }
    .click{
        cursor: pointer;
    }
    .progress{
        display: none;
    }
    hr{
        border: 0;
        height: 1px;
        background-image: -webkit-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -moz-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -ms-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -o-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
    }
    .clear{
        clear: both;
    }
    .question_wrapper{
        margin: 0 auto;
    }
    .float-none{
        float:none;
        margin: 0 auto;
    }
    .hblue{
        background-color: #fff;
        padding: 1em;
        border-radius: 4px;
        -moz-border-radius: 4px;
        -webkit-border-radius: 4px;
        margin-bottom: 25px;
        position: relative;
        line-height: 25px;
        border: 1px solid #e5e5e5;
    }
    .hblue .panel-heading{
        padding: 10px 15px;
        background-color: rgba(245, 245, 245, 0.24);
        border-top: 1px solid #ddd;

        border-bottom-right-radius: 3px;
        border-bottom-left-radius: 3px;
    }
    .list-unstyled li{
        margin-left: 1em;
    }
</style>
<?php
$this->set("meta_description", "Latest Speeli articles");
$this->set("meta_robots", "noindex");
?>
<?php
$this->Paginator->options(array(
    'update' => '.question_update',
    'evalScripts' => true
));
?>
<div class="row">
    <!-- Add the extra clearfix for only the required viewport -->
    <div  class="clearfix visible-xs-block"></div>
    <br class="clear">

    <div class="row ">

        <div class="col-md-8 float-none update_art_speeli  " style="border-left: 1px solid #ddd;">
            <!-- The time line -->
            <div class="middle-line">
                <center><h3  class="site_color">
                        Admin dashboard
                    </h3></center>     
            </div>
            <div class="hpanel forum-box question_update">
                <?php
                $i = 1;

                $user = $this->Session->read('user_quesli');
                if ($user['User']['admin'] == 1) :
                    ?>
                    <div class="hpanel hblue">
                        <div class="panel-heading hbuilt">

                            <h3>
                                <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'un_category', 'category')); ?>">
                                    ِAdd Category to articles 
                                </a> 
                            </h3>
                        </div>
                    </div>
                    <div class="hpanel hblue">
                        <div class="panel-heading hbuilt">
                            <h3>
                                <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'un_category')); ?>">
                                    ِAdd Summary to articles 
                                </a> 
                            </h3>

                        </div>
                    </div>
                    <div class="hpanel hblue">
                        <div class="panel-heading hbuilt">
                            <h3>
                                <a href="<?php echo Router::url('/logs'); ?>">
                                    Logs
                                </a> 
                            </h3>

                        </div>
                    </div>
                    <div class="hpanel hblue">
                        <div class="panel-heading hbuilt">
                            <h3>
                                <a href="<?php echo Router::url(array('controller' => 'categories', 'action' => 'all')); ?>">
                                    All category
                                </a> 
                            </h3>

                        </div>
                    </div>
                    <div class="hpanel hblue">
                        <div class="panel-heading hbuilt">
                            <h3>
                                <a href="<?php echo Router::url('/topics'); ?>">
                                    Articles sorted by number of views
                                </a> 
                            </h3>

                        </div>
                    </div>
                 <div class="hpanel hblue">
                        <div class="panel-heading hbuilt">
                            <h3>
                                <a href="<?php echo Router::url('/last_month'); ?>">
                                    Articles sorted by number of views last Month
                                </a> 
                            </h3>

                        </div>
                    </div>
                    <div class="hpanel hblue">
                        <div class="panel-heading hbuilt">
                            <h3>
                                <a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'users_index')); ?>">
                                    All users
                                </a> 
                            </h3>

                        </div>
                    </div>
                <div class="hpanel hblue">
                        <div class="panel-heading hbuilt">
                            <h3>
                                <a href="<?php echo Router::url(array('controller' => 'homes', 'action' => 'ads_index')); ?>">
                                    Sales Box
                                </a> 
                            </h3>

                        </div>
                    </div>
                    <div class="hpanel hblue">
                        <div class="panel-heading hbuilt">
                            <h3>
                                <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'feedback_latest')); ?>">
                                    Feedback Logs
                                </a> 
                            </h3>  
                        </div>
                    </div>
                <div class="hpanel hblue">
                        <div class="panel-heading hbuilt">
                            <h3>
                                <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'feedback_summary')); ?>">
                                    Feedback Summary
                                </a> 
                            </h3>  
                        </div>
                    </div>
                 <div class="hpanel hblue">
                        <div class="panel-heading hbuilt">
                            <h3>
                                <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'related_clicks')); ?>">
                                    Recent Related articles clicks
                                </a> 
                            </h3>  
                        </div>
                    </div>
                 <div class="hpanel hblue">
                        <div class="panel-heading hbuilt">
                            <h3>
                                <a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'contributors')); ?>">
                                    Search for users 
                                </a> 
                            </h3>  
                        </div>
                    </div>
                 <div class="hpanel hblue">
                        <div class="panel-heading hbuilt">
                            <h3>
                                <a href="<?php echo Router::url(array('controller' => 'homes', 'action' => 'tracks')); ?>">
                                   Add And Edit Buttons
                                </a> 
                            </h3>  
                        </div>
                    </div>
                <?php endif; ?>
                <!-- timeline time label -->
                <!-- END timeline item -->
                <!-- timeline item -->
            </div>
        </div><!-- /.col -->
    </div><!-- /.row -->

    <?php echo $this->Js->writeBuffer(); ?>
</div>
