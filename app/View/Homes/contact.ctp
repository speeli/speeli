<style>
 

.container-contact {
    font: 16px Helvetica;
  background: rgba(26, 188, 156,1.0);
  margin: 2em auto;
  overflow: hidden;
  background: rgba(255,255,255,1);
  border-radius: 5px;
}

.message, .contact, .name, .footer, header, textarea  {
 display: block;
 padding: 0;
 margin: 0;
 clear: both;
 overflow: hidden;
}

header {
  height: 75px;
  background: rgba(0,0,0,.05);
  line-height: 75px;
  padding-left: 20px;
  border-radius: 5px 5px 0 0;
    margin-bottom: 8px;
  
  h1 {
    font-size: 1.2em;
    text-transform: uppercase;
    color: rgba(51,51,51,.4);
  }
}

.first, .last {
  float: left;
  width: 278px;
  margin: 0;
  padding: 0 0 0 20px;
  border: 1px solid rgba(0,0,0,.1);
  height: 50px;
}

.last {
  width: 279px;
  border-left: 0;  
}

.email {
  height: 50px;
  width: 578px;
  line-height: 50px;
  padding: 0 0 0 20px;
  border-top: 0;
  border-left: 1px solid rgba(0,0,0,.1);
  border-right: 1px solid rgba(0,0,0,.1);
  border-bottom: 1px solid rgba(0,0,0,.1);
}

footer {
  
  height: 49px;
  border-radius: 0 0 5px 5px;
  padding-left: 0;
  padding-right: 20px;
  

  }
  
input[type="text"] {
  padding: 15px 10px;
  margin: 5px 20px;
  float:none
  }
  .email{line-height: normal;}



  textarea{
  height: 200px;
  padding: 10px;
  width: 93%;
  margin: 5px 20px;
  background: none;
  float:none
  }
  button {
    height: 32px;
    background: rgba(231, 76, 60,1.0);
    border-radius: 5px;
    border: 0;
    margin: 7px 0;
    color: rgba(255,255,255,1);
    float: right;
    padding: 0 20px 0 20px;
    border-bottom: 3px solid rgba(192, 57, 43,1.0);
    transition: all linear .2s;
    
    &:hover {
      background: rgba(192, 57, 43,1.0);  
    }
    
    &:focus {
       outline: none; 
    }

.first:focus, .last:focus, .email:focus, textarea:focus {
  outline: none;
  background: rgba(52, 152, 219,.1);
  color: rgba(51,51,51,.7);
}
.name,.contact,.message,.footer{
	text-align:center
	}
	
@media only screen and (max-width:480px){
header{height:auto!important}
header h1{font-size: 21px!important}

}

@media screen and (max-width: 480px), (-webkit-min-device-pixel-ratio: 1.5), (-o-min-device-pixel-ratio: 3/2), (min--moz-device-pixel-ratio: 1.5), (min-device-pixel-ratio: 1.5) {
#colorbox, #cboxWrapper, #cboxContent { width:320px !important; }
</style>

<div class='container-contact'>
  <header>
    <h1>Contact Speeli team</h1>
  </header>
    <?php 
    echo $this->Form->create('Contact');
    ?>
  <div class='name'>
    <input class='first' name="first" placeholder='First Name' type='text'>
  </div>
  <div class='contact'>
    <input class='email' name="email" placeholder='E-mail Address' type='text'>
  </div>
  <div class='message'>
    <textarea name="msg" placeholder='Your Message Here!'></textarea>
  </div>
  <footer>
    <button class="btn btn-save">Send </button>
  </footer>
    
</div>