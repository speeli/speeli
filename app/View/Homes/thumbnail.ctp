<style>
    .td-module-meta-info {
        
        font-size: 11px;
        margin-bottom: 7px;
        line-height: 1;
        min-height: 17px;
        margin-top: 3px;
    }
    .td-post-author-name a {
        color: #828484;
        font-size:14px;
        display: block;
        margin-top: 4px;
        display: inline-block;
    }
    .text-gray{
        font-weight: bold;
        color:#5a5959 !important;
       
    }
    .body-mobile{display: none;}
    .post img{    box-shadow: 1px 1px 1px #ddd;}
    .read-more-mobile{display: none;color:#383838}
    @media only screen and (max-width: 768px){
        .post{
            margin-top:2em;
        }
        .mobile-hide{display: none !important;}
        .body-mobile{display: block;}
        .no-padding-mobile{padding: 0px;}
        .read-more-mobile{display: block;}
    }
    
    @media only screen and (max-width: 500px) {
   .text-gray{
        
        font-size: 1.3em;
    }
    .link-url{margin-top:.5em;}
}
.explore-link{
    -moz-box-shadow: inset 1px 0px 0px 0px #a1987e;
    /* -webkit-box-shadow: inset 1px 0px 0px 0px #a1987e; */
    /* box-shadow: inset 1px 0px 0px 0px #a1987e; */
    background-color: #ffffff;
    -moz-border-radius: 42px;
    /* -webkit-border-radius: 42px; */
    /* border-radius: 42px; */
    border: 0;
    display: inline-block;
    cursor: pointer;
    float: right;
    color: #3bc8c1;
    border-style: dashed;
    font-family: Arial;
    font-size: 17px;
    padding: 0.1em 0.1em;
    text-align: right;
    text-decoration: none;
    /* text-shadow: 0px 1px 0px #3bc8c1; */
    border-bottom: 1.3px dashed #cac7c7;
}
</style>
<!--<center><h3  style="font-size:2.2em;font-weight: 700;margin-bottom: 3em" class="mobile ">
        What do you want to explore today ?
    </h3></center>
-->
<div class="header col-md-12">
    <div class="col-md-3 col-xs-12 no-padding text-center" style="background: #63B5B2">
        <h4 style="color:white;"> Latest Speeli Summaries</h4>
    </div>
    <!--
    <ul class="header col-md-9 col-xs-8 text-right no-margin no-padding right">
        <li class="list-default">
            <h5>Bussiness</h5>
        </li>
        <li class="list-default">
            <h5>Sports</h5>
        </li>
        <li class="list-default">
            <h5>Games</h5>
        </li>
        <li class="list-default mobile-hide">
            <h5>Technology</h5>
        </li>
        <li class="list-default mobile-hide">
            <h5>Entartinment</h5>
        </li>
        <li class="list-default mobile-hide">
            <h5>Science</h5>
        </li>

        <li class="list-default mobile-hide">
            <h5>Educations</h5>
        </li>

        <li class="list-default mobile-hide">
            <h5>Food</h5>
        </li>
    </ul>
    -->
</div>
<div class="clear clearfix"></div>
<div class="body col-md-12 col-xs-12 " style="padding-left: 0;">
    <?php
    $q == 0;
    foreach ($questions as $question) {
        if (!empty($question['Question']['image'])) {
            if (!empty($question['Question']['last'])) {
                $title = $question['Question']['last'];
            } else {
                if (!empty($question['Question']['title_update'])) {
                    $explode_title = explode(',,,', $question['Question']['title_update']);
                    $last = end($explode_title);
                    $title = end(unserialize(base64_decode($last)));
                } else {
                    $title = $question['Question']['title'];
                }
            }
            if ($q == 0) {
                ?>
    <div class="col-md-4 col-xs-12 no-margin no-padding-mobile" style="    padding-left: 0;">
                    <a class="col-md-12 col-xs-12 no-margin no-padding" href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $question['Question']['slug'])); ?>">
                        <img title="<?php echo $title; ?>" alt="<?php echo $title; ?>" class="img-responsive no-padding no-margin post-img-home img-thumbnail" 
                             width="360" height="180" alt="<?php echo $title; ?>"
                             src="<?php echo $this->webroot . "question/" . $question['Question']['id'] . "/" . $question['Question']['image']; ?>"
                             >
                       <!-- <div class="box-category-inside"><span class='fa fa-tags'>Technology</span></div> -->
                    </a>
                    <div class="col-md-12 col-xs-12 no-margin no-padding" style="padding-top: 1em">
                        <h3 class="no-margin no-padding link-url text-gray"style=""> 
                            <a title="  <?php echo $title; ?>" style="color:#111;font-size:20px; font-weight: bold;"
                               href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $question['Question']['slug'])); ?>" 
                               class="">
                                   <?php echo $title; ?>
                            </a>
                        </h3>
                       
                        <div class="clear clearfix"></div>
                        <div class="td-excerpt">
                            <p><?php
                                if (!empty($question['Answer']['last_body'])) {
                                    $body = $question['Answer']['last_body'];
                                    $body = trim($body);
                                } else {
                                    if (!empty($question['Answer']['body_update'])) {
                                        $explode_body = explode(',,,', $question['Answer']['body_update']);
                                        $last = end($explode_body);
                                        $body = end(unserialize(base64_decode($last)));
                                        $body = trim($body);
                                    } else {
                                        $body = $question['Answer']['body'];
                                        $body = trim($body);
                                    }
                                }
                                $pos = strpos($body, ' ', 100);
                                if ($pos) {
                                    echo substr($body, 0, $pos);
                                } else {
                                    echo $body;
                                }
                                ?> ... <a title="  <?php echo $title; ?>" style="color:#111;font-size:20px; font-weight: bold;"
                               href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $question['Question']['slug'])); ?>" 
                               class="read-more-mobile">
                                   Read more
                            </a></p>

                        </div>
                    </div> 
                </div>
                <?php
            } elseif ($q < 8) {
                if ($q == 1 || $q == 4) {
                    ?>

                    <div class="col-md-4 col-xs-12 no-margin no-padding">
                    <?php } ?>
                    <div class="post col-md-12 col-xs-12 no-margin no-padding no-margin no-padding">
                        <a class="col-md-4 col-xs-12 no-padding no-margin" href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $question['Question']['slug'])); ?>">
                            <img title="<?php echo $title; ?>" alt="<?php echo $title; ?>" class="img-responsive no-padding no-margin img-thumbnail" 
                                 width="360" height="180" alt="<?php echo $title; ?>"
                                 src="<?php echo $this->webroot . "question/" . $question['Question']['id'] . "/" . $question['Question']['image']; ?>"
                                 >

                        </a>

                        <div class="col-md-8 col-xs-12 link-url" style="">
                            <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $question['Question']['slug'])); ?>" class='text-gray mobile-large'>
                                <?php echo $title; ?>
                            </a>
                            
                        </div>
                        <div class="clear clearfix"></div>
                        <div class="td-excerpt body-mobile">
                            <p><?php
                                if (!empty($question['Answer']['last_body'])) {
                                    $body = $question['Answer']['last_body'];
                                    $body = trim($body);
                                } else {
                                    if (!empty($question['Answer']['body_update'])) {
                                        $explode_body = explode(',,,', $question['Answer']['body_update']);
                                        $last = end($explode_body);
                                        $body = end(unserialize(base64_decode($last)));
                                        $body = trim($body);
                                    } else {
                                        $body = $question['Answer']['body'];
                                        $body = trim($body);
                                    }
                                }
                                $pos = strpos($body, ' ', 100);
                                if ($pos) {
                                    echo substr($body, 0, $pos);
                                } else {
                                    echo $body;
                                }
                                ?>... <a title="  <?php echo $title; ?>" style="color:#111;font-size:20px; font-weight: bold;"
                               href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $question['Question']['slug'])); ?>" 
                               class="read-more-mobile">
                                    Read more
                            </a></p>

                        </div>
                    </div>
                    <?php if ($q == 3 || $q == 6) { ?>
                        <div class="clear clearfix"></div>

                    </div>
                <?php } ?>
                <?php
            }
        }
        $q++;
    }
    ?>

    <a class="explore-link" href="<?php echo Router::url('/feed')?>">Explore  More Summaries  <span class="fa fa-magic"></span></a>
</div>
