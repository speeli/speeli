<style>
    .page-title{
        padding: 1em;
    }
    .body{
        text-align: right;
        color:#00A6B6;
    }
    .btn-fb{
        background: #46629E;
    }
    .nav-tabs > li{
        float: right;
    }
</style>


<div class="col-md-12 col-xs-12 col-lg-12 float-none" >
    <img class="img-header-cat" src="<?php echo $this->webroot . 'img/first.jpg' ?>">    
</div>
<section class="main-container">

    <div class="container">
        <div class="row">

            <div class="main col-md-9" style="border-right: 1px solid #E4E2E2">

                <h1 class="page-title"><?php echo "الكتب"; ?></h1>
                <div class="separator-2"></div>

                <div class="tab-content clear-style ">
                    <div class="tab-pane active" id="pill-1">						
                        <div class="row masonry-grid-fitrows grid-space-10 " style="position: relative; height: 0px;">
                            <div class="tab-content">
                                <div id="all" class="tab-pane fade in active">
                                    <h3><center>الكتب الخاصة بفريق <?php echo $book['User']['username'] ?> </center> </h3>
                                    <?php foreach ($books as $book) { ?>

                                        <div class="col-sm-4 col-lg-4 masonry-grid-item " >
                                            <div class="listing-item white-bg bordered mb-20">
                                                <div class="overlay-container">

                                                    <a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'download', $book['Book']['book'])); ?>">
                                                        <img class="avatar avatar-50" style="height: 100%; width: 100%;"src="<?php echo $this->webroot . 'images/' . $book['Book']['image']; ?>" alt=""></a>
                                                    <a class="overlay-link popup-img-single" href="<?php echo $this->webroot . 'images/' . $book['Book']['image']; ?>">
                                                        <i class="fa fa-download"></i>
                                                    </a>
                                                    <div class="overlay-to-top links">
                                                        <span class="small">
                                                            <a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'download', $book['Book']['book'])); ?>" class="btn-sm-link">
                                                                <i class="fa fa-dow-o pr-10"></i> تحميل الكتاب</a>

                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="body">
                                                    <h3>
                                                        <a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'download', $book['Book']['book'])); ?>" >
                                                            <?php echo $book['Book']['name'] ?></a>
                                                    </h3>
                                                 
                                                        <p class="small"><span class="fa fa-bars">عدد الفصول  <?php echo $book['Book']['chapter'] ?></span></p>
                                                        <div class="elements-list clearfix">
                                                            <span class='fa fa-book'> عدد الصفحات<?php echo $book['Book']['pages'] ?></span>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        <?php } ?>
                                    </div>

                                </div>

                            </div>
                        </div>

                    </div>

                </div>

                <aside class="col-md-3 col-xs-12">
                    <h2 class="page-title">ملاحظات </h2>
                    <div class="sidebar">
                        <div class="block clearfix">

                            <div class="separator-2"></div>
                            <div id="carousel-sidebar" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner" role="listbox">
                                    <div class=" item active">
                                        <div class="bordered listing-item">
                                            <div class="overlay-container">

                                                <img src="<?php echo $this->webroot . 'img/right.jpg'; ?>" alt="إجراءات التلخيص">
                                                <a  href="<?php echo Router::url('/notes'); ?>" class="overlay-link iframe-edit">
                                                    <i class="fa fa-link"></i>

                                                </a>
                                            </div>
                                            <h3><a class="iframe-edit"href="<?php echo Router::url('/notes'); ?>" style="color:#00A6B6;text-align: right;"> <?php echo 'إجراءات التلخيص' ?></a></h3>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>	
                    </div>
                    <?php
                    if (!isset($user)) {
                        ?>
                        <button onclick="location.href = '<?php echo $fb_login_url ?>'"class="btn-fb"><i class="fa fa-facebook"></i>login with Facebook </button>     
                        <?php
                    } else {
                        
                    }
                    ?>

            </aside>
            <!-- sidebar end -->
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        resizeContent();
        $(window).resize(function () {
            resizeContent();
        });
    });
    function resizeContent() {
        $height = $(window).height();

        $('.masonry-grid-item .overlay-container').height($height / 3);
        $('.img-header-cat').css({'height': $height, 'width': '100%'});
    }
</script>