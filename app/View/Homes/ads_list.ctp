<style>
    .expand{
        cursor: pointer;
    }
    .click{
        cursor: pointer;
    }
    .progress{
        display: none;
    }
    hr{
        border: 0;
        height: 1px;
        background-image: -webkit-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -moz-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -ms-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -o-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
    }
    .clear{
        clear: both;
    }
    .question_wrapper{
        margin: 0 auto;
    }
    .float-none{
        float:none;
        margin: 0 auto;
    }
    .hblue{
        background-color: #fff;
        padding: 1em;
        border-radius: 4px;
        -moz-border-radius: 4px;
        -webkit-border-radius: 4px;
        margin-bottom: 25px;
        position: relative;
        line-height: 25px;
        border: 1px solid #e5e5e5;
    }
    .hblue .panel-heading{
        padding: 10px 15px;
        background-color: rgba(245, 245, 245, 0.24);
        border-top: 1px solid #ddd;

        border-bottom-right-radius: 3px;
        border-bottom-left-radius: 3px;
    }
    .list-unstyled li{
        margin-left: 1em;
    }
</style>
<?php
$this->set("meta_description", "Latest Speeli articles");
$this->set("meta_robots", "noindex");
?>

<div class="row">
    <!-- Add the extra clearfix for only the required viewport -->
    <div  class="clearfix visible-xs-block"></div>
    <br class="clear">
    <hr>
    <div class="row ">
        <div class="ads_type_id" target="<?php echo $id;?>">
        <div class="col-md-8 float-none update_art_speeli  " style="border-left: 1px solid #ddd;">
            <!-- The time line -->
            <div class="hpanel forum-box question_update">
                <?php
                $i = 1;

                $user = $this->Session->read('user_quesli');
                if ($user['User']['admin'] == 1) {
                    foreach ($questions as $question) {
                        if (!empty($question['Question']['title_update'])) {
                            $explode_title = explode(',,,', $question['Question']['title_update']);
                            $last = end($explode_title);
                            $title = end(unserialize(base64_decode($last)));
                        } else {
                            $title = $question['Question']['title'];
                        }
                        ?>
                        <div class="hpanel hblue">
                            <div class="panel-heading hbuilt">
                                <h3> <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $question['Question']['slug'])); ?>">
 <?php echo html_entity_decode($title); ?></a>  </h3>
                                <?php echo $status; ?>
                                <div style="cursor: pointer;"class="choose_click" art_id="<?php echo $question['Question']['id'] ?>">Add To The Article</div>
                            </div>
                             <div style="float:left;" class="panel-footer-status"><span class="fa fa-camera"></span><b> <?php echo $question['Question']['views'] ?></b></div>
                            
                            <br class="clear">
                        </div>
                        <?php
                        $i++;
                    }
                }
                ?>
                <!-- timeline time label -->
                <!-- END timeline item -->
                <!-- timeline item -->
            </div>
        </div><!-- /.col -->
    </div><!-- /.row -->
    <ul class="pagination">
        <?php
        echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
        echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'currentClass' => 'active', 'tag' => 'li', 'first' => 1));
        echo $this->Paginator->next(__('next'), array('tag' => 'li', 'currentClass' => 'disabled'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
        ?>
    </ul>
    <?php echo $this->Js->writeBuffer(); ?>
</div>
<script type="text/javascript">
        $('.choose_click').click(function () {
            var data_send = $(this).attr('art_id');
            var div=$(this);
            console.log(data_send);
            var ads_type_id = $('.ads_type_id').attr('target');
            console.log(ads_type_id);
            $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'homes', 'action' => 'ads_add_article')); ?>",
                data: {data_send: data_send, ads_type_id: ads_type_id}, // outer quotes removed
                cache: false,
                success: function (response, status) {
                    div.fadeOut();
                },
            });
        });
    </script>