
<?php
echo $this->Html->css(array('style.min_2', 'responsive', 'main.min_2'));
?>

<style>
    body{
        max-width: 100%;
        overflow-x: hidden;
    }
    .clear-fix{clear:both}

    #loader{
        display:none;
    }
    #QuestionIndexForm{
        margin-top: 0;
    }
    .speeli-boxes{
        margin-top:0em;
        text-align: center;
        font-family:'PT Sans Narrow', sans-serif;
        font-size:18px;
    }
    .speeli-boxes .box-title{
        color: #66bcb6;
    }
    .speeli-boxes .box-title span{
        display:block;
        margin:15px 0 30px;
        font-size:70px;
    }
    .speeli-boxes h4{
        font-size: 1.5em;
    }
    .speeli-boxes .box-body{
        font-size: 1.1em;
        color:rgb(51, 50, 50);
    }
    #Title{
        margin: auto;
        text-align:center;

    }
    .second-step{
        display: none;
        float: left;
    }
    .step_number{
        float: left;
        margin-right: 1vw;
    }
    ul{
        list-style: none;
    }
    li{
        list-style: none;
    }

    .text{
    }
    .flex-container{
        display: -webkit-flex;
        display: flex;
    }
    .flex-column{
        -webkit-flex: 1;
        flex: 1;
    }
    .thumbnail a img{
        width: 100%;
        height: 17em;
    }
    .question{
        height: 30em;
    }




    .card {
        
        height: 190px;
        text-align:center;
        /*margin: 20px;
        display: inline-block;*/
    }

    .card .green {background-color:#569173;}
    .card .blue {background-color:#3D9EAA;}
    .card .orange{background-color:#D9562C;}
    .card .lblue {background-color:#8CB5BD;}

    .card .prbole {background-color:#706E81;}
    .card .lgreen {background-color:#D4BD49;}
    .card .dblue {background-color:#2D6B8C;}
    .card .red {background-color:#C25656;}
    .front, .back {

        padding: 10px;

    }
    .front h4{
        margin-top:42px;
        font-size:32px;font-family:'PT Sans Narrow', sans-serif;
        color:#fff;

    }

    .front span {display:block;color:#fff;font-size:60px;margin-top:-20px;}
    .back {
        font-size:22px;font-family:'PT Sans Narrow', sans-serif;
        color:#fff;
    }

    .back strong
    {    display: block;
         margin: 17px 0 0px;
    }
    .form_index{
        background: rgba(241, 243, 242, 0.58);
        border-radius: 1em;
        margin-top: 35px;
        padding-bottom: 5px;
        margin-left: 50px;
    }
    .form_index div{
        margin: 0.6em auto;
    }
    .btn-fb{
        color: #fff;
        background-color: #3b5998;
        width: 100%;
        padding: 11px 20px;
        border-radius: 0px;
        text-align: center;
        font-weight: bold;
        display: block;
        font-family: "gessmed", Tahoma, Geneva, sans-serif;
        text-transform: capitalize;
        transition: 0.5s ease;
        -moz-transition: 0.5s ease;
        -o-transition: 0.5s ease;
        -webkit-transition: 0.5s ease;
        border-radius: 3px;
        border: 0px none;
        clear:both

    }


    #login{
        color: #fff;
        background-color: #66bcb6;
        width: 100%;
        padding: 0.65em !important;
        border-radius: 0px;
        text-align: center;
        font-weight: bold;
        display: block;
        font-family: "gessmed", Tahoma, Geneva, sans-serif;
        text-transform: capitalize;
        transition: 0.5s ease;
        -moz-transition: 0.5s ease;
        -o-transition: 0.5s ease;
        -webkit-transition: 0.5s ease;
        border-radius: 3px;

    }

    #login:hover{
        background: #3BC8C2
    }	

    .btn-fb:hover,.btn-fb:focus{
        color: #fff;
        background-color:#496ebc;
        text-decoration: none;
    }

    .back h4 
    {opacity:.5;font-size:22px;font-family:'PT Sans Narrow', sans-serif;
     color:#fff;margin:-2px;}
    #homeheader h1{
        margin: 35px 0 0;
    }
    .back span{color:#fff;font-size:28px;display:block;margin-top:8px;}
    @media only screen and (max-width: 768px){
        .selected-speeli{
            display: none;
        }
        #homeheader h1{
            font-size: 1.5em;
        }
        .speeli-boxes .box-body{
            padding-left: 5em;
            padding-right: 5em;
        }
        .speeli-boxes h4{
            font-size: 1.7em;
        }
        .speeli-boxes{
            border-bottom: 1px solid rgba(128, 128, 128, 0.11);
            padding-bottom: 1em;
            margin-top: 2.5em;
        }

        #login {
            margin: 5px 0;
        }

    }
    @media (min-width: 768px){
        #homeheader h1 {
            font-size: 2.5em;;
        }
        #homeheader a {
            font-size: 1.8em;;
        }
    }
    input[type="text"]{
        width: 100% !important;
    }

    #homeheader{position: relative;}
    #UserIndexForm{display:block}
    #searchboxform{
        margin: 0px auto;
        width: 60%;
        position: absolute;
        bottom: 0;
        left: 20%;
    }
    #searchboxform #QuestionIndexForm{

        background-color: transparent;
        padding: 0px;
        margin-bottom: 20px;
        border-radius: 10px !important;
        border: 4px solid rgba(0, 0, 0, 0.15);
        overflow: hidden;
    }

    #searchboxform #QuestionIndexForm .panel-heading.flex-container
    {
        overflow: hidden;
        color: #333;
        background-color: rgba(255, 255, 255, 0.9);
        margin: 0px;
        padding: 0px;
        border: 0px none;
    }

    #searchboxform #QuestionIndexForm .login-form-text
    {
        border: 0px none;
        background: transparent none repeat scroll 0% 0%;
        height: 52px;
        margin: 0px;
        padding: 0px 20px;
        box-shadow: none;
        font-size: 17px;
        -webkit-transition:all .2s;
        -moz-transition:all .2;
        transition:all .2s;
    }

    #searchboxform #QuestionIndexForm .check_status.btn.btn-main
    {
        background-color: #66BCB6;
        height: 53px;
        line-height: 53px;
        border: 0px none;
        box-shadow: none;
        border-radius: 0px;
    }

    #searchboxform #QuestionIndexForm .check_status.btn.btn-main:hover {
        border-radius: 0;
        background-color: #2EBCB2;
    }

    #searchboxform #QuestionIndexForm .check_status.btn.btn-main .fa-search:before {
        color: #FFF;
        font-size: 20px;
    }

    button.btn-fb i{
        margin-right: 10px;
    }

    .form_index h4{
        color: #FFF;
        font-size: 24px;
    }

    .form_index input[type="text"]{
        margin: inherit;
        box-shadow: inherit;
        border: 1px solid rgb(204, 204, 204);
        border-radius: 4px;
        font-family: initial;
        font-size: 14px;
        text-align:center !important;
    }
    .form-control{
        text-align:center !important;
    }
    .form_index input[type="text"]:focus{
        border-color: #66AFE9;
        outline: 0px none;
        box-shadow: 0px 1px 1px rgba(0, 0, 0, 0.075) inset, 0px 0px 8px rgba(102, 175, 233, 0.6);

    }	


    @media only screen and (max-width:768px){

        #homeheader {
            height: auto !important;
            background-position: 70% 50%;
        }

        .form_index {
            margin:auto;
        }
        #searchboxform {
            margin: 0px auto !important;
            width: 100% !important;
            position: initial !important;
        }
        #searchboxform #QuestionIndexForm .login-form-text {
            height: 52px;
            margin: 0px;
            font-size: 15px;
        }

        #searchboxform #QuestionIndexForm .check_status.btn.btn-main {
        }
        #searchboxform #QuestionIndexForm .login-form-text {
        }

        button.btn-fb{
            padding: 10px 0;
        }

        .form_index{
            margin-bottom: 25px;
            margin-top: 25px;

        }
        #login {margin: 5px 0;}

        #searchboxform #QuestionIndexForm .panel-heading.flex-container{display:table;position: relative;display: table;border-collapse: separate;width: 100%;}

        #searchboxform #QuestionIndexForm .login-form-text{display: table-cell;}
        #searchboxform #QuestionIndexForm .check_status.btn.btn-main{
            display: table-cell;
            white-space: nowrap;
            vertical-align: middle;
            box-sizing: border-box;}


    }



    @media only screen and (min-width:1360px){

        #searchboxform {
            margin: 0px auto;
            width: 50%;
            position: absolute;
            bottom: 0px;
            left: 25%;
        }
        #homeheader h1 {
            font-size: 28px;
        }
        .form_index {
            margin-top: 20px;
        }

        .form_index h4{
            margin-bottom: -10px;
        }

        .form_index div {
            margin: 0.3em auto;
        }

    }


</style>
<style>


    .card {

        height:13em;
        padding: 0;
    }


    .green {background-color:#569173;}
    .card:nth-child(0) {background-color:#3D9EAA;}
    .card:nth-child(1){background-color:#667779;}
    .card:nth-child(2) {background-color:#579999;}
    .card:nth-child(3) {background-color:#DD5422;}
    .card:nth-child(4) {background-color:#88BBBB;}
    .card:nth-child(5) {background-color:#2D6B8C;}
    .card:nth-child(6) {background-color:#C25656;}
    .front, .back {
        text-align: center;
        font-size:1.5em;
    }
    .card a{
        text-align: center;
        font-size:1.2em;
        color:white;
    }
    .back{
        background: #22A39F;
        padding-top: 2em;
    }
    .front{
        padding-top: 3em;
    }
    .front h4{
        margin-top:42px;
        font-size:32px;font-family:'PT Sans Narrow', sans-serif;
        color:#fff;

    }

    .front span {display:block;color:#fff;font-size:60px;margin-top:-20px;}
    .back {
        font-family:'PT Sans Narrow', sans-serif;
        color:#fff;
    }

    .back strong
    {    display: block;
         margin: 17px 0 0px;
    }
</style>
<?php
echo $this->Html->script(array('jquery.easing.1.3.js'));
?>
<?php
$this->set("page_title", "Speeli know it in seconds");
$this->set("meta_robots", "index");
$this->set("meta_description", "Speeli know it in seconds");
?>

<div id="searchbox" class="row question">
    <div id="loader" class="col-md-3 col-xs-3 float-none">
        <cetner><img src="<?php echo $this->webroot . 'files/8.gif'; ?>">
            Related Speeli articles
        </cetner>
    </div> 
    <div class="input-cat">
        <?php echo $this->fetch('content'); ?>
    </div>
    <br class="clear">

</div>

<div class="row">
    <br class="clear">
    <div class="col-md-4 col-xs-12">
        <div class=" speeli-boxes">
            <div class="box-title">
                <span><div class="icon1"></div></span>
                <h4><b> Speeli summarizes the internet </b></h4>
            </div>

        </div>
    </div>
    <div class="col-md-4 col-xs-12">
        <div class=" speeli-boxes">
            <div class="box-title">
                <span><div class="icon2"></div></span>
                <h4><b>Know anything in seconds</b></h4>
            </div>

        </div>
    </div>
    <div class="col-md-4 col-xs-12">
        <div class=" speeli-boxes">
            <div class="box-title">
                <span><div class="icon3"></div></span>

                <h4><b>Save hours of searching </b></h4>
            </div>
        </div>
    </div>

</div>
<?php
$user = $this->Session->read('user_quesli');
if (!empty($user)) {
    if ($user['User']['admin'] == 1) {


        echo $this->Html->script(array('jquery.flip.min'));
        ?>
        <div class="row">
            <div class="col-md-10 col-xs-12 float-none card-grid"  id="grid-containter">

            </div>
        </div>
        <?php
    }
}
?>


<?php if (isset($selecteds) && !empty($selecteds)) { ?>
    <div class="selected-speeli row">
        <center><h3 class="page-header hometitle">
                Selected speeli articles
            </h3></center>
        <br class="clear">
        <?php
        foreach ($selecteds as $article) {
            if (!empty($article['Question']['title_update'])) {
                $explode_title = explode(',,,', $article['Question']['title_update']);
                $last = end($explode_title);
                $title = end(unserialize(base64_decode($last)));
            } else {
                $title = $article['Question']['title'];
            }
            if (!empty($title) && !empty($article['Question']['image'])) {
                ?>
                <div class="col-md-4">
                    <div class="thumbnail">
                        <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $article['Question']['slug'])) ?>">
                            <img  class="img-responsive" src="<?php echo $this->webroot . "question/" . $article['Question']['id'] . "/" . $article['Question']['image']; ?>">
                        </a>
                        <h3>   <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $article['Question']['slug'])) ?>">
                                <?php echo $title; ?>
                            </a></h3>


                    </div>    
                </div>
                <?php
            } else {
                
            }
        }
        ?>
    </div>
<?php } ?>
<script>
    window.fbAsyncInit = function () {
        FB.init({
            appId: '835557989856782',
            cookie: true, // enable cookies to allow the server to access 
            // the session
            xfbml: true, // parse social plugins on this page
            version: 'v2.2' // use version 2.2
        });
    };
    function on_login_click() {
        function statusChangeCallback(response) {
            console.log('statusChangeCallback');
            console.log(response);
            if (response.status === 'connected') {
                // Logged into your app and Facebook.
                testAPI();
            } else if (response.status === 'not_authorized') {
                // The person is logged into Facebook, but not your app.
                document.getElementById('status').innerHTML = 'Please log ' +
                        'into this app.';
            } else {
                // The person is not logged into Facebook, so we're not sure if
                // they are logged into this app or not.
                document.getElementById('status').innerHTML = 'Please log ' +
                        'into Facebook.';
            }
        }

        function checkLoginState() {
            FB.getLoginStatus(function (response) {
                statusChangeCallback(response);
            });
        }
        function start_login() {
            checkLoginState();
        }

        // Load the SDK asynchronously
        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id))
                return;
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/en_US/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
        // Here we run a very simple test of the Graph API after login is
        // successful.  See statusChangeCallback() for when this call is made.
        function testAPI() {
            console.log('Welcome!  Fetching your information.... ');
            FB.api('/me', {fields: "id,name,gender,email"}, function (response) {
                console.log('Successful login for: ' + response.name);
                $.ajax({
                    type: "post", // Request method: post, get
                    url: "<?php echo Router::url(array('controller' => 'users', 'action' => 'check')); ?>",
                    data: {response: response}, // outer quotes removed
                    cache: false,
                    success: function (response, status) {
                        window.location = "<?php echo Router::url('/feed'); ?>";
                    },
                });
            });
        }
    }
    function statusChangeCallback(response) {
        console.log('statusChangeCallback');
        console.log(response);
        if (response.status === 'connected') {
            // Logged into your app and Facebook.
            testAPI();
        } else if (response.status === 'not_authorized') {
            // The person is logged into Facebook, but not your app.
            document.getElementById('status').innerHTML = 'Please log ' +
                    'into this app.';
        } else {
            // The person is not logged into Facebook, so we're not sure if
            // they are logged into this app or not.
            document.getElementById('status').innerHTML = 'Please log ' +
                    'into Facebook.';
        }
    }

    function checkLoginState() {
        FB.getLoginStatus(function (response) {
            statusChangeCallback(response);
        });
    }
    function start_login() {
        checkLoginState();
    }
    window.fbAsyncInit = function () {
        FB.init({
            appId: '835557989856782',
            cookie: true, // enable cookies to allow the server to access 
            // the session
            xfbml: true, // parse social plugins on this page
            version: 'v2.2' // use version 2.2
        });
    };
    // Load the SDK asynchronously
    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id))
            return;
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    // Here we run a very simple test of the Graph API after login is
    // successful.  See statusChangeCallback() for when this call is made.
    function testAPI() {
        console.log('Welcome!  Fetching your information.... ');
        FB.api('/me', {fields: "id,name,gender,email"}, function (response) {
            console.log('Successful login for: ' + response.name);
            $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'users', 'action' => 'check')); ?>",
                data: {response: response}, // outer quotes removed
                cache: false,
                success: function (response, status) {
                    window.location = "<?php echo Router::url('/feed'); ?>";
                },
            });
        });
    }
</script>

<script type="text/javascript">
    $(document).ready(function () {
        $('.card-grid').load('<?php echo $this->Html->url('/Homes/grid'); ?>');
        resizeContent();
        $(window).resize(function () {
            resizeContent();
        });
    });
    function resizeContent() {
        $height = $(window).height();
        $height_nav = $('.navbar').height() + 15;
        $('#homeheader').height($height - $height_nav);
        if (typeof window.orientation !== 'undefined') {

        } else {
            $('#homeheader').css({'margin-top': $height_nav});
        }
    }
    $(document).keypress(function (event) {

        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {

            event.preventDefault();
            var questitle = $('#Title').val();
            $('#QuestionTitle').val(questitle);
            console.log(questitle);
            $.ajax({
                type: "get", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'homes', 'action' => 'search')); ?>",
                data: {questitle: questitle}, // outer quotes removed
                cache: false,
                beforeSend: function () {
                    $("#loader").fadeIn();
                },
                success: function (response, status) {
                    $("#loader").fadeOut();
                    $('.input-cat').html(response);
                    $('html, body').stop().animate({
                        scrollTop: $('.wrapper').offset().top
                    }, 2000, 'easeInOutExpo');
                    event.preventDefault();
                },
            });
        }
    });
    $('.check_status').on('click', function (event) {

        event.preventDefault();
        var questitle = $('#Title').val();
        $('#QuestionTitle').val(questitle);
        $.ajax({
            type: "get", // Request method: post, get
            url: "<?php echo Router::url(array('controller' => 'homes', 'action' => 'search')); ?>",
            data: {questitle: questitle}, // outer quotes removed
            cache: false,
            beforeSend: function () {
                $("#loader").fadeIn();
            },
            success: function (response, status) {
                $("#loader").fadeOut();
                $('.input-cat').html(response);
                $('html, body').stop().animate({
                    scrollTop: $('.btn-fb').offset().top
                }, 2000, 'easeInOutExpo');
                event.preventDefault();
            },
        });
    });
</script>




