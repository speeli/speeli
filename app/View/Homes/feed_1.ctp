
<style>
    #loader{
        display:none;
    }

    .home-thumb{
        padding: 0;
    }
    .panel-footer{
        padding: 0;
        background-color: rgba(141, 205, 203, 0.82);
    }
    .page-header{
        margin-top: 0;
        float: none;
    }
    .middle-line{
        width: 100%;
        background-color: rgba(99, 181, 178, 0.78);
        left: 0;
        top: 17em;
        margin-left: -7em;
        -webkit-box-shadow: 0 2px 7px rgba(0, 0, 0, 0.25);
        box-shadow: 0 2px 7px rgba(0, 0, 0, 0.25);
        -ms-filter: "progid:DXImageTransform.Microsoft.Shadow(Strength=3,Direction=180,Color='#333333')";
        filter: progid:DXImageTransform.Microsoft.Shadow(Strength=3, Direction=180, Color='#333333');
        margin: 0 auto;
    }
    .forum-box{
        margin: 1em;
    }
    .progress{
        display: none;
    }
    .panel-footer,.panel-heading{
        text-align: center;
    }
    .panel-footer h3{
        margin-top: 0;
        padding-top: 0;
    }
    hr{
        border: 0;
        height: 1px;
        background-image: -webkit-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -moz-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -ms-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -o-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
    }
    .clear{
        clear: both;
    }
    .question_wrapper{
        margin: 0 auto;
    }
    .float-none{
        float:none;
        margin: 0 auto;
        clear:both;
    }
    .hblue{
        background-color: #fff;
        padding: 1em;
        border-radius: 4px;
        -moz-border-radius: 4px;
        -webkit-border-radius: 4px;
        margin-bottom: 25px;
        position: relative;
        line-height: 25px;
        border: 1px solid #e5e5e5;
    }
    .hblue .panel-heading{
        padding: 10px 15px;
        background-color: rgba(245, 245, 245, 0.24);

        border-bottom-right-radius: 3px;
        border-bottom-left-radius: 3px;
    }
    .list-unstyled li{
        margin-left: 1em;
    }
    .panel-footer h3 {
        padding-top: 0.3em;
    }
    .signup{
        font-size: 1.3em;
    }

    #Title {
        margin: auto;
    }
    #searchbox .check_status.btn.btn-main{
        margin-left: 5px;
    }

    input[type="text"], .login-form-text {
        width: 90%;
    }
    @media only screen and (max-width: 768px){
        #searchbox{
            margin-top: 3em;
        }
        .signup{
            font-size: 1.1em;
        }
        .fa-toggle-menu {
            display: none !important;
        }
        .panel-footer h3{
            font-size: 1.1em;
            text-align: center;
        }
        .panel-heading h3{
            font-size: 0.9em;
            text-align: left;
            padding: 0;
            margin: 0;
        }
        .middle-line{
            top:9em;
        }


    }


    @media only screen and (max-width: 560px){

        #searchbox input[type="text"], .login-form-text
        {
            width: 78%;

        }
        #searchbox .check_status.btn.btn-main{
            width:14%
        }


    }





</style>
<?php
$this->set("meta_description", "Latest Speeli articles");
$this->set("meta_robots", "noindex");
?>

<div class="row">
    <br class="clear">
    <!-- Add the extra clearfix for only the required viewport -->
    <div  class="clearfix visible-xs-block"></div>
    <div id="searchbox" class="row question">
        <?php
        echo $this->Form->create('Question');
        ?>
        <?php
        echo $this->Form->input('title', array('id' => 'Title', 'placeholder' => 'Search Speeli summaries .. . ', 'class' => 'login-form-text flex-column',
            'div' => false, 'label' => false));
        ?>
        <div class="check_status  btn btn-main"><i class="fa fa-search"></i></div>
    </div>
    <div id="loader" class="col-md-3 col-xs-3 float-none" >
        <cetner><img src="<?php echo $this->webroot . 'files/8.gif'; ?>">
            Related Speeli articles
        </cetner>
    </div> 
    <br class="clear">
    <div class="input-cat col-md-6 col-xs-12 float-none">

    </div>
    <br class="clear">

</div>

<div  class="clearfix visible-xs-block col-md-12 col-xs-12 float-none"></div>
<div class="middle-line">
    <center><h3 style="color:white;" class="">
            Selected speeli articles
        </h3></center>     
</div>
<br class="clear">
<?php if (isset($questions) && !empty($questions)) { ?>
    <div style='margin-top:2em;' class="selected-speeli col-md-12 float-none">
        <?php if (!($this->Session->read('user_quesli'))) { ?>
            <center><h3 class="signup"><a style="color:#8B8C8C" href="<?php echo Router::url(array('controller' => 'users', 'action' => 'login')) ?>">
                        <font style="color:#63B5B2">Sign up</font> to get Speeli articles matching your interests</a></h3></center>
        <?php }
        ?>
        <br class="clear">
        <?php
        $i = 0;

        for ($q = 0; $q < count($questions); $q++) {

            if ($questions[$q]['Question']['image'] != NULL) {
                if (!empty($questions[$q]['Question']['last'])) {
                    $title = $questions[$q]['Question']['last'];
                } else {
                    if (!empty($questions[$q]['Question']['title_update'])) {
                        $explode_title = explode(',,,', $questions[$q]['Question']['title_update']);
                        $last = end($explode_title);
                        $title = end(unserialize(base64_decode($last)));
                    } else {
                        $title = $questions[$q]['Question']['title'];
                    }
                }

                if ($i < 2) {
                    ?>
                    <div class="col-md-6 col-xs-12 home-thumb">
                        <div class="thumbnail">
                            <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $questions[$q]['Question']['slug'])) ?>">
                                <img style="width:100%; height: 20em;" class="img-responsive" src="<?php echo $this->webroot . "question/" . $questions[$q]['Question']['id'] . "/" . $questions[$q]['Question']['image']; ?>">
                            </a>
                            <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $questions[$q]['Question']['slug'])) ?>">
                                <div class="panel-footer" style="height: 6em;">
                                    <h3 style="color:white;" class="image-title"><?php echo $title ?></h3>
                                </div>
                            </a>
                        </div>    
                    </div>
                <?php } elseif ($i < 8) {
                    ?>
                    <div class="col-md-4 col-xs-12 home-thumb">
                        <div class="thumbnail">
                            <a   href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $questions[$q]['Question']['slug'])) ?>">
                                <img style="width:100%; height: 20em;" class="img-responsive" src="<?php echo $this->webroot . "question/" . $questions[$q]['Question']['id'] . "/" . $questions[$q]['Question']['image']; ?>">

                            </a>
                            <a   href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $questions[$q]['Question']['slug'])) ?>">
                                <div class="panel-footer" style="height: 6em;">
                                    <h3 style="color:white;" class="image-title"><?php echo $title ?></h3>
                                </div>
                            </a>
                        </div>   
                    </div>
                <?php } ?>
                <?php
                $i++;
                unset($questions[$q]);
            }
        }
        ?>
    </div>
<?php } ?>
<div class="col-md-12 float-none ">

    <div class="col-md-6">
        <div class="thumbnail">
            <div class="hpanel forum-box">
                <center>  <h4 class="page-header">New Speeli articles</h4></center>
                <?php
                $i = 1;

                $user = $this->Session->read('user_quesli');
                foreach ($questions as $question) {

                    if (!empty($question['Question']['last'])) {
                        $title = $question['Question']['last'];
                    } else {
                        if (!empty($question['Question']['title_update'])) {
                            $explode_title = explode(',,,', $question['Question']['title_update']);
                            $last = end($explode_title);
                            $title = end(unserialize(base64_decode($last)));
                        } else {
                            $title = $question['Question']['title'];
                        }
                    }
                    ?>
                    <div class="hpanel hblue">
                        <div class="panel-heading hbuilt" style='padding:1px;'>
                            <h3> 
                                <a  style="font-size: 1em; line-height:1.5em;"
                                    href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $question['Question']['slug'])); ?>"> <?php echo htmlspecialchars($title); ?></a>  </h3>
                        </div>


                    </div>
                    <?php
                    $i++;
                    if ($i > 10) {
                        break;
                    }
                }
                ?>
                <!-- timeline time label -->
                <!-- END timeline item -->
                <!-- timeline item -->
            </div>
        </div>
    </div>
    <div class="col-md-6" >
        <div class="thumbnail">
            <!-- The time line -->
            <div class="hpanel forum-box" >
                <center>  <h4 class="page-header ">Help us expand these articles</h4>    </center> 

                <?php
                $e = 0;
                foreach ($expands as $expand) {

                    if (!empty($expand['Question']['last'])) {
                        $title = $expand['Question']['last'];
                    } else {
                        if (!empty($expand['Question']['title_update'])) {
                            $explode_title = explode(',,,', $expand['Question']['title_update']);
                            $last = end($explode_title);
                            $title = end(unserialize(base64_decode($last)));
                        } else {
                            $title = $expand['Question']['title'];
                        }
                    }
                    ?>
                    <div class="hpanel hblue">
                        <div class="panel-heading hbuilt" style='padding:1px;'>
                            <h3> <a style="font-size: 1em; line-height:1.5em; " href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $expand['Question']['slug'])); ?>"> <?php echo htmlspecialchars($title); ?></a>  </h3>
                        </div>
                    </div>
                    <?php
                    $e++;
                    if ($e > 10) {
                        break;
                    }
                }
                if (isset($second_expand) && !empty($second_expand)) {
                    foreach ($second_expand as $expand) {
                        if (!empty($expand['Question']['last'])) {
                            $title = $expand['Question']['last'];
                        } else {
                            if (!empty($expand['Question']['title_update'])) {
                                $explode_title = explode(',,,', $expand['Question']['title_update']);
                                $last = end($explode_title);
                                $title = end(unserialize(base64_decode($last)));
                            } else {
                                $title = $expand['Question']['title'];
                            }
                        }
                        ?>
                        <div class="hpanel hblue">
                            <div class="panel-heading hbuilt">
                                <h3> <a style="font-size: 1em;" href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $expand['Question']['slug'])); ?>"> <?php echo htmlspecialchars($title); ?></a>  </h3>
                            </div>
                        </div>
                        <?php
                        $i++;
                        $e++;
                        if ($e > 9) {
                            break;
                        }
                    }
                }
                ?>
                <!-- timeline time label -->
                <!-- END timeline item -->
                <!-- timeline item -->
            </div>
        </div>
    </div><!-- /.col -->
</div><!-- /.row -->
</div>
<?php
echo $this->Html->css(array('colorbox'));
echo $this->Html->script(array('jquery.colorbox-min'));
?>
<script>

    $(document).keypress(function (event) {

        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {

            event.preventDefault();
            var questitle = $('#Title').val();
            $('#QuestionTitle').val(questitle);
            console.log(questitle);
            $.ajax({
                type: "get", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'homes', 'action' => 'search')); ?>",
                data: {questitle: questitle}, // outer quotes removed
                cache: false,
                beforeSend: function () {
                    $("#loader").fadeIn();
                },
                success: function (response, status) {
                    $("#loader").fadeOut();
                    $('.input-cat').html(response);
                },
            });
        }
    });


    $('.check_status').on('click', function () {

        event.preventDefault();
        var questitle = $('#Title').val();
        $('#QuestionTitle').val(questitle);
        $.ajax({
            type: "get", // Request method: post, get
            url: "<?php echo Router::url(array('controller' => 'homes', 'action' => 'search')); ?>",
            data: {questitle: questitle}, // outer quotes removed
            cache: false,
            beforeSend: function () {
                $("#loader").fadeIn();
            },
            success: function (response, status) {
                $("#loader").fadeOut();
                $('.input-cat').html(response);
            },
        });

    });


</script>