<div class="">

    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel panel">
                <div class="x_title panel-heading">
                    <h2>Add New Type  <small></small></h2>

                    <div class="clearfix"></div>
                </div>
                <div class="x_content panel-body">
                    <?php
                    echo $this->Form->create('Ads', array('novalidate','type' => 'file', 'class' => 'form-horizontal form-label-left'));
                    ?>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title">Title <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <?php
                            echo $this->Form->input('title', array('required' => 'required', 'class' => 'form-control col-md-7 col-xs-12',
                                'div' => false, 'id' => 'title', 'placeholder' => 'Title', 'label' => false));
                            ?>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="url">Url <span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <?php
                            echo $this->Form->input('url', array('required' => 'required', 'class' => 'form-control col-md-7 col-xs-12',
                                'div' => false, 'id' => 'url', 'placeholder' => 'Url', 'label' => false));
                            ?>
                        </div>
                    </div>
                    <div class="item form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="Image">Image<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <?php
                            echo $this->Form->file('image', array('class' => 'form-control col-md-7 col-xs-12',
                                'div' => false, 'id' => 'Image', 'label' => false));
                            ?>
                        </div>
                    </div>
                    <div class="ln_solid"></div>
                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-3">
                            <button onclick="goBack()" type="button" formnovalidate class="btn btn-primary">Cancel</button>
                            <button id="send" type="submit" class="btn btn-success">Submit</button>
                        </div>
                    </div>
                    <?php echo $this->Form->end(); ?>


                </div>
            </div>
        </div>
    </div>

<script>
    function goBack() {
        window.history.back();
    }
</script>
