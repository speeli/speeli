<section id="cd-timeline" class="cd-container">
    <?php foreach($articles as $article){?>
    <div class="cd-timeline-block">
        <div class="cd-timeline-img cd-picture">
            <img src="<?php echo $this->webroot . "question/" . $article['Question']['id'] . "/" . $article['Question']['image']; ?>" alt="Picture">
        </div> <!-- cd-timeline-img -->

        <div class="cd-timeline-content">
            <?php 
              if (!empty($answers[0]['Question']['title_update'])) {
                                $explode_title = explode(',,,', $answers[0]['Question']['title_update']);
                                $last = end($explode_title);
                                $title = end(unserialize(base64_decode($last)));
                            } else {
                                $title = $answers[0]['Question']['title'];
                            }
            ?>
            <h2><?php echo $title; ?></h2>

            <p>
                has been wrote 1000 Speeli summaries and made 500 edits<br>
            </p>


        </div> <!-- cd-timeline-content -->
    </div> <!-- cd-timeline-block -->
  
    <?php }?>
    <a class="btn  btn-add-article" href="/speeli/articles/add">Share your knowledge and join our community <span class="fa fa-sign-in">

        </span>
    </a>
</section> <!-- cd-timeline -->