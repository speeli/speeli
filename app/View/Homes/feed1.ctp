<style>
    .home-thumb{
        padding: 0;
    }
     .panel-footer{
        padding: 0;
  background-color: rgba(141, 205, 203, 0.82);
    }
    .page-header{
        margin-top: 0;
        float: none;
    }
   .middle-line{
        position: absolute;
        width: 100%;
        background-color: rgba(99, 181, 178, 0.78);
        left: 0;
        top: 9em;
        margin-left: -7em;
        float: left;
        -webkit-box-shadow: 0 2px 7px rgba(0, 0, 0, 0.25);
        box-shadow: 0 2px 7px rgba(0, 0, 0, 0.25);
        -ms-filter: "progid:DXImageTransform.Microsoft.Shadow(Strength=3,Direction=180,Color='#333333')";
        filter: progid:DXImageTransform.Microsoft.Shadow(Strength=3, Direction=180, Color='#333333');
        margin: 0 auto;
    }
    .forum-box{
        margin: 1em;
    }
    .progress{
        display: none;
    }
    .panel-footer,.panel-heading{
        text-align: center;
    }
    .panel-footer h3{
        margin-top: 0;
        padding-top: 0;
    }
    hr{
        border: 0;
        height: 1px;
        background-image: -webkit-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -moz-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -ms-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -o-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
    }
    .clear{
        clear: both;
    }
    .question_wrapper{
        margin: 0 auto;
    }
    .float-none{
        float:none;
        margin: 0 auto;
    }
    .hblue{
        background-color: #fff;
        padding: 1em;
        border-radius: 4px;
        -moz-border-radius: 4px;
        -webkit-border-radius: 4px;
        margin-bottom: 25px;
        position: relative;
        line-height: 25px;
        border: 1px solid #e5e5e5;
    }
    .hblue .panel-heading{
        padding: 10px 15px;
        background-color: rgba(245, 245, 245, 0.24);
        border-top: 1px solid #ddd;

        border-bottom-right-radius: 3px;
        border-bottom-left-radius: 3px;
    }
    .list-unstyled li{
        margin-left: 1em;
    }
    .panel-footer h3 {
        padding-top: 0.3em;
    }
     @media only screen and (max-width: 768px){
                            .fa-toggle-menu {
                                display: none !important;
                            }
                            .panel-footer h3{
                                font-size: 1.1em;
                                text-align: center;
                            }
                            .panel-heading h3{
                                font-size: 0.9em;
                                text-align: left;
                                padding: 0;
                                margin: 0;
                            }
                            .middle-line{
                                top:4em;
                            }
                            
                        }
</style>
<?php
$this->set("meta_description", "Latest Speeli articles");
$this->set("meta_robots", "noindex");
?>
<div class="row">
    <!-- Add the extra clearfix for only the required viewport -->
    <div  class="clearfix visible-xs-block"></div>
    <br class="clear"><br class="clear">
    
            <div class="middle-line">
    <center><h3 style="color:white;" class="">
            Selected speeli articles
        </h3></center>     
</div>
    <br class="clear">
    <?php if (isset($questions) && !empty($questions)) { ?>
        <div style='margin-top:2em;' class="selected-speeli col-md-12 float-none">
       
            <br class="clear">
            <?php 
            $i=0;
            for ($q=0;$q<count($questions);$q++) {
                
                if($questions[$q]['Question']['image']!=NULL){
                   if($i<2){
                       ?>
                    <div class="col-md-6 col-xs-6 home-thumb">
                    <div class="thumbnail">
                        <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $questions[$q]['Question']['slug'])) ?>">
                            <img style="width:100%; height: 20em;" class="img-responsive" src="<?php echo $this->webroot . "question/" . $questions[$q]['Question']['id'] . "/" . $questions[$q]['Question']['image']; ?>">
                        </a>
                        <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $questions[$q]['Question']['slug'])) ?>">
                        <div class="panel-footer" style="height: 6em;">
                                <h3 style="color:white;" class="image-title"><?php echo $questions[$q]['Question']['title']?></h3>
                       </div>
                        </a>
                    </div>    
                    </div>
                  <?php  }
                  elseif($i<8){ ?>
                         <div class="col-md-4 col-xs-6 home-thumb">
                    <div class="thumbnail">
                        <a   href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $questions[$q]['Question']['slug'])) ?>">
                            <img style="width:100%; height: 20em;" class="img-responsive" src="<?php echo $this->webroot . "question/" . $questions[$q]['Question']['id'] . "/" . $questions[$q]['Question']['image']; ?>">
                            
                        </a>
                        <a   href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $questions[$q]['Question']['slug'])) ?>">
                        <div class="panel-footer" style="height: 6em;">
                                <h3 style="color:white;" class="image-title"><?php echo $questions[$q]['Question']['title']?></h3>
                            </div>
                        </a>
                    </div>   
                </div>
                   <?php } ?>
            <?php 
            $i++;
                } } ?>
        </div>
    <?php } ?>
    <div class="col-md-12 float-none ">

        <div class="col-md-6 col-xs-6">
            <div class="thumbnail">
                <!-- The time line -->
                <div class="hpanel forum-box">
                    <center>  <h4 class="page-header">New Speeli articles</h4></center>
                    <?php
                    $i = 1;
                    $user = $this->Session->read('user_quesli');
                    foreach ($questions as $question) {
                        if ($question['Question']['status'] == 1) {
                            ?>
                            <div class="hpanel hblue">
                                <div class="panel-heading hbuilt">
                                    <h3> <a  style="font-size: 1em;" href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $question['Question']['slug'])); ?>"> <?php echo htmlspecialchars($question['Question']['title']); ?></a>  </h3>

                                </div>


                            </div>
                            <?php
                            $i++;
                        }
                    }
                    ?>
                    <!-- timeline time label -->
                    <!-- END timeline item -->
                    <!-- timeline item -->
                </div>
              
            </div>
        </div><!-- /.col -->
        <div class="col-md-6 col-xs-6 " >
            <div class="thumbnail">
                <!-- The time line -->
                <div class="hpanel forum-box" >
                       <center>  <h4 class="page-header ">Help us expand these articles</h4>    </center> 
                    <?php
                    
                    foreach ($expands as $question) {
                        if ($question['Question']['status'] == 1) {
                            ?>
                            <div class="hpanel hblue">
                                <div class="panel-heading hbuilt">
                                    <h3> <a style="font-size: 1em;" href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $question['Question']['slug'])); ?>"> <?php echo htmlspecialchars($question['Question']['title']); ?></a>  </h3>
                                </div>
                            </div>
                            <?php
                            $i++;
                        }
                    }
                    ?>
                    <!-- timeline time label -->
                    <!-- END timeline item -->
                    <!-- timeline item -->
                </div>
            </div>
        </div><!-- /.col -->
    </div><!-- /.row -->
</div>
<?php
    echo $this->Html->css(array('colorbox'));
    echo $this->Html->script(array('jquery.colorbox-min'));
?>