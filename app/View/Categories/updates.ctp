
<?php
$this->set("meta_description", "Latest Speeli articles");
?>
       
            <div class="hpanel forum-box">
                <?php
                $i = 1;
                if(isset($update_title)){
            echo "<center><h3 class='page-header'>$update_title</h3></center>";        
                }
                $user = $this->Session->read('user_quesli');
                if ($user['User']['admin'] == 1) {
                    foreach ($questions as $question) {
                     if(!empty($question['Question']['last'])){
                        $title=$question['Question']['last'];
                    }else{
                        if (!empty($question['Question']['title_update'])) {
                            $explode_title = explode(',,,', $question['Question']['title_update']);
                            $last = end($explode_title);
                            $title = end(unserialize(base64_decode($last)));
                        } else {
                            $title = $question['Question']['title'];
                        }
                    }
                    if(!empty($title)){
                        ?>
                        <div class="hpanel hblue">
                            <div class="panel-heading hbuilt">
                                <h3>
     <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $question['Question']['slug'])); ?>">
                 <?php echo $title; ?></a>  </h3>
                                
                               
                            </div>
<div style="float:right;" class="panel-footer-status">
    <span class="fa fa-camera"></span><b> <?php echo $question['Question']['views'] ?></b>
    <br class="clear">
</div>
                            <br class="clear">

                        </div>
                        <?php
                    }
                        $i++;
                    }
                } else {
                    foreach ($questions as $question) {
                        if ($question['Question']['status'] == 1) {
                             if (!empty($question['Question']['last'])) {
                            $title = $question['Question']['last'];
                        } else {
                            if (!empty($question['Question']['title_update'])) {
                                $explode_title = explode(',,,', $question['Question']['title_update']);
                                $last = end($explode_title);
                                $title = end(unserialize(base64_decode($last)));
                            } else {
                                $title = $question['Question']['title'];
                            }
                        }
                        if(!empty($title))
                        {
                            ?>
                            <div class="hpanel hblue">
                                <div class="panel-heading hbuilt">
                                    <h3> <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $question['Question']['slug'])); ?>"> <?php echo htmlspecialchars($title); ?></a>  </h3>
                                    <?php echo $status; ?>
                                </div>
                            </div>
                            <?php
                        }
                            $i++;
                        }
                    }
                }
                ?>
                <!-- timeline time label -->
                <!-- END timeline item -->
                <!-- timeline item -->
            </div>
     
<script type="text/javascript">
      
            $('body').undelegate('.update_art', 'click').delegate('.update_art', 'click', function (e) {
        event.preventDefault();       
        target=$(this).attr('speel_ques');
                $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'categories', 'action' => 'updates')); ?>",
                data: {target: target}, // outer quotes removed
                cache: false,
                success: function (response, status) {
                   $('.update_art_speeli').html(response);
                },
            });
            });
            
        $('body').undelegate('.expand', 'click').delegate('.expand', 'click', function (e) {
           
            var art_id = $(this).attr('art_id');
             var art_status = $(this).attr('art_status');
         if(art_status==0){
        $(this).html('<span style="color:red" class="fa fa-lightbulb-o">need expand</span>');
        }else{
        $(this).html('<span style="color:green" class="fa fa-lightbulb-o">done</span>'); 
        }
         $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'articles', 'action' => 'expand')); ?>",
                data: {art_id: art_id, art_status: art_status}, // outer quotes removed
                cache: false,
                success: function (response, status) {
                },
            });
        });
        </script>