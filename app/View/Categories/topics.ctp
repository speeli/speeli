<style>
    #loader{
        display: none;
    }
    .expand{
        cursor: pointer;
    }
    .progress{
        display: none;
    }
    hr{
        border: 0;
        height: 1px;
        background-image: -webkit-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -moz-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -ms-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -o-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
    }
    .clear{
        clear: both;
    }
    .question_wrapper{
        margin: 0 auto;
    }
    .float-none{
        float:none;
        margin: 0 auto;
    }
    .hblue{
        background-color: #fff;
        padding: 1em;
        border-radius: 4px;
        -moz-border-radius: 4px;
        -webkit-border-radius: 4px;
        margin-bottom: 25px;
        position: relative;
        line-height: 25px;
        border: 1px solid #e5e5e5;
    }
    .hblue .panel-heading{
        padding: 10px 15px;
        background-color: rgba(245, 245, 245, 0.24);
        border-top: 1px solid #ddd;

        border-bottom-right-radius: 3px;
        border-bottom-left-radius: 3px;
    }
    .list-unstyled li{
        margin-left: 1em;
    }
</style>

<?php
$this->set("meta_description", "Latest Speeli articles");
 echo $this->Html->script(array('jquery.colorbox-min'));
?>
<script>
  function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
    if (response.status === 'connected') {
      // Logged into your app and Facebook.
      testAPI();
    } else if (response.status === 'not_authorized') {
      // The person is logged into Facebook, but not your app.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into this app.';
    } else {
      // The person is not logged into Facebook, so we're not sure if
      // they are logged into this app or not.
      document.getElementById('status').innerHTML = 'Please log ' +
        'into Facebook.';
    }
  }

  // This function is called when someone finishes with the Login
  // Button.  See the onlogin handler attached to it in the sample
  // code below.
    function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }
   function start_login (){
       checkLoginState();
   }
  window.fbAsyncInit = function() {
  FB.init({
    appId      : '835557989856782',
    cookie     : true,  // enable cookies to allow the server to access 
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.2' // use version 2.2
  });

  // Now that we've initialized the JavaScript SDK, we call 
  // FB.getLoginStatus().  This function gets the state of the
  // person visiting this page and can return one of three states to
  // the callback you provide.  They can be:
  //
  // 1. Logged into your app ('connected')
  // 2. Logged into Facebook, but not your app ('not_authorized')
  // 3. Not logged into Facebook and can't tell if they are logged into
  //    your app or not.
  //
  // These three cases are handled in the callback function.

  };

  // Load the SDK asynchronously
  (function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

  // Here we run a very simple test of the Graph API after login is
  // successful.  See statusChangeCallback() for when this call is made.
  function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', {fields: "id,name,gender,email"}, function(response) {
      console.log('Successful login for: ' + response.name);
        $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'users', 'action' => 'check')); ?>",
                data: {response: response}, // outer quotes removed
                cache: false,
                success: function (response, status) {
                
                console.log(response);
                },
            });
    });
  }
</script>

<!--
  Below we include the Login Button social plugin. This button uses
  the JavaScript SDK to present a graphical Login button that triggers
  the FB.login() function when clicked.
-->

<?php 

?>


<div class="row">
    <!-- Add the extra clearfix for only the required viewport -->
    <div  class="clearfix visible-xs-block"></div>
    <br class="clear"><br class="clear">
    <div class="col-md-6 float-none">
        <?php
        echo $this->Form->create('Question');
        echo $this->Form->input('category', array('style' => 'position:absolute;width:90%;', 'id' => 'Title', 'placeholder' => 'Search in topics ', 'class' => 'login-form-text flex-column',
            'div' => false, 'label' => false));
        ?>
        <div style="right:0px;position: absolute;top:3px;" class="check_status  btn btn-main"><i class="fa fa-search"></i></div>
        <br class="clear">
    </div>   
    <br class="clear">
     <a href="<?php echo Router::url(array('controller' => 'homes', 'action' => 'request'))?>" 
                                        class="iframe-request "><i class="fa fa-question"></i><span>Request article</span>
                                     </a>
    <script>
        if(typeof window.orientation !== 'undefined'){
    $(".iframe-request").colorbox({opacity: '0.5', width: '100%', height: '80%', top: '10px'});        
        }else{
    $(".iframe-request").colorbox({opacity: '0.5', width: '50%', height: '70%', top: '50px'});
        }
    
    </script>
    <div  class="clearfix visible-xs-block"></div>
    <div class="row ">
        <div class="col-md-2 " style="position: fixed;">
            <div class="panel-heading">
                <ul class="nav nav-tabs">
                    <li speel_ques='0'  class="active update_art"><a href="#tab2default" data-toggle="tab">trending articles</a></li>
                    <li speel_ques='1'  class="update_art"><a href="#tab2default" data-toggle="tab">latest articles</a></li>
                    <li speel_ques='2' class='update_art' ><a href="#tab3default" data-toggle="tab">trending topics</a></li>
                    <li speel_ques='3' class='update_art' ><a href="#tab3default" data-toggle="tab">most viewed topics</a></li>
                </ul>
            </div>
        </div>
        <div class="col-md-8 float-none update_art_speeli  " style="border-left: 1px solid #ddd;">
            <!-- The time line -->
            <div class="hpanel forum-box">
                <div id="loader" class="col-md-3 col-xs-3 float-none" >
                    <cetner><img src="<?php echo $this->webroot . 'files/8.gif'; ?>">
                        Related Speeli articles
                    </cetner>
                </div>
                <?php
                $i = 1;
                if (isset($update_title)) {
                    echo "<center><h3 class='page-header'>$update_title</h3></center>";
                }
                $user = $this->Session->read('user_quesli');
                if ($user['User']['admin'] == 1) {
                    foreach ($questions as $question) {
                        if (!empty($question['Question']['last'])) {
                            $title = $question['Question']['last'];
                        } else {
                            if (!empty($question['Question']['title_update'])) {
                                $explode_title = explode(',,,', $question['Question']['title_update']);
                                $last = end($explode_title);
                                $title = end(unserialize(base64_decode($last)));
                            } else {
                                $title = $question['Question']['title'];
                            }
                        }
                        if(!empty($title)){
                            
                        ?>
                        <div class="hpanel hblue">
                            <div class="panel-heading hbuilt">
                                <h3>
                                    <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $question['Question']['slug'])); ?>"> <?php echo $title; ?></a>  </h3>

                            </div>
                            <div style="float:left;" class="panel-footer-status"><span class="fa fa-camera"></span><b> <?php echo $question['Question']['views'] ?></b></div> 
                            <br class="clear">
                        </div>
                        <br class="clear">
                        <?php
                    }
                    $i++;
                    }
                } else {
                    foreach ($questions as $question) {
                        if ($question['Question']['status'] == 1) {
                              if (!empty($question['Question']['last'])) {
                            $title = $question['Question']['last'];
                        } else {
                            if (!empty($question['Question']['title_update'])) {
                                $explode_title = explode(',,,', $question['Question']['title_update']);
                                $last = end($explode_title);
                                $title = end(unserialize(base64_decode($last)));
                            } else {
                                $title = $question['Question']['title'];
                            }
                        }
                        if(!empty($title)){
                            echo $title;
                            
                            ?>
                            <div class="hpanel hblue">
                                <div class="panel-heading hbuilt">
                                    <h3> <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $question['Question']['slug'])); ?>"> <?php echo htmlspecialchars($title); ?></a>  </h3>
                                    <?php echo $status; ?>
                                </div>


                            </div>
                            <?php
                        }
                            $i++;
                        }
                    }
                }
                ?>
                <!-- timeline time label -->
                <!-- END timeline item -->
                <!-- timeline item -->
            </div>
        </div><!-- /.col -->
    </div><!-- /.row -->
</div>
<script type="text/javascript">

    $('body').undelegate('.update_art', 'click').delegate('.update_art', 'click', function (e) {
        event.preventDefault();
        target = $(this).attr('speel_ques');
        $.ajax({
            type: "post", // Request method: post, get
            url: "<?php echo Router::url(array('controller' => 'categories', 'action' => 'updates')); ?>",
            data: {target: target}, // outer quotes removed
            cache: false,
            success: function (response, status) {
                $('.update_art_speeli').html(response);
            },
        });
    });
    $('.check_status').on('click', function () {

        event.preventDefault();
        var questitle = $('#Title').val();
        console.log(questitle);
        $('#QuestionTitle').val(questitle);
        $.ajax({
            type: "post", // Request method: post, get
            url: "<?php echo Router::url(array('controller' => 'categories', 'action' => 'search')); ?>",
            data: {questitle: questitle}, // outer quotes removed
            cache: false,
            beforeSend: function () {
                $("#loader").fadeIn();
            },
            success: function (response, status) {
                $("#loader").fadeOut();
                $('.update_art_speeli').html(response);
            },
        });

    });
    $(document).keypress(function (event) {

        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {

            event.preventDefault();
            var questitle = $('#Title').val();
            console.log(questitle);
            $('#QuestionTitle').val(questitle);
            $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'categories', 'action' => 'search')); ?>",
                data: {questitle: questitle}, // outer quotes removed
                cache: false,
                beforeSend: function () {
                    $("#loader").fadeIn();
                },
                success: function (response, status) {
                    $("#loader").fadeOut();
                    $('.update_art_speeli').html(response);
                },
            });
        }
    });

</script>