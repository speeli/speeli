<style>
    .suggest-items{
        text-transform: capitalize;
    }
</style>

<div class="suggest row col-md-12">
    <?php
    if (!empty($summaries)) {
        foreach ($summaries as $summary) {
            ?>

            <div class="btn btn-default suggest-items col-md-12" cat_parent="<?php echo $cat_id; ?>" ques-cat-id="<?php echo $summary['Summary']['id'] ?>"><?php ?><?php echo $summary['Summary']['name']; ?></div><br class="clear">

        <?php }
    } else { ?>

<?php } ?>   
    
</div>
<br class="clear">
<script>
    $(document).ready(function () {
        $('body').undelegate('.suggest-items', 'click').delegate('.suggest-items', 'click', function (e) {
            $('.suggest').hide();
            $('.new-cat-value').val('');
            var cat_id = $(this).attr('cat_parent');
            var add_new_cat = $(this).attr('ques-cat-id');
            var add_new_val = $(this).html();
            $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'categories', 'action' => 'update_sum')); ?>",
                data: {add_new_val: add_new_val, add_new_cat: add_new_cat, cat_id: cat_id}, // outer quotes removed
                cache: false,
                success: function (response, status) {
                },
            });

        });
    });
</script>