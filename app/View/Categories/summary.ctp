<style>
    #loader{
        display: none;
    }
    .expand{
        cursor: pointer;
    }
    .progress{
        display: none;
    }
    hr{
        border: 0;
        height: 1px;
        background-image: -webkit-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -moz-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -ms-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -o-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
    }
    .clear{
        clear: both;
    }
    .question_wrapper{
        margin: 0 auto;
    }
    .float-none{
        float:none;
        margin: 0 auto;
    }
    .hblue{
        background-color: #fff;
        padding: 1em;
        border-radius: 4px;
        -moz-border-radius: 4px;
        -webkit-border-radius: 4px;
        margin-bottom: 25px;
        position: relative;
        line-height: 25px;
        border: 1px solid #e5e5e5;
    }
    .hblue .panel-heading{
        padding: 10px 15px;
        background-color: rgba(245, 245, 245, 0.24);
        border-top: 1px solid #ddd;

        border-bottom-right-radius: 3px;
        border-bottom-left-radius: 3px;
    }
    .list-unstyled li{
        margin-left: 1em;
    }
</style>
<style>
    #loader{
        display:none;
    }

    .home-thumb{
        padding: 0;
    }
    .panel-footer{
        padding: 0;
        background-color: rgba(141, 205, 203, 0.82);
    }
    .page-header{
        margin-top: 0;
        float: none;
    }
    .middle-line{
        width: 100%;
        background-color: rgba(99, 181, 178, 0.78);
        left: 0;
        top: 17em;
        margin-left: -7em;
        -webkit-box-shadow: 0 2px 7px rgba(0, 0, 0, 0.25);
        box-shadow: 0 2px 7px rgba(0, 0, 0, 0.25);
        -ms-filter: "progid:DXImageTransform.Microsoft.Shadow(Strength=3,Direction=180,Color='#333333')";
        filter: progid:DXImageTransform.Microsoft.Shadow(Strength=3, Direction=180, Color='#333333');
        margin: 0 auto;
    }
    .forum-box{
        margin: 1em;
    }
    .progress{
        display: none;
    }
    .panel-footer,.panel-heading{
        text-align: center;
    }
    .panel-footer h3{
        margin-top: 0;
        padding-top: 0;
    }
    hr{
        border: 0;
        height: 1px;
        background-image: -webkit-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -moz-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -ms-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -o-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
    }
    .clear{
        clear: both;
    }
    .question_wrapper{
        margin: 0 auto;
    }
    .float-none{
        float:none;
        margin: 0 auto;
        clear:both;
    }
    .hblue{
        background-color: #fff;
        padding: 1em;
        border-radius: 4px;
        -moz-border-radius: 4px;
        -webkit-border-radius: 4px;
        margin-bottom: 25px;
        position: relative;
        line-height: 25px;
        border: 1px solid #e5e5e5;
    }
    .hblue .panel-heading{
        padding: 10px 15px;
        background-color: rgba(245, 245, 245, 0.24);

        border-bottom-right-radius: 3px;
        border-bottom-left-radius: 3px;
    }
    .list-unstyled li{
        margin-left: 1em;
    }
    .panel-footer h3 {
        padding-top: 0.3em;
    }

    #Title {
        margin: auto;
    }
    #searchbox .check_status.btn.btn-main{
        margin-left: 5px;
    }

    input[type="text"], .login-form-text {
        width: 90%;
    }
    @media only screen and (max-width: 768px){
        #searchbox{
            margin-top: 3em;
        }
        .signup{
            font-size: 1.1em;
        }
        .fa-toggle-menu {
            display: none !important;
        }
        .panel-footer h3{
            font-size: 1.1em;
            text-align: center;
        }
        .panel-heading h3{
            font-size: 1.5em;
            text-align: left;
            padding: 0;
            margin: 0;
        }
        .middle-line{
            top:9em;
        }


    }


    @media only screen and (max-width: 560px){

        #searchbox input[type="text"], .login-form-text
        {
            width: 78%;

        }
        #searchbox .check_status.btn.btn-main{
            width:14%
        }


    }





</style>
<style>
        .category_top{
            background: #63b5b2;
            position: absolute;
            padding: 0.5em;
            color: white;
            border-bottom-right-radius: 1em;
            border: 1px solid;
            font-weight: bold;
        }
        .category_top a {
            color: white;
        }
        .thumbnail{
            padding: 0;
        }
        .panel-footer h3{

            color:black !important;
        }
        .panel-footer{
            background: none !important;
        }
    </style>
    <?php
$this->set("meta_robots", "index");
$this->set("page_title", "Speeli - " . $update_title);
?>
<div class="row">
    <!-- Add the extra clearfix for only the required viewport -->
    <br class="clear">
    <!-- Add the extra clearfix for only the required viewport -->
    <div  class="clearfix visible-xs-block"></div>
    <div  id="searchbox" class="row question">
        <div class="col-md-12 col-xs-12">    
            <?php
            echo $this->Form->create('Question');
            ?>
            <?php
            echo $this->Form->input('title', array('id' => 'Title', 'placeholder' => 'Search Speeli summaries ... ', 'class' => 'login-form-text flex-column',
                'div' => false, 'label' => false));
            ?>
            <div class="check_status  btn btn-main"><i class="fa fa-search"></i></div></h1>
        </div>
        <br class="clear">
        <div id="loader" class="col-md-3 col-xs-3 float-none" >
            <cetner><img src="<?php echo $this->webroot . 'files/8.gif'; ?>">
                Related Speeli articles
            </cetner>
        </div> 
        <div class="input-cat">

        </div>
        <br class="clear">

    </div>

    <div  class="clearfix visible-xs-block col-md-12 col-xs-12 float-none"></div>
    <div class="middle-line">
        <center><h3 style="color:white;" class="">
                <?php echo $update_title; ?>
            </h3></center>     
    </div>
    <br class="clear">
    <?php if (isset($questions) && !empty($questions)) {
        ?>
        <div style='margin-top:2em;' class="selected-speeli col-md-12 float-none">
            <?php if (!($this->Session->read('user_quesli'))) { ?>
                <center><h3 class="signup"><a style="color:#8B8C8C" href="<?php echo Router::url(array('controller' => 'users', 'action' => 'login')) ?>">
                            <font style="color:#63B5B2">Sign up</font> to get Speeli articles matching your interests</a></h3></center>
            <?php }
            ?>
            <br class="clear">
            <?php
            $i = 0;

            for ($q = 0; $q < count($questions); $q++) {

                if ($questions[$q]['Question']['image'] != NULL) {
                    if (!empty($questions[$q]['Question']['last'])) {
                        $title = $questions[$q]['Question']['last'];
                    } else {
                        if (!empty($questions[$q]['Question']['title_update'])) {
                            $explode_title = explode(',,,', $questions[$q]['Question']['title_update']);
                            $last = end($explode_title);
                            $title = end(unserialize(base64_decode($last)));
                        } else {
                            $title = $questions[$q]['Question']['title'];
                        }
                    }

                    if ($i < 2) {
                        ?>
                        <div class="col-md-6 col-xs-12 home-thumb">
                            
                            <div class="category_top">
                                <a title='explore  more <?php echo $questions[$q]['MainCategory']['name'];?>'href="<?php echo Router::url(array('controller'=>'categories','action'=>'category',$questions[$q]['MainCategory']['slug']) ) ;?>">
                                    <?php echo $questions[$q]['MainCategory']['name'] ?>
                               
                            </div>
                            
                            <div class="thumbnail">
                                <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $questions[$q]['Question']['slug'])) ?>">
                                    <img style="width:100%; height: 20em;" class="img-responsive" src="<?php echo $this->webroot . "question/" . $questions[$q]['Question']['id'] . "/" . $questions[$q]['Question']['image']; ?>">
                                </a>
                                <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $questions[$q]['Question']['slug'])) ?>">
                                    <div class="panel-footer" style="height: 6em;">
                                        <h3 class="text-fit" style="color:white;" class="image-title"><?php echo $title ?></h3>
                                    </div>
                                </a>
                            </div>    
                        </div>
                    <?php } elseif ($i < 21) {
                        ?>
                        <div class="col-md-4 col-xs-12 home-thumb">
                            <?php if(!empty($questions[$q]['MainCategory']['name'])){ ?>
                            <div class="category_top">
                                <a title='explore  more <?php echo $questions[$q]['MainCategory']['name'];?>'href="<?php echo Router::url(array('controller'=>'categories','action'=>'category',$questions[$q]['MainCategory']['slug']) ) ?>">
                                    <?php echo $questions[$q]['MainCategory']['name'] ?>
                               
                            </div>
                            <?php } ?>
                            <div class="thumbnail">
                                <a   href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $questions[$q]['Question']['slug'])) ?>">
                                    <img style="width:100%; height: 20em;" class="img-responsive" src="<?php echo $this->webroot . "question/" . $questions[$q]['Question']['id'] . "/" . $questions[$q]['Question']['image']; ?>">

                                </a>
                                <a   href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $questions[$q]['Question']['slug'])) ?>">
                                    <div class="panel-footer" style="height: 6em;">
                                        <h3 class="text-fit" style="color:white;" class="image-title"><?php echo $title ?></h3>
                                    </div>
                                </a>
                            </div>   
                        </div>
                    <?php } ?>
                    <?php
                    $i++;
                    unset($questions[$q]);
                }
            }
            ?>
        </div>
        <div class="col-md-8 float-none update_art_speeli  " >
            <!-- The time line -->
            <div class="hpanel forum-box forum-wrapper">
                <div id="loader" class="col-md-3 col-xs-3 float-none" >
                    <cetner><img src="<?php echo $this->webroot . 'files/8.gif'; ?>">
                        Related Speeli articles
                    </cetner>
                </div>
                <?php
                $i = 1;
                if (isset($update_title)) {
                    echo "<center><h3 class='page-header'>$update_title</h3></center>";
                }
                $user = $this->Session->read('user_quesli');
                
                    foreach ($questions as $question) {
                        if (!empty($question['Question']['last'])) {
                            $title = $question['Question']['last'];
                        } else {
                            if (!empty($question['Question']['title_update'])) {
                                $explode_title = explode(',,,', $question['Question']['title_update']);
                                $last = end($explode_title);
                                $title = end(unserialize(base64_decode($last)));
                            } else {
                                $title = $question['Question']['title'];
                            }
                        }
                        if (!empty($title)) {
                            ?>
                            <div class="hpanel hblue article-item">
                                <div class="panel-heading hbuilt">
                                    <h3>
                                        <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $question['Question']['slug'])); ?>">
                                            <?php echo $title; ?>
                                        </a>
                                    </h3>
                                </div>
                                <br class="clear">
                            </div>
                            <br class="clear">
                            <?php
                        }
                        $i++;
                    }
                }
                ?>
                <!-- timeline time label -->
                <!-- END timeline item -->
                <!-- timeline item -->
            </div>
        </div><!-- /.col -->
    
</div>
<?php
echo $this->Paginator->next('Show more Speeli article...');
?>
<?php
echo $this->Html->script(array( 'jquery.infinitescroll.min'));
?>
<script type="text/javascript">

    $(document).keypress(function (event) {

        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {

            event.preventDefault();
            var questitle = $('#Title').val();
            $('#QuestionTitle').val(questitle);
            console.log(questitle);
            $.ajax({
                type: "get", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'homes', 'action' => 'search')); ?>",
                data: {questitle: questitle}, // outer quotes removed
                cache: false,
                beforeSend: function () {
                    $("#loader").fadeIn();
                },
                success: function (response, status) {
                    $("#loader").fadeOut();
                    $('.input-cat').html(response);
                    $('html, body').stop().animate({
                        scrollTop: $('.btn-fb').offset().top
                    }, 2000, 'easeInOutExpo');
                    event.preventDefault();
                },
            });
        }
    });
    $('.check_status').on('click', function (event) {

        event.preventDefault();
        var questitle = $('#Title').val();
        $('#QuestionTitle').val(questitle);
        $.ajax({
            type: "get", // Request method: post, get
            url: "<?php echo Router::url(array('controller' => 'homes', 'action' => 'search')); ?>",
            data: {questitle: questitle}, // outer quotes removed
            cache: false,
            beforeSend: function () {
                $("#loader").fadeIn();
            },
            success: function (response, status) {
                $("#loader").fadeOut();
                $('.input-cat').html(response);
                $('html, body').stop().animate({
                    scrollTop: $('.btn-fb').offset().top
                }, 2000, 'easeInOutExpo');
                event.preventDefault();
            },
        });
    });


</script>
<script>
    $(function () {
        var $container = $('.forum-wrapper');

        $container.infinitescroll({
            navSelector: '.next', // selector for the paged navigation 
            nextSelector: '.next a', // selector for the NEXT link (to page 2)
            itemSelector: '.article-item', // selector for all items you'll retrieve
            debug: true,
            dataType: 'html',
            loading: {
                finishedMsg: 'No more Speeli articles to load',
                img: '<?php echo $this->webroot; ?>files/8.gif'
            }
        }
        );
    });

</script>