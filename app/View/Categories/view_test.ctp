<style>
    #loader{
        display:none;
    }

    .home-thumb{
        padding: 0;
    }
    .panel-footer{
        padding: 0;
        background-color: rgba(141, 205, 203, 0.82);
    }
    .page-header{
        margin-top: 0;
        float: none;
    }
    .middle-line{
        width: 100%;
        background-color: rgba(99, 181, 178, 0.78);
        left: 0;
        top: 17em;
        margin-left: -7em;
        -webkit-box-shadow: 0 2px 7px rgba(0, 0, 0, 0.25);
        box-shadow: 0 2px 7px rgba(0, 0, 0, 0.25);
        -ms-filter: "progid:DXImageTransform.Microsoft.Shadow(Strength=3,Direction=180,Color='#333333')";
        filter: progid:DXImageTransform.Microsoft.Shadow(Strength=3, Direction=180, Color='#333333');
        margin: 0 auto;
    }
    .forum-box{
        margin: 1em;
    }
    .progress{
        display: none;
    }
    .panel-footer,.panel-heading{
        text-align: center;
    }
    .panel-footer h3{
        margin-top: 0;
        padding-top: 0;
    }
    hr{
        border: 0;
        height: 1px;
        background-image: -webkit-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -moz-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -ms-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -o-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
    }
    .clear{
        clear: both;
    }
    .question_wrapper{
        margin: 0 auto;
    }
    .float-none{
        float:none;
        margin: 0 auto;
        clear:both;
    }
    .hblue{
        background-color: #fff;
        padding: 1em;
        border-radius: 4px;
        -moz-border-radius: 4px;
        -webkit-border-radius: 4px;
        margin-bottom: 25px;
        position: relative;
        line-height: 25px;
        border: 1px solid #e5e5e5;
    }
    .hblue .panel-heading{
        padding: 10px 15px;
        background-color: rgba(245, 245, 245, 0.24);

        border-bottom-right-radius: 3px;
        border-bottom-left-radius: 3px;
    }
    .list-unstyled li{
        margin-left: 1em;
    }
    .panel-footer h3 {
        padding-top: 0.3em;
    }

    #Title {
        margin: auto;
    }
    #searchbox .check_status.btn.btn-main{
        margin-left: 5px;
    }

    input[type="text"], .login-form-text {
        width: 90%;
    }
    @media only screen and (max-width: 768px){
        #searchbox{
            margin-top: 3em;
        }
        .signup{
            font-size: 1.1em;
        }
        .fa-toggle-menu {
            display: none !important;
        }
        .panel-footer h3{
            font-size: 1.1em;
            text-align: center;
        }
        .panel-heading h3{
            font-size: 0.9em;
            text-align: left;
            padding: 0;
            margin: 0;
        }
        .middle-line{
            top:9em;
        }


    }


    @media only screen and (max-width: 560px){

        #searchbox input[type="text"], .login-form-text
        {
            width: 78%;

        }
        #searchbox .check_status.btn.btn-main{
            width:14%
        }


    }





</style>
<?php
$this->set("meta_description", "Latest Speeli articles");
$this->set("meta_robots", "noindex");
?>

<?php 

$this->set("page_title", $category_name); ?>
<div class="row">


    <!-- Add the extra clearfix for only the required viewport -->
    <div  class="clearfix visible-xs-block"></div>
    <div id="searchbox" class="row question">
        <div class="col-md-12 col-xs-12">    
            <?php
            echo $this->Form->create('Question');
            ?>
            <?php
            echo $this->Form->input('title', array('id' => 'Title', 'placeholder' => 'Search Speeli summaries .. . ', 'class' => 'login-form-text flex-column',
                'div' => false, 'label' => false));
            ?>
            <div class="check_status  btn btn-main"><i class="fa fa-search"></i></div></h1>
        </div>
        <br class="clear">
        <div id="loader" class="col-md-3 col-xs-3 float-none" >
            <cetner><img src="<?php echo $this->webroot . 'files/8.gif'; ?>">
                Related Speeli articles
            </cetner>
        </div> 
        <div class="input-cat">

        </div>
        <br class="clear">

    </div>

    <div  class="clearfix visible-xs-block col-md-12 col-xs-12 float-none"></div>
    <div class="middle-line">
        <center><h3 style="color:white;"> 
                <?php echo $category_name; ?> </h3></center>  
    </div>
    <br class="clear">

    <div class="row ">

        <?php if (isset($categories) && !empty($categories)) { ?>
            <div style='margin-top:2em;' class="selected-speeli col-md-12 float-none">
                <?php if (!($this->Session->read('user_quesli'))) { ?>
                    <center><h3 class="signup"><a style="color:#8B8C8C" href="<?php echo Router::url(array('controller' => 'users', 'action' => 'login')) ?>">
                                <font style="color:#63B5B2">Sign up</font> to get Speeli articles matching your interests</a></h3></center>
                <?php }
                ?>
                <br class="clear">
                <?php
                $i = 0;

                for ($q = 0; $q < count($categories); $q++) {
                    if ($categories[$q]['Question']['image'] != NULL) {
                        if (!empty($categories[$q]['Question']['last'])) {
                            $title = $categories[$q]['Question']['last'];
                        } else {
                            if (!empty($categories[$q]['Question']['title_update'])) {
                                $explode_title = explode(',,,', $categories[$q]['Question']['title_update']);
                                $last = end($explode_title);
                                $title = end(unserialize(base64_decode($last)));
                            } else {
                                $title = $categories[$q]['Question']['title'];
                            }
                        }

                        if ($i < 2) {
                            ?>
                            <div class="col-md-6 col-xs-12 home-thumb">
                                <div class="thumbnail">
                                    <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $categories[$q]['Question']['slug'])) ?>">
                                        <img style="width:100%; height: 20em;" class="img-responsive" src="<?php echo $this->webroot . "question/" . $categories[$q]['Question']['id'] . "/" . $categories[$q]['Question']['image']; ?>">
                                    </a>
                                    <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $categories[$q]['Question']['slug'])) ?>">
                                        <div class="panel-footer" style="height: 6em;">
                                            <h3 class="text-fit" style="color:white;" class="image-title"><?php echo $title ?></h3>
                                        </div>
                                    </a>
                                </div>    
                            </div>
                        <?php } elseif ($i < 8) {
                            ?>
                            <div class="col-md-4 col-xs-12 home-thumb">
                                <div class="thumbnail">
                                    <a   href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $categories[$q]['Question']['slug'])) ?>">
                                        <img style="width:100%; height: 20em;" class="img-responsive" src="<?php echo $this->webroot . "question/" . $categories[$q]['Question']['id'] . "/" . $categories[$q]['Question']['image']; ?>">

                                    </a>
                                    <a   href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $categories[$q]['Question']['slug'])) ?>">
                                        <div class="panel-footer" style="height: 6em;">
                                            <h3 class="text-fit" style="color:white;" class="image-title"><?php echo $title ?></h3>
                                        </div>
                                    </a>
                                </div>   
                            </div>
                        <?php } ?>
                        <?php
                        $i++;
                        unset($categories[$q]);
                    }
                }
                ?>
            </div>
        <?php } ?>
        <div class="clearfix"></div>
        <div class="col-md-12 float-none " >

            <div class="col-md-8 col-xs-12 float-none" style="padding:1em;">
                <div class="thumbnail">
                    <div class="hpanel forum-box">
                        <center>  <h4 class="page-header">New Speeli <b style="color:#63B5B2;"><?php echo $category_name ?></b> articles</h4></center>
                        <?php
                        $i = 1;

                        $user = $this->Session->read('user_quesli');
                        foreach ($categories as $question) {

                            if (!empty($question['Question']['last'])) {
                                $title = $question['Question']['last'];
                            } else {
                                if (!empty($question['Question']['title_update'])) {
                                    $explode_title = explode(',,,', $question['Question']['title_update']);
                                    $last = end($explode_title);
                                    $title = end(unserialize(base64_decode($last)));
                                } else {
                                    $title = $question['Question']['title'];
                                }
                            }
                            ?>
                            <div class="hpanel hblue">
                                <div class="panel-heading hbuilt" style='padding:1px;'>
                                    <h3 class="text-fit"> 
                                        <a  class="text-fit"
                                            href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $question['Question']['slug'])); ?>"> <?php echo htmlspecialchars($title); ?></a>  </h3>
                                </div>


                            </div>
                            <?php
                            $i++;
                            if ($i > 10) {
                                break;
                            }
                        }
                        ?>
                        <!-- timeline time label -->
                        <!-- END timeline item -->
                        <!-- timeline item -->
                    </div>
                </div>
            </div>

        </div><!-- /.row -->
    </div><!-- /.row -->
</div><?php
echo $this->Html->script(array('jquery.nicefileinput.min'));
?><script>
    $('document').ready(function () {
        $(document).ready(function () {
            // your code...
            $("input[type=file]").nicefileinput();
        });
        $("#FileImage1").nicefileinput({
            label: 'Upload your new photo...' // Spanish label
        });
        /* update image start*/
        $('body').undelegate('.update-image', 'click').delegate('.update-image', 'click', function (e) {
            var form = $(this).parents('form');
            e.preventDefault();
            if (document.getElementById('FileImage1').value != '') {
                start_upload(form);
            } else {
                alert('choose file');
            }
        });
        function generate(quesuserd, quesuserv) {
            return generatble = ' <h3 quesuserd=' + quesuserd + '>' + quesuserv + '</h3> <div class="btn btn-success edit edit-bio">edit</div>';
        }
        function start_upload(form) {
            var data = new FormData();

            var UploadedFile = document.getElementById("FileImage1");
            for (var i = 0; i < UploadedFile.files.length; i++) {
                data.append('data[Logo][image]', UploadedFile.files[i]);
            }
            //console.dir(UploadedFile.files);
            var request = new XMLHttpRequest();
            $('.progress').show();
            $('#upload_progress').addClass('active');
            $('#upload_progress').show(1);
            request.upload.addEventListener('load', function () {
                $('#upload_progress').removeClass('active');
                $('#upload_progress').hide(200);
            });
            request.upload.addEventListener('progress', function (event) {
                if (event.lengthComputable) {
                    var progress = Math.round(100 * event.loaded / event.total);
                    $('#upload_progress .bar').css({'width': progress + '%'});
                }
            });

            request.open("POST", '<?php echo Router::url(array('controller' => 'users', 'action' => 'logo')); ?>');
            request.onreadystatechange = function ()
            {
                if (request.readyState == 4 && request.status == 200)
                {
                    var url = request.responseText;

                    $('#profile_img').attr("src", url);

                }
            }
            request.setRequestHeader('Cache-Control', 'no-cache');
            request.send(data);
        }
        ;
        /* update image end*/

        $('.edit').on('click', function () {
            event.preventDefault();

            var target = $(this).attr('target');
            var text_change = $(this).html();
            console.log(text_change);
            if (text_change == 'edit') {
                $(this).html('update');
                $(this).parent().find('h3').attr('contenteditable', 'true');
                $(this).attr('update', target);
                $(this).parent().find('h3').focus();
            } else {
                $(this).html('edit');
                $(this).parent().find('h3').removeAttr('contenteditable');
                var quesuserv = $(this).parent().find('h3').html();
                var quesuserd = $(this).parent().find('h3').attr('quesuserd');

                $(this).removeAttr('update');
                $.post("<?php echo Router::url(array('controller' => 'users', 'action' => 'update')); ?>",
                        {
                            quesuserv: quesuserv,
                            quesuserd: quesuserd
                        },
                function (data) {

                });
            }
        });

        $('.add').on('click', function () {
            event.preventDefault();
            var text_change = $(this).html();

            if (text_change == 'add') {

                $(this).html('edit');
                var quesuserd = $(this).attr('quesuserd');

                var quesuserv = $(this).parent().find('#quesuserput').val();
                console.log(quesuserv);
                $.post("<?php echo Router::url(array('controller' => 'users', 'action' => 'add_profile')); ?>",
                        {
                            quesuserv: quesuserv,
                            quesuserd: quesuserd
                        },
                function (data) {
                    console.log('wla 7mo ltmsa7 2l');

                    $(this).parent().find('#quesuserput').remove();
                });
            }
        });


    });
</script>