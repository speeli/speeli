<style>
    .progress{
        display: none;
    }
    hr{
        border: 0;
        height: 1px;
        background-image: -webkit-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -moz-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -ms-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -o-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
    }
    .clear{
        clear: both;
    }
    .question_wrapper{
        margin: 0 auto;
    }
    .float-none{
        float:none;
        margin: 0 auto;
    }
    .hblue{
        background-color: #fff;
        padding: 1em;
        border-radius: 4px;
        -moz-border-radius: 4px;
        -webkit-border-radius: 4px;
        margin-bottom: 25px;
        position: relative;
        line-height: 25px;
        border: 1px solid #e5e5e5;
    }
    .hblue .panel-heading{
        padding: 10px 15px;
        background-color: rgba(245, 245, 245, 0.24);
        border-bottom-right-radius: 3px;
        border-bottom-left-radius: 3px;
    }
    .list-unstyled li{
        margin-left: 1em;
    }
</style>
<?php $this->set("page_title",  $categories[0]['Category']['name']); ?>
<div class="row">


    <!-- Add the extra clearfix for only the required viewport -->
    <div  class="clearfix visible-xs-block"></div>
    <br class="clear"><br class="clear">

  <br class="clear">
  <center><h2 class="page-header"> 
          <b><?php echo $categories[0]['Category']['name']; ?></b> </h2></center>
    <div class="row ">
     
        <div class="col-md-8 float-none ">
            <!-- The time line -->
            <div class="hpanel forum-box">
                <?php
                $i = 1;
                foreach ($categories as $question) {
                    if(!empty($question['Question']['title'])){
                    ?>
                    <div class="hpanel hblue">
                        <div class="panel-heading hbuilt">
                            <h3> <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view',$question['Question']['slug'])); ?>"> <?php echo $question['Question']['title'];?></a>  </h3>
                        </div>
                     

                    </div>
                    <?php
                    $i++;
                }
                }
                ?>
                <!-- timeline time label -->
                <!-- END timeline item -->
                <!-- timeline item -->
            </div>
        </div><!-- /.col -->
    </div><!-- /.row -->
</div><?php
echo $this->Html->script(array('jquery.nicefileinput.min'));
?><script>
    $('document').ready(function () {
        $(document).ready(function () {
            // your code...
            $("input[type=file]").nicefileinput();
        });
        $("#FileImage1").nicefileinput({
            label: 'Upload your new photo...' // Spanish label
        });
        /* update image start*/
        $('body').undelegate('.update-image', 'click').delegate('.update-image', 'click', function (e) {
            var form = $(this).parents('form');
            e.preventDefault();
            if (document.getElementById('FileImage1').value != '') {
                start_upload(form);
            } else {
                alert('choose file');
            }
        });
        function generate(quesuserd, quesuserv) {
            return generatble = ' <h3 quesuserd=' + quesuserd + '>' + quesuserv + '</h3> <div class="btn btn-success edit edit-bio">edit</div>';
        }
        function start_upload(form) {
            var data = new FormData();

            var UploadedFile = document.getElementById("FileImage1");
            for (var i = 0; i < UploadedFile.files.length; i++) {
                data.append('data[Logo][image]', UploadedFile.files[i]);
            }
            //console.dir(UploadedFile.files);
            var request = new XMLHttpRequest();
            $('.progress').show();
            $('#upload_progress').addClass('active');
            $('#upload_progress').show(1);
            request.upload.addEventListener('load', function () {
                $('#upload_progress').removeClass('active');
                $('#upload_progress').hide(200);
            });
            request.upload.addEventListener('progress', function (event) {
                if (event.lengthComputable) {
                    var progress = Math.round(100 * event.loaded / event.total);
                    $('#upload_progress .bar').css({'width': progress + '%'});
                }
            });

            request.open("POST", '<?php echo Router::url(array('controller' => 'users', 'action' => 'logo')); ?>');
            request.onreadystatechange = function ()
            {
                if (request.readyState == 4 && request.status == 200)
                {
                    var url = request.responseText;

                    $('#profile_img').attr("src", url);

                }
            }
            request.setRequestHeader('Cache-Control', 'no-cache');
            request.send(data);
        }
        ;
        /* update image end*/

        $('.edit').on('click', function () {
            event.preventDefault();

            var target = $(this).attr('target');
            var text_change = $(this).html();
            console.log(text_change);
            if (text_change == 'edit') {
                $(this).html('update');
                $(this).parent().find('h3').attr('contenteditable', 'true');
                $(this).attr('update', target);
                $(this).parent().find('h3').focus();
            } else {
                $(this).html('edit');
                $(this).parent().find('h3').removeAttr('contenteditable');
                var quesuserv = $(this).parent().find('h3').html();
                var quesuserd = $(this).parent().find('h3').attr('quesuserd');

                $(this).removeAttr('update');
                $.post("<?php echo Router::url(array('controller' => 'users', 'action' => 'update')); ?>",
                        {
                            quesuserv: quesuserv,
                            quesuserd: quesuserd
                        },
                function (data) {

                });
            }
        });

        $('.add').on('click', function () {
            event.preventDefault();
            var text_change = $(this).html();

            if (text_change == 'add') {

                $(this).html('edit');
                var quesuserd = $(this).attr('quesuserd');

                var quesuserv = $(this).parent().find('#quesuserput').val();
                console.log(quesuserv);
                $.post("<?php echo Router::url(array('controller' => 'users', 'action' => 'add_profile')); ?>",
                        {
                            quesuserv: quesuserv,
                            quesuserd: quesuserd
                        },
                function (data) {
                    console.log('wla 7mo ltmsa7 2l');

                    $(this).parent().find('#quesuserput').remove();
                });
            }
        });


    });
</script>