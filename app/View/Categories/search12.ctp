<style>
    .clear{
        clear:both;
    }
    .js-example-placeholder-multiple{
        width: 100%;
    }
    .select2{
        max-height: 10%;
    }

</style>

<ol class="rounded-list" style="width: 100%; float:left;">
    <?php
    $this->Paginator->options(array(
        'update' => '.input-cat',
        'evalScripts' => true,
        'before' => $this->Js->get('#loader')->effect(
                'fadeIn', array('buffer' => false)
        ),
        'complete' => $this->Js->get('#loader')->effect(
                'fadeOut', array('buffer' => false)
        ),
    ));
    if (!empty($questions)) {
        foreach ($questions as $question) {
            ?>
            <?php
            if (!empty($question['Question']['title_update'])) {
                $explode_title = explode(',,,', $question['Question']['title_update']);
                $last = end($explode_title);
                $title = end(unserialize(base64_decode($last)));
            } else {
                $title = $question['Question']['title'];
            }
            $title = preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', $title);
            echo"<li>" . $this->Html->link($title, array('controller' => 'articles', 'action' => 'view', $question['Question']['slug']), array('class' => 'navbar-brand', 'escape' => false,)
            ) . "</li>";
            ?>
            <?php
        }
    } else {
        echo "<br class='clear'><div class='ask-new'> ";
        ?>
        <?php
        echo "</div>";
    }
    echo $this->Html->css(array('lists'));
    ?>
</ol>
<?php
if ($this->Paginator->params()['nextPage'] == true) {
    echo $this->Paginator->next('Show more results ... ', array('limit' => '6'));
}
echo $this->Js->writeBuffer();
echo '<br class="clear">';echo '<br class="clear">';
?>
<?php if(!empty($questions)){ ?>
<a href="<?php echo Router::url(array('controller' => 'homes', 'action' => 'request')) ?>" 
   class="iframe-request stop">Can't find what you are looking for ? <span> Request a Speeli summary ?</span>
</a>    
<?php } else{ ?>
<a href="<?php echo Router::url(array('controller' => 'homes', 'action' => 'request')) ?>" 
   class="iframe-request stop">No Speeli summaries found . <span> Request a Speeli summary ?</span>
</a>    
<?php } ?>

<br class="clear">

<script>
    if (typeof window.orientation !== 'undefined') {
        $(".iframe-request").colorbox({opacity: '0.5', width: '100%', height: '80%', top: '10px'});
    } else {
        $(".iframe-request").colorbox({opacity: '0.5', width: '50%', height: '70%', top: '50px'});
    }
</script>