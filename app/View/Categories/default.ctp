
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

        <title>
            <?php if (isset($page_title) && !empty($page_title)) { ?>
                الحلقة-  <?php echo $page_title; ?>
                <?php
            } else {
                echo 'الحلقة -خلي للفكرة سكة';
            }
            ?>

        </title>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-74373090-1', 'auto');
            ga('send', 'pageview');

        </script>
        <meta name="description" content="بيع.اشتري .اتعلم .اعرض 
              كل ما يندرج تحت مسمي الابداع والابتكار والاعمال اليدوية ,بداية من الشغل اليدوي البسيط إلي الابتكارات الالكترونية والهندسية ">
        <meta name="keywords" content="بيع, اشتري, اتعلم ,اعرض,فن ,اعمال يدوية ,اعمال كروشية ,ديكور ">

        <?php if (isset($meta_robots) && !empty($meta_robots)) { ?>

            <?php
        } else {
            echo '<meta name="robots" content="noindex" >';
        }
        ?>

        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php
        echo $this->Html->css(array('bootstrap.min', 'font-awesome.min', 'all', 'reset', 'full-slider', 'handmade'));
        echo $this->Html->script(array('jquery-1.11.2.min', 'bootstrap.min'));
        ?>
        <link rel="shortcut icon" href="<?php echo $this->webroot . 'img/logo.ico'; ?>" type="image/x-icon">
        <style>
            .wrapper{
                position: absolute;
                width:100%;
            }
            .cyan{color:#63B5B2;}
            .page{
                width: 100%;
            }
            a,li,h1,h2,h3,h4,h5,h6,p,div{
                font-family:'DroidArabicKufi-Regular', sans-serif, Arial;

            }
            .header-container-nav{
                width: 96%;
            }
            @media only screen and (max-width: 768px){
                .navbar-brand{
                    padding: 7px;
                }
                .navbar-brand img{
                    width: 4em;
                }

            }
            a{
                font-size:normal normal 16px/1.4;
            }
            .mb-10 img{
                width: 120px;
                height: 70px;
            }
            ul.navbar{
                direction: rtl;
            }
            .navbar a {
                padding: 0.2em;
            }

        </style>
    </head>
    <body class=" cms-index-index cms-porto-home-2-new">

        <div class="navbar yamm navbar-default navbar-fixed-top">
            <div class="container header-container-nav">
                <div class="navbar-header">
                    <button type="button" data-toggle="collapse" data-target="#navbar-collapse-1" class="navbar-toggle">
                        <span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button>
                    <a href="<?php echo Router::url('/'); ?>" class="navbar-brand"><img src="<?php echo $this->webroot . 'img/logo.png' ?>"></a>
                </div>
                <div id="navbar-collapse-1" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav right">

                        <?php
                        if ($this->Session->check('user_handmade')) {

                            $session = $this->Session->read('user_handmade');
                            ?>
                            <li class="yamm-fw">
                                <a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'logout')); ?>">
                                    <span class="fa fa-sign-out">تسجيل خروج   </span>
                                </a>
                            </li>
                            <?php
                            if ($session['User']['type'] == 2) {
                                ?>
                                <li class="yamm-fw">
                                    <a href="<?php echo Router::url('/my_gallery'); ?>">
                                        <span class="fa fa-shopping-bag">معرضي   </span>
                                    </a>
                                </li>

                            <?php } elseif ($session['User']['type'] == 1) {
                                ?>
                                <li class="yamm-fw">
                                    <a href="<?php echo Router::url('/profile'); ?>">
                                        <span class="fa fa-shopping-bag">ابداعاتي   </span>
                                    </a>
                                </li>
                            <?php } else { ?>
                                <li class="yamm-fw">
                                    <a href="<?php echo Router::url('/category/'); ?>">
                                        <span class="fa fa-shopping-bag">حسابي   </span>
                                    </a>
                                </li>
                            <?php } ?>

                        <?php } else { ?>

                        <?php } ?>

                        <li class="yamm-fw">
                            <a href="<?php echo Router::url('/news/'); ?>"><span class="fa fa-lightbulb-o">! اعملها بنفسك   </span></a>
                        </li>

                        <li class="yamm-fw">
                            <a href="<?php echo Router::url('/arts'); ?>">
                                <span class="fa fa-paint-brush">معرض المواهب   </span>
                            </a>
                        </li>
                        <li class="yamm-fw">
                            <a href="<?php echo Router::url('/category/'); ?>">
                                <span class="fa fa-shopping-bag">السوق   </span>
                            </a>
                        </li>
                        <li class="yamm-fw">
                            <a href="<?php echo Router::url('/'); ?>">
                                <span class="fa fa-home">الرئيسية   </span>
                            </a>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
        <div class="clearfix clear"></div>


        <div class="wrapper">




            <div class="mobile-nav-overlay close-mobile-nav"></div>

            <?php echo $this->fetch('content'); ?>
            <div class="footer">
                <div class="container">
                    <div class="footer-inner">
                        <div class="row">
                            <div class="col-md-12 col-xs-12"
                                 style="height: 4.5em;background-image:url(<?php echo $this->webroot . 'img/footer.png' ?>);background-size:50%;background-position:50%;background-repeat:no-repeat;"
                                 ></div>    
                        </div>

                        <br class="clear">
                        <ul class="social-links circle animated-effect-1">
                            <li class="facebook"><a target="_blank" href="https://www.facebook.com/events/928312463933898/"><i class="fa fa-facebook"></i></a></li>
                            <li class="twitter"><a target="_blank" href="#"><i class="fa fa-twitter"></i></a></li>
                            <li class="instagram"><a target="_blank" href="#"><i class="fa fa-instagram"></i></a></li>
                            <li class="youtube"><a target="_blank" href="https://www.youtube.com/channel/UC3s1iOZCgq8fJpu5TSnL6qw"><i class="fa fa-youtube"></i></a></li>
                        </ul>
                        <div class="separator-2"></div>
                        <ul class="list-icons">
                            <li><i class="fa fa-phone pr-10 text-default"></i> 010 9881 1829</li>
                            <li><a href="mailto:info@theproject.com"><i class="fa fa-envelope-o pr-10"></i>info@el7alaka.com</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <?php
            echo $this->Html->css(array('colorbox'));
            ?>

            <?php
            echo $this->Html->script(array('jquery.colorbox-min'));
            ?>
            <script type="text/javascript">
                $(document).ready(function () {
                    resizeContent();
                    $(window).resize(function () {
                        resizeContent();
                    });
                    function resizeContent() {
                        $height = $('.navbar-fixed-top').height();
                        $('.wrapper').css({'top': $height});
                    }

                });
                jQuery(document).ready(function () {
                    $(".dropdown").hover(
                            function () {
                                $('.dropdown-menu', this).fadeIn("fast");
                            },
                            function () {
                                $('.dropdown-menu', this).fadeOut("fast");
                            });
                });



            </script>                
        </div>
    </div>
    <?php
    echo $this->Html->script(array('modernizr', 'jquery.mobile.custom.min'));
    ?>

</body>

</html>
