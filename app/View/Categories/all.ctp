<style>
    .clear{
        clear:both;
    }
</style>

<br class="clear">

<table class="table table-hover table-responsive col-md-12 col-xs-12">
    <tr>
        <th><?php echo $this->Paginator->sort('id', 'No') ?></th>   
        <th><?php echo $this->Paginator->sort('name', 'Topic name') ?></th>
         <th>Total No of articles</th>
        <th>Total No of articles views</th>
        <th><?php echo $this->Paginator->sort('views', 'Number of topic views') ?></th>
        <th><?php echo $this->Paginator->sort('created', 'Date created') ?></th>
        <th>Actions</th>

    </tr>

    <?php
    $i = 0;
    foreach ($categories as $category) {
        ?>
        <?php
        if (!empty($category['Category']['name'])) {
            $i++;
            ?>
            <tr>
                <td><?php echo $category['Category']['id']; ?></td>
                <td class="col-md-2 col-xs-4" >
                    <a  href="<?php
                    echo Router::url(array('controller' => 'categories',
                        'action' => 'view', $category['Category']['slug']))
                    ?>" class="btn  btn-main left"><?php echo $category['Category']['name']; ?>
                    </a>
                </td>
                <td><?php echo $category['Category']['count'][0][0]['q_count']; ?></td> 
                <td><?php echo $category['Category']['count'][0][0]['q_views']; ?></td>
                 
                <td><?php echo $category['Category']['views']; ?></td>
                <td><?php echo $category['Category']['created']; ?></td>

                <td class="col-md-2">
                    <div class="btn btn-danger" target="<?php echo $category['Category']['id'] ?>"><span class="fa fa-trash"></span></div>
                    <a class='col-md-2 iframe-edit' style='color:rgba(136, 136, 136, 0.88);' href="<?php
                    echo Router::url(array('controller' => 'categories',
                        'action' => 'edit', $category['Category']['id']))
                    ?>" class=" "><span class="fa fa-edit"></span>

                    </a> 
                </td>
            </tr>
            <?php ?>



        <?php }
    } ?>
</table>

<ul class="pagination">
    <?php
    echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
    echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'currentClass' => 'active', 'tag' => 'li', 'first' => 1));
    echo $this->Paginator->next(__('next'), array('tag' => 'li', 'currentClass' => 'disabled'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
    ?>
</ul>
<?php
echo $this->Html->css(array('sweet-alert'));
echo $this->Html->script(array('sweet-alert'));
?>
<script>
    $(document).ready(function () {
        $(".iframe-edit").colorbox({opacity: '0.5', width: '50%', top: '10px'});
    });
    $('.btn-danger').click(function () {
        target = $(this).attr('target');
        swal({
            title: "Confirm Delete?",
            type: "warning", showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Delete",
            cancelButtonText: "Cancel",
            closeOnConfirm: false, closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.post("<?php echo Router::url(array('controller' => 'categories', 'action' => 'delete')); ?>",
                        {
                            target: target,
                        },
                        function (data) {
                            swal("Delete");
                        });
            } else {
                swal("Canceling", ")", "error");

            }
        });
    });
    $('.new-cat-value').keyup(function () {
        var data_send = $(this).val();
        var cat_id = $(this).attr('target');
        console.log($('.results_' + cat_id));

        $.ajax({
            type: "post", // Request method: post, get
            url: "<?php echo Router::url(array('controller' => 'categories', 'action' => 'search_parent')); ?>",
            data: {data_send: data_send, cat_id: cat_id}, // outer quotes removed
            cache: false,
            success: function (response, status) {

                $('.results_' + cat_id).html(response);
            },
        });
    });


</script>
