<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:og="http://ogp.me/ns#"
      xmlns:fb="https://www.facebook.com/2008/fbml">
    <head>
        <title>

            <?php if (isset($page_title) && !empty($page_title)) { ?>
                <?php echo $page_title; ?>
                <?php
            } else {
                echo 'Speeli';
            }
            ?>

        </title>
        <meta name="viewport" content="width=device-width, initial-scale=1">

            <link rel="shortcut icon" href="<?php echo $this->webroot . 'files/favicon.ico'; ?>" type="image/x-icon">
                <?php
                echo $this->Html->css(array('login', 'bootstrap.min'));
                echo $this->fetch('css');
                echo $this->fetch('script');
                echo $this->Html->script(array('jquery-1.11.3.min', 'bootstrap.min'));
                ?>

                <!--SCRIPTS-->
                <!--Slider-in icons-->
                <style>
                    #login_button{
                        float:left;
                        width: 100%;
                    }
                </style>
                <div id="fb-root"></div>

                <link rel="stylesheet" type="text/css" href="/css/font-awesome.min.css" />
                </head>
                <body>

                    <style>
                        .btn{
                            cursor: pointer;
                        }
                    </style>
                    <!--WRAPPER-->

                    <style>
                        .clear{
                            clear:both;
                        }
                        .float-none{
                            float:none;
                            margin: 0 auto;
                        }
                        /* start registration */
                        .registration{
                            padding: 3% 1%;
                        }
                        .registration h2{
                            font-size:1.5em;
                            color: #00405d;
                            text-transform:capitalize;
                        }
                        .panel-heading{
                            background-color: none;
                        }
                        .registration span{
                            color: #777777;
                        }
                        .registration_form{
                            display: block;
                        }
                        .registration_form div{
                            padding:0.2em 0;
                        }
                        .sky_form1{
                            margin-bottom: -30px;
                        }
                        .sky_form1 ul{
                            padding:0;
                            list-style:none;
                        }
                        .sky_form1 ul li{
                            float: right;
                            margin-right: 20px;
                        }
                        .sky_form1 ul li:first-child{
                            margin-right: 0;
                        }
                        label {
                            display: block;
                            margin-bottom: 0;
                            font-weight: normal;
                        }
                        .registration_form input[type="text"],.registration_form input[type="email"],.registration_form input[type="tel"],.registration_form input[type="password"] , form input[type="text"]{
                            padding: 8px;
                            display: block;
                            width:100%;
                            outline: none;
                            font-family: "gessmed", Tahoma, Geneva, sans-serif ;
                            font-size: 0.8925em;
                            color: #333333;
                            -webkit-appearance: none;
                            text-transform: capitalize;
                            background: #FFFFFF;
                            border: 1px solid rgb(231, 231, 231);
                            font-weight: normal;
                            text-align:left;
                            transition: 0.5s ease;
                            -moz-transition: 0.5s ease;
                            -o-transition: 0.5s ease;
                            -webkit-transition: 0.5s ease;
                        }
                        .registration_form input[type="text"]:focus,.registration_form input[type="email"]:focus,.registration_form input[type="tel"]:focus,.registration_form input[type="password"]:focus ,form input[type="text"]:focus
                        {

                            box-shadow: 1px 1px 10px #00405d;
                        }
                        .registration_form input[type="submit"]{
                            -webkit-appearance: none;
                            font-family: "gessmed", Tahoma, Geneva, sans-serif ;
                            color: #ffffff;
                            text-transform: capitalize;
                            display: inline-block;
                            background:#64B2B2;
                            padding: 10px 20px;
                            transition: 0.5s ease;
                            -moz-transition: 0.5s ease;
                            -o-transition: 0.5s ease;
                            -webkit-transition: 0.5s ease;
                            cursor:pointer;
                            border:none;
                            outline:none;
                            font-size:1.3em;
                            margin-bottom: 5px;
                            width: 100%;
                            border-radius: 3px;

                        }
                        .registration_form input[type="submit"]:hover{
                            color: #ffffff;
                            background:#2AC2C2;;
                        }
                        .panel-heading{
                            background-color: transparent !important;
                            border: none
                        }
                        .img-responsive{
                            padding: 0;
                            margin: -1em auto 0 auto;
                        }

                        input[type="radio"], input[type="checkbox"] {
                            margin: 4px 4px 4px 0;
                        }
                        .btn-fb{
                            color: #fff;
                            background-color: #3b5998;
                            width: 100%;
                            padding: 11px 20px;
                            border-radius: 0px;
                            text-align: center;
                            font-weight: bold;
                            display: block;
                            font-family: "gessmed", Tahoma, Geneva, sans-serif;
                            text-transform: capitalize;
                            transition: 0.5s ease;
                            -moz-transition: 0.5s ease;
                            -o-transition: 0.5s ease;
                            -webkit-transition: 0.5s ease;
                            border-radius: 3px;

                        }
                        .btn-fb:hover,.btn-fb:focus{
                            color: #fff;
                            background-color:#496ebc;
                            text-decoration: none;
                        }

                        i.fa.fa-facebook {
                            font-size: 20px;
                            margin-right: 10px;
                        }

                        @media only screen and (max-width:480px)
                        {
                            .imgcover{display:none}
                            .row{margin:0}
                            .registration_left.panel{margin:5px auto 0 auto}

                            .col-md-5.col-xs-5.float-none.img-responsive{
                                margin:0 auto
                            }
                        }

                    </style>
                    <!--END WRAPPER-->
                    <!-- START WRAPPER REGISTER-->
                    <div class="row">
                        <div id="row">
                            <div class="col-md-12 col-xs-12 imgcover" style="position: absolute;">
                                <img style="width:100vw; height: 97vh" src="<?php echo $this->webroot . 'img/cover2.png'; ?>">
                            </div>
                        </div>
                        <script>
                            window.fbAsyncInit = function () {
                                FB.init({
                                    appId: '835557989856782',
                                    cookie: true, // enable cookies to allow the server to access 
                                    // the session
                                    xfbml: true, // parse social plugins on this page
                                    version: 'v2.2' // use version 2.2
                                });
                            };
                            function on_login_click() {
                                function statusChangeCallback(response) {
                                    console.log('statusChangeCallback');
                                    console.log(response);
                                    if (response.status === 'connected') {
                                        // Logged into your app and Facebook.
                                        testAPI();
                                    } else if (response.status === 'not_authorized') {
                                        // The person is logged into Facebook, but not your app.
                                        document.getElementById('status').innerHTML = 'Please log ' +
                                                'into this app.';
                                    } else {
                                        // The person is not logged into Facebook, so we're not sure if
                                        // they are logged into this app or not.
                                        document.getElementById('status').innerHTML = 'Please log ' +
                                                'into Facebook.';
                                    }
                                }

                                function checkLoginState() {
                                    FB.getLoginStatus(function (response) {
                                        statusChangeCallback(response);
                                    });
                                }
                                function start_login() {
                                    checkLoginState();
                                }

                                // Load the SDK asynchronously
                                (function (d, s, id) {
                                    var js, fjs = d.getElementsByTagName(s)[0];
                                    if (d.getElementById(id))
                                        return;
                                    js = d.createElement(s);
                                    js.id = id;
                                    js.src = "//connect.facebook.net/en_US/sdk.js";
                                    fjs.parentNode.insertBefore(js, fjs);
                                }(document, 'script', 'facebook-jssdk'));

                                // Here we run a very simple test of the Graph API after login is
                                // successful.  See statusChangeCallback() for when this call is made.
                                function testAPI() {
                                    console.log('Welcome!  Fetching your information.... ');
                                    FB.api('/me', {fields: "id,name,gender,email"}, function (response) {
                                        console.log('Successful login for: ' + response.name);
                                        $.ajax({
                                            type: "post", // Request method: post, get
                                            url: "<?php echo Router::url(array('controller' => 'users', 'action' => 'check')); ?>",
                                            data: {response: response}, // outer quotes removed
                                            cache: false,
                                            success: function (response, status) {
                                                window.location = "<?php echo Router::url('/feed'); ?>";
                                            },
                                        });
                                    });
                                }
                            }
                            function statusChangeCallback(response) {
                                console.log('statusChangeCallback');
                                console.log(response);
                                if (response.status === 'connected') {
                                    // Logged into your app and Facebook.
                                    testAPI();
                                } else if (response.status === 'not_authorized') {
                                    // The person is logged into Facebook, but not your app.
                                    document.getElementById('status').innerHTML = 'Please log ' +
                                            'into this app.';
                                } else {
                                    // The person is not logged into Facebook, so we're not sure if
                                    // they are logged into this app or not.
                                    document.getElementById('status').innerHTML = 'Please log ' +
                                            'into Facebook.';
                                }
                            }

                            function checkLoginState() {
                                FB.getLoginStatus(function (response) {
                                    statusChangeCallback(response);
                                });
                            }
                            function start_login() {
                                checkLoginState();
                            }
                            window.fbAsyncInit = function () {
                                FB.init({
                                    appId: '835557989856782',
                                    cookie: true, // enable cookies to allow the server to access 
                                    // the session
                                    xfbml: true, // parse social plugins on this page
                                    version: 'v2.2' // use version 2.2
                                });
                            };
                            // Load the SDK asynchronously
                            (function (d, s, id) {
                                var js, fjs = d.getElementsByTagName(s)[0];
                                if (d.getElementById(id))
                                    return;
                                js = d.createElement(s);
                                js.id = id;
                                js.src = "//connect.facebook.net/en_US/sdk.js";
                                fjs.parentNode.insertBefore(js, fjs);
                            }(document, 'script', 'facebook-jssdk'));

                            // Here we run a very simple test of the Graph API after login is
                            // successful.  See statusChangeCallback() for when this call is made.
                            function testAPI() {
                                console.log('Welcome!  Fetching your information.... ');
                                FB.api('/me', {fields: "id,name,gender,email"}, function (response) {
                                    console.log('Successful login for: ' + response.name);
                                    $.ajax({
                                        type: "post", // Request method: post, get
                                        url: "<?php echo Router::url(array('controller' => 'users', 'action' => 'check')); ?>",
                                        data: {response: response}, // outer quotes removed
                                        cache: false,
                                        success: function (response, status) {
                                            window.location = "<?php echo Router::url('/feed'); ?>";
                                        },
                                    });
                                });
                            }
                        </script>
                        <div class="registration_left float-none col-md-4 panel">
                            <div style='margin-bottom: 0' class="panel with-nav-tabs panel-default">
                                <div class="panel-heading">
                                    <div class="row">
                                        <div class="col-md-5 col-xs-5 float-none img-responsive">
                                            <?php
                                            echo $this->Html->link('<img style="width:100%;margin-bottom:15px" src="' . $this->webroot . "files/logo1.png" . '">', '/', array('escape' => false,)
                                            );
                                            ?>
                                        </div>
                                    </div>
                                    <ul class="nav nav-tabs">
                                        <li class="reg"><a href="#register" data-toggle="tab">Register</a></li>
                                        <li class="log active" ><a href="#login" data-toggle="tab">Login</a></li>
                                    </ul>
                                </div>
                                <div class="panel-body-larger">
                                    <div class="tab-content">
                                        <div class="tab-pane fade in active" id="login">
                                            <h2 style="text-align: center">Login Form</h2>
                                            <div class="registration_form panel-body">
                                                <!-- Form -->
                                                <?php echo $this->Form->create('User', array('id' => 'registration_form', 'url' => array('action' => 'login'))); ?>

                                                <div>  
                                                    <?php echo $this->Form->input('register', array('type' => 'hidden', 'value' => 0)); ?>
                                                    <?php echo $this->Form->input('username', array('class' => array('text'), 'id' => 'username', 'div' => false, 'placeholder' => __('Username'))); ?>                                                                                
                                                </div>
                                                <div>
                                                    <?php echo $this->Form->input('password', array('class' => array('text'), 'id' => 'password', 'div' => false, 'placeholder' => __('Password'))); ?>                                                                                
                                                </div>

                                                <div>
                                                    <center>
                                                        <?php
                                                        echo $this->Form->submit('login', array('value' => __('Login'), 'id' => 'login', 'div' => false));
                                                        echo $this->Form->end();
                                                        ?>
                                                        <button onclick="location.href = '<?php echo $fb_login_url; ?>'"class="btn-fb"><i class="fa fa-facebook"></i>login with Facebook </button>

                                                    </center>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="tab-pane fade " id="register">
                                            <h2 style='margin:0;text-align: center;'>Registration Form</h2>



                                            <div class="registration_form panel-body">
                                                <!-- Form -->
                                                <?php echo $this->Form->create('User', array('id' => 'registration_form', 'url' => array('action' => 'login'))); ?>
                                                <div>
                                                    <?php echo $this->Form->input('register', array('type' => 'hidden', 'value' => 1)); ?>
                                                    <?php echo $this->Form->input('username', array('required' => true, 'class' => array('text'), 'id' => 'username', 'div' => false, 'placeholder' => __('Username'))); ?>                                                                                
                                                </div>
                                                <div>
                                                    <?php echo $this->Form->input('email', array('required' => true, 'class' => array('text'), 'id' => 'username', 'div' => false, 'placeholder' => __('Email'))); ?>                                                                                
                                                </div>
                                                <div>
                                                    <?php echo $this->Form->input('password', array('required' => true,
                                                          'pattern'=>'.{6,}', 'title'=>"6 characters minimum",'class' => array('text'), 'id' => 'username', 'div' => false)); ?>                                                                                
                                                </div>

                                                <div style="margin: 0.3em 0;">
                                                    <label style="float:left;">Gender : </label>
                                                    <div style="float: left;margin-left:0.7em; margin-top: -0.4em;">
                                                        <?php echo $this->Form->input('gender', array('label' => false, 'legend' => false, 'fieldset' => false, 'type' => 'radio', 'options' => array('m' => 'Male', 'f' => 'Female'), 'style' => array(''), 'class' => array('text radio-inline'), 'div' => false)); ?>
                                                    </div>    
                                                </div>
                                                <br class="clear">
                                                    <center>
                                                        <?php
                                                        echo $this->Form->submit('Sign up', array('value' => __('Sign up'), 'id' => 'login', 'class' => 'button', 'title' => 'دخول'));
                                                        echo $this->Form->end();
                                                        ?>

                                                    </center>
                                                    <center>
                                                        <button onclick="location.href = '<?php echo $fb_login_url; ?>'"class="btn-fb"><i class="fa fa-facebook"></i>login with Facebook </button>
                                                    </center>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                            <center>
                                <a target="blank" style='color:#64B2B2;' href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', 'Why-its-better-to-register-at-Speeli')); ?>">Why register</a>    
                            </center>
                        </div>
                    </div>
                    <script>
                        $('document').ready(function () {
                            var hash = window.location.hash.substring(1);
                            console.log(window.location.hash.substring(1));
                            if (hash.length != 0) {
                                
                                    $('.tab-pane').removeClass('active');
                                    $('.tab-pane').removeClass('in');
                                    var target = $(this).attr('id');
                                    $('#' + hash).addClass('active');
                                    $('#' + hash).addClass('in ');
                                    if(hash=='register'){
                                        $('.reg').addClass('active');
                                        $('.log').removeClass('active');
                                    }else{
                                        $('.log').addClass('active');
                                        $('.reg').removeClass('active');
                                    }
                                    console.log(target);
                                
                            }
                            $('.tab-pane').click(function () {
                                $(this).removeClass('active');
                                var target = $(this).attr('id');
                                $('#' + target).addClass('active');
                                console.log(target);
                            })
                            $('.register-form').hide();
                            $('.change').click(function () {
                                var wrapper = $(this).attr('parent');
                                var show = $(this).attr('target');
                                $('.' + wrapper).fadeOut(1000);
                                $('.' + show).fadeIn(2000);

                            });

                        });
                    </script>
                    <!--END WRAPPER-->

                    <!--GRADIENT--><div class="gradient"></div><!--END GRADIENT-->

                </body>
                </html>