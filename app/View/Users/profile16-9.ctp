﻿<style>
    .update-image{
        display: none;
    }
      .btn-success{
          background-color: #63B5B2;
  border-color: white;
    }
    .btn-success:hover, .btn-success:active, .btn-success.hover {
  background-color: rgba(72, 136, 133, 0.77);
}
input[type="text"]{
        width: 79%;
        float:left;
    }
     .close{
        font-size: 1em;
        float: none;
    }
    .progress{
        display: none;
    }
    hr{
        border: 0;
        height: 1px;
        background-image: -webkit-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -moz-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -ms-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -o-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
    }
    .clear{
        clear: both;
    }
    .question_wrapper{
        margin: 0 auto;
    }
    .float-none{
        float:none;
        margin: 0 auto;
    }
    .h3-bio{
        font-size: 1.2em;
        margin: 0;
    }
	.nice .NFI-button{width:100%!important}
	
</style>
<link rel="stylesheet" href="/css/nicefileinput.css">

<div class="row">
    <div class="col-xs-12 col-md-2">
        <div class="">
            <?php if ($user['User']['image'] == "") {
                $gender = ($user['User']['gender'] == 'm') ? 'male' : 'female';
                ?>
                <img id='profile_img' src="<?php echo $this->webroot . 'user' . '/' . $gender . '.png'; ?>" alt="..." class="img-thumbnail">
            <?php } else { ?>
                <img id='profile_img' src="<?php echo $this->webroot . 'user' . '/' . $user['User']['image']; ?>" alt="..." class="img-thumbnail">
<?php } ?>
            <div class="progress progress-striped" id="upload_progress">
                <div class="bar" ></div>
            </div>
            <div>
                <label class="uploadprofileimg">
                    <?php echo $this->Form->create("Logo", array('class' => 'form-horizontal', 'id' => 'registration_form', "type" => 'file')); ?>
<?php echo $this->Form->input('image', array('id' => 'FileImage1', 'type' => 'file', 'label' => false)); ?>                                                                          
                </label>
            </div>

            <div>
                ٍ<?php echo$this->Form->end(array('label' => __("Update image", true),'class' => 'update-image btn  btn-save')); ?>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-5">
        <div class="profile_name">
            <h3 quesuserd='u'><?php
            if($user['User']['name']!=""){
                $name=$user['User']['name'];
            }else{
                $name=$user['User']['username'];
            }
            echo $name;
            ?></h3> <div style='  margin-bottom: 1em; margin-left: 1em;' class="btn btn-success edit edit-bio">edit</div></div>
        <div class="bio"> 
            <div style='margin-left: 0.2em;' class="box-header">Title: </div>
            <?php if ($user['User']['knows'] != "") { ?>
                <h3 class='h3-bio' quesuserd='b'><?php echo $user['User']['knows']; ?></h3> <div class="btn btn-speeli edit edit-bio">edit</div>

            <?php } else { ?>
                <input id='quesuserput' type="text" placeholder="write something about yourself.."><div style="float:left;height: 2.5em;padding-top: 0.4em!important; " quesuserd='b'  class="btn btn-success add">add</div>
            <?php }
            ?>
        </div>
       
      
    </div>
    <div class="col-md-5">
         <div class="bio"> 
            <div class="box-header">Topics i know about: </div>
            <?php
            if ($user['User']['bio'] != "") {
                $explode = explode(',,', $user['User']['bio']);
                for ($i = 0; $i < count($explode); $i++) {
                    $sep = explode(',', $explode[$i]);
                    ?>
                    <h5 style='float:left; ' class=' click'>  <?php echo $sep[0] ?>                 
                        <label cat_id="<?php echo $sep[1] ?>" cat_name="<?php echo $sep[0] ?>" class='close remove-cat'>X</label>
                    </h5>  <br class="clear">     
                <?php } ?>
            <?php }
            ?>
        
        </div>

        <div class="new_cat_result row"> 

        </div>
               <br class="clear">
        <?php echo $this->Form->input('new', array('class' => 'new-cat-value', 'placeholder' => 'add your skills', 'label' => false, 'div' => false)); ?>
       <div class="results">

        </div>
    </div>

    <!-- Add the extra clearfix for only the required viewport -->
    <div  class="clearfix visible-xs-block"></div>
    <br class="clear"><br class="clear">
    

      <div class="row ">
     
        <div class="col-md-8 float-none ">
            <!-- The time line -->
            <div class="hpanel forum-box">
                <?php
                $i = 1;
                 foreach ($user_questions as $user_question) { 
                    ?>
                <hr>
                    <div class="hpanel hblue">
                        <div class="panel-heading hbuilt">
                            <h3> <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view',$user_question['Question']['slug'])); ?>"> <?php echo $user_question['Question']['title'];?></a>  </h3>
                        </div>
                     

                    </div>
                    <?php
                    $i++;
                }
                ?>
                <!-- timeline time label -->
                <!-- END timeline item -->
                <!-- timeline item -->
            </div>
        </div><!-- /.col -->
    </div><!-- /.row -->
</div><?php
echo $this->Html->script(array('jquery.nicefileinput.min'));
?><script>
    $('document').ready(function () {
        $(document).ready(function () {
            // your code...
            $("input[type=file]").nicefileinput();
        });
        $("#FileImage1").nicefileinput({
            label: 'Upload  photo...' // Spanish label
        });
        /* update image start*/
        $('body').undelegate('.update-image', 'click').delegate('.update-image', 'click', function (e) {
            var form = $(this).parents('form');
            e.preventDefault();
            if (document.getElementById('FileImage1').value != '') {
                start_upload(form);
            } else {
                alert('choose file');
            }
        });
        function generate(quesuserd, quesuserv) {
            return generatble = ' <h3 quesuserd=' + quesuserd + '>' + quesuserv + '</h3> <div class="btn btn-success edit edit-bio">edit</div>';
        }
        function start_upload(form) {
            var data = new FormData();

            var UploadedFile = document.getElementById("FileImage1");
            for (var i = 0; i < UploadedFile.files.length; i++) {
                data.append('data[Logo][image]', UploadedFile.files[i]);
            }
            //console.dir(UploadedFile.files);
            var request = new XMLHttpRequest();
            $('.progress').show();
            $('#upload_progress').addClass('active');
            $('#upload_progress').show(1);
            request.upload.addEventListener('load', function () {
                $('#upload_progress').removeClass('active');
                $('#upload_progress').hide(200);
            });
            request.upload.addEventListener('progress', function (event) {
                if (event.lengthComputable) {
                    var progress = Math.round(100 * event.loaded / event.total);
                    $('#upload_progress .bar').css({'width': progress + '%'});
                }
            });

            request.open("POST", '<?php echo Router::url(array('controller' => 'users', 'action' => 'logo')); ?>');
            request.onreadystatechange = function ()
            {
                if (request.readyState == 4 && request.status == 200)
                {
                    var url = request.responseText;

                    $('#profile_img').attr("src", url);

                }
            }
            request.setRequestHeader('Cache-Control', 'no-cache');
            request.send(data);
        }
        ;
        /* update image end*/
        $('.edit').on('click', function () {
            event.preventDefault();
            var target = $(this).attr('target');
            var text_change = $(this).html();
            console.log(text_change);
            if (text_change == 'edit') {
                $(this).html('update');
                $(this).parent().find('h3').attr('contenteditable', 'true');
                $(this).attr('update', target);
                $(this).parent().find('h3').focus();
            } else {
                $(this).html('edit');
                $(this).parent().find('h3').removeAttr('contenteditable');
                var quesuserv = $(this).parent().find('h3').html();
                var quesuserd = $(this).parent().find('h3').attr('quesuserd');

                $(this).removeAttr('update');
                $.post("<?php echo Router::url(array('controller' => 'users', 'action' => 'update')); ?>",
                        {
                            quesuserv: quesuserv,
                            quesuserd: quesuserd
                        },
                function (data) {
                });
            }
        });
        $('.add').on('click', function () {
            event.preventDefault();
            var text_change = $(this).html();

            if (text_change == 'add') {

                $(this).html('edit');
                var quesuserd = $(this).attr('quesuserd');

                var quesuserv = $(this).parent().find('#quesuserput').val();

                $.post("<?php echo Router::url(array('controller' => 'users', 'action' => 'add_profile')); ?>",
                        {
                            quesuserv: quesuserv,
                            quesuserd: quesuserd
                        },
                function (data) {

                    $(this).parent().find('#quesuserput').remove();
                });
            }
        });
        $('body').undelegate('.suggest-items', 'click').delegate('.suggest-items', 'click', function (e) {
            $('.suggest').hide();
            $('.new-cat-value').val('');
            var cat_id = $(this).attr('ques-cat-id');
            var cat_name = $(this).html();
            $('.new_cat_result').append("<h5 style='float:left; padding-right:1em;' class=' click'>\n\
<input name='new[]' value=" + cat_id + " type='hidden'>" + cat_name + "  <label cat_id=" + cat_id + " cat_name=" + cat_name + " class='close remove-cat'>X</label> </h5>  ")
            $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'users', 'action' => 'add_skills')); ?>",
                data: {cat_id: cat_id, cat_name: cat_name}, // outer quotes removed
                cache: false,
                success: function (response, status) {
                },
            });
        });
        $('body').undelegate('.remove-cat', 'click').delegate('.remove-cat', 'click', function (e) {
            var cat_id = $(this).attr('cat_id');
            var cat_name = $(this).attr('cat_name');
            $(this).parent().remove();
            $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'users', 'action' => 'remove_skills')); ?>",
                data: {cat_id: cat_id, cat_name: cat_name}, // outer quotes removed
                cache: false,
                success: function (response, status) {
                },
            });
        })
          $('input[type=file]').change(function() { 
    // select the form and submit
      var form = $('update-image').parents('form');
            event.preventDefault();
            if (document.getElementById('FileImage1').value != '') {
                start_upload(form);
            } else {
                alert('choose file');
            }
        });
        $('.new-cat-value').keyup(function () {
            var data_send = $(this).val();

            $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'users', 'action' => 'search_cat')); ?>",
                data: {data_send: data_send}, // outer quotes removed
                cache: false,
                success: function (response, status) {

                    $('.results').html(response);
                },
            });
        });
    });
</script>