<style>
    .clear{
        clear:both;
    }
</style>
<?php
$this->set("meta_robots", "noindex");
?>
<br class="clear">

<table class="table table-hover table-responsive col-md-12 col-xs-12">
    <tr>
        
        <th><?php echo $this->Paginator->sort('id', 'Id'); ?>  </th> 
        <th><?php echo $this->Paginator->sort('username', 'Username'); ?>  </th> 
        <th><?php echo $this->Paginator->sort('name', 'Name'); ?>  </th>
                                            <th><?php echo $this->Paginator->sort('about', 'about'); ?>  </th>
                                            <th><?php echo $this->Paginator->sort('image', 'image'); ?>  </th>
                                            <th><?php echo $this->Paginator->sort('gender', 'Gender'); ?></th>
                                              <th><?php echo $this->Paginator->sort('created', 'Date'); ?></th>
    </tr>
    <?php foreach ($users as $user) { ?>
        <?php if (!empty($user)) { ?>

            <tr>
                <td class="" >
                    <a  href="<?php
                    echo Router::url(array('controller' => 'users',
                        'action' => 'view', $user['User']['id']))
                    ?>" class="btn  btn-main left"><?php echo $user['User']['id']; ?>
                    </a>
                </td>
                <td class="" >
                    <a  href="<?php
                    echo Router::url(array('controller' => 'users',
                        'action' => 'view', $user['User']['id']))
                    ?>" class="btn  btn-main left"><?php echo $user['User']['username']; ?>
                    </a>
                </td>

                <td  ><?php echo $user['User']['name']; ?></td>
                <td  ><?php echo $user['User']['knows']; ?></td>
                
                <td>  <?php
                    if ($user['User']['image'] == "") {
                       $gender = ($user['User']['gender'] == 'f' || $user['User']['gender'] == 'female') ? 'female' : 'male';
                      
                        ?>
                        <img src="<?php echo $this->webroot . 'user' . '/' . $gender . '.png'; ?>" alt="..." class="img-thumbnail col-md-4 col-xs-6">
                    <?php } else { ?>
                        <img   src="<?php echo $this->webroot . 'user' . '/' . $user['User']['image']; ?>" alt="..." class="img-thumbnail col-md-4 col-xs-6">
                    <?php } ?></td>
                <td>  <?php echo $gender; ?></td>
               
                <td style="text-align: center;"><?php echo $this->Time->nice($user['User']['created']); ?></td>
                                       
            </tr>
            <?php
            ?>



        <?php }
    }
    ?>
</table>

<ul class="pagination">
    <?php
    echo $this->Paginator->prev(__('prev'), array('tag' => 'li'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
    echo $this->Paginator->numbers(array('separator' => '', 'currentTag' => 'a', 'currentClass' => 'active', 'tag' => 'li', 'first' => 1));
    echo $this->Paginator->next(__('next'), array('tag' => 'li', 'currentClass' => 'disabled'), null, array('tag' => 'li', 'class' => 'disabled', 'disabledTag' => 'a'));
    ?>
</ul>