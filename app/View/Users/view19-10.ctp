<style>
    .progress{
        display: none;
    }
    hr{
        border: 0;
        height: 1px;
        background-image: -webkit-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -moz-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -ms-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -o-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
    }
    .clear{
        clear: both;
    }
    .question_wrapper{
        margin: 0 auto;
    }
    .float-none{
        float:none;
        margin: 0 auto;
    }
    .h3-bio{
        font-size: 1.2em;
        margin: 0;
    }
</style>
<div class="row">
    <div class="col-xs-12 col-md-2">
        <div class="">
            <?php if ($user['User']['image'] == "") {
                $gender = ($user['User']['gender'] == 'male') ? 'male' : 'female';
                ?>
                <img id='profile_img' src="<?php  echo $this->webroot . 'user' . '/' . $gender . '.png'; ?>" alt="..." class="img-thumbnail">
            <?php } else { ?>
                <img id='profile_img' src="<?php  echo $this->webroot . 'user' . '/' . $user['User']['image']; ?>" alt="..." class="img-thumbnail">
<?php } ?>
      

          
        </div>
    </div>
     <div class="col-xs-9 col-md-5">
        <div class="profile_name">
            <h3 ><?php
            if($user['User']['name']!=""){
                $name=$user['User']['name'];
            }else{
                $name=$user['User']['username'];
            }
            echo $name;
            ?></h3> </div>
        <div class="bio"> 
            <?php
            if ($user['User']['knows'] != "") { ?>
           <h4 class='h3-bio' quesuserd='b'><?php echo $user['User']['knows']; ?></h4> 
           <?php }
            ?>
        </div>
    </div>
    <div class="col-xs-12 col-md-5">
        <div class="bio"> 
            <div class="box-header"><h4> Knows about:</h4> </div>
            <?php
            if ($user['User']['bio'] != "") {
            echo "<h5>";
            $explode = explode(',,', $user['User']['bio']);
            for ($i = 0;$i < count($explode);$i++) {
            $sep = explode(',', $explode[$i]);
              App::import('Model', 'Question');
    $this->Question = new Question();
    
            $slug=implode("-", $this->Question->remove_Space($sep[0]));
            ?>
            
           <a href="<?php echo Router::url(array('controller' => 'categories', 'action' => 'view', $slug)); ?>"> <?php echo $sep[0]; ?></a> 
           <?php 
            echo "<br class='clear'><br class='clear'>";
            }
             echo "</h5> ";
            ?>
            <?php }
            ?>
        </div>
        
    </div>
    <!-- Add the extra clearfix for only the required viewport -->
    <div  class="clearfix visible-xs-block"></div>
    <br class="clear"><br class="clear">
    <hr>

       <div class="row ">
     
        <div class="col-md-8 float-none ">
            <!-- The time line -->
            <div class="hpanel forum-box">
                <?php
                $i = 1;
                 foreach ($user_questions as $user_question) { 
                    ?>
                <div class="article-item">
                
                    <div class="hpanel hblue">
                        <div class="panel-heading hbuilt">
                            <h3> <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view',$user_question['Question']['slug'])); ?>"> <?php echo $user_question['Question']['title'];?></a>  </h3>
                        </div>
                     

                    </div>
                </div>
                    <?php
                    $i++;
                }
                ?>
                <!-- timeline time label -->
                <!-- END timeline item -->
                <!-- timeline item -->
            </div>
        </div><!-- /.col -->
    </div><!-- /.row -->
</div>
    <?php
        echo $this->Paginator->next('Show more star wars posts...');
?>
    <?php
echo $this->Html->script(array('jquery.nicefileinput.min','jquery.infinitescroll.min'));
?>
<script>
  $(function(){
    var $container = $('.forum-box');
 
    $container.infinitescroll({
      navSelector  : '.next',    // selector for the paged navigation 
      nextSelector : '.next a',  // selector for the NEXT link (to page 2)
      itemSelector : '.article-item',     // selector for all items you'll retrieve
      debug         : true,
      dataType      : 'html',
      loading: {
          finishedMsg: 'No more posts to load',
          img: '<?php echo $this->webroot; ?>files/8.gif'
        }
      }
    );
  });
 
</script>