<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:og="http://ogp.me/ns#"
      xmlns:fb="https://www.facebook.com/2008/fbml">

    <head>

        <?php echo $this->Html->charset(); ?>
        <?php if (isset($ads_toplevel)) { ?>
            <script>console.log("testads")</script>
            <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
            <script>
                (adsbygoogle = window.adsbygoogle || []).push({
                    google_ad_client: "ca-pub-9514847333269574",
                    enable_page_level_ads: true
                });
            </script>
            <?php
        }
        ?>
        <title>

            <?php if (isset($page_title) && !empty($page_title)) { ?>
                <?php echo $page_title; ?>
                <?php
            } else {
                echo 'Speeli';
            }
            ?>

        </title>
        <link rel="shortcut icon" href="<?php echo $this->webroot . 'files/favicon.ico'; ?>" type="image/x-icon">
            <?php if (isset($meta_description) && !empty($meta_description)) { ?>
                <?php
            } else {
                $meta_description = '';
            }
            ?>
            <?php if (isset($meta_keywords) && !empty($meta_keywords)) { ?>

                <?php
            } else {
                $meta_keywords = '';
            }
            ?>
            <?php if (isset($meta_robots) && !empty($meta_robots)) { ?>

                <?php
            } else {
                echo '<meta name="robots" content="noindex" >';
            }
            ?>
            <?php if (isset($meta_fb) && $meta_fb == true) { ?>
                <meta property="fb:app_id" content="835557989856782" />

                <?php
            } else {
                
            }
            ?>
            <meta name="viewport" content="width=device-width, initial-scale=1">
                <meta name="description" content="<?php echo $meta_description; ?>" />
                <meta name="keywords" content="" />  
                <meta property="fb:app_id" content="835557989856782" />
                <meta property="og:title" content="<?php echo $answers[0]['Question']['title']; ?>" />
                <meta property="og:description" content="<?php echo $answers[0]['Question']['title']; ?>" />
                <meta property="og:site_name" content="speeli"/>
                <meta property="og:image" content="http://speeli.com<?php echo $this->webroot . 'question/' . $answers[0]['Question']['id'] . '/' . $answers[0]['Question']['image']; ?>"/>
                <meta name="theme-color" content="#3BC8C2">
                    <script>
                        (function (i, s, o, g, r, a, m) {
                            i['GoogleAnalyticsObject'] = r;
                            i[r] = i[r] || function () {
                                (i[r].q = i[r].q || []).push(arguments)
                            }, i[r].l = 1 * new Date();
                            a = s.createElement(o),
                                    m = s.getElementsByTagName(o)[0];
                            a.async = 1;
                            a.src = g;
                            m.parentNode.insertBefore(a, m)
                        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

                        ga('create', 'UA-49654229-2', 'auto');
                        ga('send', 'pageview');
                    </script>

                    <?php if ($google_ads == 1) { ?>
                        <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                        <script>
                            (adsbygoogle = window.adsbygoogle || []).push({
                                google_ad_client: "ca-pub-7655680100976144",
                                enable_page_level_ads: true
                            });
                        </script>
                    <?php } ?>
                    <?php
                    echo $this->Html->script(array('jquery-1.11.3.min'));
                    ?>
                    </head>

                    <body data-spy="scroll" data-target=".navbar">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.10&appId=835557989856782";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>


                        <?php
                        echo $this->Html->css(array('bootstrap.min'));
                        ?>



                        <style>
                            p{word-break: break-all;}
                            .fa-toggle-menu{
                                float:right;
                                display: block!important;
                            }
                            .navbar-brand{
                            }
                            a.btn-add-article{
                                color: #FFF;
                                font-family: arial;
                                font-size: 14px;
                                font-weight: bold;
                                letter-spacing: 0px;
                                float: right;
                                background: #63B5B2 none repeat scroll 0% 0%;
                                margin-right: 1em;
                                margin-top: 0.9em;
                                height: 32px;
                                line-height: 32px;
                                padding: 0px 14px;
                            }
                            a.btn-add-article:hover{

                                background: #3BC8C2 none repeat scroll 0% 0%;
                                color: #FFF;

                            }	
                            .btn:hover, .btn:focus {
                                color: #FFF;
                                text-decoration: none;
                            }

                            .btn:focus, .btn:active:focus, .btn.active:focus {
                                outline: 0;
                            }

                            .fa-tasks{
                                cursor: pointer;
                                color: rgb(129, 129, 129);
                                font-size: 2em;
                            } 
                            .menu_toogle{
                                display: none;
                                width:5em ;
                                right: 1em !important;
                            }
                            .navbar-nav{
                                text-align: center;
                            }
                            .navbar-default .navbar-nav>li>a:hover{
                                color:#63B5B2;
                            }
                            .nav_menu_toggle{
                                width: 100%;
                            }
                            .navbar-nav:before ,.navbar-nav:after{
                                left: 1.9em !important;
                            }

                            #homeheader a{
                                font-family: arial;
                                font-size: 1.2em;
                                padding: 0.7em;
                                margin: 2em auto;
                                font-weight: bold;
                                display: block;
                                background-color: #66bcb6;
                                float: left;
                                text-align: center;
                                border-radius: 10px;
                                color: #fff;
                                text-transform: uppercase;
                                border: 2px solid #3FB0AB;
                            }

                            .navbar-header{}

                            .navbar-header h5 {
                                float: left;
                                margin: 21px 0px 0px 1px;
                                padding: 0px;
                                font-family: tahoma;
                                color: #838383;
                                font-size: 15px;
                            }

                            .menu_toogle ul li{
                                padding-top: 0.5em;
                            }

                            .navbar-right{
                                opacity: 0;
                            }
                            .navbar-brand img{
                                width: 110px;
                            }
                            .navbar-brand h5{
                                margin: 0;
                            }
                            #footer b{
                                padding:0 4px
                            }
                            @media only screen and (min-width: 768px){
                                .nodesktop{
                                    display: none;
                                }
                                .btn-search_header{display: none;}
                                .speeli-write{

                                    display: none;
                                }
                            }

                            /* newdesign 21 / 1 */
                            @media only screen and (max-width: 769px){
                                .navbar-default .navbar-toggle{
                                    right: 0.2em;
                                    float: left;
                                    border: none!important;
                                    margin-right: 6em!important;
                                    background: transparent !important;
                                }
                                .speeli-write{

                                    display: block;
                                }
                                .navbar-default .navbar-toggle .icon-bar{height: 3px}
                                .btn-search_header{width: auto!important;right: 0;}
                                .navbar-header h5{
                                    display: none;
                                }
                                @media only screen and (max-width: 375px){
                                    .navbar-default .navbar-toggle{
                                        margin-right: 5em!important;
                                    }
                                }
                                /*new design end*/

                                @media only screen and (max-width: 768px){
                                    .fa-toggle-menu {
                                        display: none !important;

                                    }
                                    .navbar-brand img{
                                        width: 5em;
                                    }
                                    .menu_toogle{
                                        display: none;
                                    }
                                    .btn-add-article{
                                        display: none;
                                    }
                                    .navbar-right{
                                        opacity: 1;
                                    }
                                    .navbar-nav{
                                        text-align: left;
                                    }

                                    #footer ul li a{
                                        margin: 0 5px 0 5px;
                                        font-size: 1.4em;
                                    }

                                }


                            </style>
                            <header>
                                <?php if ($admin) { ?>
                                    <div id="searchboxform_header" class="searchboxform_header">
                                        <?php
                                        echo $this->Form->create('Answer');
                                        ?>
                                        <div class="panel-heading flex-container">
                                            <?php
                                            echo $this->Form->input('title', array('id' => 'Titles', 'placeholder' => 'Search Speeli Summaries .... ', 'class' => 'login-form-texts flex-columns',
                                                'div' => false, 'label' => false));
                                            ?>

                                            <div class="check_statuss  btn btn-main"><i class="fa fa-search"></i></div>
                                        </div>
                                        <?php echo $this->Form->end(); ?>
                                    </div>
                                    <div id="menuboxform_header"  class="searchboxform_header">
                                        <ul class="  col-md-6 col-xs-12 col-md-offset-3">

                                            <li class="col-xs-5 col-md-5 col-xs-offset-1 col-md-offset-0"><a href="<?php echo Router::url('/feed'); ?>">
                                                    Explore Speeli  <span class="fa fa-list"> </span>
                                                </a>
                                            </li>
                                            <li class="col-xs-5 col-md-5 col-md-offset-1"><a href="<?php echo Router::url('/login'); ?>">
                                                    Join Community <span class="fa fa-users"></span>
                                                </a>
                                            </li>

                                        </ul>
                                    </div>
                                <?php } ?>
                                <nav class="navbar navbar-default navbar-fixed-top nav-open" role="navigation">
                                    <div class="navbar-header">
                                        <button type="button" style="right:0.2em;" class="navbar-toggle speeli-uncollapse" data-toggle="collapse" >
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                                        <?php
                                        echo $this->Html->link('<span class="brand-joyce"><img src="' . $this->webroot . "files/logo.png" . '"></span>', '/', array('class' => 'navbar-brand', 'escape' => false,)
                                        );
                                        ?>
                                        <h5> Everything Summarised </h5><br class="clear">
                                    </div>

                                    <button style="right:0.2em;" type="button" class="navbar-toggle fa-toggle-menu" data-toggle="collapse"  >
                                        <span class="sr-only"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                     <?php if ($this->Session->read('user_quesli') == true) { 
                                         $url =   Router::url(array('controller' => 'articles', 'action' => 'add'));
                                      } ?>
                                   

                                    <?php if ($this->Session->read('user_quesli') == true) {
                                        ?>
                                     <a class="log_btn_web btn btn-main-always btn-add-article btn-add-value" href="<?php echo $url;?>">
                                        <span class="glyphicon glyphicon-plus"></span>Add Speeli Summary</a>

                                    <div class='btn-main-always  btn-search_header' href="#">
                                        <a  href="<?php echo $url;?>" class="log_btn_mobile btn-add-value btn btn-speeli speeli-write" style="padding:.3em .6em !important;">
                                            Write 
                                            <span class="fa fa-edit"></span>    
                                        </a>  
                                    </div>
                                        <a class='btn btn-main-always btn-add-article' style="background: transparent;" href="<?php echo Router::url('/message') ?>">
                                            <?php if ($message_count > 0) { ?>
                                                <span class="fa " style="background: url('<?php echo $this->webroot . 'img/active.png'; ?>') ; background-size: cover;background-repeat: no-repeat;width:50px;height:40px;"> </span>
                                                <span  style="color:white; position: absolute;top: 2.5em;right: 23.3em;"> <?php echo $message_count ?></span>

                                            <?php } else {
                                                ?>
                                                <span class="fa " style="background: url('<?php echo $this->webroot . 'img/msg.png'; ?>') ; background-size: cover;background-repeat: no-repeat;width:40px;height:30px;"> </span>
                                            <?php } ?>
                                        </a>
                                    <?php } else{ ?>
                                        <a class=" log_btn_web btn btn-main-always btn-add-article btn-add-value" data-toggle="modal" data-target="#addContact">
                                        <span class="glyphicon glyphicon-plus"></span>Add Speeli Summary</a>

                                    <div class='btn-main-always  btn-search_header' href="#">
                                        <a  data-toggle="modal" data-target="#addContact" class="log_btn_mobile btn-add-value btn btn-speeli speeli-write" style="padding:.3em .6em !important;">
                                            Write 
                                            <span class="fa fa-edit"></span>    
                                        </a>  
                                    </div>
                                   <?php } ?>
                                    <div class="collapse navbar-collapse navbar-right collapsed">
                                        <ul class="nav navbar-nav nodesktop">
                                            <li><a href="http://www.speeli.com"><i class="fa fa-home"></i><span>Home</span></a></li>        
                                            <?php if ($this->Session->read('user_quesli')) { ?>
                                                <li><a href="http://www.speeli.com/dashboard"><i class="fa fa-calendar-o"></i><span>Dashboard</span></a></li>         
                                            <?php } ?>
                                            <li><a href="http://www.speeli.com/feed"><i class="fa fa-newspaper-o"></i><span>News Feed</span></a></li>        
                                            <li>                                    
                                                <a href="<?php echo Router::url(array('controller' => 'homes', 'action' => 'request')) ?>" 
                                                   class="iframe-request "><i class="fa fa-question"></i><span>Request a summary</span>
                                                </a>
                                            </li>  
                                            <?php
                                            if ($this->Session->read('user_quesli')) {
                                                $user = $this->Session->read('user_quesli');
                                                if ($user['User']['admin'] == 1) {
                                                    ?>
                                                    <li><a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'show_news')) ?>"><i class="fa fa-newspaper-o"></i><span>latest summaries</span></a></li>        
                                                    <li><a href="<?php echo Router::url('/dash_admin') ?>"><i class="fa fa-newspaper-o"></i><span>admin panel</span></a></li>        
                                                <?php } ?>
                                                <li>
                                                    <a  href="<?php echo Router::url('/message') ?>">
                                                        <?php
                                                        $color = ($message_count > 0) ? '#E62117' : 'white';
                                                        ?>
                                                        <i class="fa fa-envelope" style="color:<?php echo $color; ?>"> <?php echo $message_count ?></i><span>Messages</span>
                                                    </a>
                                                </li>
                                                <li><a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'profile')) ?>"><i class="fa fa-user"></i><span>profile</span></a></li>
                                                <li><a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'logout')) ?>"><i class="fa fa-sign-out"></i><span>signout</span></a></li>
                                            <?php } else { ?>
                                                <li><a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'login')) ?>"><i class="fa fa-user"></i><span>Sign up / Login</span></a></li>    
                                            <?php } ?>
                                        </ul>
                                    </div>   
                                    <?php if ($admin) {
                                        /*
                                        ?>
                                        <div class="col-xs-12  second-mobile-menu no-padding" style="background: #ddd;">
                                            <div class="col-xs-4 no-padding"><a class="btn-menu-mobile col-xs-12" href="#">Search <span class="fa fa-search"></span></a></div>
                                            <div class="col-xs-4 no-padding"><a class="btn-menu-mobile col-xs-12" href="#">Explore <span class="fa fa-bars"></span></a></div>
                                            <div class="col-xs-4 no-padding"><a class="btn-menu-mobile col-xs-12" href="#">Profile <span class="fa fa-user"></span></a></div>

                                        </div>
                                    <?php
                                    */
                                    
                                    } ?>
                                    <div class="menu_toogle" >
                                        <ul class="nav navbar-nav nav_menu_toggle" >
                                            <li><a href="http://www.speeli.com"><i class="fa fa-home"></i><span>Home</span></a></li>        
                                            <?php if ($this->Session->read('user_quesli')) { ?>
                                                <li><a href="http://www.speeli.com/dashboard"><i class="fa fa-calendar-o"></i><span>Dashboard</span></a></li>         
                                            <?php } ?>
                                            <li><a href="http://www.speeli.com/feed"><i class="fa fa-newspaper-o"></i><span>News Feed</span></a></li>         
                                            <li>
                                                <a href="<?php echo Router::url(array('controller' => 'homes', 'action' => 'request')) ?>" 
                                                   class="iframe-request "><i class="fa fa-question"></i><span>Request a summary</span>
                                                </a>
                                            </li>        
                                            <?php
                                            if ($this->Session->read('user_quesli')) {
                                                $user = $this->Session->read('user_quesli');
                                                if ($user['User']['admin'] == 1) {
                                                    ?>
                                                    <li><a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'show_news')) ?>"><i class="fa fa-newspaper-o"></i><span>latest summaries</span></a></li>        
                                                    <li><a href="<?php echo Router::url('/dash_admin') ?>"><i class="fa fa-newspaper-o"></i><span>admin panel</span></a></li>        
                                                <?php } ?>

                                                <li><a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'profile')) ?>"><i class="fa fa-user"></i><span>profile</span></a></li>

                                                <li><a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'logout')) ?>"><i class="fa fa-sign-out"></i><span>signout</span></a></li>
                                            <?php } else { ?>
                                                <li><a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'login')) ?>"><i class="fa fa-user"></i><span>login</span></a></li>    
                                                <li><a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'login#register')) ?>"><i class="fa fa-sign-in"></i><span>Sign up</span></a></li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </nav>
                            </header>
                            <?php
                            echo $this->fetch('css');
                            ?>
                            <?php if ($this->here == $this->webroot . "homes/index1") { ?>

                                <div id="homeheader">
                                    <div class="left">
                                        <h1>Summarizing the cluttered Internet into lists</h1>
                                        <?php
                                        if ($this->Session->read('user_quesli')) {
                                            
                                        } else {
                                            ?>
                                            <a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'login#register')) ?>">Sign up</a>
                                        <?php } ?>
                                    </div> 
                                    <div class="right">
                                    </div>
                                </div>
                            <?php } ?>
                            <?php
                            echo $this->Html->script(array('bootstrap.min'), array('defer' => 'defer'));
                            ?>

                            <?php if ($this->here == $this->webroot) { ?>

                                <div class="row">
                                    <div id="homeheader" class="col-md-12 col-lg-12 col-xs-12">
                                        <div class=" col-xs-12 col-sm-6 col-md-6 col-lg-6 homeheader">

                                            <?php if ($this->Session->read('user_quesli')) { ?>
                                                <h1 style="margin-top: 2em"><span >Relax. We're</span> <font style="font-size: 32px;"> Summarizing</font> <span>The Cluttered Internet Into Lists</span></h1>
                                                <div class="col-md-6 col-lg-6 form_index" style="min-height: 22em; opacity :0;" ></div>
                                                <?php
                                            } else {
                                                ?>
                                                <h1><span >Relax. We're</span><font style="font-size: 32px;"> Summarizing</font> <span>The Cluttered Internet Into Lists</span></h1>
                                                <div class="col-md-6 col-lg-6 form_index">
                                                    <?php echo $this->Form->create('User', array('id' => 'loginform', 'url' => array('action' => 'signup_ajax'))); ?>
                                                    <div style='text-align: center;' class="col-md-12 col-lg-12">
                                                        <h4><center><b>Join our community </b></center></h4>
                                                    </div>
                                                    <div class="col-md-12 col-lg-12">
                                                        <?php echo $this->Form->input('register', array('type' => 'hidden', 'value' => 1)); ?>                                                                                
                                                        <?php
                                                        echo $this->Form->input('username', array('check' => 'Please enter username', 'placeholder' => __('Username'), 'class' => 'check_username form-control', 'div' => false, 'label' => false));
                                                        ?>
                                                    </div>
                                                    <div class="col-md-12 col-lg-12">
                                                        <?php
                                                        echo $this->Form->input('email', array('check' => 'please write valid email', 'placeholder' => __('Email'), 'class' => 'check_email form-control', 'div' => false, 'label' => false));
                                                        ?>
                                                    </div>
                                                    <div class="clear-fix"></div>
                                                    <div class="col-md-12 col-lg-12">
                                                        <?php
                                                        echo $this->Form->input('password', array('check' => '6 characters minimum', 'autocomplete' => 'off', 'class' => 'check_pass form-control',
                                                            'pattern' => '.{6,}', 'required' => true, 'title' => "6 characters minimum",
                                                            'placeholder' => __('Password'), 'div' => false, 'label' => false));
                                                        ?>
                                                    </div>
                                                    <div class="col-md-12 col-lg-12">
                                                        <center>
                                                            <?php echo $this->Form->input('gender', array('label' => false, 'default' => 'm', 'legend' => false, 'fieldset' => false, 'type' => 'radio', 'options' => array('m' => 'Male', 'f' => 'Female'), 'style' => array(''), 'class' => array('text radio-inline'), 'div' => false)); ?>
                                                        </center>
                                                    </div>  
                                                    <div class="clear-fix"></div>
                                                    <div class="col-md-12 col-lg-12">
                                                        <?php
                                                        echo $this->Form->submit('Sign Up', array('value' => __('Sign up'), 'class' => 'btn col-md-12 col-lg-12 col-xs-12', 'id' => 'login', 'div' => false));
                                                        echo $this->Form->end();
                                                        ?>
                                                    </div><div class="clear-fix"></div>
                                                    <!--
                                                    <div class="col-md-12 col-lg-12">
                                                        <button onclick="location.href = '<?php echo $fb_login_url ?>'"class="btn-fb"><i class="fa fa-facebook"></i>login with Facebook </button>
                                                    </div>
                                                    -->
                                                    <div class="col-md-12 col-lg-12 hidden_msg" style="text-align: center;display: none;">

                                                    </div>
                                                    <a style="font-size: 1em;float:none;background: transparent; padding: 0; border: none;text-align: center;color: white;margin-bottom: 0; margin-top: 0.1em; text-transform: none;"
                                                       href="<?php echo Router::url(array('controller' => 'users', 'action' => 'login')); ?>">Log In</a>
                                                    <div class="clear-fix alert-danger"></div>
                                                    <br class="clear">
                                                </div><div class="clear-fix"></div>

                                                <script type="text/javascript">
                                                    var frm = $('#loginform');
                                                    frm.submit(function (ev) {
                                                        if (typeof window.orientation !== 'undefined') {
                                                            if ($('.check_email').val().length < 2) {
                                                                alert($('.check_email').attr('check'));
                                                                ev.preventDefault();
                                                            } else if ($('.check_pass').val().length < 6) {
                                                                alert($('.check_pass').attr('check'));
                                                                ev.preventDefault();
                                                            } else if ($('.check_username').val().length < 6) {
                                                                alert($('.check_username').attr('check'));
                                                                ev.preventDefault();
                                                            } else {
                                                                ev.preventDefault();
                                                                $.ajax({
                                                                    type: frm.attr('method'),
                                                                    url: frm.attr('action'),
                                                                    data: frm.serialize(),
                                                                    success: function (data) {
                                                                        if (data === "1") {


                                                                            window.location.replace("<?php echo Router::url('/dashboard') ?>");
                                                                        } else {
                                                                            $('.hidden_msg').fadeIn();
                                                                            $('.hidden_msg').html(data);
                                                                        }

                                                                    }
                                                                });
                                                            }

                                                        } else {

                                                            ev.preventDefault();
                                                            $.ajax({
                                                                type: frm.attr('method'),
                                                                url: frm.attr('action'),
                                                                data: frm.serialize(),
                                                                success: function (data) {
                                                                    if (data === "1") {


                                                                        window.location.replace("<?php echo Router::url('/dashboard') ?>");
                                                                    } else {
                                                                        $('.hidden_msg').fadeIn();
                                                                        $('.hidden_msg').html(data);
                                                                    }

                                                                }
                                                            });
                                                        }





                                                    });</script>
                                            <?php } ?>
                                        </div><div class="col-md-6 col-lg-12 col-xs-0"></div>
                                        <div class="clear-fix"></div>
                                        <div class="" >
                                            <div id="searchboxform" class="row question">
                                                <?php
                                                echo $this->Form->create('Question');
                                                ?>
                                                <div class="panel-heading flex-container">
                                                    <?php
                                                    echo $this->Form->input('title', array('id' => 'Title', 'placeholder' => 'Search Speeli Summaries .... ', 'class' => 'login-form-text flex-column',
                                                        'div' => false, 'label' => false));
                                                    ?>

                                                    <div class="check_status  btn btn-main"><i class="fa fa-search"></i></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                            <?php } ?>
                            <?php if ($this->here == $this->webroot . 'dashboard') { ?>
                                <div  class="col-md-12 col-lg-12 col-xs-12 float-none" style="padding: 0;">
                                    <img class="back-image" style="width: 100%!important;height:auto!important" src="<?php echo$this->webroot . 'img/back.jpg' ?>">
                                </div>
                            <?php } ?>
                            <!--/ nav -->
                            <div class="clear"></div>
                            <?php
//echo $this->Html->css(array('style.min'));
// echo $this->Html->css(array( 'bootstrap.min'));
                            ?>

                            <div class="wrapper">
                                <?php echo $this->fetch('content'); ?>
                            </div>


                            <div id="footer">
                                <div class="container">
                                    <center>
                                        <style>
                                            @media only screen and (max-width: 768px){
                                                #footer ul li a {
                                                    margin: 0 5px 0 5px;
                                                    font-size: 1em;
                                                }
                                            }
                                        </style>
                                        <div class="row">
                                            <ul class="col-md-8 col-xs-12 float-none">
                                                <li class="" style="" >
                                                    <?php
                                                    echo $this->Html->link('What is speeli', array('controller' => 'articles', 'action' => 'view', 'What-is-Speeli'), array('style' => 'border:none;', 'class' => 'page-header', 'target' => '_blank', 'escape' => false,)
                                                    );
                                                    ?>
                                                    <b> .</b> 

                                                    <a style="border: none;"  target='_blank' href="<?php
                                                    echo Router::url(array('controller' => 'articles',
                                                        'action' => 'view', 'The-guidelines-for-writing-a-Speeli-article'))
                                                    ?>" class="page-header  ">Writing guidelines   
                                                    </a> 
                                                    <b> .</b> 
                                                    <a style="border: none;"  target='_blank' href="<?php
                                                    echo Router::url(array('controller' => 'articles',
                                                        'action' => 'show_news'))
                                                    ?>" class="page-header  ">Latest Speeli Summaries
                                                    </a>
                                                    <b> .</b>
                                                    <a style="border: none;"  href="<?php
                                                    echo Router::url('/terms-of-service')
                                                    ?>" class="page-header ">Terms of Service

                                                    </a>
                                                    <b> .</b>
                                                    <a style="border: none;"  href="<?php
                                                    echo Router::url('/privacy-policy')
                                                    ?>" class="page-header   ">Privacy Policy

                                                    </a>
                                                    <b> .</b>
                                                    <a style="border: none;"  href="<?php
                                                    echo Router::url(array('controller' => 'homes',
                                                        'action' => 'contact'))
                                                    ?>" class="page-header  iframe-edit ">Contact us

                                                    </a>
                                                </li>
                                            </ul>  
                                        </div>
                                        <br class="clear">

                                    </center>
                                </div>
                            </div>
                            <?php
                            echo $this->element('sql_dump');
                            ?>


                            <script>

                                $(document).ready(function () {
                                   
                                    if (typeof window.orientation !== 'undefined') {
                                        $(".iframe-request").colorbox({opacity: '0.5', width: '100%', height: '80%', top: '10px'});
                                    } else {
                                        $(".iframe-request").colorbox({opacity: '0.5', width: '50%', height: '70%', top: '50px'});
                                    }
                                    $(function () {
                                        $('.more_info a').bind('click', function (event) {
                                            var $anchor = $(this);
                                            $('html, body').stop().animate({
                                                scrollTop: $($anchor.attr('href')).offset().top
                                            }, 2000, 'easeInOutExpo');
                                            event.preventDefault();
                                        });
                                    });
                                    //$('.form_index').toggle();
                                    $(".iframe-edit").colorbox({opacity: '0.5', width: '50%', top: '10px'});
                                    if ($(window).width() < 960) {
                                        $(".iframe-edit").colorbox({opacity: '0.5', width: '90%', top: '10px'});

                                    }

                                    if ($(window).width() < 768) {
                                        $('body').undelegate('.speeli-uncollapse', 'click').delegate('.speeli-uncollapse', 'click', function (e) {
                                            console.log('dsadsa');
                                            $(this).addClass('speeli-collapse');
                                            $(this).removeClass('speeli-uncollapse');
                                            $('.navbar-collapse.navbar-right').height('auto');
                                            $('.navbar-collapse.navbar-right').removeClass('collapse');
                                            $('.navbar-collapse.navbar-right').addClass('collapsed');

                                        });
                                        $('body').undelegate('.speeli-collapse', 'click').delegate('.speeli-collapse', 'click', function (e) {
                                            console.log('press');
                                            $('.navbar-collapse.navbar-right').height('1px');
                                            $('.navbar-collapse.navbar-right').removeClass('collapsed');
                                            $(this).removeClass('speeli-collapse');
                                            $(this).addClass('speeli-uncollapse');
                                            $('.navbar-collapse.navbar-right').addClass('collapse');

                                        });
                                    }


                                    $('.menu_toogle').hide();
                                    $('.fa-toggle-menu').click(function () {
                                        $('.menu_toogle').slideToggle();
                                    });
                                });


                            </script>

                            <?php
                            echo $this->Html->css(array('colorbox'));
                            echo $this->Html->script(array('jquery.colorbox-min'));
                            ?>
                            
                            <!-- modal login start-->
                            <style>


                                html{font-family:sans-serif;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%}body{margin:0}article,aside,details,figcaption,figure,footer,header,hgroup,main,menu,nav,section,summary{display:block}audio,canvas,progress,video{display:inline-block;vertical-align:baseline}audio:not([controls]){display:none;height:0}[hidden],template{display:none}a{background-color:transparent}a:active,a:hover{outline:0}abbr[title]{border-bottom:1px dotted}b,strong{font-weight:bold}dfn{font-style:italic}h1{font-size:2em;margin:0.67em 0}mark{background:#ff0;color:#000}small{font-size:80%}sub,sup{font-size:75%;line-height:0;position:relative;vertical-align:baseline}sup{top:-0.5em}sub{bottom:-0.25em}img{border:0}svg:not(:root){overflow:hidden}figure{margin:1em 40px}hr{-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;height:0}pre{overflow:auto}code,kbd,pre,samp{font-family:monospace, monospace;font-size:1em}button,input,optgroup,select,textarea{color:inherit;font:inherit;margin:0}button{overflow:visible}button,select{text-transform:none}button,html input[type="button"],input[type="reset"],input[type="submit"]{-webkit-appearance:button;cursor:pointer}button[disabled],html input[disabled]{cursor:default}button::-moz-focus-inner,input::-moz-focus-inner{border:0;padding:0}input{line-height:normal}input[type="checkbox"],input[type="radio"]{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;padding:0}input[type="number"]::-webkit-inner-spin-button,input[type="number"]::-webkit-outer-spin-button{height:auto}input[type="search"]{-webkit-appearance:textfield;-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box}input[type="search"]::-webkit-search-cancel-button,input[type="search"]::-webkit-search-decoration{-webkit-appearance:none}fieldset{border:1px solid #c0c0c0;margin:0 2px;padding:0.35em 0.625em 0.75em}legend{border:0;padding:0}textarea{overflow:auto}optgroup{font-weight:bold}table{border-collapse:collapse;border-spacing:0}td,th{padding:0}*{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}*:before,*:after{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box}html{font-size:10px;-webkit-tap-highlight-color:rgba(0,0,0,0)}body{font-family:"Helvetica Neue",Helvetica,Arial,sans-serif;font-size:14px;line-height:1.42857143;color:#333;background-color:#fff}input,button,select,textarea{font-family:inherit;font-size:inherit;line-height:inherit}a{color:#337ab7;text-decoration:none}a:hover,a:focus{color:#23527c;text-decoration:underline}a:focus{outline:5px auto -webkit-focus-ring-color;outline-offset:-2px}figure{margin:0}img{vertical-align:middle}.img-responsive{display:block;max-width:100%;height:auto}.img-rounded{border-radius:6px}.img-thumbnail{padding:4px;line-height:1.42857143;background-color:#fff;border:1px solid #ddd;border-radius:4px;-webkit-transition:all .2s ease-in-out;-o-transition:all .2s ease-in-out;transition:all .2s ease-in-out;display:inline-block;max-width:100%;height:auto}.img-circle{border-radius:50%}hr{margin-top:20px;margin-bottom:20px;border:0;border-top:1px solid #eee}.sr-only{position:absolute;width:1px;height:1px;margin:-1px;padding:0;overflow:hidden;clip:rect(0, 0, 0, 0);border:0}.sr-only-focusable:active,.sr-only-focusable:focus{position:static;width:auto;height:auto;margin:0;overflow:visible;clip:auto}[role="button"]{cursor:pointer}.modal-open{overflow:hidden}.modal{display:none;overflow:hidden;position:fixed;top:0;right:0;bottom:0;left:0;z-index:1050;-webkit-overflow-scrolling:touch;outline:0}.modal.fade .modal-dialog{-webkit-transform:translate(0, -25%);-ms-transform:translate(0, -25%);-o-transform:translate(0, -25%);transform:translate(0, -25%);-webkit-transition:-webkit-transform 0.3s ease-out;-o-transition:-o-transform 0.3s ease-out;transition:transform 0.3s ease-out}.modal.in .modal-dialog{-webkit-transform:translate(0, 0);-ms-transform:translate(0, 0);-o-transform:translate(0, 0);transform:translate(0, 0)}.modal-open .modal{overflow-x:hidden;overflow-y:auto}.modal-dialog{position:relative;width:auto;margin:10px}.modal-content{position:relative;background-color:#fff;border:1px solid #999;border:1px solid rgba(0,0,0,0.2);border-radius:6px;-webkit-box-shadow:0 3px 9px rgba(0,0,0,0.5);box-shadow:0 3px 9px rgba(0,0,0,0.5);-webkit-background-clip:padding-box;background-clip:padding-box;outline:0}.modal-backdrop{position:fixed;top:0;right:0;bottom:0;left:0;z-index:1040;background-color:#000}.modal-backdrop.fade{opacity:0;filter:alpha(opacity=0)}.modal-backdrop.in{opacity:.5;filter:alpha(opacity=50)}.modal-header{padding:15px;border-bottom:1px solid #e5e5e5}.modal-header .close{margin-top:-2px}.modal-title{margin:0;line-height:1.42857143}.modal-body{position:relative;padding:15px}.modal-footer{padding:15px;text-align:right;border-top:1px solid #e5e5e5}.modal-footer .btn+.btn{margin-left:5px;margin-bottom:0}.modal-footer .btn-group .btn+.btn{margin-left:-1px}.modal-footer .btn-block+.btn-block{margin-left:0}.modal-scrollbar-measure{position:absolute;top:-9999px;width:50px;height:50px;overflow:scroll}@media (min-width:768px){.modal-dialog{width:600px;margin:30px auto}.modal-content{-webkit-box-shadow:0 5px 15px rgba(0,0,0,0.5);box-shadow:0 5px 15px rgba(0,0,0,0.5)}.modal-sm{width:300px}}@media (min-width:992px){.modal-lg{width:900px}}.popover{position:absolute;top:0;left:0;z-index:1060;display:none;max-width:276px;padding:1px;font-family:"Helvetica Neue",Helvetica,Arial,sans-serif;font-style:normal;font-weight:normal;letter-spacing:normal;line-break:auto;line-height:1.42857143;text-align:left;text-align:start;text-decoration:none;text-shadow:none;text-transform:none;white-space:normal;word-break:normal;word-spacing:normal;word-wrap:normal;font-size:14px;background-color:#fff;-webkit-background-clip:padding-box;background-clip:padding-box;border:1px solid #ccc;border:1px solid rgba(0,0,0,0.2);border-radius:6px;-webkit-box-shadow:0 5px 10px rgba(0,0,0,0.2);box-shadow:0 5px 10px rgba(0,0,0,0.2)}.popover.top{margin-top:-10px}.popover.right{margin-left:10px}.popover.bottom{margin-top:10px}.popover.left{margin-left:-10px}.popover-title{margin:0;padding:8px 14px;font-size:14px;background-color:#f7f7f7;border-bottom:1px solid #ebebeb;border-radius:5px 5px 0 0}.popover-content{padding:9px 14px}.popover>.arrow,.popover>.arrow:after{position:absolute;display:block;width:0;height:0;border-color:transparent;border-style:solid}.popover>.arrow{border-width:11px}.popover>.arrow:after{border-width:10px;content:""}.popover.top>.arrow{left:50%;margin-left:-11px;border-bottom-width:0;border-top-color:#999;border-top-color:rgba(0,0,0,0.25);bottom:-11px}.popover.top>.arrow:after{content:" ";bottom:1px;margin-left:-10px;border-bottom-width:0;border-top-color:#fff}.popover.right>.arrow{top:50%;left:-11px;margin-top:-11px;border-left-width:0;border-right-color:#999;border-right-color:rgba(0,0,0,0.25)}.popover.right>.arrow:after{content:" ";left:1px;bottom:-10px;border-left-width:0;border-right-color:#fff}.popover.bottom>.arrow{left:50%;margin-left:-11px;border-top-width:0;border-bottom-color:#999;border-bottom-color:rgba(0,0,0,0.25);top:-11px}.popover.bottom>.arrow:after{content:" ";top:1px;margin-left:-10px;border-top-width:0;border-bottom-color:#fff}.popover.left>.arrow{top:50%;right:-11px;margin-top:-11px;border-right-width:0;border-left-color:#999;border-left-color:rgba(0,0,0,0.25)}.popover.left>.arrow:after{content:" ";right:1px;border-right-width:0;border-left-color:#fff;bottom:-10px}.clearfix:before,.clearfix:after,.modal-header:before,.modal-header:after,.modal-footer:before,.modal-footer:after{content:" ";display:table}.clearfix:after,.modal-header:after,.modal-footer:after{clear:both}.center-block{display:block;margin-left:auto;margin-right:auto}.pull-right{float:right !important}.pull-left{float:left !important}.hide{display:none !important}.show{display:block !important}.invisible{visibility:hidden}.text-hide{font:0/0 a;color:transparent;text-shadow:none;background-color:transparent;border:0}.hidden{display:none !important}.affix{position:fixed}
                                .to_load_ajax{
                                    padding: 0 2em;
                                    font-size: 1.2em;
                                }
                                #addContact {
                                    z-index: 9999999999999999999;
                                }
                                body.modal-open {
                                    overflow: hidden;
                                    position:fixed;
                                    width: 100%;
                                }
                            </style>
                            <script>
                                $('document').ready(function () {
                                        $('.log_btn_web').click(function(){
                                   $.ajax({
                            type: "POST", // Request method: post, get
                             async:false,
                            url: "<?php echo Router::url(array('controller' => 'homes', 'action' => 'track_btn')); ?>",
                            data: {type: 0},
                            success: function (data) {
                                
                            }
                        });
                                });
                                $('.log_btn_mobile').click(function(){
                                    $.ajax({
                            type: "POST", // Request method: post, get
                             async:false,
                            url: "<?php echo Router::url(array('controller' => 'homes', 'action' => 'track_btn')); ?>",
                            data: {type: 1},
                            success: function (data) {
                               
                            }
                        });
                                });
                                    
                                    $('.btn-add-value').click(function(){
                                       $('#type_submit').val('add'); 
                                         $('#type_submit').attr('editbutton','');
                                       
                                    });
                                    $('.btn-edit-value').click(function(){
                                       var edit_url = $(this).attr('edit_url');
                                       $('#type_submit').val(edit_url); 
                                       $('#type_submit').attr('editbutton','editbutton');
                                       
                                    });
                                    var navbar_fixed_height = $(".navbar-fixed-top").height();
                                    var wrapper_progress_modal = $('.wrapper_progress').height();
                                    if (document.body.clientWidth < 500) {

                                        $('.modal-content').css({'margin-top': navbar_fixed_height + (wrapper_progress_modal - 10) + "px"});

                                    } else {
                                        $('.modal-content').css({'margin-top': (navbar_fixed_height * 2) + "px"});
                                    }

                                });

                                $.ajax({
                                    type: "post", // Request method: post, get
                                    url: "<?php echo Router::url(array('controller' => 'users', 'action' => 'login_light')); ?>",
                                    cache: false,
                                    success: function (data) {
                                        $('.to_load_ajax').html(data);
                                    }
                                });

                            </script>
                            <div class="modal fade" id="addContact" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content" style="border-top-left-radius: 1em;border-top-right-radius: 1em;">
                                        <div class="modal-header" style="padding: 0.5em ;text-align: center;background:#63B5B2;border-top-left-radius: 1em;border-top-right-radius: 1em;">
                                            <h4 style="color: white;">Start Summarizing The Internet</h4>

                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>

                                        </div>
                                        <div class="modal-body to_load_ajax" id="to_load_ajax">

                                        </div>

                                    </div>
                                </div>
                            </div>
                            <script>
function statusChangeCallback(response) {
    console.log('statusChangeCallback');
    console.log(response);
     if (response.status === 'connected') {
    
      testAPI();
    } else {
      
      document.getElementById('status').innerHTML = 'Please log ' +
        'into this app.';
    }
  }
 function checkLoginState() {
     testAPI
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  window.fbAsyncInit = function() {
  FB.init({
    appId      : '835557989856782',
    cookie     : true,  // enable cookies to allow the server to access 
                        // the session
    xfbml      : true,  // parse social plugins on this page
    version    : 'v2.8' // use graph api version 2.8
  });
};


  function testAPI() {
    console.log('Welcome!  Fetching your information.... ');
    FB.api('/me', {"fields":"id,name,email,picture"},function(response) {
     
      $.ajax({
                                            url: '<?php echo Router::url(array('controller' => 'users', 'action' => 'login_face')); ?>',
                                            data: {
                                                id:response.id,
                                                name:response.name,
                                                email:response.email
                                            },
                                            success: function (data) {
                                                window.location = "<?php echo Router::url('/');?>";
                                            },
                                            type: 'POST'
                                        });
    });
  }
</script>
                        </body>
                        </html>