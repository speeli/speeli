<div class="col-md-10  update_art_speeli  " >
            <!-- The time line -->
            <div class="hpanel forum-box">
               
                <?php
                $i = 1;
                if (isset($update_title)) {
                    echo "<center><h3 class='page-header'>$update_title</h3></center>";
                }
                $user = $this->Session->read('user_quesli');

                foreach ($users as $user) {
                    if (isset($user['User']['name'])) {
                        
                        ?>
                        <div class="thumbnail clear">
                                    <div class="col-md-2 left clear">    
                                    <?php
                                    if ($user['User']['image'] == "") {
                                        
                                        $gender = ($user['User']['gender'] == 'male') ? 'male' : 'female';
                                        ?>
                                    
                                    <a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'view', $user['User']['id'])) ?>">
                                        <img style="width: 100%; height: 6em;" src="<?php echo $this->webroot . 'user' . '/' . $gender . '.png'; ?>" alt="..." class="img-thumbnail ">
                                        </a>
                                    
                                    <?php } else { ?>
                                        <a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'view', $user['User']['id'])) ?>">
                                            <img  style="width: 100%; height: 6em;" src="<?php echo $this->webroot . 'user' . '/' . $user['User']['image']; ?>" alt="..." class="img-thumbnail ">
                                        </a>
                                    <?php } ?>
                                    </div>
                                    
                                    
                                    <h3>  
                                        <a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'view',  $user['User']['id'])) ?>">
                                            <?php
                                            if($user['User']['name']!=""){
                                            echo  $user['User']['name'];     
                                            }else{
                                            echo  $user['User']['username'];     
                                            }
                                            ?>
                                        </a></h3>
                            
                            <h4 style="display: block">
                                <?php 
                                echo $user['User']['knows'];
                                ?>
                            </h4>
                            <br class="clear">
                            <h6 style="position: absolute; right: 3em" class="right">Wrote <b><?php echo $user[0]['user']; ?></b> Speeli articles</h6>
                            <br class="clear">   
                            <div class="clearfix"></div>
                                </div>
                <div class="clearfix"></div>
                        <?php
                    }
                }
                ?>
                <!-- timeline time label -->
                <!-- END timeline item -->
                <!-- timeline item -->
            </div>
        </div><!-- /.col -->
<script type="text/javascript">

    $('body').undelegate('.update_art', 'click').delegate('.update_art', 'click', function (e) {
        event.preventDefault();
        target = $(this).attr('speel_ques');
        $.ajax({
            type: "post", // Request method: post, get
            url: "<?php echo Router::url(array('controller' => 'categories', 'action' => 'updates')); ?>",
            data: {target: target}, // outer quotes removed
            cache: false,
            success: function (response, status) {
                $('.update_art_speeli').html(response);
            },
        });
    });
    $('.check_status').on('click', function () {

        event.preventDefault();
        var questitle = $('#Title').val();
        console.log(questitle);
        $('#QuestionTitle').val(questitle);
        $.ajax({
            type: "post", // Request method: post, get
            url: "<?php echo Router::url(array('controller' => 'users', 'action' => 'search')); ?>",
            data: {questitle: questitle}, // outer quotes removed
            cache: false,
            beforeSend: function () {
                $("#loader").fadeIn();
            },
            success: function (response, status) {
                $("#loader").fadeOut();
                $('.update_art_speeli').html(response);
            },
        });

    });
    $(document).keypress(function (event) {

        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {

            event.preventDefault();
            var questitle = $('#Title').val;
            $('#QuestionTitle').val(questitle);
            console.log(questitle);
            $.ajax({
                type: "post", // Request method: post, get
                url: "<?php echo Router::url(array('controller' => 'users', 'action' => 'search')); ?>",
                data: {questitle: questitle}, // outer quotes removed
                cache: false,
                success: function (response, status) {
                 $("#loader").fadeOut();
                $('.update_art_speeli').html(response);
                },
            });
        }
    });

</script>