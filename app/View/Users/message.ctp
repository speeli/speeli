<style>
    .site_color{
        color: #63B5B2;
    }
    @media only screen and (max-width: 768px){
        .wrapper{
            margin-top: 50px !important;
        }
	.col-md-2.col-xs-4.col-xs-offset-4.senderimg-div{
	float:left !important
	}
	
	.hblue .panel-heading {
    padding: 10px 0px;
	}
	
	.hblue {
		padding: 1em 0 !important;
	}
	
    }
    .progress{
        display: none;
    }
    .btn-message{
        background: #63B5B2 none repeat scroll 0% 0%;
        color: white;
    }
    hr{
        border: 0;
        height: 1px;
        background-image: -webkit-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -moz-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -ms-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -o-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
    }
    .clear{
        clear: both;
    }
    .question_wrapper{
        margin: 0 auto;
    }
    .float-none{
        float:none;
        margin: 0 auto;
    }
    .h3-bio{
        font-size: 1.2em;
        margin: 0;
    }
</style>
<div class="clearfix"></div>
<br class="clear">
<div class="row ">
    <div class="clearfix">
        <?php $idee = (isset($id) && !empty($id)) ? 'mobile_check' : ' '; ?>
        <div class="col-md-3 col-xs-12 <?php echo $idee ?>">
            <div class="hpanel forum-box">
                <?php
                $i = 1;
                if (isset($message_all) && !empty($message_all)) {
                    foreach ($message_all as $message) {
                        $sender = ($user['User']['id'] == $message['Sender']['id']) ? $message['Receiver']['id'] : $message['Sender']['id'];
                        $sender_name = ($user['User']['id'] == $message['Sender']['id']) ? $message['Receiver']['username'] : $message['Sender']['username'];
                        ?>
                        <div class="article-item">
                            <div class="hpanel hblue" style="padding: 0.5em;">

                                <div class="col-md-12 col-xs-12 float-none">
                                    <div class="col-md-4 col-xs-4" >

                                        <div class="">
                                            <a href="<?php echo Router::url('/message/' . $sender); ?>"> 
                                                <?php
                                                $sender_img = ($user['User']['id'] == $message['Sender']['id']) ? $message['Receiver']['image'] : $message['Sender']['image'];
                                                if ($sender_img == "") {

                                                    $gender = ($user['User']['gender'] == 'female') ? 'female' : 'male';
                                                    ?>
                                                    <img id='profile_img' src="<?php echo $this->webroot . 'user' . '/' . $gender . '.png'; ?>" alt="..." class="img-thumbnail">
                                                <?php } else { ?>
                                                    <img id='profile_img' src="<?php echo $this->webroot . 'user' . '/' . $sender_img; ?>" alt="..." class="img-thumbnail">
                                                <?php } ?>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="col-md-8 panel-body col-xs-8">
                                        <h4>
                                            <a href="<?php echo Router::url('/message/' . $sender); ?>"> 
                                                <?php echo $sender_name; ?>
                                            </a> 
                                        </h4>
                                        <br class="clear">
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="clearfix"></div>
                        <?php
                        $i++;
                    }
                }
                ?>

            </div>
        </div><!-- /.col -->  
        <?php $hide = (isset($id) && !empty($id)) ? '' : 'hide'; ?>
        <div class="col-xs-12 mobile_show <?php echo $hide?>">
            <center><a class="btn btn-message btn-animate" href="<?php echo Router::url('/message'); ?>">
                    <span class="fa fa-envelope">

                    </span>See all conversations</a></center>
            <br class="clear">
        </div>
        <?php if ($unseen == false) {
            ?>
            <div class="col-md-8 col-xs-12">



                <?php if (isset($messages) && !empty($messages)) { ?>
                    <div class="hpanel forum-box">
                        <div class="article-item">
                            <div class="hpanel hblue">
                                <div class="panel-heading hbuilt">
                                    <?php
                                    $i = 1;
                                    foreach ($messages as $message) {
                                        $sender = ($user['User']['id'] == $message['Sender']['id']) ? 'left' : 'right';
                                        $dir = ($user['User']['id'] == $message['Sender']['id']) ? 'ltr' : 'rtl';
                                        $text = ($user['User']['id'] == $message['Sender']['id']) ? 'left' : 'right';
                                        $me = ($user['User']['id'] == $message['Sender']['id']) ? 'me' : $message['Sender']['username'];
                                        if ($user['User']['id'] != $message['Sender']['id']) {
                                            $seen = ($message['Message']['seen'] == 1) ? '<span class=" fa fa-check-circle"></span>' : ' ';
                                        } else {
                                            $seen = ' ';
                                        }
                                        ?>
                                        <div class="col-md-12 col-xs-12" style="float:<?php echo $sender; ?>;direction: <?php echo $dir ?>">
                                            <div class="col-md-2 col-xs-4 col-xs-offset-4 senderimg-div" style="float:<?php echo $sender; ?>;direction: <?php echo $dir ?>">
                                                <div class="">
                                                    <?php
                                                    $sender_img = $message['Sender']['image'];

                                                    if ($sender_img == "") {

                                                        $gender = ($user['User']['gender'] == 'female') ? 'female' : 'male';
                                                        ?>
                                                        <img id='profile_img ' src="<?php echo $this->webroot . 'user' . '/' . $gender . '.png'; ?>" alt="..." class="img-thumbnail">
                                                    <?php } else { ?>
                                                        <img id='profile_img' src="<?php echo $this->webroot . 'user' . '/' . $sender_img; ?>" alt="..." class="img-thumbnail">
                                                    <?php } ?>

                                                </div>
                                            </div>
                                            <div class="col-md-10 col-xs-12" style="float:<?php echo $sender; ?>;direction: <?php echo $dir ?>">
                                                <h4> 
                                                    <a href="<?php
                                                    echo
                                                    Router::url(array('controller' => 'users', 'action' => 'view',
                                                        $message['Sender']['id']));
                                                    ?>"> 
                                                       <?php echo $me; ?>
                                                    </a> 
                                                </h4>
                                                <br class="clear">
                                                <p style="text-align: <?php echo $text; ?>;direction: ltr;">
                                                    <?php echo $message['Message']['message'] ?>
                                                </p>
                                                <?php echo $seen; ?>
                                            </div>
                                           <!-- <hr>-->
                                        </div>

                                        <div class="clearfix"></div>
                                        <?php
                                        $i++;
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="hpanel forum-box">
                        <div class="article-item">

                            <div class="hpanel hblue">
                                <div class="panel-heading hbuilt">
                                    <?php
                                    echo $this->Form->create('Message');
                                    echo $this->Form->hidden('user_recieve', array('value' => $id));
                                    echo $this->Form->textarea('message', array('','placeholder' => 'Send new message', 'rows' => 4, 'maxlength' => 1000));

                                    echo $this->Form->submit('Send message', array('id' => 'login', 'class' => 'btn btn-message btn-animate', 'div' => false));
                                    echo $this->Form->end();
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } else { ?>
                    <div class="hpanel forum-box">
                        <div class="article-item">

                            <div class="hpanel hblue">
                                <div class="panel-heading hbuilt">
                                    <?php
                                    echo $this->Form->create('Message');
                                    echo $this->Form->hidden('user_recieve', array('value' => $id));
                                    echo $this->Form->textarea('message', array('placeholder' => 'Send new message', 'rows' => 4, 'maxlength' => '300'));

                                    echo $this->Form->submit('Send message', array('id' => 'login', 'class' => 'btn btn-message btn-animate', 'div' => false));
                                    echo $this->Form->end();
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        <?php } else {
            ?>

            <?php
            $i = 1;
            if (isset($messages) && !empty($messages)) {
                ?>
                <div class="col-md-5 col-xs-12">
                    <center><h3 class="box-header site_color">New unread messages</h3></center>
                    <div class="hpanel forum-box">
                        <?php
                        foreach ($messages as $message) {
                            $sender = $message['Sender']['id'];
                            $sender_name = $message['Sender']['username'];
                            ?>
                            <div class="article-item">
                                <div class="hpanel hblue" style="padding: 0.5em;">

                                    <div class="col-md-12 col-xs-12 float-none">
                                        <div class="col-md-4" >
                                            <div class="">
                                                <?php
                                                $sender_img = $message['Sender']['image'];
                                                if ($sender_img == "") {
                                                    $gender = ($user['User']['gender'] == 'female') ? 'female' : 'male';
                                                    ?>
                                                    <img id='profile_img' src="<?php echo $this->webroot . 'user' . '/' . $gender . '.png'; ?>" alt="..." class="img-thumbnail">
                                                <?php } else { ?>
                                                    <img id='profile_img' src="<?php echo $this->webroot . 'user' . '/' . $sender_img; ?>" alt="..." class="img-thumbnail">
                                                <?php } ?>

                                            </div>
                                        </div>
                                        <div class="col-md-8 panel-body">
                                            <h4>
                                                <a href="<?php echo Router::url('/message/' . $sender); ?>"> 
                                                    <?php echo $sender_name; ?>
                                                </a> 
                                            </h4>
                                            <span class="fa fa-envelope site_color"><?php echo $message[0]['message_c']; ?></span>

                                            <br class="clear">
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="clearfix"></div>
                            <?php
                            $i++;
                        }
                        ?>
                    </div>
                </div>
                <?php
            } else {
                echo '  <div class="col-md-5 col-xs-12"><center><h3 class="box-header site_color">There are no recent messages</h3></center></div>';
            }
            ?>

            <?php
        }
        ?>
    </div><!-- /.row -->
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("textarea").focus();
        $("textarea").val(' ');
    });
</script>
<script>
    if (typeof window.orientation !== 'undefined') {
        $(".mobile_check").hide();
        $('.mobile_show').show();
        $(".hide").hide();
    } else {
        $('.mobile_show').hide();
    }

</script>