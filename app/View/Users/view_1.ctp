<style>
    .progress{
        display: none;
    }
    hr{
        border: 0;
        height: 1px;
        background-image: -webkit-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -moz-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -ms-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -o-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
    }
    .clear{
        clear: both;
    }
    .question_wrapper{
        margin: 0 auto;
    }
    .float-none{
        float:none;
        margin: 0 auto;
    }
    .h3-bio{
        font-size: 1.2em;
        margin: 0;
    }
</style>
<div class="row">
    <div class="col-xs-12 col-md-2">
        <div class="">
            <?php if ($user['User']['image'] == "") {
                $gender = ($user['User']['gender'] == 'male') ? 'male' : 'female';
                ?>
                <img id='profile_img' src="<?php  echo $this->webroot . 'user' . '/' . $gender . '.png'; ?>" alt="..." class="img-thumbnail">
            <?php } else { ?>
                <img id='profile_img' src="<?php  echo $this->webroot . 'user' . '/' . $user['User']['image']; ?>" alt="..." class="img-thumbnail">
<?php } ?>
      

          
        </div>
    </div>
     <div class="col-xs-9 col-md-5">
        <div class="profile_name"><h3><?php echo $user['User']['name']; ?></h3></div>
        <div class="bio"> 
            <?php
            if ($user['User']['knows'] != "") { ?>
           <h4 class='h3-bio' quesuserd='b'><?php echo $user['User']['knows']; ?></h4> 
           <?php }
            ?>
        </div>
    </div>
    <div class="col-xs-12 col-md-5">
        <div class="bio"> 
            <div class="box-header"><h4> Knows about:</h4> </div>
            <?php
            if ($user['User']['bio'] != "") {
            echo "<h5>";
            $explode = explode(',,', $user['User']['bio']);
            for ($i = 0;$i < count($explode);$i++) {
            $sep = explode(',', $explode[$i]);
              App::import('Model', 'Question');
    $this->Question = new Question();
    
            $slug=implode("-", $this->Question->remove_Space($sep[0]));
            ?>
            
           <a href="<?php echo Router::url(array('controller' => 'categories', 'action' => 'view', $slug)); ?>"> <?php echo $sep[0]; ?></a> 
           <?php 
            echo "<br class='clear'><br class='clear'>";
            }
             echo "</h5> ";
            ?>
            <?php }
            ?>
        </div>
        
    </div>
    <!-- Add the extra clearfix for only the required viewport -->
    <div  class="clearfix visible-xs-block"></div>
    <br class="clear"><br class="clear">
    <hr>

       <div class="row ">
     
        <div class="col-md-8 float-none ">
            <!-- The time line -->
            <div class="hpanel forum-box">
                <?php
                $i = 1;
                 foreach ($user_questions as $user_question) { 
                    ?>
                    <div class="hpanel hblue">
                        <div class="panel-heading hbuilt">
                            <h3> <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view',$user_question['Question']['slug'])); ?>"> <?php echo $user_question['Question']['title'];?></a>  </h3>
                        </div>
                     

                    </div>
                    <?php
                    $i++;
                }
                ?>
                <!-- timeline time label -->
                <!-- END timeline item -->
                <!-- timeline item -->
            </div>
        </div><!-- /.col -->
    </div><!-- /.row -->
</div><?php
echo $this->Html->script(array('jquery.nicefileinput.min'));
?><script>
    $('document').ready(function () {
        $(document).ready(function () {
            // your code...
            $("input[type=file]").nicefileinput();
        });
        $("#FileImage1").nicefileinput({
            label: 'Upload your new photo...' // Spanish label
        });
        /* update image start*/
        $('body').undelegate('.update-image', 'click').delegate('.update-image', 'click', function (e) {
            var form = $(this).parents('form');
            e.preventDefault();
            if (document.getElementById('FileImage1').value != '') {
                start_upload(form);
            } else {
                alert('choose file');
            }
        });
        function generate(quesuserd, quesuserv) {
            return generatble = ' <h3 quesuserd=' + quesuserd + '>' + quesuserv + '</h3> <div class="btn btn-success edit edit-bio">edit</div>';
        }
        function start_upload(form) {
            var data = new FormData();

            var UploadedFile = document.getElementById("FileImage1");
            for (var i = 0; i < UploadedFile.files.length; i++) {
                data.append('data[Logo][image]', UploadedFile.files[i]);
            }
            //console.dir(UploadedFile.files);
            var request = new XMLHttpRequest();
            $('.progress').show();
            $('#upload_progress').addClass('active');
            $('#upload_progress').show(1);
            request.upload.addEventListener('load', function () {
                $('#upload_progress').removeClass('active');
                $('#upload_progress').hide(200);
            });
            request.upload.addEventListener('progress', function (event) {
                if (event.lengthComputable) {
                    var progress = Math.round(100 * event.loaded / event.total);
                    $('#upload_progress .bar').css({'width': progress + '%'});
                }
            });

            request.open("POST", '<?php echo Router::url(array('controller' => 'users', 'action' => 'logo')); ?>');
            request.onreadystatechange = function ()
            {
                if (request.readyState == 4 && request.status == 200)
                {
                    var url = request.responseText;

                    $('#profile_img').attr("src", url);

                }
            }
            request.setRequestHeader('Cache-Control', 'no-cache');
            request.send(data);
        }
        ;
        /* update image end*/

        $('.edit').on('click', function () {
            event.preventDefault();

            var target = $(this).attr('target');
            var text_change = $(this).html();
            console.log(text_change);
            if (text_change == 'edit') {
                $(this).html('update');
                $(this).parent().find('h3').attr('contenteditable', 'true');
                $(this).attr('update', target);
                $(this).parent().find('h3').focus();
            } else {
                $(this).html('edit');
                $(this).parent().find('h3').removeAttr('contenteditable');
                var quesuserv = $(this).parent().find('h3').html();
                var quesuserd = $(this).parent().find('h3').attr('quesuserd');

                $(this).removeAttr('update');
                $.post("<?php echo Router::url(array('controller' => 'users', 'action' => 'update')); ?>",
                        {
                            quesuserv: quesuserv,
                            quesuserd: quesuserd
                        },
                function (data) {

                });
            }
        });

        $('.add').on('click', function () {
            event.preventDefault();
            var text_change = $(this).html();

            if (text_change == 'add') {

                $(this).html('edit');
                var quesuserd = $(this).attr('quesuserd');

                var quesuserv = $(this).parent().find('#quesuserput').val();

                $.post("<?php echo Router::url(array('controller' => 'users', 'action' => 'add_profile')); ?>",
                        {
                            quesuserv: quesuserv,
                            quesuserd: quesuserd
                        },
                function (data) {

                    $(this).parent().find('#quesuserput').remove();
                });
            }
        });


    });
</script>