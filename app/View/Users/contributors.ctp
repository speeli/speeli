<style>
    #loader{
        display: none;
    }
    .expand{
        cursor: pointer;
    }
    .progress{
        display: none;
    }
    hr{
        border: 0;
        height: 1px;
        background-image: -webkit-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -moz-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -ms-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -o-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
    }
    .clear{
        clear: both;
    }
    .question_wrapper{
        margin: 0 auto;
    }
    .float-none{
        float:none;
        margin: 0 auto;
    }
    .hblue{
        background-color: #fff;
        padding: 1em;
        border-radius: 4px;
        -moz-border-radius: 4px;
        -webkit-border-radius: 4px;
        margin-bottom: 25px;
        position: relative;
        line-height: 25px;
        border: 1px solid #e5e5e5;
    }
    .hblue .panel-heading{
        padding: 10px 15px;
        background-color: rgba(245, 245, 245, 0.24);
        border-top: 1px solid #ddd;

        border-bottom-right-radius: 3px;
        border-bottom-left-radius: 3px;
    }
    .list-unstyled li{
        margin-left: 1em;
    }
</style>
<?php
$this->set("meta_description", "Latest Speeli articles");
$this->set("meta_robots", "noindex");
?>
<div class="row">
    <!-- Add the extra clearfix for only the required viewport -->
    <div  class="clearfix visible-xs-block"></div>
    <br class="clear"><br class="clear">
    
    <div class="row ">
        <div class="col-md-6 float-none">
        <?php
        echo $this->Form->create('Question');
        echo $this->Form->input('category', array('style' => 'position:absolute;width:90%;', 'id' => 'Title', 'placeholder' => 'Search for contributors.. ', 'class' => 'login-form-text flex-column',
            'div' => false, 'label' => false));
        ?>
        <div style="right:0px;position: absolute;top:3px;" class="check_status  btn btn-main"><i class="fa fa-search"></i></div>
        <br class="clear">
    </div>   
     <div  class="clearfix visible-xs-block"></div>
     <br class="clear"><br class="clear">
        <div class="col-md-8  update_art_speeli  " >
            <!-- The time line -->
            <div class="hpanel forum-box">
                <div id="loader" class="col-md-3 col-xs-3 float-none" >
                    <cetner><img src="<?php echo $this->webroot . 'files/8.gif'; ?>">
                        Speeli contributors
                    </cetner>
                </div>
                <?php
                $i = 1;
                if (isset($update_title)) {
                    echo "<center><h3 class='page-header'>$update_title</h3></center>";
                }
                $user = $this->Session->read('user_quesli');

                foreach ($users as $user) {
                    if (isset($user['User']['name'])) {
                        
                        ?>
                        <div class="thumbnail clear">
                                    <div class="col-md-2 left clear">    
                                    <?php
                                    if ($user['User']['image'] == "") {
                                        
                                        $gender = ($user['User']['gender'] == 'male') ? 'male' : 'female';
                                        ?>
                                    
                                    <a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'view', $user['User']['id'])) ?>">
                                        <img style="width: 100%; height: 6em;" src="<?php echo $this->webroot . 'user' . '/' . $gender . '.png'; ?>" alt="..." class="img-thumbnail ">
                                        </a>
                                    
                                    <?php } else { ?>
                                        <a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'view', $user['User']['id'])) ?>">
                                            <img  style="width: 100%; height: 6em;" src="<?php echo $this->webroot . 'user' . '/' . $user['User']['image']; ?>" alt="..." class="img-thumbnail ">
                                        </a>
                                    <?php } ?>
                                    </div>
                                    
                                    
                                    <h3>  
                                        <a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'view',  $user['User']['id'])) ?>">
                                            <?php 
                                             if($user['User']['name']!=""){
                                            echo  $user['User']['name'];     
                                            }else{
                                            echo  $user['User']['username'];     
                                            }
                                            ?>
                                        </a></h3>
                            
                            <h4 style="display: block">
                                <?php 
                                echo $user['User']['knows'];
                                ?>
                            </h4>
                            <br class="clear">
                            <h6 style="position: absolute; right: 3em" class="right">Wrote <b><?php echo $user[0]['user']; ?></b> Speeli articles</h6>
                            <br class="clear">   
                            <div class="clearfix"></div>
                                </div>
                <div class="clearfix"></div>
                        <?php
                    }
                }
                ?>
                <!-- timeline time label -->
                <!-- END timeline item -->
                <!-- timeline item -->
            </div>
        </div><!-- /.col -->
    </div><!-- /.row -->
</div>
<script type="text/javascript">

    $('body').undelegate('.update_art', 'click').delegate('.update_art', 'click', function (e) {
        event.preventDefault();
        target = $(this).attr('speel_ques');
        $.ajax({
            type: "post", // Request method: post, get
            url: "<?php echo Router::url(array('controller' => 'categories', 'action' => 'updates')); ?>",
            data: {target: target}, // outer quotes removed
            cache: false,
            success: function (response, status) {
                $('.update_art_speeli').html(response);
            },
        });
    });
    $('.check_status').on('click', function () {

        event.preventDefault();
        var questitle = $('#Title').val();
        console.log(questitle);
        $('#QuestionTitle').val(questitle);
        $.ajax({
            type: "post", // Request method: post, get
            url: "<?php echo Router::url(array('controller' => 'users', 'action' => 'search')); ?>",
            data: {questitle: questitle}, // outer quotes removed
            cache: false,
            beforeSend: function () {
                $("#loader").fadeIn();
            },
            success: function (response, status) {
                $("#loader").fadeOut();
                $('.update_art_speeli').html(response);
            },
        });

    });
    $(document).keypress(function (event) {

        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {

             event.preventDefault();
        var questitle = $('#Title').val();
        console.log(questitle);
        $('#QuestionTitle').val(questitle);
        $.ajax({
            type: "post", // Request method: post, get
            url: "<?php echo Router::url(array('controller' => 'users', 'action' => 'search')); ?>",
            data: {questitle: questitle}, // outer quotes removed
            cache: false,
            beforeSend: function () {
                $("#loader").fadeIn();
            },
            success: function (response, status) {
                $("#loader").fadeOut();
                $('.update_art_speeli').html(response);
            },
        });
        }
    });

</script>