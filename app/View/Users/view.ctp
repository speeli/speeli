<style>
    .progress{
        display: none;
    }
    .btn-message{
        background: #63B5B2 none repeat scroll 0% 0%;
        color: white;
    }
    hr{
        border: 0;
        height: 1px;
        background-image: -webkit-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -moz-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -ms-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
        background-image: -o-linear-gradient(left, rgba(0,0,0,0), rgba(0,0,0,0.75), rgba(0,0,0,0)); 
    }
    .clear{
        clear: both;
    }
    .question_wrapper{
        margin: 0 auto;
    }
    .float-none{
        float:none;
        margin: 0 auto;
    }
    .h3-bio{
        font-size: 1.2em;
        margin: 0;
    }
</style>
<div class="row">
    <div class="col-xs-12 col-md-2">
        <div class="">
            <?php
            if ($user['User']['image'] == "") {
                $gender = ($user['User']['gender'] == 'female') ? 'female' : 'male';
                ?>
                <img id='profile_img' src="<?php echo $this->webroot . 'user' . '/' . $gender . '.png'; ?>" alt="..." class="img-thumbnail">
            <?php } else { ?>
                <img id='profile_img' src="<?php echo $this->webroot . 'user' . '/' . $user['User']['image']; ?>" alt="..." class="img-thumbnail">
            <?php } ?>

        </div>
        <br class="clear">
        <a class="btn btn-message btn-animate col-md-12 col-xs-12" href="<?php echo Router::url('/message/' . $user['User']['id']); ?>">
            <span class="fa fa-envelope">
                Send message
            </span>
        </a>
    </div>
    <div class="col-xs-9 col-md-5">
        <div class="profile_name">
            <h3 ><?php
                if ($user['User']['name'] != "") {
                    $name = $user['User']['name'];
                } else {
                    $name = $user['User']['username'];
                }
                echo $name;
                ?></h3> </div>
        <div class="bio"> 
            <?php if ($user['User']['knows'] != "") { ?>
                <h4 class='h3-bio' quesuserd='b'><?php echo $user['User']['knows']; ?></h4> 
                <?php
            }
            echo '<br class="clear"><br class="clear">';
            $wrote = ($count > 0) ? ' wrote ' . $count . ' Speeli articles' : FALSE;
            $edit = ($count_edit > 0) ? ' made ' . $cound_edit_only . ' edits  ' : FALSE;
            if ($wrote) {
                if ($edit) {
                    echo "<div style='margin-left: 0.2em;' class='box-header'><b>Contributions <span class='fa  fa-edit'></span></b>: </div><h5>  <b style='color:#63B5B2;'>$name  </b>has  $edit and $wrote  </h5>";
                } else {
                    echo "<div style='margin-left: 0.2em;' class='box-header'><b>Contributions <span class='fa fa-edit'></span></b> </div><h5> <b style='color:#63B5B2;'> $name  </b> has   $wrote  </h5>";
                }
            } else {
                if ($edit) {
                    echo "<div style='margin-left: 0.2em;' class='box-header'><b>Contributions <span class='fa fa-edit'></span></b> </div><h5>  <b style='color:#63B5B2;'>$name  </b> has $edit  </h5>";
                }
            }

            echo "<h5><b style='color:#63B5B2;'> $name </b>current Rank is <b style='color:#63B5B2;'> " . $ranking['rank'] . "</b> With <b style='color:#63B5B2;'> " . $points . " </b>Points </b>";
            ?>
        </div>
    </div>
    <div class="col-xs-12 col-md-5">
        <div class="bio"> 
            <div class="box-header"><h4> Knows about:</h4> </div>
            <?php
            if ($user['User']['bio'] != "") {
                echo "<h5>";
                $explode = explode(',,', $user['User']['bio']);
                for ($i = 0; $i < count($explode); $i++) {
                    $sep = explode(',', $explode[$i]);
                    App::import('Model', 'Question');
                    $this->Question = new Question();

                    $slug = implode("-", $this->Question->remove_Space($sep[0]));
                    ?>

                    <a href="<?php echo Router::url(array('controller' => 'categories', 'action' => 'view', $slug)); ?>"> <?php echo $sep[0]; ?></a> 
                    <?php
                    echo "<br class='clear'><br class='clear'>";
                }
                echo "</h5> ";
                ?>
            <?php }
            ?>
        </div>
    </div>
    <!-- Add the extra clearfix for only the required viewport -->
    <div  class="clearfix visible-xs-block"></div>
    <br class="clear"><br class="clear">
    <hr>
    <div class="row">
        <!-- Add the extra clearfix for only the required viewport -->
        <div  class="clearfix visible-xs-block"></div>
        <article class="blog-content blog-grid">
            <div class="container">
                <div class="content">
                    <div class="main section" id="main">
                        <div class="widget Blog" data-version="1" id="Blog1">
                            <div class="post-wrapper" style="position: relative; height: 1965px;">
                                <?php
                                $i = 0;
                              
                                foreach ($questions as $question) {
                                    if (!empty($question['Question']['image'])) {


                                        if (!empty($question['Question']['last'])) {
                                            $title = $question['Question']['last'];
                                        } else {
                                            if (!empty($question['Question']['title_update'])) {
                                                $explode_title = explode(',,,', $question['Question']['title_update']);
                                                $last = end($explode_title);
                                                $title = end(unserialize(base64_decode($last)));
                                            } else {
                                                $title = $question['Question']['title'];
                                            }
                                        }
                                        ?>
                                        <div class="grid-item col-md-4 col-xs-12" >

                                            <div class="post hentry" itemprop="blogPost" itemscope="itemscope" >
                                                <div class="entry-content" id="post-body-202452633544924310" i>
                                                    <div class="post-media">
                                                        <div class="image-wrap">
                                                            <div class="mask"></div>
                                                            <img width="360" height="180" alt="<?php echo $title; ?>" src="<?php echo $this->webroot . "question/" . $question['Question']['id'] . "/" . $question['Question']['image']; ?>">
                                                        </div>

                                                    </div>
                                                    <div class="post-meta">
                                                        <div class="post-date">
                                                            <span class="year">2015</span>
                                                            <span class="month">Mar</span>
                                                            <span class="day">04</span></div>
                                                        <div class="post-comment">
                                                            <i class="fa fa-eye"></i>
                                                            <a href="#">
                                                                <?php echo $question['Question']['views'] ?> views
                                                            </a>
                                                        </div>
                                                    </div>
                                                    <div class="post-body">
                                                        <div class="post-author">
                                                            <div class="image-thumb">
                                                                <?php
                                                                if ($user['User']['image'] == "") {
                                                                    $gender = ($user['User']['gender'] == 'f' || $user['User']['gender'] == 'female') ? 'female' : 'male';
                                                                    ?>
                                                                    <img   src="<?php echo $this->webroot . 'user' . '/' . $gender . '.png'; ?>" alt="..." class="img-responsive">
                                                                <?php } else { ?>
                                                                    <img  src="<?php echo $this->webroot . 'user' . '/' . $user['User']['image']; ?>" alt="<?php echo $user['User']['username'] ?>" class="img-responsive">
                                                                <?php } ?>

                                                            </div>
                                                            <div class="name-author">
                                                                <cite>
                                                                    <?php if (!empty($user['User']['username'])) { ?>
                                                                        <a alt="<?php echo $user['User']['username'] ?>" href="<?php echo Router::url(array('controller' => 'users', 'action' => 'view', $user['User']['id'])); ?>">
                                                                            <?php echo $name; ?>
                                                                        </a>
                                                                        <?php
                                                                    } else {
                                                                        echo "Annonymus";
                                                                    }
                                                                    ?>
                                                                </cite>
                                                            </div>
                                                        </div>
                                                        <div class="post-title">
                                                            <h2><span>
                                                                    <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $question['Question']['slug'])); ?>">
                                                                        <?php echo $title; ?>
                                                                    </a></span></h2>
                                                        </div>
                                                        <div class="post-entry">
                                                            <p><?php
                                                                        if (!empty($question['Answer']['last_body'])) {
                                                                            $body = $question['Answer']['last_body'];
                                                                            $body = trim($body);
                                                                        } else {
                                                                            if (!empty($question['Answer']['body_update'])) {
                                                                                $explode_body = explode(',,,', $question['Answer']['body_update']);
                                                                                $last = end($explode_body);
                                                                                $body = end(unserialize(base64_decode($last)));
                                                                                $body = trim($body);
                                                                            } else {
                                                                                $body = $question['Answer']['body'];
                                                                                $body = trim($body);
                                                                            }
                                                                        }
                                                                        $pos = strpos($body, ' ', 100);
                                                                        if ($pos) {
                                                                            echo substr($body, 0, $pos);
                                                                        } else {
                                                                            echo $body;
                                                                        }
                                                                        ?></p>
                                                        </div>
                                                        <div class="traingle"></div>
                                                        <div class="postfooter clearfix">
                                                            <div class="socialpost">
                                                                <div class="icons clearfix">
                                                                    <a href="http://twitter.com/share?url=www.speeli.com<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $question['Question']['slug'])); ?>" target="_blank"><i class="fa fa-twitter"></i>
                                                                        <div class="texts">Twitter</div>
                                                                    </a>
                                                                    <a href="http://www.facebook.com/sharer.php?u=www.speeli.com<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $question['Question']['slug'])); ?>" target="_blank"><i class="fa fa-facebook"></i>
                                                                        <div class="texts">Facebook</div>
                                                                    </a>
                                                                    <a href="https://plus.google.com/share?url=www.speeli.com<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $question['Question']['slug'])); ?>" target="_blank"><i class="fa fa-google-plus"></i>
                                                                        <div class="texts">Google+</div>
                                                                    </a>
                                                                    <a href="http://pinterest.com/pin/create/button/?source_url=www.speeli.com<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $question['Question']['slug'])); ?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');
                                                                                return false
                                                                                        ;" target="_blank"><i class="fa fa-pinterest"></i><div class="texts">Pinterest</div></a>
                                                                </div>
                                                            </div>
                                                            <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $question['Question']['slug'])); ?>">
                                                                <div class="read">
                                                                    Continue... </div>
                                                            </a>
                                                        </div>

                                                    </div>
                                                    <div style="clear: both;"></div>
                                                </div>
                                            </div>

                                        </div>

                                        <?php
                                        $i++;
                                    }
                                }
                                ?>
                            </div>
                            <div class="clear"></div>

                        </div>

                    </div>
                </div>    
            </div>
        </article>
        <?php
        echo $this->Paginator->next('Show more Speeli article...');
        ?>
    </div>
    <?php
echo $this->Html->css(array('blog'));
?>
    <?php
    echo $this->Html->script(array('jquery.nicefileinput.min', 'jquery.infinitescroll.min'));
    ?>
    <script>
        $(function () {
            var $container = $('.forum-box');

            $container.infinitescroll({
                navSelector: '.next', // selector for the paged navigation 
                nextSelector: '.next a', // selector for the NEXT link (to page 2)
                itemSelector: '.article-item', // selector for all items you'll retrieve
                debug: true,
                dataType: 'html',
                loading: {
                    finishedMsg: 'No more Speeli articles to load',
                    img: '<?php echo $this->webroot; ?>files/8.gif'
                }
            }
            );
        });

    </script>