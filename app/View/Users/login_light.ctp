<style>

    .btn{
        cursor: pointer;
    }
    .float-none{
        float:none;
        margin: 0 auto;
    }
    /* start registration */
    .registration{
        padding: 3% 1%;
    }
    .registration h4{
        font-size:1.5em;
        color: #00405d;
        text-transform:capitalize;
    }

    .registration span{
        color: #777777;
    }
    .registration_form{
        display: block;
    }
    .registration_form div{
        padding:0.2em 0;
        font-size: 0.9em;
    }


    .registration_form label {
        display: block;
        margin-bottom: 0;
        font-weight: normal;
        color: gray;
    }
    form input[type="text"], form input[type="email"],form input[type="tel"], form input[type="password"], form input[type="text"] {
        padding: 7px 5px;
        display: block;
        width: 100%;
        outline: none;
        font-family: "gessmed", Tahoma, Geneva, sans-serif;
        font-size: 0.8em;
        color: gray;
        -webkit-appearance: none;
        text-transform: capitalize;

        border: 1px solid rgb(231, 231, 231);
        font-weight: normal;
        text-align: left;
        transition: 0.5s ease;
        -moz-transition: 0.5s ease;
        -o-transition: 0.5s ease;
        -webkit-transition: 0.5s ease;
        margin: 0;
        height: auto;
        margin-bottom: 0.5em;

    }
    input-placeholder{
        color: gray;
        transition: opacity 250ms ease-in-out;
        opacity: 0.5; 
    }
    ::-webkit-input-placeholder { /* Chrome */
        color: gray;
        transition: opacity 250ms ease-in-out;
        opacity: 0.5;  
    }
    :focus::-webkit-input-placeholder {
        opacity: 0.5;
    }
    :-ms-input-placeholder { /* IE 10+ */
        color: gray;
        transition: opacity 250ms ease-in-out;
    }
    :focus:-ms-input-placeholder {
        opacity: 0.5;
    }
    ::-moz-placeholder { /* Firefox 19+ */
        color: gray;
        opacity: 1;
        transition: opacity 250ms ease-in-out;
    }
    :focus::-moz-placeholder {
        opacity: 0.5;
    }
    :-moz-placeholder { /* Firefox 4 - 18 */
        color: gray;
        opacity: 1;
        transition: opacity 250ms ease-in-out;
    }
    :focus:-moz-placeholder {
        opacity: 0.5;
    }
    .registration_form input[type="text"]:focus,.registration_form input[type="email"]:focus,.registration_form input[type="tel"]:focus,.registration_form input[type="password"]:focus 
    {

        box-shadow: 1px 1px 10px #00405d;
    }
    .registration_form input[type="submit"] {
        -webkit-appearance: none;
        font-family: "gessmed", Tahoma, Geneva, sans-serif;
        color: #ffffff;
        text-transform: capitalize;
        display: inline-block;
        background: #64B2B2;
        padding: 0.5em 0;

        transition: 0.5s ease;
        -moz-transition: 0.5s ease;
        -o-transition: 0.5s ease;
        -webkit-transition: 0.5s ease;
        cursor: pointer;
        border: none;
        outline: none;
        font-size: 1em;

        border-radius: 3px;
    }
    .registration_left .registration_form input[type="submit"]:hover{
        color: #ffffff;
        background:#2AC2C2;;
    }
    .registration_left .panel-heading{
        background-color: transparent !important;
        border: none
    }
    .registration_left .panel-body{
        font-size: 1.2em;
        line-height: 1.1em;
    }
    .registration_left .panel-body-larger{    padding: 0 .5em; }



    @media screen and (-webkit-min-device-pixel-ratio:0) { 

        select,

        textarea,

        input {

            font-size: 16px !important;

        }

    }

    @media only screen and (max-width:480px)
    {

        .row{margin:0}
        .registration_left.panel{margin:0px auto 0 auto}
        form input[type="text"], form input[type="email"],form input[type="tel"], form input[type="password"], form input[type="text"] {
            font-size: 17px;
        }


    }

    .click {
        cursor:pointer;
    }
</style>
<div class="row">

    <div class="registration_left float-none col-md-12  no-padding no-margin "style="box-shadow: none;">
        <div class="registration_form signup_form ">
            <!-- Form -->
            <?php echo $this->Form->create('User', array('url' => array('action' => 'signup_ajax'), 'id' => 'registration_form', 'class' => 'signform')); ?>
            <br class="clear">
            <div>
                <?php echo $this->Form->input('register', array('type' => 'hidden', 'value' => 1)); ?>


                <?php
                echo $this->Form->input('name', array('required' => true,
                    'class' => array('text'), 'id' => 'username_register', 'class' => 'inline',
                    'div' => false, "label" => 'Full Name', 'placeholder' => __('Full Name')));
                ?>                                                                                
            </div>

            <div>
                <?php
                echo $this->Form->hidden('type_submit', array('type_submit', 'id' => 'type_submit'));
                echo $this->Form->input('email', array('required' => true,
                    'class' => array('text'), 'autocomplete' => false, 'id' => 'email_register', 'div' => false, 'placeholder' => __('Email')));
                ?>
                <?php
                echo $error_message;
                if (isset($error_message)) {
                    ?>
                    <label class="error-message"><?php echo $error_message; ?></label>
                <?php } ?>
            </div>
            <div>
                <?php
                echo $this->Form->input('password', array('required' => true,
                    'pattern' => '.{6,}', 'autocomplete' => false, 'title' => "6 characters minimum", 'placeholder' => __('******'), 'class' => array('text'), 'id' => 'password_register', 'div' => false));
                ?>                                                                                
            </div>

            <div class="clear"></div>
            <span style="font-weight: normal; font-size: .75em" class="">* By registering, I agree that I have read and agree to the 
                <a  rel="noopener" href="<?php echo Router::url('/terms-of-service'); ?>">Terms of Service</a>
                and
                <a  rel="noopener" href="<?php echo Router::url('/privacy-policy
						'); ?>">Privacy Policy</a>
            </span>

            <div class="clear"></div>
            <center>
                <?php
                echo $this->Form->submit('Sign up', array('style' => 'margin-top: .6em;', 'value' => __('Sign up'), 'id' => 'logins', 'class' => 'button col-md-4 col-xs-12 float-none', 'title' => 'Sign Up'));
                echo $this->Form->end();
                ?>
                <div onlogin="checkLoginState();" style="margin-left: -2.5em;margin-top: 0.5em;margin-bottom: 0.5em;" 
                              class="float-none  fb-login-button col-md-6 col-xs-12" 
                              data-max-rows="1" data-size="medium" data-button-type="continue_with"
                             data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="true"></div>
                <div class="show_login click" style="font-size: 1em;float:none;background: transparent; padding: 0; border: none;text-align: center;margin-bottom: 1em;; margin-top: 0.5em; text-transform: none;"
                     >Log In </div>


            </center>
            <center>
                <div class="col-md-12 col-lg-12 hidden_msg_register" style="text-align: center;display: none;">

                </div>
                <div class="col-md-12 col-lg-12 hidden_msg_sign" style="text-align: center;display: none;">
                </div>
            </center>
            <!--
             <center>
                 <button onclick="location.href = '<?php echo $fb_login_url; ?>'"class="btn-fb"><i class="fa fa-facebook"></i>login with Facebook </button>
             </center>
            -->
        </div>
        <div class="registration_form login_form " style="display: none;">
            <!-- Form -->
<?php echo $this->Form->create('User', array('url' => array('action' => 'login_ajax'), 'id' => 'registration_forms', 'class' => 'loginform')); ?>

            <div>  
                <br class="clear">
<?php echo $this->Form->input('register', array('type' => 'hidden', 'value' => 0)); ?>
                <?php echo $this->Form->input('username', array('class' => array('text'), 'id' => 'username', 'div' => false, 'placeholder' => __('Username'))); ?>                                                                                
            </div>
            <div>
<?php echo $this->Form->input('password', array('class' => array('text'), 'id' => 'password', 'div' => false, 'placeholder' => __('Password'))); ?>                                                                                
            </div>

            <div>
                <center>
                    <?php
                    echo $this->Form->submit('login', array('value' => __('Login'), 'id' => 'login_ajax', 'class' => 'button col-md-4 col-xs-12 float-none', 'div' => false));
                    echo $this->Form->end();
                    ?>
                    <!--
                    <button onclick="location.href = '<?php echo $fb_login_url; ?>'"class="btn-fb"><i class="fa fa-facebook"></i>login with Facebook </button>
                    -->
                    <div class="clear clearfix"></div>
                    <div class="show_signup click" style="font-size: 1em;float:none;background: transparent; padding: 0; border: none;text-align: center;    margin-bottom: 1em; margin-top: 0.1em; text-transform: none;"
                         >Sign Up</div>
                </center>
                <center>
                    <div class="col-md-12 col-lg-12 hidden_msg" style="text-align: center;display: none;">
                    </div>
                    <div class="col-md-12 col-lg-12 flag_msg" style="text-align: center;display: none;">
                        <span>Speeli updated our terms of service and privacy polices, 
                            by continuing using Speeli you must agree on our <a href="<?php echo Router::url('/terms-of-service'); ?>"> " Terms of Service " </a>
                            and
                            <a href="<?php echo Router::url('/privacy-policy
						'); ?>"> " Privacy Policy " </a>
                        </span>
                        <br/><br/>
                        <a   href="<?php echo Router::url(array('controller' => 'users', 'action' => 'continue_speeli')); ?>">"<span class="fa fa-check"></span> I agree on the Terms of Service & Privacy Policy " </a><br/>
                        OR <a  style="color:red;" href="<?php echo Router::url(array('controller' => 'users', 'action' => 'delete_account')); ?>">"<span class="fa fa-remove"></span> Exit Speeli & Delete my account "</a>

                    </div>
                </center>
            </div>
        </div>
        <script type="text/javascript">
            $('.show_login').click(function () {
                $('.signup_form').fadeOut('fast');
                $('.login_form').fadeIn('slow');
            });
            $('.show_signup').click(function () {
                $('.login_form').fadeOut('fast');
                $('.signup_form').fadeIn('slow');
            });
            var frm = $('#registration_forms');
            $("#registration_forms").submit(function (event) {
                event.preventDefault();
                console.log('loginformss');
                $.ajax({
                    type: $('#registration_forms').attr('method'),
                    url: $('#registration_forms').attr('action'),
                    data: $('#registration_forms').serialize(),
                    success: function (data) {
                        console.log("why" + data);
                        if (data === "1") {
                             var type = $('#type_submit').val();
                              var type_attr = $('#type_submit').attr('editbutton');
                            if (type == "add") {
                                    window.location.replace("<?php echo Router::url('/articles/add') ?>");
                                } else {
                                    if (type_attr== "editbutton") {
                                     window.location.replace(type);
                                    }else{
                                    window.location.replace("<?php echo Router::url('/dashboard') ?>");
                                    }
                                    
                                }
                        } else if (data === "2") {
                            $('.flag_msg').fadeIn();

                        } else {
                            $('.hidden_msg').fadeIn();
                            $('.hidden_msg').html(data);
                        }

                    }
                });
            });
            function isEmail(email) {
                var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                return regex.test(email);
            }
             
            $("#registration_form").submit(function (event) {
                event.preventDefault();
                console.log('ads');

                if ($("#username_register").val() < 3) {
                    $("#username_register").attr('placeholder', 'Please Write Full name');
                    $('.hidden_msg_register').fadeIn();
                    $('.hidden_msg_register').html('<b style="color:red;">Please Write Full name</b>');
                } else if (!isEmail($("#email_register").val())) {
                    $("#email_register").val('');
                    $("#email_register").attr('placeholder', 'Please enter your email');
                    $('.hidden_msg_register').fadeIn();
                    $('.hidden_msg_register').html('<b style="color:red;">Please enter your email</b>');

                }
                if ($("#password_register").val() < 6) {
                    $("#password_register").attr('placeholder', 'six Characters Minimum');
                    $('.hidden_msg_register').fadeIn();
                    $('.hidden_msg_register').html('<b style="color:red;">Password : six Characters Minimum</b>');
                } else {

                    
                    $.ajax({
                        type: $("#registration_form").attr('method'),
                        url: $("#registration_form").attr('action'),
                        data: $("#registration_form").serialize(),
                        success: function (data) {
                            if (data === "1") {
                             var type = $('#type_submit').val();
                              var type_attr = $('#type_submit').attr('editbutton');
                            if (type == "add") {
                                    window.location.replace("<?php echo Router::url('/articles/add') ?>");
                                } else {
                                    if (type_attr== "editbutton") {
                                     window.location.replace(type);
                                    }else{
                                    window.location.replace("<?php echo Router::url('/dashboard') ?>");
                                    }
                                    
                                }
                        } else {
                                $('.hidden_msg_sign').fadeIn();
                                $('.hidden_msg_sign').html(data);
                            }

                        }
                    });
                }
            });

        </script>
    </div>
</div>

