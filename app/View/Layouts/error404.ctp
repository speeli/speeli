<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:og="http://ogp.me/ns#"
      xmlns:fb="https://www.facebook.com/2008/fbml">

    <head>

        <?php echo $this->Html->charset(); ?>

        <title>

            <?php if (isset($page_title) && !empty($page_title)) { ?>
                <?php echo $page_title; ?>
                <?php
            } else {
                echo 'Speeli';
            }
            ?>

        </title>
        <link rel="shortcut icon" href="<?php echo $this->webroot . 'files/favicon.ico'; ?>" type="image/x-icon">


            <?php
            echo $this->Html->script(array('jquery-1.11.3.min'));
            ?>
    </head>

    <body data-spy="scroll" data-target=".navbar">



        <?php
        echo $this->Html->css(array('bootstrap.min'));
        ?>



        <style>
            .fa-toggle-menu{
                float:right;
                display: block!important;
            }
            .navbar-brand{
            }
            a.btn-add-article{
                color: #FFF;
                font-family: arial;
                font-size: 14px;
                font-weight: bold;
                letter-spacing: 0px;
                float: right;
                background: #63B5B2 none repeat scroll 0% 0%;
                margin-right: 1em;
                margin-top: 0.9em;
                height: 32px;
                line-height: 32px;
                padding: 0px 14px;
            }
            a.btn-add-article:hover{

                background: #3BC8C2 none repeat scroll 0% 0%;
                color: #FFF;

            }	
            .btn:hover, .btn:focus {
                color: #FFF;
                text-decoration: none;
            }

            .btn:focus, .btn:active:focus, .btn.active:focus {
                outline: 0;
            }

            .fa-tasks{
                cursor: pointer;
                color: rgb(129, 129, 129);
                font-size: 2em;
            } 
            .menu_toogle{
                display: none;
                width:5em ;
                right: 1em !important;
            }
            .navbar-nav{
                text-align: center;
            }
            .navbar-default .navbar-nav>li>a:hover{
                color:#63B5B2;
            }
            .nav_menu_toggle{
                width: 100%;
            }
            .navbar-nav:before ,.navbar-nav:after{
                left: 1.9em !important;
            }

            #homeheader a{
                font-family: arial;
                font-size: 1.2em;
                padding: 0.7em;
                margin: 2em auto;
                font-weight: bold;
                display: block;
                background-color: #66bcb6;
                float: left;
                text-align: center;
                border-radius: 10px;
                color: #fff;
                text-transform: uppercase;
                border: 2px solid #3FB0AB;
            }

            .navbar-header{}

            .navbar-header h5 {
                float: left;
                margin: 21px 0px 0px 1px;
                padding: 0px;
                font-family: tahoma;
                color: #838383;
                font-size: 15px;
            }

            .menu_toogle ul li{
                padding-top: 0.5em;
            }

            .navbar-right{
                opacity: 0;
            }
            .navbar-brand img{
                width: 110px;
            }
            .navbar-brand h5{
                margin: 0;
            }
            #footer b{
                padding:0 4px
            }
            @media only screen and (min-width: 768px){
                .nodesktop{
                    display: none;
                }
                .btn-search_header{display: none;}
                .speeli-write{

                    display: none;
                }
            }

            /* newdesign 21 / 1 */
            @media only screen and (max-width: 769px){
                .navbar-default .navbar-toggle{
                    right: 0.2em;
                    float: left;
                    border: none!important;
                    margin-right: 6em!important;
                    background: transparent !important;
                }
                .speeli-write{

                    display: block;
                }
                .navbar-default .navbar-toggle .icon-bar{height: 3px}
                .btn-search_header{width: auto!important;right: 0;}
                .navbar-header h5{
                    display: none;
                }
                @media only screen and (max-width: 375px){
                    .navbar-default .navbar-toggle{
                        margin-right: 5em!important;
                    }
                }
                /*new design end*/

                @media only screen and (max-width: 768px){
                    .fa-toggle-menu {
                        display: none !important;

                    }
                    .navbar-brand img{
                        width: 5em;
                    }
                    .menu_toogle{
                        display: none;
                    }
                    .btn-add-article{
                        display: none;
                    }
                    .navbar-right{
                        opacity: 1;
                    }
                    .navbar-nav{
                        text-align: left;
                    }

                    #footer ul li a{
                        margin: 0 5px 0 5px;
                        font-size: 1.4em;
                    }

                }


            </style>
            <header>
                <?php if ($admin) { ?>
                    <div id="searchboxform_header" class="searchboxform_header">
                        <?php
                        echo $this->Form->create('Answer');
                        ?>
                        <div class="panel-heading flex-container">
                            <?php
                            echo $this->Form->input('title', array('id' => 'Titles', 'placeholder' => 'Search Speeli Summaries .... ', 'class' => 'login-form-texts flex-columns',
                                'div' => false, 'label' => false));
                            ?>

                            <div class="check_statuss  btn btn-main"><i class="fa fa-search"></i></div>
                        </div>
                        <?php echo $this->Form->end(); ?>
                    </div>
                    <div id="menuboxform_header"  class="searchboxform_header">
                        <ul class="  col-md-6 col-xs-12 col-md-offset-3">

                            <li class="col-xs-5 col-md-5 col-xs-offset-1 col-md-offset-0"><a href="<?php echo Router::url('/feed'); ?>">
                                    Explore Speeli  <span class="fa fa-list"> </span>
                                </a>
                            </li>
                            <li class="col-xs-5 col-md-5 col-md-offset-1"><a href="<?php echo Router::url('/login'); ?>">
                                    Join Community <span class="fa fa-users"></span>
                                </a>
                            </li>

                        </ul>
                    </div>
                <?php } ?>
                <nav class="navbar navbar-default navbar-fixed-top nav-open" role="navigation">
                    <div class="navbar-header">
                        <button type="button" style="right:0.2em;" class="navbar-toggle speeli-uncollapse" data-toggle="collapse" >
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <?php
                        echo $this->Html->link('<span class="brand-joyce"><img src="' . $this->webroot . "files/logo.png" . '"></span>', '/', array('class' => 'navbar-brand', 'escape' => false,)
                        );
                        ?>
                        <h5> Everything Summarised </h5><br class="clear">
                    </div>

                    <button style="right:0.2em;" type="button" class="navbar-toggle fa-toggle-menu" data-toggle="collapse"  >
                        <span class="sr-only"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="btn btn-main-always btn-add-article" href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'add')) ?>"><span class="glyphicon glyphicon-plus"></span>Add Speeli Summary</a>

                    <div class='btn-main-always  btn-search_header' href="#">
                        <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'add')) ?>" class="btn btn-speeli speeli-write" style="padding:.3em .6em !important;">
                            Write 
                            <span class="fa fa-edit"></span>    
                        </a>  
                    </div>

                    <?php if ($this->Session->read('user_quesli') == true) {
                        ?>
                        <a class='btn btn-main-always btn-add-article' style="background: transparent;" href="<?php echo Router::url('/message') ?>">
                            <?php if ($message_count > 0) { ?>
                                <span class="fa " style="background: url('<?php echo $this->webroot . 'img/active.png'; ?>') ; background-size: cover;background-repeat: no-repeat;width:50px;height:40px;"> </span>
                                <span  style="color:white; position: absolute;top: 2.5em;right: 23.3em;"> <?php echo $message_count ?></span>

                            <?php } else {
                                ?>
                                <span class="fa " style="background: url('<?php echo $this->webroot . 'img/msg.png'; ?>') ; background-size: cover;background-repeat: no-repeat;width:40px;height:30px;"> </span>
                            <?php } ?>
                        </a>
                    <?php } ?>
                    <div class="collapse navbar-collapse navbar-right collapsed">
                        <ul class="nav navbar-nav nodesktop">
                            <li><a href="http://www.speeli.com"><i class="fa fa-home"></i><span>Home</span></a></li>        
                            <?php if ($this->Session->read('user_quesli')) { ?>
                                <li><a href="http://www.speeli.com/dashboard"><i class="fa fa-calendar-o"></i><span>Dashboard</span></a></li>         
                            <?php } ?>
                            <li><a href="http://www.speeli.com/feed"><i class="fa fa-newspaper-o"></i><span>News Feed</span></a></li>        
                            <li>                                    
                                <a href="<?php echo Router::url(array('controller' => 'homes', 'action' => 'request')) ?>" 
                                   class="iframe-request "><i class="fa fa-question"></i><span>Request a summary</span>
                                </a>
                            </li>  
                            <?php
                            if ($this->Session->read('user_quesli')) {
                                $user = $this->Session->read('user_quesli');
                                if ($user['User']['admin'] == 1) {
                                    ?>
                                    <li><a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'show_news')) ?>"><i class="fa fa-newspaper-o"></i><span>latest summaries</span></a></li>        
                                    <li><a href="<?php echo Router::url('/dash_admin') ?>"><i class="fa fa-newspaper-o"></i><span>admin panel</span></a></li>        
                                <?php } ?>
                                <li>
                                    <a  href="<?php echo Router::url('/message') ?>">
                                        <?php
                                        $color = ($message_count > 0) ? '#E62117' : 'white';
                                        ?>
                                        <i class="fa fa-envelope" style="color:<?php echo $color; ?>"> <?php echo $message_count ?></i><span>Messages</span>
                                    </a>
                                </li>
                                <li><a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'profile')) ?>"><i class="fa fa-user"></i><span>profile</span></a></li>
                                <li><a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'logout')) ?>"><i class="fa fa-sign-out"></i><span>signout</span></a></li>
                            <?php } else { ?>
                                <li><a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'login')) ?>"><i class="fa fa-user"></i><span>Sign up / Login</span></a></li>    
                            <?php } ?>
                        </ul>
                    </div>   
                    <?php if ($admin) { ?>
                        <div class="col-xs-12  second-mobile-menu no-padding" style="background: #ddd;">
                            <div class="col-xs-4 no-padding"><a class="btn-menu-mobile col-xs-12" href="#">Search <span class="fa fa-search"></span></a></div>
                            <div class="col-xs-4 no-padding"><a class="btn-menu-mobile col-xs-12" href="#">Explore <span class="fa fa-bars"></span></a></div>
                            <div class="col-xs-4 no-padding"><a class="btn-menu-mobile col-xs-12" href="#">Profile <span class="fa fa-user"></span></a></div>

                        </div>
                    <?php } ?>
                    <div class="menu_toogle" >
                        <ul class="nav navbar-nav nav_menu_toggle" >
                            <li><a href="http://www.speeli.com"><i class="fa fa-home"></i><span>Home</span></a></li>        
                            <?php if ($this->Session->read('user_quesli')) { ?>
                                <li><a href="http://www.speeli.com/dashboard"><i class="fa fa-calendar-o"></i><span>Dashboard</span></a></li>         
                            <?php } ?>
                            <li><a href="http://www.speeli.com/feed"><i class="fa fa-newspaper-o"></i><span>News Feed</span></a></li>         
                            <li>
                                <a href="<?php echo Router::url(array('controller' => 'homes', 'action' => 'request')) ?>" 
                                   class="iframe-request "><i class="fa fa-question"></i><span>Request a summary</span>
                                </a>
                            </li>        
                            <?php
                            if ($this->Session->read('user_quesli')) {
                                $user = $this->Session->read('user_quesli');
                                if ($user['User']['admin'] == 1) {
                                    ?>
                                    <li><a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'show_news')) ?>"><i class="fa fa-newspaper-o"></i><span>latest summaries</span></a></li>        
                                    <li><a href="<?php echo Router::url('/dash_admin') ?>"><i class="fa fa-newspaper-o"></i><span>admin panel</span></a></li>        
                                <?php } ?>

                                <li><a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'profile')) ?>"><i class="fa fa-user"></i><span>profile</span></a></li>

                                <li><a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'logout')) ?>"><i class="fa fa-sign-out"></i><span>signout</span></a></li>
                            <?php } else { ?>
                                <li><a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'login')) ?>"><i class="fa fa-user"></i><span>login</span></a></li>    
                                <li><a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'login#register')) ?>"><i class="fa fa-sign-in"></i><span>Sign up</span></a></li>
                            <?php } ?>
                        </ul>
                    </div>
                </nav>
            </header>
            <?php
            echo $this->fetch('css');
            ?>

            <?php
            echo $this->Html->script(array('bootstrap.min'), array('defer' => 'defer'));
            ?>

            <!--/ nav -->
            <div class="clear"></div>
            <?php
//echo $this->Html->css(array('style.min'));
// echo $this->Html->css(array( 'bootstrap.min'));
            ?>

            <div class="wrapper">
                <?php
                $this->set("page_title", "404 Not Found - Speeli Summary");
                ?>
                <h1 class="text-center col-md-12  col-xs-12" style=" margin-bottom: 1.5em;">404 Page Not Found</h1>

                <img src="<?php echo $this->webroot . 'files/logo.png' ?>" class="col-md-2 col-xs-9 float-none img-responsive">
                    <h3 class="text-center col-md-12 col-xs-12">
                        <a href="<?php echo Router::url('/feed') ?>" style=" color: #4b9e9b;">
                            Discover More Speeli Summaries <span class="fa fa-sign-in"></span>
                        </a>
                    </h3>

            </div>


            <div id="footer">
                <div class="container">
                    <center>
                        <style>
                            @media only screen and (max-width: 768px){
                                #footer ul li a {
                                    margin: 0 5px 0 5px;
                                    font-size: 1em;
                                }
                            }
                        </style>
                        <div class="row">
                            <ul class="col-md-8 col-xs-12 float-none">
                                <li class="" style="" >
<?php
echo $this->Html->link('What is speeli', array('controller' => 'articles', 'action' => 'view', 'What-is-Speeli'), array('style' => 'border:none;', 'class' => 'page-header', 'target' => '_blank', 'escape' => false,)
);
?>
                                    <b> .</b> 

                                    <a style="border: none;"  target='_blank' href="<?php
                                    echo Router::url(array('controller' => 'articles',
                                        'action' => 'view', 'The-guidelines-for-writing-a-Speeli-article'))
?>" class="page-header  ">Writing guidelines   
                                    </a> 
                                    <b> .</b> 
                                    <a style="border: none;"  target='_blank' href="<?php
                                    echo Router::url(array('controller' => 'articles',
                                        'action' => 'show_news'))
?>" class="page-header  ">Latest Speeli Summaries
                                    </a>
                                    <b> .</b>
                                    <a style="border: none;"  href="<?php
                                    echo Router::url('/terms-of-service')
?>" class="page-header ">Terms of Service

                                    </a>
                                    <b> .</b>
                                    <a style="border: none;"  href="<?php
                                    echo Router::url('/privacy-policy')
?>" class="page-header   ">Privacy Policy

                                    </a>
                                    <b> .</b>
                                    <a style="border: none;"  href="<?php
                                    echo Router::url(array('controller' => 'homes',
                                        'action' => 'contact'))
?>" class="page-header  iframe-edit ">Contact us

                                    </a>
                                </li>
                            </ul>  
                        </div>
                        <br class="clear">

                    </center>
                </div>
            </div>
<?php
echo $this->element('sql_dump');
?>


            <script>

                $(document).ready(function () {
                    window.fbAsyncInit = function () {
                        FB.init({
                            appId: '835557989856782',
                            xfbml: true,
                            version: 'v2.3'
                        });
                    };

                    (function (d, s, id) {
                        var js, fjs = d.getElementsByTagName(s)[0];
                        if (d.getElementById(id)) {
                            return;
                        }
                        js = d.createElement(s);
                        js.id = id;
                        js.src = "//connect.facebook.net/en_US/sdk.js";
                        fjs.parentNode.insertBefore(js, fjs);
                    }(document, 'script', 'facebook-jssdk'));
                    if (typeof window.orientation !== 'undefined') {
                        $(".iframe-request").colorbox({opacity: '0.5', width: '100%', height: '80%', top: '10px'});
                    } else {
                        $(".iframe-request").colorbox({opacity: '0.5', width: '50%', height: '70%', top: '50px'});
                    }
                    $(function () {
                        $('.more_info a').bind('click', function (event) {
                            var $anchor = $(this);
                            $('html, body').stop().animate({
                                scrollTop: $($anchor.attr('href')).offset().top
                            }, 2000, 'easeInOutExpo');
                            event.preventDefault();
                        });
                    });
                    //$('.form_index').toggle();
                    $(".iframe-edit").colorbox({opacity: '0.5', width: '50%', top: '10px'});
                    if ($(window).width() < 960) {
                        $(".iframe-edit").colorbox({opacity: '0.5', width: '90%', top: '10px'});

                    }

                    if ($(window).width() < 768) {
                        $('body').undelegate('.speeli-uncollapse', 'click').delegate('.speeli-uncollapse', 'click', function (e) {
                            console.log('dsadsa');
                            $(this).addClass('speeli-collapse');
                            $(this).removeClass('speeli-uncollapse');
                            $('.navbar-collapse.navbar-right').height('auto');
                            $('.navbar-collapse.navbar-right').removeClass('collapse');
                            $('.navbar-collapse.navbar-right').addClass('collapsed');

                        });
                        $('body').undelegate('.speeli-collapse', 'click').delegate('.speeli-collapse', 'click', function (e) {
                            console.log('press');
                            $('.navbar-collapse.navbar-right').height('1px');
                            $('.navbar-collapse.navbar-right').removeClass('collapsed');
                            $(this).removeClass('speeli-collapse');
                            $(this).addClass('speeli-uncollapse');
                            $('.navbar-collapse.navbar-right').addClass('collapse');

                        });
                    }


                    $('.menu_toogle').hide();
                    $('.fa-toggle-menu').click(function () {
                        $('.menu_toogle').slideToggle();
                    });
                });


            </script>

<?php
echo $this->Html->css(array('colorbox'));
echo $this->Html->script(array('jquery.colorbox-min'));
?>
        </body>
    </html>