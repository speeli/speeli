<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
$cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
?>
<!DOCTYPE html>
<html>
    <head>

        <?php echo $this->Html->charset(); ?>
        <title>
           Speeli Know it in seconds
        </title>
        <?php

        echo $this->Html->css('cake.generic');

        echo $this->fetch('meta');
        echo $this->fetch('css');
        echo $this->fetch('script');
        ?>
        <script language="javascript">
            window.location.href = "<?php
        echo Router::url('/')
        ?>"
        </script>
    </head>
    <body>
        <script>
            $(document).ready(function () {
            })
        </script>
        <?php
        $this->layout = 'my_error'
        ?>
        <div id="container">
            <div id="header">
                <h1><?php echo 'Speeli'; ?></h1>
            </div>
            <div id="content">

                Error occured
            </div>

        </div>
        <?php echo $this->element('sql_dump'); ?>
    </body>
</html>
