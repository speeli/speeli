<style>

    .hpanel p, .hpanel h4,.hpanel h2{
          border: 1px solid rgb(121, 199, 197);
    }
</style>
<?php if (isset($answers)&&!empty($answers)){ ?>
<div class="row">


    <!-- Add the extra clearfix for only the required viewport -->
    <div  class="clearfix visible-xs-block"></div>
    <br class="clear"><br class="clear">

<hr>
    <div class="row ">
         <div class="col-md-2 flex-column ">
             
    </div>
        <div class="col-md-8  ">
            <!-- The time line -->
            <div class="hpanel forum-box">
                <div class="panel-heading">
                    <div class="quesuserarticle-i"><?php echo $answers[0]['Question']['id']; ?></div>
                    <?php
                if(!empty($answers[0]['Question']['title_update'])){
                      $explode_title=  explode(',,,', $answers[0]['Question']['title_update']);
                $last=  end($explode_title);
                    $titles=end(unserialize(base64_decode($last)));
                }else{
                    $titles=$answers[0]['Question']['title'];
                }
                
                    ?>
                    <center>  
                        <h2 >  <?php echo $titles; ?></h2>
                            
                            <div  class="edit-art-t btn btn-main" edit-article="f"  ><span class="glyphicon glyphicon-edit"></span></div>
                            <br class="clear">
                            <a class='btn btn-always-blue right' href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view',$answers[0]['Question']['slug'])); ?>">
                            <span class="glyphicon glyphicon-check">Return to article</span></a>
                    <br class="clear">
                               
                    </center>
                </div>
                
                <?php
                $i = 1;
                foreach ($answers as $answer) {
                if(!empty($answer['Answer']['title_update'])){
                      $explode_title=  explode(',,,', $answer['Answer']['title_update']);
                $last=  end($explode_title);
                
                    $title=end(unserialize(base64_decode($last)));
                }else{
                    $title=$answer['Answer']['title'];
                }
                if(!empty($answer['Answer']['body_update'])){
                 $explode_body=  explode(',,,', $answer['Answer']['body_update']);
                $last=  end($explode_body);
                 $body=end(unserialize(base64_decode($last)));
                }else{
                    $body=$answer['Answer']['body'];
                }
                    ?>
                    <div class="hpanel hblue">
                        <div class="panel-heading hbuilt">
                            <b class="left"><?php echo  $i . " )  "; ?> </b><h4 ques-ans-id="<?php echo $answer['Answer']['id']; ?>" ><?php echo $title; ?></h4>
                            <div  class="edit-ques-t btn btn-main" edit-article="f"  ><span class="glyphicon glyphicon-edit"></span></div>
                        </div>
                        <br class="clear">
                        <div class="panel-body" >
                            <?php if ($answer['Answer']['image'] != NULL) { ?>

                                <img id='profile_img' src="<?php echo $this->webroot . 'question/' . $answers[0]['Question']['id'] . '/' . $answer['Answer']['image']; ?>" alt="..." class="img-thumbnail">
                            <?php } ?>
                                <p ques-ans-id="<?php echo $answer['Answer']['id']; ?>" contenteditable="true"><?php echo $body; ?></p>
                                <div  class="edit-ques-b btn btn-main" edit-article="f" ><span class="glyphicon glyphicon-edit"></span></div>
                        </div>
                        <div class="panel-footer"><a  href="<?php echo Router::url(array('controller' => 'articles',
            'action' => 'history',$answer['Answer']['id'])) ?>" class="btn iframe-edit btn-main right">Show Edit History
                            </a>
                        <br class="clear"></div>
                        

                    </div>
                    <?php
                    $i++;
                }
                ?>
                <!-- timeline time label -->
                <!-- END timeline item -->
                <!-- timeline item -->
            </div>
        </div><!-- /.col -->
    </div><!-- /.row -->
    
      <br class="clear">
   <div class="container">
    <div class="row">
    	<div class="box effect7 col-md-4 col-xs-4 col-lg-4 float-none">
            <div class="panel with-nav-tabs panel-default">
                <div class="panel-heading">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#tab1default" data-toggle="tab">related Speelis</a></li>
                            <li><a href="#tab2default" data-toggle="tab">contributors</a></li>
                        </ul>
                </div>
                <div class="panel-body-larger">
                    <div class="tab-content">
                        <div class="tab-pane fade in active" id="tab1default">
                            <ul class="list-unstyled" >

                                <?php foreach ($categories as $category) { ?>
                                    <li>
                                        <a href="<?php echo Router::url(array('controller' => 'articles', 'action' => 'view', $category['Question']['slug'])); ?>"><i class="fa fa-tag"></i> <?php echo $category['Question']['title']; ?></a></li>
                                <?php } ?> 
                            </ul>
                        </div>
                        <div class="tab-pane fade" id="tab2default">
                           <ul class="list-unstyled" >
                                <li><h4>Article writers:
                                       <?php if($question_write!=NULL){
                                           ?>
                                           <a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'view', $question_write['User']['id'])); ?>"><i class="fa fa-tag"></i> <?php echo $question_write['User']['username']; ?></a>
                                     <?php   }else{ ?>
                                           <a href="#"><i class="fa fa-tag"></i> <?php echo $question_write; ?></a>
                                     <?php   } ?>
                                       
                               </h4></li>
                               
                                <?php
                                $j=1;
                                
                                foreach ($answers as $answer) { ?>
                                    <li>
                               <?php if($answer['User']['username']){
                                   ?>
                                    <h5>
                                        <a href="<?php echo Router::url(array('controller' => 'users', 'action' => 'view', $answer['User']['id'])); ?>"><i class="fa fa-tag"></i> <?php echo $answer['User']['username']; ?></a>
                                 Write point No <?php echo "<b>".$j."</b>";?>
                                </h5>  
                              <?php  } else{ ?>
                                        <h5>
                                        <a href="#"><i class="fa fa-tag"></i> <?php echo "Anonymous";  ?></a>
                                 Write point No <?php echo "<b>".$j."</b>";?>
                                </h5>  
                                    </li>
                              <?php } $j++; } ?> 
                            </ul>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
       
	</div>
</div>

</div>
    <?php } else{
        
    }
?>
    <?php
echo $this->Html->script(array('jquery.nicefileinput.min','jquery.colorbox-min'));
?>
<script>
    $('document').ready(function () {
        $(".iframe-edit").colorbox({opacity:'0.5',width:'50%',minHeight:'50%'});
        $('.hpanel h4').attr('contenteditable', 'true');
        $('.hpanel p').attr('contenteditable', 'true');
        $('.hpanel h2').attr('contenteditable', 'true');
        $(document).ready(function () {
            // your code...
            $("input[type=file]").nicefileinput();
        });
           $('.edit-ques-t').on('click', function () {
            event.preventDefault();
            var quesuserarticle_i=$('.quesuserarticle-i').html();
            
     var text_change=$(this).attr('edit-article');
     console.log(text_change);
           
            $(this).attr('edit-article','f');   
                
                $(this).parent().find('h4').removeAttr('contenteditable');
                var after_edit=$(this).parent().find('h4');
        var quesuseri = $(this).parent().find('h4').attr('ques-ans-id');        
        var quesuserv = $(this).parent().find('h4').html();
                var quesuserd ='t';
                console.log(quesuseri+quesuserv+quesuserarticle_i);
                $.post("<?php echo Router::url(array('controller' => 'articles', 'action' => 'update')); ?>",
                        {   
                            quesuserarticle_i:quesuserarticle_i,
                            quesuserv: quesuserv,
                            quesuseri:quesuseri,
                            quesuserd: quesuserd
                        },
                function (data) {
                        after_edit.html(quesuserv);
                });
            
        });
    $('.edit-art-t').on('click', function () {
            event.preventDefault();
            var quesuserarticle_i=$('.quesuserarticle-i').html();
            
     var text_change=$(this).attr('edit-article');
     console.log(text_change);
           
            $(this).attr('edit-article','f');   
                
                $(this).parent().find('h2').removeAttr('contenteditable');
                var after_edit=$(this).parent().find('h2');
                
        var quesuserv = $(this).parent().find('h2').html();
                var quesuserd ='t';
                console.log(quesuserv+quesuserarticle_i);
                $.post("<?php echo Router::url(array('controller' => 'articles', 'action' => 'update_article')); ?>",
                        {   
                            quesuserarticle_i:quesuserarticle_i,
                            quesuserv: quesuserv,
                            quesuserd: quesuserd
                        },
                function (data) {
                        after_edit.html(quesuserv);
                });
            
        });
$('.edit-ques-b').on('click', function () {
            event.preventDefault();
           var text_change=$(this).attr('edit-article');
            var quesuserarticle_i=$('.quesuserarticle-i').html();
            var text_change = $(this).html();
            $(this).attr('edit-article','f');
        $(this).parent().find('p').removeAttr('contenteditable');
                var after_edit=$(this).parent().find('p');
        var quesuseri = $(this).parent().find('p').attr('ques-ans-id');        
        var quesuserv = $(this).parent().find('p').html();
                var quesuserd ='b';
                $.post("<?php echo Router::url(array('controller' => 'articles', 'action' => 'update')); ?>",
                        {   
                            quesuserarticle_i:quesuserarticle_i,
                            quesuserv: quesuserv,
                            quesuseri:quesuseri,
                            quesuserd: quesuserd
                        },
                function (data) {
                        after_edit.html(quesuserv);
                });
            
        });
   

      


    });
</script>