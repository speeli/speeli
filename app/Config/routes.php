<?php

/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Config
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
/**
 * Here, we are connecting '/' (base path) to controller called 'Pages',
 * its action called 'display', and we pass a param to select the view file
 * to use (in this case, /app/View/Pages/home.ctp)...
 */
Router::connect('/', array('controller' => 'homes', 'action' => 'index', 'home'));
Router::connect('/dashboard', array('controller' => 'homes', 'action' => 'dashboard'));
Router::connect('/terms-of-service', array('controller' => 'homes', 'action' => 'terms'));
Router::connect('/privacy-policy', array('controller' => 'homes', 'action' => 'privacy'));
Router::connect('/about', array('controller' => 'homes', 'action' => 'about', 'home'));
Router::connect('/feed', array('controller' => 'homes', 'action' => 'feed',));
Router::connect('/logs', array('controller' => 'homes', 'action' => 'logs', 'home'));
Router::connect('/contributors', array('controller' => 'users', 'action' => 'contributors', 'home'));
Router::connect('/topics', array('controller' => 'categories', 'action' => 'topics', 'home'));
Router::connect('/summary/*', array('controller' => 'categories', 'action' => 'summary'));
Router::connect('/profile', array('controller' => 'users', 'action' => 'profile', 'home'));
Router::connect('/message/*', array('controller' => 'users', 'action' => 'message'));
Router::connect('/dash_admin', array('controller' => 'homes', 'action' => 'dash_admin'));
Router::connect('/last_month', array('controller' => 'articles', 'action' => 'emails'));
/**
 * ...and connect the rest of 'Pages' controller's URLs.
 */
/**
 * Load all plugin routes. See the CakePlugin documentation on
 * how to customize the loading of plugin routes.
 */
CakePlugin::routes();

/**
 * Load the CakePHP default routes. Only remove this if you do not want to use
 * the built-in default routes.
 */
require CAKE . 'Config' . DS . 'routes.php';
