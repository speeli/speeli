<?php

App::uses('AuthComponent', 'Controller/Component');

class Message extends AppModel {

    public $avatarUploadDir = 'img/avatars';

    /**
     * Before isUniqueUsername
     * @param array $options
     * @return boolean
     */
    public $belongsTo = array(
        'Sender' => array(
            'className' => 'User',
            'foreignKey' => 'user_send',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Group' => array(
            'className' => 'MessageGroup',
            'foreignKey' => 'group_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Receiver' => array(
            'className' => 'User',
            'foreignKey' => 'user_recieve',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
