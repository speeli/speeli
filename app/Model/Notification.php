<?php
	
App::uses('AuthComponent', 'Controller/Component');
 
class Notification extends AppModel {
     
    public $avatarUploadDir = 'img/avatars';
        /**
     * Before isUniqueUsername
     * @param array $options
     * @return boolean
     */
     
     var $belongsTo = array(
         'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
         
    );
 
}