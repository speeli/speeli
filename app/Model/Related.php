<?php

App::uses('AuthComponent', 'Controller/Component');

class Related extends AppModel {

    public $avatarUploadDir = 'img/avatars';

    /**
     * Before isUniqueUsername
     * @param array $options
     * @return boolean
     */
    var $belongsTo = array(
        'Question' => array(
            'className' => 'Question',
            'foreignKey' => 'question_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );

}
