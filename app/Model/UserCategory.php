<?php
	
App::uses('AuthComponent', 'Controller/Component');
 
class UserCategory extends AppModel {
     
    public $avatarUploadDir = 'img/avatars';
        
     var $belongsTo = array(   
       'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
         'Category' => array(
            'className' => 'Category',
            'foreignKey' => 'category_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
         
         
    );
    /**
     * Before isUniqueUsername
     * @param array $options
     * @return boolean
     */
     
 
}