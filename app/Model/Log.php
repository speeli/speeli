<?php

App::uses('AuthComponent', 'Controller/Component');

class Log extends AppModel {

 
     var $belongsTo = array(   
       'Question' => array(
            'className' => 'Question',
            'foreignKey' => 'question_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
         'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
         
    );   
}
