<?php
	
App::uses('AuthComponent', 'Controller/Component');
 
class Answer extends AppModel {
     
    public $avatarUploadDir = 'img/avatars';
        /**
     * Before isUniqueUsername
     * @param array $options
     * @return boolean
     */
        
   var $belongsTo = array(   
       'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
       'Question' => array(
            'className' => 'Question',
            'foreignKey' => 'question_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
    );
    
   
}