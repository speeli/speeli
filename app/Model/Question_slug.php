<?php

App::uses('AuthComponent', 'Controller/Component');

class Question extends AppModel {

    public $avatarUploadDir = 'img/avatars';

    /**
     * Before isUniqueUsername
     * @param array $options
     * @return boolean
     */
    var $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),'MainCategory' => array(
            'className' => 'MainCategory',
            'foreignKey' => 'category_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
        'Summary' => array(
            'className' => 'Summary',
            'foreignKey' => 'summary_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    public $hasMany = array(
        'QuestionCategory'
    );

    function getTags() {
        $category = ClassRegistry::init('Category');
        $data = array();
        $categories = $category->find('list', array('fields' => array('id', 'name')));
        return $categories;
        foreach ($categories as $i => $tag) {
            $data[] = array('Category' => array('id' => $tag['Category']['id'], 'name' => $tag['Category']['name']));
        }
        return $data;
    }

    function username($userid) {
        ClassRegistry::init('User');
        $username = $this->User->find('first', array('fields' => array('User.username'), 'conditions' => array('User.id' => $userid)));
        if (!empty($username)) {
            return $username['User']['username'];
        } else {
            return 'Anonymous';
        }
    }

    public function last_article($primary,$edit) {
        if (!empty($edit)) {
            $explode_title = explode(',,,', $edit);
            $last = end($explode_title);
            $title = end(unserialize(base64_decode($last)));
        } else {
            $title = $primary;
        }
        return $title;
    }

    function spilt_title($title, $string = false) {
        $input = trim($title);

        $input = preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', $input);

        $replace = array(
            'a', 'an', 'of', 'off', 'at',
            'good', 'bad',
            'how', 'why', 'whose','when',
            'what', 'is', 'on', 'in','the', 'these', 'those', 'there', 'can', 'could', "could't", 'would', 'will', "won't", "wouldn't", 'shall', 'should');
        $explode = explode(' ', $input);
        foreach ($explode as $key => $value) {
            if (in_array(" ", $replace)) {
                unset($explode[$key]);
            }
            if (in_array($value, $replace)) {
                unset($explode[$key]);
            }
        }
        if ($string == 'string') {
            return $implode = implode(" ", $explode);
        } else {
            return $explode;
        }
    }

    function remove_Space($title, $string = false) {
        $input = trim($title);

        $input = preg_replace(array('/\s{2,}/', '/[\t\n]/'), ' ', $input);
        $explode = explode(' ', $input);
        if ($string == 'string') {
            return $implode = implode(" ", $explode);
        } else {
            return $explode;
        }
    }

}
