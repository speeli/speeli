<?php
	
App::uses('AuthComponent', 'Controller/Component');
 
class QuestionCategory extends AppModel {
     
    public $avatarUploadDir = 'img/avatars';
        
     var $belongsTo = array(   
       'Question' => array(
            'className' => 'Question',
            'foreignKey' => 'question_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
         'Category' => array(
            'className' => 'Category',
            'foreignKey' => 'category_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        ),
         
         'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
         
    );
    /**
     * Before isUniqueUsername
     * @param array $options
     * @return boolean
     */
     
 
}