<?php

App::uses('AuthComponent', 'Controller/Component');

class FeedbackGroup extends AppModel {

    public $avatarUploadDir = 'img/avatars';

    /**
     * Before isUniqueUsername
     * @param array $options
     * @return boolean
     */
    var $belongsTo = array(
        'Question' => array(
            'className' => 'Question',
            'foreignKey' => 'question_id',
            'conditions' => '',
            'fields' => '',
            'order' => ''
        )
    );
    var $hasMany = array(
		'Feedback' => array(
			'className' => 'Feedback',
			'foreignKey' => 'group_id',
			'dependent' => false,
			'conditions' => '',
			'fields' => '',
			'order' => '',
			'limit' => '',
			'offset' => '',
			'exclusive' => '',
			'finderQuery' => '',
			'counterQuery' => ''
		)
	);

}
